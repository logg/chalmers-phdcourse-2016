from fenics import *
import numpy as np

def joule_heating_solver(phi_D, u_0, nx, ny):

    def sigma(u):
        return 0.5*(pi/2 + atan(u))

    tol = 1E-14

    def boundary_u(x, on_boundary):
	    return on_boundary

    def boundary_phi(x, on_boundary):
	    return on_boundary and near(x[0], 0.0, tol)

    T = 1.0
    num_steps = 20
    dt = T/num_steps

    # Define mesh and function spaces
    mesh = UnitSquareMesh(nx, ny)
    P1 = FiniteElement('P', triangle, 1)
    element = MixedElement([P1, P1])
    V = FunctionSpace(mesh, element)

    # Test and trial spaces and functions
    v = Function(V)
    u, phi = split(v)
    w_1, w_2 = TestFunctions(V)

    # BC
    bc1 = DirichletBC(V.sub(0), Constant(0.0), boundary_u)
    bc2 = DirichletBC(V.sub(1), phi_D, boundary_phi)
    bcs = [bc1, bc2]

    # Initial data (phi_n only 'dummy')
    v_n = interpolate(u_0, V)
    u_n, phi_n = split(v_n)

    # Variaitonal problem
    F = u*w_1*dx - u_n*w_1*dx + dt*dot(grad(u),grad(w_1))*dx \
      - dt*sigma(u)*dot(grad(phi),grad(phi))*w_1*dx \
      + sigma(u)*dot(grad(phi),grad(w_2))*dx

    for n in range(num_steps):
        	
        # Solve system
        solve(F == 0, v, bcs)
        assign(v_n,v)
        
    return v

def run_solver():
    
    phi_D = Expression('x[1]', degree=1)
    u_0 = Expression(('x[0]*(1-x[0])*x[1]*(1-x[1])','0.0'), degree=2)
    nx, ny = 8, 8
    v = joule_heating_solver(phi_D, u_0, nx, ny)
    u, phi = split(v)
    
    # Plot
    plot(u)
    plot(phi)
    
    #vtkfile = File("sol.pvd")
    #vtkfile << v

if __name__ == '__main__':
    run_solver()
    interactive()

def test_solver():
    "Reproducing the solution u=phi=0"
    
    # Data
    phi_D = Constant(0.0)
    u_0 = Expression(('0.0','0.0'), degree=2)
    nx, ny = 8, 8
    
    # Solve
    v = joule_heating_solver(phi_D, u_0, nx, ny)
    
    # Compute vertex values
    mesh = v.function_space().mesh()
    vertex_values_v = v.compute_vertex_values(mesh)
    max_val = np.max(np.abs(vertex_values_v))
    
    # Check
    tol = 1E-14
    assert max_val<tol
