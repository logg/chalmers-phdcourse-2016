# Test Non_linear_heat_equation_solver with the exact solution u(t, x) = t*x*(1 - x)

from fenics import *

execfile('Non_linear_heat_equation_solver.py')

Nt = 50
Nx = 16
Ny = 16

T = 1.0
Temp = 0

x0 = Point(0, 0)
x1 = Point(1, 1)

def q(u):
    return 1 + u**2


import sympy as sym

# Computing the source term from the exact solution
t, x, y = sym.symbols('t, x[0], x[1]')
u = t*x*(1 - x)
f = sym.diff(u, t) - sym.diff(q(u)*sym.diff(u, x), x) - sym.diff(q(u)*sym.diff(u, y), y)
f = sym.simplify(f)
fcode = sym.printing.ccode(f)
u_0 = Expression('0', degree = 2)
f = Expression(fcode, degree = 1, t = 0)

u_n = Non_linear_heat_equation_solver(Nt, Nx, Ny, T, x0, x1, Temp, Temp, u_0, f)