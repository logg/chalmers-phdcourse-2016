from fenics import *

def Non_linear_heat_equation_solver(Nt, Nx, Ny, T, x0, x1, Temp1, Temp2, u_0, f):
    """
    Solver for the non-linear heat equation
    u_t - div((1 + u^2)*grad(u)) = f
    on the rectangular domain R given by the two points x0 and x1.
    The initial value is given by u_0.
    The boundary conditions are given by
    u(t, x) = Temp1 on the left vertical side,
    u(t, x) = Temp2 on the right vertical side and
    isolated at the two horisontal sides.
    Uses first order lagrange elements over partition of R
    with number of points on each side given by Nx and Ny.
    Backward Euler in time with Nt steps up until time T.
    """
    
    # Partitions and functions space
    dt = T/Nt
    mesh = RectangleMesh(x0, x1, Nx, Ny)
    V = FunctionSpace(mesh, 'P', 1)

    # Boundary conditions
    tol = 1E-14
    def boundary_d1(x):
        return near(x[0], x0[0], tol)
   
    def boundary_d2(x):
        return near(x[0], x1[0], tol)
    
    bcd1 = DirichletBC(V, Constant(Temp1), boundary_d1)
    bcd2 = DirichletBC(V, Constant(Temp2), boundary_d2)
    bc = [bcd1, bcd2]

    # Non-linear term
    def q(u):
        return 1 + u**2

    # Projection of initial condition
    u_n = project(u_0, V)

    # Defining variational problem
    u = Function(V)
    v = TestFunction(V)
    F = u*v*dx + dt*q(u)*dot(grad(u), grad(v))*dx - u_n*v*dx - dt*f*v*dx

    t = 0.0

    # Save initial condition
    vtkfile = File('heat/solution.pvd')
    u.assign(u_n)
    vtkfile << (u, t)

    for n in range(Nt):
    
        # Update current  time
        t = t + dt
        f.t = t
    
        # Solve the non-linear equation
        solve(F == 0, u, bc)
        
        # Save and update
        u_n.assign(u)
        vtkfile << (u, t)
    
    return u_n