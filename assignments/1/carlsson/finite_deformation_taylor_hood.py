"""
Contains classes to run
"""

from __future__ import division
from fenics import *
from mshr import *


class MaterialParameters(object):
    def __init__(self, E, nu):
        self.E = E
        self.nu = nu
        self.G = E / (2 + (1+nu))
        self.K = E / (2 * (1-2*nu))
        self.lamda = E * nu / ((1+nu) * (1-2*nu))


class ElasticityProblem(object):

    # Wtihtout quadrature degree Fenics tries to
    # use 1000 quadrature points per cell. Wut?
    FFC_OPTIONS = {"optimize": True, \
                   "eliminate_zeros": True, \
                   "precompute_basis_const": True, \
                   "precompute_ip_const": True, \
                   "quadrature_degree": 3}

    def __init__(self, material_parameters, filename):

        self.mp = material_parameters

        self.T = Constant((0.0, 0.0, 0.0))
        self.B = Constant((0.0, 0.0, 0.0))

        self.bcs = []

    def set_bodyforce(self, B):
        self.B.assign(B)

    def set_traction(self, T):
        self.T.assign(T)

    def do_export(self, up):
        u, p = up.split()
        self.fu << u
        self.fp << p


class Linear(ElasticityProblem):

    def __init__(self, mesh, material_parameters, filename):
        super(Linear, self).__init__(material_parameters, filename)
        # Would want to type assert that this is actually a mesh
        self.W = VectorFunctionSpace(mesh, "Lagrange", 1)

        self.u = Function(self.W)
        self.v = TestFunction(self.W)
        self.du = TrialFunction(self.W)
        self.fu = File(filename + "_displacement.pvd")


    def add_bcs(self, value, boundary):
        self.bcs.append(DirichletBC(self.W, value, boundary))

    def potential(self):
        G, lamda = Constant(self.mp.G), Constant(self.mp.lamda)

        d = len(self.u)
        I = Identity(d)
        F = I + grad(self.u)
        C = F.T*F
        Ic = tr(C)
        J = det(F)
        psi = (G/2)*(Ic - 3) - G*ln(J) + (lamda/2)*(ln(J))**2
        return psi*dx - dot(self.B, self.u)*dx - dot(self.T, self.u)*ds

    def solve(self):
        Pi = self.potential()
        F = derivative(Pi, self.u, self.v)
        J = derivative(F, self.u, self.du)

        solve(F == 0, self.u, self.bcs, J=J,  form_compiler_parameters=self.FFC_OPTIONS)
        return self.u

    def do_export(self, u):
        self.fu << u


class TaylorHood(ElasticityProblem):

    def __init__(self, mesh, material_parameters, filename):
        super(TaylorHood, self).__init__(material_parameters, filename)

        P2 = VectorElement("Lagrange", mesh.ufl_cell(), 2)
        P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
        TH = P2 * P1
        self.W = FunctionSpace(mesh, TH)

        self.up = Function(self.W)
        self.u, self.p = split(self.up)
        self.vq = TestFunction(self.W)
        self.v, self.q = split(self.vq)
        self.dvq = TrialFunction(self.W)

        self.fu = File(filename + "_displacement.pvd")
        self.fp = File(filename + "_pressure.pvd")

    def add_bcs(self, value, boundary):
        self.bcs.append(DirichletBC(self.W.sub(0), value, boundary))

    def potential(self):
        G, K, lamda = Constant(self.mp.G), Constant(self.mp.K), Constant(self.mp.lamda)

        d = len(self.u)
        I = Identity(d)
        F = I + grad(self.u)
        C = F.T*F
        J = det(F)
        Chat = det(C)**(-1/3) * C
        Ichat = tr(Chat)

        psi =  (G/2)*(Ichat - 3) +  self.p * (J-1) # - 0.5 / K * self.p**2
        Pi = psi * dx - dot(self.B, self.u)*dx - dot(self.T, self.u)*ds
        return Pi

    def solve(self):
        Pi = self.potential()
        F = derivative(Pi, self.up, self.vq)

        # Compute Jacobian of F
        J = derivative(F, self.up, self.dvq)

        #J = derivative(F, self.up)
        solve(F == 0, self.up, self.bcs, J=J,  form_compiler_parameters=self.FFC_OPTIONS)
        return self.up
