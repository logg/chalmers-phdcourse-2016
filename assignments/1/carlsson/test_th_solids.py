from finite_deformation_taylor_hood import *

def _create_mesh():
    h = 5
    L_mesh = 64.0
    mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(10.0, 10.0, 10.0), h, h, h)
    left =  CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)

    return mesh, left

def run_analysis_TH():

    mesh, left = _create_mesh()
    T = Constant((0.0, 1.0 / 64.0 , 0.0))
    c = Constant((0.0, 0.0, 0.0))

    mp = MaterialParameters(10.0, 0.4999999)

    TH = TaylorHood(mesh, mp, "TH_" + str(mp.nu))
    TH.add_bcs(c, left)
    TH.set_traction(T)
    up = TH.solve()
    u, p = up.split()
    return norm(u), norm(p)

def test_TH():
    u, p = run_analysis_TH()
    print(u, p)
    assert abs(u -  6.2546376) < 1e-6
    assert abs(p - 1.383270) < 1e-6