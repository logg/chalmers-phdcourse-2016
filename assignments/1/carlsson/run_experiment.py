from finite_deformation_taylor_hood import *
import matplotlib.pyplot as plt
import numpy as np

discretizations = [3, 5, 8, 12, 15]
nus = [0.3, 0.4, 0.45, 0.49, 0.499]

results_Lin = np.zeros((len(discretizations), len(nus)))
results_TH = np.zeros((len(discretizations), 1))

for (i, h) in enumerate(discretizations):
    L_mesh = 64.0
    T = Constant((0.0, 1.0 / 64.0 , 0.0))
    c = Constant((0.0, 0.0, 0.0))

    mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(10.0, 10.0, 10.0), h, h, h)
    left =  CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)

    for (j, nu) in enumerate(nus):
        mp = MaterialParameters(10.0, nu)

        # Solve with P1
        Lin = Linear(mesh, mp, "Lin_" + str(nu))
        Lin.add_bcs(c, left)
        Lin.set_traction(T)
        u = Lin.solve()
        results_Lin[i,j] = norm(u)

    # Solve with Taylor Hood
    mp = MaterialParameters(10.0, 0.4999999)

    TH = TaylorHood(mesh, mp, "TH_" + str(mp.nu))
    TH.add_bcs(c, left)
    TH.set_traction(T)
    up = TH.solve()
    TH.do_export(up)
    u, p = up.split()
    results_TH[i] = norm(u)

# Plot it
fig, ax = plt.subplots(nrows=1, ncols=2, sharex=True, sharey=True, figsize=(1, 2))
ax[0].set_xlabel('n elements')
ax[1].set_xlabel('n elements')
ax[0].set_ylabel('||u||_2')
ax[1].set_xscale("log")
ax[0].set_xscale("log")
ax[0].set_title("P1 elements")
ax[1].set_title("Taylor hood elements")

n_elements = [k**3 for k in discretizations]

for j in range(len(nus)):
    ax[0].plot(n_elements, results_Lin[:, j], label = "nu = " + str(nus[j]))

ax[1].plot(n_elements, results_TH, label = "nu = " + str(nus[j]))

legend = ax[0].legend(loc = 'best')
plt.show()