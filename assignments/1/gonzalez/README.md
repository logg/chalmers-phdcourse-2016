FEM for 1D BGK model
======================

We prepare a short python program using FENICS to solve numerically a one-dimensional BGK model. 

In addition, we have a test file `test_bgkcompare.py` to be executed with py.test, together with the file `conftest.py` where we define what is the test comparing.

This can be executed using the Makefile, with the options:

	make run
	make test

A samll report containing a short description of the problem and the methods used is provided in the folder `report`. 

