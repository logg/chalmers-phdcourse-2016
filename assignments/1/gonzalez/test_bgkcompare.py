# content of test_bgkcompare.py
from bgk import BgkSolver, Settings, Problem

def test_compare_DG():
    settings = Settings()
    problem  = Problem()

    settings.FEMtype = "DG"
    bgk1 = BgkSolver(settings, problem)
    bgk2 = BgkSolver(settings, problem)
    assert bgk1 == bgk2

def test_compare_CG():
    settings = Settings()
    problem  = Problem()

    settings.FEMtype = "CG"
    bgk1 = BgkSolver(settings, problem)
    bgk2 = BgkSolver(settings, problem)
    assert bgk1 == bgk2

