from fenics import *
import numpy as np
import matplotlib.pyplot as plt

class Settings:
    """ For a given problem, we can choose different settings
    for the solver, where higher values of the different parameters
    should provide a better approximation, except for the time step."""
    def __init__(self):
        self.Nx = 100 # partition of the spatial domain
        self.deg = 1  # approximation order for the spatial domain
        self.Nq = 40  # approximation order for the velocity domain
        self.dt = 0.0005; # increment in the time integration
        self.FEMtype = "DG" # type of FEM

class Problem:
    """ A particular problem is defined by the spatial domain,
    the time domain, and the boundary conditions, together with the 
    relaxation time for the collision term. The velocity domain
    is always the whole space so it is problem independent."""
    def __init__(self, testn=1):
        self.tau = 1.e-0
        # spatial domain
        self.x0 = 0.00
        self.xL = 1.00; 
        # time domain 
        self.t = 0.00;
        self.t_end = 0.05;
        # and the boundary conditions
        if testn == 0 :
            self.R0L = 1.00; self.R0R = 1.00;
            self.U0L = 0.00; self.U0R = 0.00;
            self.T0L = 1.00; self.T0R = 1.00;
        elif testn == 1 :
            self.R0L = 1.00; self.R0R = 0.25;
            self.U0L = 0.00; self.U0R = 0.00;
            self.T0L = 5.00; self.T0R = 4.00;
        else:
			raise Exception('Only tests 0 and 1 are available') 

class TimePrinter:
    """ This class is for printing which time step are we solving"""
    def __init__(self, t_end, p_freq=0.01):
        self.nextt = 0.
        self.t_end = t_end
        self.p_freq = p_freq
    def decide(self, t):
        if(t>self.nextt-1.e-6 and t < self.t_end+1.e-6):
            print('t = {0:2f} t_end = {1:2f}'.format(t, self.t_end))
            self.nextt += self.p_freq
            self.nextt = min(self.nextt, self.t_end-1.e-6)

class BgkPlotter:
    """ This is the main class. We use it to solve a given problem,
    with given settings, and we store the macroscopic density, velocity
    and temperature, for comparison with a different simulation"""
    def __init__(self, mesh, deg, FEMtype):
		# generate mesh for high order FEM
        meshplot = mesh
        for ref in range(1,deg):
            meshplot = refine(meshplot)
            meshplot.coordinates().sort(axis=0)
        meshplot.self = meshplot
        # generate FEM space over high order mesh for ploting
        self.Vplot = FunctionSpace(meshplot, FEMtype, 1)
        self.initialized = False
    
    def show(self, R0, U0, T0):
        if (self.initialized == False):
			# create the handles for ploting
            self.R = interpolate(R0, self.Vplot)
            self.U = interpolate(U0, self.Vplot)
            self.T = interpolate(T0, self.Vplot)

            plot(self.R, title='Density')
            plot(self.U, title='Velocity')
            plot(self.T, title='Temperature')
            # Create files for storing the results
            #file = File("results/results.pvd")
            #file << R0, U0, T0
            self.initialized = True
        else:
			# plot using previous handles
            self.R.vector()[:] = interpolate(R0, self.Vplot).vector().copy()
            self.U.vector()[:] = interpolate(U0, self.Vplot).vector().copy()
            self.T.vector()[:] = interpolate(T0, self.Vplot).vector().copy()
            plot(self.R), plot(self.U), plot(self.T)
            # Save to file
            #file << R0, U0, T0
    def tofile(self, R0, U0, T0):
        self.R.vector()[:] = interpolate(R0, self.Vplot).vector().copy()
        self.U.vector()[:] = interpolate(U0, self.Vplot).vector().copy()
        self.T.vector()[:] = interpolate(T0, self.Vplot).vector().copy()
        rplot = plot(self.R, title='Density')
        rplot.write_png("rF")
        uplot = plot(self.U, title='Velocity')
        uplot.write_png("uF")
        tplot = plot(self.T, title='Temperature')
        tplot.write_png("tF")


                        
class BgkSolver:
    def __init__(self, settings, problem):
		self.settings = settings
		self.problem = problem
		self.solve()

    def solve(self):
        print '\nSolving the BGK equation with ', self.settings.FEMtype
        ##---
        settings = self.settings
        problem = self.problem
        # I use this to setup the solver
        Nx = settings.Nx; deg = settings.deg; 
        Nq = settings.Nq; FEMtype = settings.FEMtype
        dt = settings.dt; 
        # and this for the problem constant (only tau) 
        tau = problem.tau;
        x0 = problem.x0; xL = problem.xL; # spatial domain
        t = problem.t; t_end = problem.t_end; # time domain 
        # and the boundary conditions
        R0L = problem.R0L; R0R = problem.R0R;
        U0L = problem.U0L; U0R = problem.U0R;
        T0L = problem.T0L; T0R = problem.T0R;
        ##---
        # Spatial domain: 1D for ux
        mesh=IntervalMesh(Nx,x0,xL)
        # Velocity domain: Quadratures and weights, and renormalize weights
        q,w = np.polynomial.hermite.hermgauss(Nq)
        w = [w[j]*np.exp(q[j]**2) for j in range(Nq)]
        ##---
        # We generate different spaces of functions
        V0=FunctionSpace(mesh, FEMtype, deg)
        #V=[FunctionSpace(mesh, FEMtype, deg) for j in range(Nq)]
        # The initial macroscopic quantities
        R0=interpolate(Expression('(x[0]<=0.5)*R0L + (x[0]>0.5)*R0R', 
		    R0L=R0L, R0R=R0R, degree=deg, name='rho'), V0)
        U0=interpolate(Expression('(x[0]<=0.5)*U0L + (x[0]>0.5)*U0R',
            U0L=U0L, U0R=U0R, degree=deg, name='u'), V0)
        T0=interpolate(Expression('(x[0]<=0.5)*T0L + (x[0]>0.5)*T0R',
            T0L=T0L, T0R=T0R, degree=deg, name='T'), V0)
        # and boundary values for the initial distribution function F0
        F0R=[R0R/np.sqrt(2.*np.pi*T0R)*np.exp(-.5*pow(q[j]-U0R,2)/T0R) for j in range(Nq)]
        F0L=[R0L/np.sqrt(2.*np.pi*T0L)*np.exp(-.5*pow(q[j]-U0L,2)/T0L) for j in range(Nq)]

        ##---
        # Define boundary conditions
        class Left(SubDomain):
            def inside(self, x , on_boundary):
               return near(x[0], x0)

        class Right(SubDomain):
            def inside(self, x , on_boundary):
               return near(x[0], xL)
               
        bcs = [0]*Nq
        for j in range(Nq):
            if q[j] > 0: #inflow
                bcs[j] = [DirichletBC(V0, F0L[j], Left(), "pointwise")]
            elif q[j] < 0: #outflow
                bcs[j] = [DirichletBC(V0, F0R[j], Right(), "pointwise")]
        bcsR = [DirichletBC(V0, R0L, Left(), "pointwise"),
                DirichletBC(V0, R0R, Right(), "pointwise")]
        bcsU = [DirichletBC(V0, U0L, Left(), "pointwise"),
                DirichletBC(V0, U0R, Right(), "pointwise")]
        bcsT = [DirichletBC(V0, T0L, Left(), "pointwise"), 
                DirichletBC(V0, T0R, Right(), "pointwise")]

        ##---
        # The Maxwellian
        def Mf(R,U,T):
            """ The Maxwellian depending on the macroscopic quantities"""
            F = [Function(V0) for j in range(Nq)]
            for j in range(Nq):
                F[j] = interpolate(
                    Expression('r0/sqrt(2.*pi*t0)*exp(-0.5*pow(q-u0,2)/t0)', 
                    pi=np.pi, q=q[j], r0=R, u0=U, t0=T, degree=deg), V0)
            return F

        ##---
        # The macroscopic moments
        def macro_moments(vq,vw,F):
            """ Computing Macroscopic Moments using the quadrature rule
            [r, u, t, E] = macro_moments(qv, wv, f) """
            # Macroscopic density
            # r = <f> = f*vw;
            R = Function(V0, name="rho")
            R.vector()[:] = 0
            for j in range(0,Nq):
                R.vector()[:] += vw[j]*F[j].vector()[:]
            # Macroscopic moment in x to obtain the macroscopic velocity
            # ur = <f*v> f*(vq .* vw);
            UR = Function(V0)
            for j in range(0,Nq):
                UR.vector()[:] += (vq[j]*vw[j])*F[j].vector()[:]
            U = Function(V0, name="u") #  u = ur./r;
            U.vector()[:] = UR.vector().array()/R.vector().array()
            # Macroscopic temperature 
            # tr=<f*(v-u)^2>=<f*v^2>-r*u^2 = 1/d*f*((vq.^2).*vw)-1/d*u.*ur;
            TR = Function(V0)
            for j in range(0,Nq):
                TR.vector()[:] += (vq[j]**2*vw[j])*F[j].vector()[:]
            TR.vector()[:] += - U.vector()[:]*UR.vector()[:]
            T = Function(V0, name="T") #  t = tr./r;
            T.vector()[:] = TR.vector().array()/R.vector().array()
            # Macroscopic energy
            # E = 1/2*<f*v^2> = f*(1/2*(vq.^2).*vw);
            E = Function(V0)
            for j in range(0,Nq):
                E.vector()[:] += (1/2*vq[j]**2*vw[j])*F[j].vector()[:]
                
            [bc.apply(R.vector()) for bc in bcsR]
            [bc.apply(U.vector()) for bc in bcsU]
            [bc.apply(T.vector()) for bc in bcsT]
            return  R, U, T, E

        ##---
        # The collision depends on the macroscopic moments and the local Maxwellian
        def Collision(F):
            R, U, T, E = macro_moments(q,w,F)
            MF = Mf(R,U,T)
            C = []
            for j in range(Nq):
                C.append( (MF[j]-F[j])/tau )
            return C

        ##---
        # advecting velocity
        Mv = VectorFunctionSpace(mesh, FEMtype, degree=deg)
        vel = [interpolate(Expression(("q",), q=q[j], degree=deg), Mv)
               for j in range(Nq)]

        # Define unknown and test function(s)
        v = TestFunction(V0)
        u = TrialFunction(V0)

        n = FacetNormal(mesh)
        # veln = ( dot(v, n) + |dot(v, n)| )/2.0
        veln = [(dot(vel[j], n) + abs(dot(vel[j], n)))/2.0 for j in range(Nq)]
        # Define streaming term and the mass matrix and assemble them
        def streaming(u,v,j):
            a_int = dot(grad(v), -vel[j]*u)*dx
            #a_flux = (dot(jump(v), veln[j]('+')*u('+') - veln[j]('-')*u('-') )*dS  
            a_flux = (dot(jump(v), jump(veln[j]*u))*dS  
                + dot(v, veln[j]*u)*ds)
            a = a_int + a_flux
            return a
        def mass(u,v):
            return v*u*dx
        # Define variational forms 
        A = [assemble(mass(u,v) + dt*streaming(u,v,j)) for j in range(Nq)]

        ##---
        # Time loop
        # initialize the distribution with the macroscopic quantities
        F0 = Mf(R0,U0,T0)

        tprinter = TimePrinter(t_end)
        bgkplotter = BgkPlotter(mesh, deg, FEMtype)
        bgkplotter.show(R0,U0,T0)
        bgkplotter.tofile(R0,U0,T0)

        F = list(F0)
        l = [0]*Nq
        while t < t_end-dt/2. :
            # Compute the collisions and the right hand side for all j
            C = Collision(F0)
            l = [F0[j]*v*dx + dt*inner(C[j],v)*dx for j in range(Nq)]
            L = [assemble(l[j]) for j in range(Nq)]
            # apply the boundary conditions to F and solve for all j
            for j in range(Nq):
                [bc.apply(A[j], L[j]) for bc in bcs[j]]
                solve(A[j], F[j].vector(), L[j])
            # move to original container
            for j in range(Nq):
                F0[j].vector()[:] = F[j].vector().copy()
            # Do we plot?
            R0, U0, T0, E0 = macro_moments(q,w,F0)
            bgkplotter.show(R0,U0,T0)
            t += dt
            tprinter.decide(t);
        R0, U0, T0, E0 = macro_moments(q,w,F0)
        #bgkplotter.tofile(R0,U0,T0) # we save the plots to files

        #interactive()
        # we save some things for comparisons purposes
        self.mesh=mesh;
        self.V0=V0;
        self.R0, self.U0, self.T0, self.E0 = macro_moments(q,w,F0)

    def __eq__(self, other):
        """We say that two instances are "equal" if the different 
        macroscopic quantities differ less than a given tolerance. 
        In order to use different spaces I must modify this to create
        a new space and interpolate the solutions in the new space."""
        eR0 = assemble((self.R0 - other.R0)**2*dx(self.mesh))
        eU0 = assemble((self.U0 - other.U0)**2*dx(self.mesh))
        eT0 = assemble((self.T0 - other.T0)**2*dx(self.mesh))
        print "The  error for the different macroscopic quantities is: "
        print "eR0 = ", eR0, "; eU0 = ", eU0, "; eT0 = ", eT0
        tol = 1.e-6
        return eR0 < tol and eU0 < tol and eT0 < tol

if __name__ == "__main__":
    """stand alone support for this file"""
    import sys
    try:
        settings = Settings()
        problem  = Problem(1)
        settings.FEMtype = "DG" 
        #settings.FEMtype = "CG" # type of FEM
        bgk1 = BgkSolver(settings, problem)
    except: # catch *all* exceptions
        #e = sys.exc_info()[0]
        #print "Unexpected error:", e 
        raise






