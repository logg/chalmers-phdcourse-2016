\documentclass[a4paper,10pt]{article}


\usepackage{amsmath}    % math enhancements latex2e (replaces amstex)
\usepackage{amsthm}     % proclaims theoremstyle/proof environments
\usepackage{amssymb}    % AMSFonts and symbols
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{color}

\usepackage{subcaption}
\usepackage{listings}
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
%    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\scriptsize\ttfamily
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle,linewidth=14.5cm}

\usepackage{mathtools}
\usepackage{bbm}
\usepackage{dsfont}
\usepackage{stmaryrd}

\usepackage{titling}

\setlength{\droptitle}{-10em} 

%========================================================%
%         COMMANDS                                       %
%========================================================%

\newcommand{\abs}[1]{\lvert#1\rvert}
\newcommand{\norm}[1]{\|#1\|}
%\newcommand{\Tnorm}[1]{\lvert\|#1\|\rvert}
\newcommand{\Tnorm}[1]{\lvert\lvert\lvert#1\rvert\rvert\rvert}
%\newcommand{\Tnorm}[1]{\lvert\lvert\abs{#1}\rvert\rvert}
\newcommand{\Cnorm}[1]{[\lvert\lvert#1\rvert\rvert]}
\newcommand{\Jnorm}[1]{[[#1]]}
\newcommand{\Pnorm}[1]{\lvert\lvert\lvert#1\rvert\rvert\rvert_{s,\delta}}
\newcommand{\Hnorm}[1]{[#1]}
\newcommand{\bi}[1]{\textbf{\textit{#1}}}
\newcommand{\DD}[1]{\{#1^{ij}\}}
\newcommand{\Inner}[1]{\langle\langle#1\rangle\rangle}
\newcommand{\Jpro}[1]{\langle#1\rangle}

\newcommand{\supp}{\mathop{\mathrm{Supp}}}
\newcommand{\app}{\mathop{\mathrm{approx}}}
\newcommand{\sign}{\mathop{\mathrm{sign}}}

\newcommand{\avg}[1]{\ensuremath{ \lbrace \!\! \lbrace #1 \rbrace \!\! \rbrace}}
\newcommand{\jump}[1]{\ensuremath{ \llbracket #1 \rrbracket}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\partder}[2]{\frac{\partial #1}{\partial #2} } % defined by sebastian
\newcommand{\partdiff}[2]{\frac{\text{d} #1}{\text{d} #2} } % defined by sebastian
\newcommand{\exptext}[1]{\ds \exp \left( #1 \right)} % defined by sebastian
\newcommand{\erfc}[1]{\ds \mathop{\mathrm{erfc}} \left( #1 \right)} % defined by sebastian
\newcommand{\expnum}[1]{\ds e^{#1}} % defined by sebastian
\newcommand{\innerprod}[2]{\left( #1 , #2 \right)}
\newcommand{\kn}{\text{Kn}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\ds{\displaystyle}
\def\dt{{\Delta t}}
\def\d{{\, d}}  
\def\dx{{\, dx}}  



\title{FEM discretization of a 1D BGK model}
\author{S. Gonz\'alez-Pintor}
\date{\today}

\begin{document}


\maketitle

\begin{abstract}
We use a DG-FEM method for the spatial discretization of a one-dimensional BGK model of the Boltzmann equation. For the velocity domain we use a quadrature set, and the equation is integrated in time using a semi-implicit scheme that considers the collision term in the current time, but is implicit for the streaming operator.
\end{abstract}

\section{Introduction \label{introduction}}
%
The Bhatnagar-Gross-Kroo~\cite{Bhatnagar1954} (BGK) model can be written for one-dimensional geometries in the following form
%
\begin{align}
\partder{f}{t} + \xi \partder{f}{x}
= \frac{1}{\tau} \left(M_f - f\right) , && x \in I:=[0,L] ,
\label{eq:BGK:f}
\end{align}
%
where $f$ denotes the distribution function representing the number density on the phase space point $(x,v)$ in $I\times\mathbb{R}_{v}$ at time $t \in \mathbb{R}_+$. $\tau$ is the relaxation time, as the inverse of the collision frequency, and it can be defined as depending on the distribution function $f$, while here it is going to be considered as a constant given by the Knudsen number $\kn$. The isotropic Gaussian $M_f$ in the right hand side is defined as follows: First we define the gas density $\rho$, the gas flow velocity $u$, and the gas temperature $T$, as the moments of $f$ as follows
%
\begin{align*} 
\left(\begin{array}{c}
  \rho \\ \rho u \\ \rho T
\end{array}\right) 
= 
\left\langle f 
\left(\begin{array}{c}
  1 \\ v \\ \norm{u-v}^2
\end{array}\right) 
\right \rangle  ,
\quad \text{where} \quad
\langle g \rangle = \int_{\mathbb{R}_{v}} g(v) dv \approx \sum_{q = 0}^{N_q-1} \omega_q g(v_q) ,
\end{align*}
%
where $\langle g \rangle$ denotes the integral of a function $g$ on $\mathbb{R}_{v}$, which will be approximated here using a quadrature rule of $N_q$ points, $\{\omega_q, v_q\}_{q=0}^{N_q-1}$. The isotropic Gaussian distribution representing the Maxwellian is then defined as 
%
\begin{align*}
M_f (x,v,t) = M_{\rho, u, T} (x,v,t)
= \frac{\rho(x,t)}{\sqrt{2\pi T(x,t)}} \exptext{ \frac{-\norm{v-u(x,t)}^2}{2T(x,t)} } .
\end{align*}
%
where we highlight that the Maxwellian depends on $f$ through the macroscopic density, velocity and temperature. 

Finaly, initial and boundary conditions will be defined as an initial Maxwellian distribution for a given initial density, velocity and temperature distributions
%
\begin{align}
f(x,v,0) & =: f^0(x,v) = M_{f^0}(x,v) = M_{\rho^0,u^0,T^0}(x,v) \\
f(0,v,t) & =: f^0(0,v) = M_{f^0}(0,v), \quad v > 0 , \\
f(L,v,t) & =: f^0(L,v) = M_{f^0}(L,v), \quad v < 0 .
\label{eq:bc:f}
\end{align}
%
where $\rho^0$, $u^0$, and $T^0$ are piecewise constants functions, defined by the value at the extremes over each of the two halfs of the domain $I = I_l \cup I_r = [0, \frac{L}{2}) \cup [\frac{L}{2}, L] $, as
%
\begin{align*}
\rho^0(x) = 
\begin{cases}
\rho_l, &\!\! x \in I_l,  \\
\rho_r, &\!\! x \in I_r ;
\end{cases} 
\quad
u^0(x) = 
\begin{cases}
u_l, &\!\! x \in I_l ,  \\
u_r, &\!\! x \in I_r ;
\end{cases} 
\quad
T^0(x) = 
\begin{cases}
T_l, &\!\! x \in I_l ,  \\
T_r, &\!\! x \in I_r ;
\end{cases} .
\end{align*}

\subsection{System of PDEs after velocity discretization}
%
After discretization of the velocity variable by a fixed quadrature rule, we have the following system of $N_q$ partial differential equations
%
\begin{align}
\partder{f_q}{t} + v_q \partder{f_q}{x} 
= \frac{1}{\tau} \left(M_{f,q} - f_q \right) , \quad 0 \leq q < N_q,
\label{eq:BGKsystem}
\end{align}
%
We use the value of the initial boundary conditions on the extreme of the interval in order to obtain boundary conditions for $f$ as follows
%
\begin{align}
f_q(0,t)
& = M_{\rho_l,u_l,T_l}(0,v_q) ,
&& q \leq N_q/2 , \\
%
f_q(L,t)
& = M_{\rho_r,u_r,T_r}(L,v_q),
&& q > N_q/2 , 
\end{align}

\section{Spatial discretization}
%
We now proceed to the spatial discretization by a finite element method. The first step is to multiply by a test function $\phi(x)$ of a functional space $V$, and integrate over the space for a fixed time $t$, to obtain
%
\begin{align}
\innerprod{\partder{f_q}{t}}{\phi}_{\mathcal{L}^2(I)} 
+ v_q \innerprod{\partder{f_q}{x}}{\phi}_{\mathcal{L}^2(I)} 
= \frac{1}{\tau} \innerprod{M_{f,q} - f_q}{\phi}_{\mathcal{L}^2(I)} 
\end{align}
%
where we use the standard notation for the inner product in $\mathcal{L}^2$. Now, we define partition of the domain $\mathcal{I}_h:=\{I_k, \ k = 0, \ldots, N_x\}$, and consider the space of functions $V:=\mathcal{H}^1(\mathcal{I}_h) = \{v\in \mathcal{H}^1(I_k),\ k = 0, \ldots, N_x\}$. We split the integral with the spatial derivative over the subintervals defining a partiation of the original domain, and integrate by parts over each subinterval, obtaining
%
\begin{align*}
\innerprod{\partder{f_q}{t}}{\phi}_{\mathcal{I}_h} 
& - v_q \innerprod{f_q}{\partder{\phi}{x}}_{\mathcal{I}_h}
+ \innerprod{\jump{\mathcal{F}_q(f_q)}}{\jump{\phi}}_{\mathcal{E}_h^0}
+ \innerprod{\jump{\mathcal{F}_q(f_q)}}{\jump{\phi}}_{\mathcal{E}_h^\partial} \\
& = \frac{1}{\tau} \innerprod{M_{f,q} - f_q}{\phi}_{\mathcal{I}_h} 
\end{align*}
%
where the products with $\mathcal{I}_h$ and $\mathcal{E}_h^{0,\partial}$ are the 1 and 0 meassures over each subinterval or edge (interior or boundary), and the jump $\jump{\cdot}$ and the flux $\mathcal{F}_q(\cdot)$ defined as 
%
\begin{align*}
\jump{\phi} = \phi^- - \phi^+, 
\quad \mathcal{F}_q(f_q) = \left( |v_q| + \jump{v_q} \right) f_q ,
\end{align*}
%
are used to stabilize the method, generating an upwind scheme. The final system, by using a basis, $\{\phi_i\}$, for a finite subspace $V_h \subset V$, and the ansatz for the solution $f_q = \sum_{j} f_{q,j}\phi_j$ and for the maxwellian $M_{f,q} = \sum_{j} Q_{q,j}(F)\phi_j$, and the vector notation $F_q = \{f_{q,j}\}$ and $Q_q(F) = \{Q_{q,j}(F)\}$, looks like
%
\begin{align}
M\partder{F_q}{t} + G_q F_q = \frac{1}{\tau}(M Q_q(F) - M F_q ) ,  \quad \forall q
\end{align}
%
where the mass and streaming matrices are defined by
%
\begin{align*}
M_{ij} &= \innerprod{\phi_j}{\phi_i}_{\mathcal{I}_h} , \\
G_{q,ij} &= - v_q \innerprod{\phi_j}{\partder{\phi_i}{x}}_{\mathcal{I}_h}
\!\! + \innerprod{\jump{\mathcal{F}_q(\phi_j)}}{\jump{\phi_i}}_{\mathcal{E}_h^0}
\!\!+ \innerprod{\jump{\mathcal{F}_q(\phi_j)}}{\jump{\phi_i}}_{\mathcal{E}_h^\partial} .
\end{align*}
%
For different values of $q$, different boundary conditions and jumps are imposed in the matrix $G_q$, through the dependence of the numerical flux from $q$.

\section{Time discretization}
%
Now we consider the collision happening at time $t_n$, so we do not have to solve a non-linear system at each time step, and we use an implicit method for the rest of the equation, 
%
%\begin{align}
%M\frac{F^{n+1}_q - F^{n}_q}{h} + G_q F^{n+1}_q= Q^{n} F^{n}_q ,  \quad \forall q
%\end{align}
%
obtaining an scheme that require to solve, each time step, a the following linear system for every $q$
%
\begin{align}
(M+h G_q) F^{n+1}_q = M F^{n}_q + \frac{h}{\tau}(M Q_q(F^n) - M F_q^n ) , \quad \forall q .
\end{align}

\section{Results}
%
We considering the initial boundary value problem given by
%
\begin{align*}
\{
\rho_l = 1.00, \ \rho_r = 0.25 ;\quad
u_l = 1.00, \ u_r = 0.25 ;\quad
T_l = 5.00, \ T_r = 4.00
\} ,
\end{align*}
%
where the spatial domain is $[0,1]$, and the relaxation time is $\tau = 1$. After we define the problem, we choose specific settings for the discretization, as the number of quadratures for the velocity discretization $N_q = 40$, the number of subintervals in space is $N_x = 100$, the degree of the FEM approximation is $deg = 1$, the time domain is $[0, 0.05]$, and the time step chosen is $h = 0.0005$. Then, we simulate the transient and look at the final shapes of $\rho(x)$, $u(x)$ and $T(x)$ at the final time $T = 0.05$,  with DG (see Figure~\ref{fig:DG}) and with CG (see Figure~\ref{fig:CG}). It is shown that both methods reproduce a similar shape, but the CG solutions have some oscilations, mainly due to the lack of stabilization. 
%
\begin{figure}[!ht]
\centering
\begin{subfigure}[b]{1.0\textwidth}
\includegraphics[width=0.33\columnwidth]{DGrF.png} \hspace{-0.5cm}
\includegraphics[width=0.33\columnwidth]{DGuF.png} \hspace{-0.5cm}
\includegraphics[width=0.33\columnwidth]{DGtF.png}
\caption{Solution using DG FEM at $t = 0.05$.} \label{fig:DG}
\end{subfigure}
\begin{subfigure}[b]{1.0\textwidth}
\includegraphics[width=0.33\columnwidth]{CGrF.png} \hspace{-0.5cm}
\includegraphics[width=0.33\columnwidth]{CGuF.png} \hspace{-0.5cm}
\includegraphics[width=0.33\columnwidth]{CGtF.png}
\caption{Solution using CG FEM at $t = 0.05$.} \label{fig:CG}
\end{subfigure}
\caption{From left to right: Density ($\rho$), Velocity ($u$), and Temperature ($T$).} \label{fig:results}
\end{figure}

\bibliographystyle{unsrt}
\begin{thebibliography}{9}
\bibitem{Bhatnagar1954} 
Bhatnagar, Prabhu Lal, Eugene P. Gross, and Max Krook. 
"A model for collision processes in gases. I. Small amplitude processes in charged and neutral one-component systems." Physical review 94.3 (1954): 511.
\end{thebibliography} 

\section{The Code}

%\inputminted{python}{../bgk.py}
\lstinputlisting[language=Python]{../bgk.py}

\end{document}
