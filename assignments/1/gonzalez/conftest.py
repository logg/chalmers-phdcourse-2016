# content of conftest.py
#from test_bgkcompare import Bgk
from bgk import BgkSolver
def pytest_assertrepr_compare(op, left, right):
    if isinstance(left, BgkSolver) and isinstance(right, BgkSolver) and op == "==":
        return ['Comparing BgkSolver instances. We compare, with respect ',
                'to a tolerance 1.e-6 in L2 norm, the macroscopic quantities',
                '   R0 = density; ', 
                '   U0 = velocity; ', 
                '   T0 = temperature; ']

