# Solve equations of radiative heat transfer using the SP_1 
# approximation for the radiative transfer equation and
# standard backwards Euler-Galerkin method.
import numpy as np
from fenics import *
from mshr import *

folder = "results"
meshfile = File(folder + "/mesh.pvd")
Tfile = File(folder + "/T.pvd")
rhofile = File(folder + "/rho.pvd")

# FE solver
def radiantHeatSolver(V, mesh, ds, u_init, T_R, rho_R, f, g, t_f, nSteps):
  # Define test functions
  v_1, v_2 = TestFunctions(V)

  # Define functions
  u = Function(V)
  u0 = Function(V)
  u.interpolate(u_init)
  u0.interpolate(u_init)

  # Split system functions to access components
  T, rho = split(u)
  T0, _ = split(u0)

  # Set time step
  dt = t_f/nSteps
  tau = Constant(dt)

  # Define material data (all assumed to be constant)
  k, a, kappa, epsilon, h, alpha, gamma = getMaterialData()

  F = ((T - T0)/tau + f)*v_1*dx \
    + k*inner(nabla_grad(T), nabla_grad(v_1))*dx \
    + 1/(3*kappa)*inner(nabla_grad(rho), nabla_grad(v_1))*dx \
    + k*alpha*(T-T_R)*v_1*ds(1) \
    + 1/(3*kappa)*gamma*(rho - rho_R)*v_1*ds(1) \
    + epsilon*epsilon/(3*kappa)*inner(nabla_grad(rho), nabla_grad(v_2))*dx \
    + kappa*(rho + g - 4*DOLFIN_PI*a*T*T*T*T)*v_2*dx \
    + epsilon*epsilon/(3*kappa)*gamma*(rho - rho_R)*v_2*ds(1)

  # Save initial data
  t = 0.0
  _T, _ = u.split(deepcopy=True)
  T_list = [_T.copy(),]
  rho_list = []
  Tfile << (_T, t)
  T_diff_list = [0.0,]
  T_mean_list = [800.0,]
  T_amb_list = [800.0,]
  t_list = [t,]

  # Time-stepping
  for n in range(nSteps):

    # Update current time
    t += dt

    # Update functions
    f.t = t
    g.t = t
    T_R.t = t
    rho_R.t = t

    # Solve variational problem
    solve(F == 0, u, solver_parameters={"newton_solver": {"relative_tolerance": 1e-9, \
                                'maximum_iterations': 50, \
                                'absolute_tolerance': 1e-10, \
                                'relaxation_parameter': 1.0}})

    # Save solution
    _T, _rho = u.split()
    Tfile << (_T, t)
    rhofile << (_rho, t)

    # Compute the mean temperature over Omega
    T_temp, rho_temp = u.split(deepcopy=True)
    T_mean = np.sum(T_temp.vector().array())/len(T_temp.vector().array())

    # Store data
    T_list.append(T_temp.copy())
    rho_list.append(rho_temp.copy())
    T_mean_list.append(T_mean)
    T_amb_list.append(T_R.compute_vertex_values(mesh)[0])
    T_diff_list.append(T_mean - T_amb_list[-1])
    t_list.append(t)

    # Update u^(n-1)
    u0.assign(u)

  return T_list, rho_list, T_diff_list, T_mean_list, T_amb_list, t_list

def getMaterialData():
  k = Constant(1.0) # heat conductivity
  a = Constant(5.670367e-8/DOLFIN_PI)
  kappa = Constant(1.0) # absorption coefficient
  epsilon = Constant(0.001) # scaled optical thickness
  h = Constant(1.0) # heat transfer coefficient
  alpha = Constant(h/(epsilon*k))
  gamma = Constant(3*kappa/(2*epsilon))
  return k, a, kappa, epsilon, h, alpha, gamma

##########################################################

# Unit test
def test_solver():
  # Exact solution
  T_ref = Expression('x[0]*x[0] + 2.0*x[0] + 4.0 + t', t = 0.0)
  rho_ref = Expression('x[0]*x[0] - 4.0*x[0] + 2.0 + t', t = 0.0)
  
  # Define mesh
  mesh = UnitIntervalMesh(500) 
  P1 = FiniteElement('CG', interval, 1)
  P2 = FiniteElement('CG', interval, 1)
  element = MixedElement([P1, P2]) # same as element = P1*P2
  V = FunctionSpace(mesh, element)
  
  # Define boundary
  boundary_parts = MeshFunction('size_t', mesh, mesh.topology().dim()-1)
  class RobinBoundary(SubDomain):
      def inside(self, x, on_boundary):
          return on_boundary
  Gamma = RobinBoundary()
  Gamma.mark(boundary_parts, 1)
  ds = Measure("ds")[boundary_parts]

  # Set time stepping data
  t_f = 0.001
  nSteps = 10
  t = 0.0
  dt = t_f/nSteps

  # Set initial condition
  u_init = Expression(('x[0]*x[0] + 2.0*x[0] + 4.0', '0.0'))

  # Set sources and functions in BC's
  k, a, kappa, epsilon, h, alpha, gamma = getMaterialData()
  f = Expression('-1.0 + 2.0*k + 2/(3.0*kappa)', \
                 t = 0.0, k = k, kappa = kappa)
  g = Expression('2.0*epsilon*epsilon/(3.0*kappa) - kappa*(x[0]*x[0] - 4.0*x[0] + 2.0 + t)' \
                 '+ 4.0*5.670367e-8*kappa*pow(x[0]*x[0] + 2.0*x[0] + 4.0 + t, 4)', \
                 t = 0.0, epsilon = epsilon, kappa = kappa)
  # Set functions in BC's
  class T_robin(Expression):
    def __init__(self, t, alpha):
      self.t = t
      self.alpha = alpha
    def eval(self, value, x):
      if x[0] < 0.5:
        value[0] = x[0]*x[0] + 2.0*x[0] + 4.0 + self.t - (2.0*x[0] + 2.0)/self.alpha
      elif x[0] >= 0.5:
        value[0] = x[0]*x[0] + 2.0*x[0] + 4.0 + self.t + (2.0*x[0] + 2.0)/self.alpha
  class rho_robin(Expression):
    def __init__(self, t, gamma):
      self.t = t
      self.gamma = gamma
    def eval(self, value, x):
      if x[0] < 0.5:
        value[0] = x[0]*x[0] - 4.0*x[0] + 2.0 + self.t - (2.0*x[0] - 4.0)/self.gamma
      elif x[0] >= 0.5:
        value[0] = x[0]*x[0] - 4.0*x[0] + 2.0 + self.t + (2.0*x[0] - 4.0)/self.gamma
  T_R = T_robin(0.0, alpha)
  rho_R = rho_robin(0.0, gamma)

  T_list, rho_list, _, _, _, _ = radiantHeatSolver(V, mesh, ds, u_init, T_R, rho_R, f, g, t_f, nSteps)

  # check L^2-norms in each time step
  tol = 1e-6
  for i in xrange(0, len(rho_list)):
    t += dt
    T_ref.t = t
    rho_ref.t = t
    assert errornorm(T_ref, T_list[i+1], 'L2', 3, mesh) < tol
    assert errornorm(rho_ref, rho_list[i], 'L2', 3, mesh) < tol

##########################################################

# Main
if __name__ == "__main__":
  # Create mesh and define functions space for system
  # # 3d plate
  # p1 = Point(0.0, 0.0, 0.0)
  # p2 = Point(1.0, 1.0, 0.05)
  # mesh = BoxMesh(p1, p2, 16, 16, 4) 
  # P1 = FiniteElement('CG', tetrahedron, 1)
  # P2 = FiniteElement('CG', tetrahedron, 1)
  # 2d disc
  mesh = generate_mesh(Circle(Point(0,0), 0.5), 20) 
  P1 = FiniteElement('CG', triangle, 1)
  P2 = FiniteElement('CG', triangle, 1)
  meshfile << mesh
  
  element = MixedElement([P1, P2]) # same as element = P1*P2
  V = FunctionSpace(mesh, element)

  # Define boundary
  boundary_parts = MeshFunction('size_t', mesh, mesh.topology().dim()-1)
  class RobinBoundary(SubDomain):
      def inside(self, x, on_boundary):
          return on_boundary
  Gamma = RobinBoundary()
  Gamma.mark(boundary_parts, 1)
  ds = Measure("ds")[boundary_parts]

  # Set time stepping data
  # t_f = 0.00001 # 3d
  t_f = 0.001 # 2d 
  nSteps = 100

  # Set initial condition
  u_init = Constant((800.0, 0.0))

  # Set ambient temperature (chosen based on the application in [Hinze])
  class TempAmb(Expression):
    def __init__(self, t, t_f):
      self.t_f = t_f
      self.t = t
    def eval(self, value, x):
      if self.t >= 0.0 and self.t <= 0.2*t_f:
        value[0] =  800.0 + 100.0*self.t/(0.2*self.t_f)
      elif self.t > 0.2*self.t_f and self.t <= 0.6*self.t_f:
        value[0] =  900.0
      elif self.t > 0.6*self.t_f and self.t <= 0.65*self.t_f:
        value[0] =  900.0 + 100.0/(0.05*self.t_f)*(self.t - 0.6*self.t_f)
      elif self.t > 0.65*self.t_f and self.t <= 0.75*self.t_f:
        value[0] =  1000.0
      elif self.t > 0.75*self.t_f and self.t <= 0.8*self.t_f:
        value[0] =  1000.0 - 150.0/(0.05*self.t_f)*(self.t - 0.75*self.t_f)
      elif self.t > 0.8*self.t_f and self.t <= self.t_f + 1e-14:
        value[0] =  850.0
  T_amb = TempAmb(0.0, t_f)

  # Set sources and functions in BC's
  f = Constant(0.0)
  g = Constant(0.0)
  T_R = T_amb
  rho_R = Expression('4*5.670367e-8*T_amb*T_amb*T_amb*T_amb', T_amb = T_amb)

  # Solve
  _, _, T_diff_list, T_mean_list, T_amb_list, t_list = \
      radiantHeatSolver(V, mesh, ds, u_init, T_R, rho_R, f, g, t_f, nSteps)

  # Store data on file for plotting in Matlab
  tempFile = open('tempDiff.txt', 'w')
  for item in T_diff_list:
    tempFile.write("%s\n" % item)
  tempFile = open('tempMean.txt', 'w')
  for item in T_mean_list:
    tempFile.write("%s\n" % item)
  tempFile = open('tempAmb.txt', 'w')
  for item in T_amb_list:
    tempFile.write("%s\n" % item)
  tempFile = open('times.txt', 'w')
  for item in t_list:
    tempFile.write("%s\n" % item)