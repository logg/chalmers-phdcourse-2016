# Code for trying to mimic a single beat on a drum membrane

import matplotlib.pyplot as plt
import numpy as np
from fenics import *
from mshr import *

# Define the domain and generate a mesh
c_center = Point(0.0, 0.0)
c_radius = 1.0
domain = Circle(c_center, c_radius)
mesh = generate_mesh(domain, 30)
File('solutions/mesh.pvd') << mesh

# Define final time and time-step
T = 10.0
dt = 0.03         # Fulfills the CFL-conditon when dt < hmin
kn = Constant(dt)

# Define solver data,
def solver_data(FEMdeg, f, u_boundary, u0, dudt0):

    # Define FE-space and trial and test functions
    V = FunctionSpace(mesh, 'P', FEMdeg)
    u = TrialFunction(V)
    v = TestFunction(V)

    # A central difference quotient to approximate the time derivative,
    # i.e., d2u/dt2 = (u - 2*u1 + u0)/kn^2
    u0 = interpolate(u0, V)
    u1 = project(dudt0*dt + u0, V)

    # The other u needs to be approximated in a stable way by u_mid
    # which should be approximated by a convex combination of u0, u1, and u2,
    w0 = Constant(1.0); w1 = Constant(2.0); w2 = Constant(1.0)
    sumws = Constant(w0 + w1 + w2)
    def u_mid(u):
        return (w0*u0 + w1*u1 + w2*u)/(sumws)

    # Define bilinear and linear forms
    F = (u - 2*u1 + u0)*v*dx + kn*kn*(inner(grad(u_mid(u)), grad(v)) - f*v)*dx
    a = lhs(F); L = rhs(F)

    # Define boundary conditions
    bc = DirichletBC(V, u_boundary, DomainBoundary())

    # Assemble system matrix and apply bcs
    A = assemble(a)
    bc.apply(A)

    # Initialize the FE-solution u2
    u2 = Function(V)

    # Define total energy functional
    energy = 0.5*(((u2 - u0)/(2*kn))**2 + inner(grad(u_mid(u2)), grad(u_mid(u2))))*dx

    # Package the u:s
    us = (u0, u1, u2)

    return (A, us, L, bc, energy)

# Define and solve different wave equation problems ----------------------------------

def drum_problem():    # Drum membrane problem

    # Define problem data
    FEMdeg = 1
    f = Constant(0.0)
    u_boundary = Constant(0.0)
    u0 = Constant(0.0)
    dudt0 = Expression('-1.0*exp(-100*(x[0]*x[0] + x[1]*x[1]))', degree = FEMdeg)

    # Get the FE-data needed to solve
    (A, us, L, bc, energy) = solver_data(FEMdeg, f, u_boundary, u0, dudt0)
    u0 = us[0]; u1 = us[1]; u2 = us[2]

    # Create a solution file for storage
    #u_file = File('solutions/u_FE.pvd')
    #u_file = File('solutions/u_BE.pvd')
    #u_file = File('solutions/u_CN.pvd')
    u_file = File('solutions/u.pvd')

    # Start time-stepping
    E = []; timevec = []
    t = dt
    while t < T:

        # Update times
        t0 = t - dt; t1 = t; t2 = t + dt

        # Assemble load vector and apply bcs
        b = assemble(L)
        bc.apply(b)

        # Solve the system
        solve(A, u2.vector(), b)

        # Save solution to file
        u_file << (u0, t0)

        # Compute and store the total energy
        E.append(assemble(energy))
        timevec.append(t1)

        # Update previous solutions and time
        u0.assign(u1)
        u1.assign(u2)
        t += dt

    plt.plot(timevec, E)
    plt.xlabel('Time')
    plt.ylabel('Total energy of the system')
    #plt.title('Forward Euler')
    #plt.title('Backward Euler')
    plt.title('Crank-Nicolson')
    plt.axis([0, T, 0, 0.009])
    plt.show()

def tp_solver(A, us, L, bc, u_boundary):    # Solver for test_problems
    u0 = us[0]; u1 = us[1]; u2 = us[2]
    # Start time-stepping

    t = dt
    while t < T:

        # Update times
        t0 = t - dt; t1 = t; t2 = t + dt

        # Update the boundary conditions
        u_boundary.t = t2

        # Assemble load vector and apply bcs
        b = assemble(L)
        bc.apply(b)

        # Solve the system
        solve(A, u2.vector(), b)

        # Compare the FE-solution with the exact solution
        # Update the exact solution
        u_boundary.t = t0
        vv_u_exact = u_boundary.compute_vertex_values(mesh)
        vv_u_FE = u0.compute_vertex_values(mesh)
        error_max = np.max(np.abs(vv_u_exact - vv_u_FE))
        print error_max

        # Check maxium error
        msg = 'error_max = %g' % error_max
        assert error_max < 1e-10, msg

        # Update previous solutions and time
        u0.assign(u1)
        u1.assign(u2)
        t += dt

def test_problem_linear():    # Test problem to reproduce u_exact = x + y + t

    # Define problem data
    FEMdeg = 1
    f = Constant(0.0)
    u_boundary = Expression('x[0] + x[1] + t', t = 0.0, degree = FEMdeg) # This is u_exact!
    u0 = Expression('x[0] + x[1]', degree = FEMdeg)
    dudt0 = Constant(1.0)

    # Get the FE-data needed to solve
    (A, us, L, bc, energy) = solver_data(FEMdeg, f, u_boundary, u0, dudt0)

    # Solve the FE-problem
    tp_solver(A, us, L, bc, u_boundary)


def test_problem_quadratic():    # Test problem to reproduce u_exact = x^2 + y^2 + t

    # Define problem data
    FEMdeg = 2
    f = Constant(-4.0)
    u_boundary = Expression('x[0]*x[0] + x[1]*x[1] + t', t = 0.0, degree = FEMdeg) # This is u_exact!
    u0 = Expression('x[0]*x[0] + x[1]*x[1]', degree = FEMdeg)
    dudt0 = Constant(1.0)

    # Get the FE-data needed to solve
    (A, us, L, bc, energy) = solver_data(FEMdeg, f, u_boundary, u0, dudt0)

    # Solve the FE-problem
    tp_solver(A, us, L, bc, u_boundary)
