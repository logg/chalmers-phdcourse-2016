from fenics import *

def run_timedependent_maxwell():
    eps   = Expression("2.0 + 1.0*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])", degree = 2)
    muinv = Expression("1.0", degree = 2)
    F     = Expression(("-4*pi*pi*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])*sin(2*pi*t)*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*cos(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*sin(2*pi*x[1])*cos(2*pi*x[2])"),
                       degree = 2, t = 0.0)
    Eb    = Expression(("0.0", "0.0", "0.0"), degree = 0, t = 0.0)
    E0    = Expression(("0.0", "0.0", "0.0"), degree = 0)
    dE0   = Expression(("2*pi*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])", "0.0", "0.0"), degree = 2)
    x0    = Point(0.0, 0.0, 0.0)
    x1    = Point(2.0, 1.0, 1.0)
    T     = 1.0
    Nx    = 20
    Ny    = 10
    Nz    = 10
    Nt    = 60
    elm   = 1  # 0 for P1, otherwise N1curl
    E     = timedependent_maxwell_solver(eps, muinv, F, Eb, E0, dE0, x0, x1, T, Nx, Ny, Nz, Nt, elm)
