from fenics import *
import time

def timedependent_maxwell_solver(eps, muinv, F, Eb, E0, dE0, x0, x1, T, Nx, Ny, Nz, Nt, elm):
    """
    Solves the time-dependent Maxwell system
    eps(x)E''(t, x) + curl(muinv(x)curl(E(t, x))) = F(t, x), (t, x) in (0, T) x G,
    E(t, x) = 0, (t, x) in (0, T) x dG,
    E(0, x) = E0(x), E'(0, x) = dE0(x), x in G
    where G is the rectangular domain sepcified by the points x0 and x1.
    First order edge elements over partition of G with number of points per side
    given by Nx, Ny, Nz, and central difference approximation in time with
    Nt steps, are used. Solution is printed to files "solution..."
    The solution at final time is returned.
    """
    # Partitions and function spaces
    dt   = T/Nt
    mesh = BoxMesh(x0, x1, Nx, Ny, Nz)
    if elm == 0:
        X = VectorFunctionSpace(mesh, 'P', 1)
    else:
        X = FunctionSpace(mesh, "N1curl", 1)

    # Boundary conditions
    def DirichletBoundary(x, on_boundary):
        return on_boundary
    
    # Two initial time steps
    Ekm2 = project(E0, X)
    step = project(dE0, X)  
    Ekm1 = project(Ekm2 + dt*step, X)
    
    # Test and trial functions
    G  = TrialFunction(X)
    v  = TestFunction(X)
    
    # Bilinear and linear forms
    Fk   = F
    Fkm1 = F
    Fkm2 = F
    a = 6*inner(eps*G, v)*dx + dt*dt*inner(muinv*curl(G), curl(v))*dx 
    L = dt*dt*inner(Fk, v)*dx + 4*dt*dt*inner(Fkm1, v)*dx + dt*dt*inner(Fkm2, v)*dx + 12*inner(eps*Ekm1, v)*dx - 6*inner(eps*Ekm2, v)*dx - 4*dt*dt*inner(muinv*curl(Ekm1), curl(v))*dx - dt*dt*inner(muinv*curl(Ekm2), curl(v))*dx
    
    # File for saving, and save initial condition
    vtkfile = File('solution.pvd')
    
    # Assign initial value
    E = Function(X)
    E.assign(Ekm2)
    t = 0.0
    vtkfile << (E, t)
    
    # Assign value after first time step
    E.assign(Ekm1)
    t += dt
    vtkfile << (E, t)
    
    # Main time stepping
    for k in range(Nt - 1):
        
        # Evaluate right-hand side of PDE, and boundary conditions 
        Fk.t   = t + dt
        Fkm1.t = t
        Fkm2.t = t - dt
        Eb.t   = t
        BC     = DirichletBC(X, Eb, DirichletBoundary)

        # Compute solution at next point
        solve(a == L, E, BC)
        
        # Save and update
        t += dt
        vtkfile << (E, t)
        Ekm2.assign(Ekm1)
        Ekm1.assign(E)
        
    return E
