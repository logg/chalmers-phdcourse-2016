""" Demo for solution of a finite strain elasticity problem with
    weak Dirichlet boundary conditions. This is a mixed problem
    where the unknowns are the displacement u in the whole domain 
    and the traction t on the faces of the external
    boundary.

    The material is modeled using St.Venant-Kirchhoff elasticity, 
    and for the geometry we use the most classical Representative
    Volume Element (RVE): A cube with a spherical hole.

    Written by 
    Erik Svenning
    Chalmers University of Technology
    November 2016
"""

# Begin demo

from dolfin import *
from fenics import *
from mshr import *


# Optimization options for the form compiler
parameters["form_compiler"]["cpp_optimize"] = True
ffc_options = {"optimize": True, \
               "eliminate_zeros": True, \
               "precompute_basis_const": True, \
               "precompute_ip_const": True}


def solve_rve_dirichlet(Hxx, Hxy, Hxz, Hyx, Hyy, Hyz, Hzx, Hzy, Hzz, mp_mu, mp_lambda):

    ################################################
    # Create mesh and define function space
    
    
    #mesh = UnitCubeMesh(10, 10, 10)
    
    center = Point(0.5,0.5,0.5)
    sphere = Sphere(center,0.25)
    cube = Box( Point(0.0,0.0,0.0), Point(1.0,1.0,1.0) )
    domain = cube - sphere
    mesh = generate_mesh(domain,10)
    
    
    # Define function spaces
    P2 = VectorElement("CG", mesh.ufl_cell(), 2)
    T1 = VectorElement("CG", mesh.ufl_cell(), 1)
    PT = P2 * T1
    W = FunctionSpace(mesh, PT)
    
    
    def interior(x):
        if not( x[0] > DOLFIN_EPS and x[0] < 1.0 - DOLFIN_EPS ):
            return False
    
        if not( x[1] > DOLFIN_EPS and x[1] < 1.0 - DOLFIN_EPS ):
            return False
    
        if not( x[2] > DOLFIN_EPS and x[2] < 1.0 - DOLFIN_EPS ):
            return False
    
        return True
    
    
    # Dirty trick: We would like to have tractions that live only on the external boundary.
    # For simplicity, we let the tractions live everywhere, but add (strong) Dirichlet BCs for
    # the traction in the interior of the domain.
    pres_traction = Constant((0, 0, 0))
    bc_trac = DirichletBC(W.sub(1), pres_traction, interior)
    
    bcs = [bc_trac]
    
    # Define functions
    vw  = TestFunction(W)             # Test function
    (vu,vt) = split(vw)
    w  = Function(W)                 # Displacement and traction from previous iteration
    (u,t) = split(w)
    
    # Kinematics
    dim = len(u)
    I = Identity(dim)             # Identity tensor
    
    
    # Strain measure
    Fu = grad(u) + I
    Cu = Fu.T*Fu
    Eu = 0.5*(Cu-I)
    
    # Gradient of test function
    Fv = grad(vu)
    
    # Stress
    S2 = mp_lambda*tr(Eu)*I + 2*mp_mu*Eu # Second Piola-Kirchhoff stress
    P = dot(Fu,S2)  # First Piola-Kirchhoff stress
     
    
    # LHS
    a = inner(P, Fv )*dx - inner(t,vu)*ds - inner(u,vt)*ds
    
    # RHS
    f = Expression( ('Hxx*x[0] + Hxy*x[1] + Hxz*x[2]','Hyx*x[0] + Hyy*x[1] + Hyz*x[2]','Hzx*x[0] + Hzy*x[1] + Hzz*x[2]'), degree=2, Hxx=Hxx, Hxy=Hxy, Hxz=Hxz, Hyx=Hyx, Hyy=Hyy, Hyz=Hyz, Hzx=Hzx, Hzy=Hzy, Hzz=Hzz )
    L = -inner(f,vt)*ds
    
    eq = a - L
    
    solve( eq == 0, w, bcs, form_compiler_parameters=ffc_options)
    (u,t) = w.split()
    
    # Save solution in VTK format
    ufile_pvd = File("rve_dirichlet_u.pvd")
    ufile_pvd << u
    tfile_pvd = File("rve_dirichlet_t.pvd")
    tfile_pvd << t

    return u,t

####################################
# Unit test
def test_mesh():
    # An extremely simple unit test: 
    # Check that prescibing H = 0
    # leads to u = 0.
    print 'Running unit test...'

    ################################################
    # Input parameters
    
    # Components of the prescribed 
    # effective displacement gradient
    Hxx = Constant(0.0)
    Hxy = Constant(0.0)
    Hxz = Constant(0.0)
    Hyx = Constant(0.0)
    Hyy = Constant(0.0)
    Hyz = Constant(0.0)
    Hzx = Constant(0.0)
    Hzy = Constant(0.0)
    Hzz = Constant(0.0)
    
    
    ################################################
    # Material parameters
    E, nu = 10.0, 0.3
    mp_mu = Constant(E/(2*(1 + nu)))
    mp_lambda = Constant(E*nu/((1+nu)*(1-2*nu)))

    u,t = solve_rve_dirichlet(Hxx, Hxy, Hxz, Hyx, Hyy, Hyz, Hzx, Hzy, Hzz, mp_mu, mp_lambda)

    norm_u = norm(u)
    print 'norm(u): ', norm_u
    
    assert norm_u < DOLFIN_EPS


####################################
# Default options
if __name__ == '__main__':

    ################################################
    # Input parameters
    
    # Components of the prescribed 
    # effective displacement gradient
    Hxx = Constant(0.3)
    Hxy = Constant(0.4)
    Hxz = Constant(0.0)
    Hyx = Constant(0.0)
    Hyy = Constant(0.0)
    Hyz = Constant(0.0)
    Hzx = Constant(0.3)
    Hzy = Constant(0.0)
    Hzz = Constant(0.0)
    
    
    ################################################
    # Material parameters
    E, nu = 10.0, 0.3
    mp_mu = Constant(E/(2*(1 + nu)))
    mp_lambda = Constant(E*nu/((1+nu)*(1-2*nu)))

    solve_rve_dirichlet(Hxx, Hxy, Hxz, Hyx, Hyy, Hyz, Hzx, Hzy, Hzz, mp_mu, mp_lambda)
