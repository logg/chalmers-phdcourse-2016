from fenics import *
import time
import numpy as np
import sympy as sym

start = time.time()
T = 1.0            # final time
num_steps = 10     # number of time steps
dt = T / num_steps # time step size
# Create mesh and function space
nx = ny = 8
mesh = UnitSquareMesh(nx, ny)
V = FunctionSpace(mesh, 'P', 2)

# Differentiate u to find right hand side f
x, y, t = sym.symbols('x[0], x[1], t')
u = 1 + x*x + y*y + t
a = 3*x*x+1
c = x*y
f = sym.diff(u,t) - sym.diff(a*sym.diff(u, x), x) - sym.diff(a*sym.diff(u, y), y) + c*u
f = sym.simplify(f)
f_code = sym.printing.ccode(f)
a_code = sym.printing.ccode(a)
u_code = sym.printing.ccode(u)
c_code = sym.printing.ccode(c)
# Convert f,a,c,u to expressions,interpolate a and c
u_D = Expression(u_code,degree=2, t=0)
f = Expression(f_code, degree=4, t=0)
a = interpolate(Expression(a_code, degree=2),V)
c = interpolate(Expression(c_code, degree=2),V)
print('f =', f_code)

# Define boundary condition for test problem
def boundary(x, on_boundary):
    return on_boundary

bc = DirichletBC(V, u_D, boundary)
u_n = interpolate(u_D, V) # Initial value

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
F = u*v*dx + dt*a*dot(grad(u), grad(v))*dx + dt*c*u*v*dx - (u_n + dt*f)*v*dx
AA, L = lhs(F), rhs(F)

# Time-stepping
u = Function(V)
t = 0
for n in range(num_steps):
    # Update current time
    t += dt
    u_D.t = t
    f.t = t
    # Compute anbd plot solution
    solve(AA == L, u, bc)
    plot(u)
    # Compute max error
    u_e = interpolate(u_D, V)
    error = np.abs(u_e.vector().array() - u.vector().array()).max()
    print('t = %.2f: error = %.3g' % (t, error)) # Should be machine precision
    # Update previous solution
    u_n.assign(u)
end = time.time()
print('computation time is %.2f seconds' %(end - start))

tol = 1E-12
def test_solution():
	given = u
	expected = interpolate(u_D, V)
	assert near(np.abs(expected.vector().array() - given.vector().array()).max(),0,tol)
