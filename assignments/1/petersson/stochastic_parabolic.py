from fenics import *
import time
import numpy as np

start = time.time() # measure computation time
T = 1.0            # end time
num_steps = 100     # time steps
dt = T / num_steps # time step size
np.random.seed(seed=2) # seed for replicability
N = 10 # N^2 terms in the K-L expansion
s = np.random.normal(0, 1, (num_steps,N,N)) # random numbers for the increments
Q_eigval = lambda i,j: 1/(float(i)*float(j))**3; # eigenvalues of Q

class QWienerProcess(Expression): # Q-Wiener process expression
    def __init__(self, t_index, degree):
        self.t_index = t_index
	self.degree = degree

    def eval(self, value, x):
	v = 0
        for i in range(0,N):
            for j in range(0,N):
                v += 2*sqrt(dt)*sin((i+1)*np.pi*x[0])*sin((j+1)*np.pi*x[1])*s[self.t_index-1,i,j]*sqrt(Q_eigval(i+1,j+1))
	value[0]=v

# mesh and function space
nx = ny = 8
mesh = UnitSquareMesh(nx, ny)
V = FunctionSpace(mesh, 'P', 2)

# zero boundary conditions
def boundary(x, on_boundary):
    return on_boundary

bc = DirichletBC(V, 0, boundary)

# initial value
u_0 = Expression('10*(x[0]*(1-x[0]))*(x[1]*(1-x[1]))',degree=2)
u_n = project(u_0, V)

# multiplicative Nemytskii noise term
def lam(u):
    return sqrt(u**2+0.05)

# define a, c and noise
a = Expression('1*x[0]*x[0]+0.1',degree=2)
a = interpolate(a,V)
c = Expression('x[0]*x[1]',degree=2)
c = interpolate(c,V)
gW = QWienerProcess(degree=2,t_index=0)

# define variational problem
u = TrialFunction(V)
v = TestFunction(V)
F = u*v*dx + dt*a*dot(grad(u), grad(v))*dx + + dt*c*u*v*dx - (u_n + lam(u_n)*gW)*v*dx
aa, L = lhs(F), rhs(F)

# create VTK files for solution and Wiener process
vtkfilesol = File('stochastic_parabolic/solution.pvd')
vtkfilewie = File('stochastic_parabolic/wiener.pvd')

# time-stepping
t = 0
u = Function(V)
uplot = Function(V)
Wplot = Function(V)
cumW = Function(V) # to plot wiener process
cumW.interpolate(Constant(0))
for n in range(num_steps):

    t += dt    # update time
    gW.t_index += 1 # update Wiener process
    solve(aa == L, u, bc) # compute solution
    Wplot.interpolate(gW)
    cumW.assign(project(cumW+Wplot,V))
    uplot.assign(u) 

    # save solution and Wiener process 
    vtkfilesol << (uplot, t)
    vtkfilewie << (cumW, t)

    # update previous solution
    u_n.assign(u)

end = time.time()
print('Computation time is %.2f seconds' %(end - start))
