from run_coupled import *

def test_problem():
    # Simple unit test
    # Solve the problem once with the thermo-elastic model without
    # updating the temperature, and once withouth the coupling
    # Check that L_2 norm of displacements, and von Mise stress
    # agree within given tolerance
    
    # Thermoelastic model
    u_te, t_te, mise_te = solve_thermoelastic_problem(True, True, False)
    
    # Elastic model
    u_e, t_e, mise_e = solve_thermoelastic_problem(False, True, False)
    
    TOL = 1e-12
    
    assert abs(norm(u_te) - norm(u_e))       < TOL
    assert abs(norm(mise_te) - norm(mise_e)) < TOL
