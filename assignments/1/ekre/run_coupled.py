from __future__ import division
from fenics import *
from mshr import *

# Solution of a coupled problem with displacements and temperature
# as unknowns in a finite strain setting. The material is a
# Sant Venant-Kirchoff model, modified to include the strain from
# the thermal expansion. The geometry is a simple cube, which is
# extended and twisted.
#
# Fredrik Ekre

def solve_thermoelastic_problem(thermoelastic, updatedisp, updatetemp):
    # Compiler options
    ffc_options = {"optimize": True, \
               "eliminate_zeros": True, \
               "precompute_basis_const": True, \
               "precompute_ip_const": True}

    # Elastic parameters
    E = 10.0
    nu = 0.3
    lambd = E*nu/((1+nu) * (1-2*nu))   # Lame's first parameter
    mu = E/(2*(1+nu))                  # Lame's second parameter

    # Thermal parameters
    alpha = 0.05                       # thermal expansion coefficient
    k = 1.0                            # heat conductivity
    c = 1.0                            # heat capacity

    # Create a simple mesh
    Lx, Ly, Lz = 1.0, 1.0, 1.0
    nelx, nely, nelz = 10, 10, 10
    mesh = BoxMesh(Point(-Lx, -Ly, -Lz), Point(Lx, Ly, Lz), nelx, nely, nelz)

    # Function spaces
    P1v = VectorElement("Lagrange", mesh.ufl_cell(), 1)
    U = FunctionSpace(mesh, P1v)
    P1s = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
    T = FunctionSpace(mesh, P1s)

    P1vP1s = P1v * P1s
    W = FunctionSpace(mesh, P1vP1s)

    # Define functions
    w = Function(W)
    u, t = split(w)
    dw = TestFunction(W)
    du, dt = split(dw)

    # Source terms
    f_u = Constant((0.0, 0.0, 0.0))
    f_t = Constant(0.0)

    # Function for bot temperature
    def temp_bot(ti, Ti):
        return t_0 + max(t_0 * sin(pi * ti / Ti), 0.0)

    # Initial condition
    w_init = Function(W)
    u_init = interpolate(Constant((0.0, 0.0, 0.0)), U)
    assign(w_init.sub(0), u_init)
    t_0 = 5.0
    t_init = interpolate(Constant((t_0)), T)
    assign(w_init.sub(1), t_init)
    u_n, t_n = split(w_init)

    # Boundary conditions
    bot =  CompiledSubDomain("near(x[2], side) && on_boundary", side = -Lz)
    top = CompiledSubDomain("near(x[2], side) && on_boundary", side = Lz)

    u_bot_D = Constant((0.0, 0.0, 0.0))
    angle = pi / 4
    u_top_D = Expression(("scale * ((x[0]*cos(angle) - x[1]*sin(angle)) - x[0])",
                          "scale * ((x[0]*sin(angle) + x[1]*cos(angle)) - x[1])",
                          "scale * 0.5 * Lz"),
                          scale = 1.0, angle = angle, Lz = Lz, degree = 2)

    t_bot_D = Expression("temperature", temperature = t_0, degree = 2)

    bc_u_bot = DirichletBC(W.sub(0), u_bot_D, bot)
    bc_u_top = DirichletBC(W.sub(0), u_top_D, top)
    bc_t_bot = DirichletBC(W.sub(1), t_bot_D, bot)

    bcs = [bc_u_bot, bc_u_top, bc_t_bot]

    # Strain
    dim = len(u)
    I = Identity(dim)          # Identity tensor
    F = grad(u) + I            # Deformation gradient
    E = 0.5 * (F.T * F - I)    # Green-Lagrange strain

    # Second Piola-Kirchoff
    if thermoelastic:
        S = lambd * tr(E) * I + 2 * mu * E - 3 * alpha * (t - t_0) * (lambd + 2 * mu / 3) * I
    else:
        S = lambd * tr(E) * I + 2 * mu * E

    P = F * S # First Piola-Kirchoff

    # Effective von Mise stress
    detF = det(F)
    sigma = 1.0 / detF * P * F.T  # Cauchy stress
    sigmadev = sigma - 1 / 3 * tr(sigma) * I
    mise = sqrt(3/2 * inner(sigmadev, sigmadev))
    M = FunctionSpace(mesh, "Lagrange", 1)
    mise_h = project(mise, M)

    # Time loop
    nsteps = 30
    Ti = 5.0 # Total time
    deltat = Ti / nsteps
    ti = 0.0

    # Residuals
    R_u = inner(grad(du), P) * dx - dot(du, f_u) * dx
    R_t = dt * (t -t_n) * c * dx + deltat * k * dot(grad(dt), grad(t)) * dx - deltat * dt * f_t * dx
    R = R_u + R_t

    # Jacobian
    J = derivative(R, w)

    # Files to export solution
    ufile = File("results/coupled_u.pvd")
    tfile = File("results/coupled_t.pvd")
    misefile = File("results/coupled_mise.pvd")

    # Start time-loop
    for step in range(nsteps):
        # Update bc and loads
        if updatedisp:
            u_top_D.scale = ti/Ti
        if updatetemp:
            t_bot_D.temperature = temp_bot(ti, Ti)

        # Solve
        solve(R == 0, w, bcs, J = J, form_compiler_parameters = ffc_options)
        u, t = w.split()

        # Re-evaluate von Mise stress
        detF = det(F)
        sigma = 1.0 / detF * P * F.T  # Cauchy stress
        sigmadev = sigma - 1 / 3 * tr(sigma) * I
        mise = sqrt(3/2 * inner(sigmadev, sigmadev))
        assign(mise_h, project(mise, M))

        # Export
        ufile << (u, ti)
        tfile << (t, ti)
        misefile << (mise_h, ti)

        # Prepare for next timestep
        print "Done with timestep:", step + 1, "of", nsteps
        assign(w_init.sub(0), u)
        assign(w_init.sub(1), t)
        ti += deltat

    return u, t, mise_h

if __name__ == '__main__':

    u, t, mise = solve_thermoelastic_problem(True, True, True)
