/*
 * CGSolver.cpp
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#include "CGSolver.h"
#include <iostream>

template<typename T>
Vector<T> CGSolver<T>::solve(const SparseMatrix<T> &A, const Vector<T> &b, const Vector<T> &x0) const {

	// CG algorithm adopted from D.G. Luenberger: Linear and Nonlinear Programming, Addison-Wesley (1984)

	// Absolute tolerance
	T absTol = 1.0e-6;

	// Max number of iterations
	unsigned int maxIter = 10000;

	// Number of unknowns
	unsigned int N = b.size();

	// Initial guess: 0
	Vector<T> x = x0;

	// Residual
	Vector<T> g = A*x - b;

	Vector<T> d = b - A*x;

	Vector<T> Ad(N);
	T dAd = 0.;
	T alpha = 0.;
	T beta = 0.;

	T res_norm = 0.;

	for(unsigned int iter = 0; iter < maxIter; iter++) {


		res_norm = g.compute_norm();
		printf("Iter: %u res_norm: %e\n", iter, double(res_norm) );

		if( res_norm < absTol ) {
			cout << "Converged.\n";
			break;
		}

		Ad = A*d;
		dAd = d*Ad;
		alpha = -( g*d )/( dAd );
		x = x + d*(alpha);

		g = A*x - b;

		beta = (g*Ad)/( dAd );
		d = d*beta - g;
	}


	return x;
}


template class CGSolver<double>;
template class CGSolver<float>;
