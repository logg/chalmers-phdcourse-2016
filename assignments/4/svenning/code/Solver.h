/*
 * Solver.h
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include "Vector.h"
#include "SparseMatrix.h"

template<typename T> class Solver {
public:
	Solver() {/*Do nothing*/};
	virtual ~Solver() {/*Do nothing*/};

	virtual Vector<T> solve(const SparseMatrix<T> &A, const Vector<T> &b, const Vector<T> &x0) const = 0;

};

#endif /* SOLVER_H_ */
