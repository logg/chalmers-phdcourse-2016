/*
 * Vector.h
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include <vector>
#include <iostream>
#include <cmath>
#include <string>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace ublas = boost::numeric::ublas;

using namespace std;

template<typename T> class Vector {
public:
	Vector(unsigned int N) { _data.resize(N, T(0.));}
	Vector(unsigned int N, T val) { _data.resize(N, val);}
	virtual ~Vector() { /* Do nothing.*/ };

	void print_to_screen() const;
	void print_to_matlab(string name) const;

	void add(unsigned int i, const T val) {_data[i] += val;}

	const T &operator[](unsigned int i) const {return _data[i];}
	T &operator[](unsigned int i) {return _data[i];}

	Vector<T> operator+(const Vector<T> &v) const;
	Vector<T> operator-(const Vector<T> &v) const;

	Vector<T> operator*(const T val) const;
	T operator*(const Vector<T> &v) const;

	Vector<T> give_sub_vector( vector<unsigned int> ind ) const;

	T compute_norm() const;

	unsigned int size() const {return _data.size();}

	ublas::vector<T> give_ublas_vector() const;

protected:

	vector<T> _data;
};

template<typename T>
void Vector<T>::print_to_screen() const {
	cout << "Size: " << _data.size() << "\n";

	for(const auto& val : _data) {
		cout << val << " ";
	}

	cout << "\n";

}


template<typename T>
Vector<T> Vector<T>::operator+(const Vector<T> &v) const {

	unsigned int N = v.size();
	Vector<T> w( N, T(0.) );

	for( unsigned int i = 0; i < N; i++ ) {
		w[i] = _data[i] + v._data[i];
	}

	return w;
}

template<typename T>
Vector<T> Vector<T>::operator-(const Vector<T> &v) const {

	unsigned int N = v.size();
	Vector<T> w( N, T(0.) );

	for( unsigned int i = 0; i < N; i++ ) {
		w[i] = _data[i] - v._data[i];
	}

	return w;
}

template<typename T>
Vector<T> Vector<T>::operator*(const T val) const {

	unsigned int N = size();
	Vector<T> w( N, T(0.) );

	for( unsigned int i = 0; i < N; i++ ) {
		w[i] = _data[i]*val;
	}

	return w;
}

template<typename T>
T Vector<T>::operator*(const Vector<T> &v) const {

	unsigned int N = v.size();
	T x = T(0);

	for( unsigned int i = 0; i < N; i++ ) {
		x += _data[i] * v._data[i];
	}

	return x;
}

template<typename T>
T Vector<T>::compute_norm() const {
	T norm = 0.;

	for(T x : _data) {
		norm += x*x;
	}

	return sqrt(norm);
}

#endif /* VECTOR_H_ */
