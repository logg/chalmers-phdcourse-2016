/*
 * Mesh.h
 *
 *  Created on: Feb 6, 2017
 *      Author: svennine
 */

#ifndef MESH_H_
#define MESH_H_

#include <sstream>
#include <vector>

#include "Element.h"

using namespace std;

template<typename T>
class Mesh {
public:
	Mesh();
	virtual ~Mesh();

	void read_from_vtk_file(const string file_name);

	void print_nodes_to_screen() const;
	void print_elements_to_screen() const;

	void write_to_vtk_file(const string file_name, const Vector<T> &node_solution) const;

	unsigned int give_num_elements() const {return _elements.size();}
	const Element<T> &give_element(unsigned int i) const {return _elements[i];}

	unsigned int give_num_nodes() const {return _nodes.size();}
	const vector< Node<T> > &give_nodes() const {return _nodes;}
	const vector< Element<T> > &give_elements() const {return _elements;}

protected:
	vector< Node<T> > _nodes;
	vector< Element<T> > _elements;

};

#endif /* MESH_H_ */
