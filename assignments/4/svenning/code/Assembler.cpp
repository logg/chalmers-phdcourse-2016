/*
 * Assembler.cpp
 *
 *  Created on: Feb 7, 2017
 *      Author: svennine
 */

#include "Assembler.h"
#include "Mesh.h"
#include "SparseMatrix.h"

template<typename T>
Assembler<T>::Assembler() {
	// Do nothing

}

template<typename T>
Assembler<T>::~Assembler() {
	// Do nothing
}

template<typename T>
void Assembler<T>::assemble_elements(SparseMatrix<T> &K, Vector<T> &b, const Mesh<T> &mesh, const T shear_mod, const T bulk_mod) const {

	// Loop over elements and assemble element contributions
	for(unsigned int i = 0; i < mesh.give_num_elements(); i++ ) {

		const auto &el = mesh.give_elements()[i];
		Matrix<T> Ke = el.calc_element_stiffness(shear_mod, bulk_mod, mesh.give_nodes() );
		vector<unsigned int> el_dofs = give_el_dofs(i, mesh);

		for(unsigned int m = 0; m < el_dofs.size(); m++) {
			for(unsigned int n = 0; n < el_dofs.size(); n++) {

				// Contribution to stiffness
				K.add( el_dofs[m], el_dofs[n], Ke(m,n) );

				// Contribution to Dirichlet BCs
				const auto it = _dirichlet_bcs.find(el_dofs[n]);
				if( it != _dirichlet_bcs.end() ) {
					b[el_dofs[m]] -= Ke(m,n)*it->second;
				}
			}
		}
	}

}

template<typename T>
vector<unsigned int> Assembler<T>::give_node_dofs(unsigned int node_ind, const Mesh<T> &mesh) const {

	// Compute DOF (Degree Of Freedom) numbers for a given node.
	vector<unsigned int> dofs = {3*node_ind, 3*node_ind+1, 3*node_ind+2};

	return dofs;
}

template<typename T>
vector<unsigned int> Assembler<T>::give_el_dofs(unsigned int el_ind, const Mesh<T> &mesh) const {

	// Compute DOF (Degree Of Freedom) numbers for a given element.
	vector<unsigned int> dofs;

	const auto& el = mesh.give_element(el_ind);
	for(unsigned int n : el.give_node_ind() ) {
		const auto node_dofs = give_node_dofs(n, mesh);
		dofs.insert(dofs.end(), node_dofs.begin(), node_dofs.end());
	}

	return dofs;
}

template<typename T>
unsigned int Assembler<T>::give_num_dofs(const Mesh<T> &mesh) const {

	unsigned int num_dofs = 3*mesh.give_num_nodes();

	return num_dofs;
}

template<typename T>
void Assembler<T>::add_dirichlet_bc(unsigned int dof_ind, T pres_val) {

	_dirichlet_bcs[dof_ind] = pres_val;
}

template<typename T>
vector<unsigned int> Assembler<T>::give_free_dofs(const Mesh<T> &mesh) const {

	vector<unsigned int> free_dofs;
	unsigned int num_dofs = give_num_dofs(mesh);


	for(unsigned int i = 0; i < num_dofs; i++) {

		if( _dirichlet_bcs.find(i) == _dirichlet_bcs.end() ) {
			free_dofs.push_back(i);
		}
	}

	return free_dofs;
}

template<typename T>
Vector<T> Assembler<T>::give_full_solution_vector( Vector<T> a_free, const Mesh<T> &mesh ) const {

	unsigned int num_dofs = give_num_dofs(mesh);

	Vector<T> a(num_dofs);


	// Add BC values
	for( auto entry : _dirichlet_bcs ) {
		a[entry.first] = entry.second;
	}

	// Add values from solution
	vector<unsigned int> free_dofs = give_free_dofs(mesh);
	for(unsigned int i = 0; i < a_free.size(); i++) {
		a[ free_dofs[i] ] = a_free[i];
	}

	return a;
}

template class Assembler<double>;
template class Assembler<float>;


