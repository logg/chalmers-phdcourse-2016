/*
 * Matrix.cpp
 *
 *  Created on: Feb 6, 2017
 *      Author: svennine
 */

#include "Matrix.h"

#include <vector>
#include <iostream>
#include <cmath>
#include <string>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "Vector.h"

using namespace boost::numeric::ublas;
using namespace std;

template<typename T>
Matrix<T>::Matrix(unsigned int M, unsigned int N) {

	resize(M,N);
}

template<typename T>
Matrix<T>::~Matrix() {
	// Do nothing.
}

template<typename T>
void Matrix<T>::resize(unsigned int M, unsigned int N) {

	_rows.clear();

	for(unsigned int i = 0; i < M; i++) {
		_rows.push_back( Vector<T>(N) );
	}

}

template<typename T>
Matrix<T> Matrix<T>::operator*( const Matrix<T> &mat ) const {

	unsigned int M = nrows();
	unsigned int N = mat.ncols();
	unsigned int K = ncols();
	Matrix<T> ans( M, N );

	for( unsigned int m = 0; m <  M; m++) {
		for( unsigned int n = 0; n <  N; n++) {

			for(unsigned int k = 0; k < K; k++) {
				ans(m,n) += (*this)(m,k)*mat(k,n);
			}

		}
	}

	return ans;
}

template<typename T>
void Matrix<T>::print_to_screen() const {
	cout << "Number of rows: " << _rows.size() << "\n";

	unsigned int N = 0;
	if(_rows.size() > 0) {
		N = _rows[0].size();
	}
	cout << "Number of cols: " << N << "\n";

	for(const auto& row : _rows) {
		for( unsigned int j = 0; j < row.size(); j++ ) {
			cout << row[j] << " ";
		}
		cout << "\n";
	}

	cout << "\n";

}

template<typename T>
Matrix<T> Matrix<T>::inverse() const {

	// Adopted from
	// https://savingyoutime.wordpress.com/2009/09/21/c-matrix-inversion-boostublas/

	typedef permutation_matrix<std::size_t> pmatrix;

	boost::numeric::ublas::matrix<T> A( this->give_boost_matrix() );

	// create a permutation matrix for the LU-factorization
	pmatrix pm(A.size1());

	// perform LU-factorization
	int res = lu_factorize(A, pm);
	if (res != 0) {
		cout << "Warning in Matrix<T>::inverse(): Could not factorize matrix.\n";
	}

	// create identity matrix of "inverse"
	matrix<T> inverse(A.size1(),A.size2());
	inverse.assign(identity_matrix<T> (A.size1()));

	// backsubstitute to get the inverse
	lu_substitute(A, pm, inverse);


	Matrix<T> ans(A.size1(),A.size1());
	ans.set_from_boost_matrix(inverse);
	return ans;
}

template<typename T>
Matrix<T> Matrix<T>::transpose() const {
	Matrix<T> ans(ncols(), nrows());

	for(unsigned int i = 0; i < nrows(); i++) {
		for(unsigned int j = 0; j < ncols(); j++) {
			ans(j,i) = (*this)(i,j);
		}
	}

	return ans;
}

template<typename T>
boost::numeric::ublas::matrix<T> Matrix<T>::give_boost_matrix() const {
	boost::numeric::ublas::matrix<T> mat( nrows(), ncols() );

	for(unsigned int i = 0; i < nrows(); i++) {
		for(unsigned int j = 0; j < ncols(); j++) {
			mat(i,j) = (*this)(i,j);
		}
	}

	return mat;
}

template<typename T>
void Matrix<T>::set_from_boost_matrix(const boost::numeric::ublas::matrix<T> &mat) {


	unsigned int M = mat.size1();
	unsigned int N = mat.size2();

	resize(M,N);

	for(unsigned int i = 0; i < M; i++) {
		for(unsigned int j = 0; j < N; j++) {
			(*this)(i,j) = mat(i,j);
		}
	}

}

template class Matrix<double>;
template class Matrix<float>;


