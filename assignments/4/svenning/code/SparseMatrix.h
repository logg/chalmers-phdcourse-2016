/*
 * SparseMatrix.h
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#ifndef SPARSEMATRIX_H_
#define SPARSEMATRIX_H_

#include <vector>
#include <map>
#include <iostream>

#include "Vector.h"

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace ublas = boost::numeric::ublas;

using namespace std;

template<typename T> class SparseMatrix {
public:
	SparseMatrix(unsigned int num_rows, unsigned int num_cols);
	virtual ~SparseMatrix();

	void add(unsigned int row, unsigned int col, T val);

	Vector<T> mult(const Vector<T> &v) const;

	Vector<T> operator*(const Vector<T> &v) const {return mult(v);}

	const T &operator()(unsigned int row, unsigned int col) const;


	void print_to_screen() const;

	ublas::compressed_matrix<double, ublas::column_major, 0, ublas::unbounded_array<int>, ublas::unbounded_array<double> > give_ublas_matrix() const;

	void read_from_matlab_file(const string file_name);

	void print_to_matlab_file(const string file_name) const;
	void give_coordinate_form( vector<unsigned int> &rows, vector<unsigned int> &cols, vector<T> &data ) const;

	unsigned int num_rows() const {return _num_rows;}
	unsigned int num_cols() const {return _num_cols;}
	unsigned int num_entries() const;

	SparseMatrix<T> give_sub_matrix( vector<unsigned int> row_ind, vector<unsigned int> col_ind ) const;

protected:

	vector< map<unsigned int, T> > _data;

	unsigned int _num_rows, _num_cols;

	T zero;
};

template<typename T>
SparseMatrix<T>::SparseMatrix(unsigned int num_rows, unsigned int num_cols): zero( T(0.) ) {
	cout << "Entering SparseMatrix::SparseMatrix().\n";

	_num_rows = num_rows;
	_num_cols = num_cols;
	_data.resize(_num_rows);
}

template<typename T>
SparseMatrix<T>::~SparseMatrix() {
	// Do nothing
}

template<typename T>
void SparseMatrix<T>::add(unsigned int row, unsigned int col, const T val) {

	auto it = _data[row].find(col);

	if( it == _data[row].end() ) {
		_data[row][col] = val;
	}
	else {
		it->second += val;
	}

}


template<typename T>
Vector<T> SparseMatrix<T>::mult(const Vector<T> &v) const {

	Vector<T> x(v.size());

	unsigned int i = 0;
	for( const auto& row : _data ) {
		for( const auto& entry : row ) {
			x[i] += entry.second*v[entry.first];
		}
		i++;
	}


	return x;
}

template<typename T>
void SparseMatrix<T>::print_to_screen() const {
	cout << "Number of rows: " << _num_rows << "\n";
	cout << "Number of columns: " << _num_cols << "\n";

	unsigned int i = 0;
	for( const auto& row : _data ) {
		for( const auto& entry : row ) {
			cout << "(" << i << ", " << entry.first << "): " << entry.second << "\n";
		}
		i++;
	}

}

#endif /* SPARSEMATRIX_H_ */
