/*
 * Matrix.h
 *
 *  Created on: Feb 6, 2017
 *      Author: svennine
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>
#include <iostream>
#include <cmath>
#include <string>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "Vector.h"

using namespace std;

template<typename T>class Matrix {
public:
	Matrix(unsigned int M, unsigned int N);
	virtual ~Matrix();

	void resize(unsigned int M, unsigned int N);

	const T &operator()(unsigned int i, unsigned int j) const {return _rows[i][j];}
	T &operator()(unsigned int i, unsigned int j) {return _rows[i][j];}

	Matrix<T> operator*( const Matrix<T> &mat ) const;


	void print_to_screen() const;

	unsigned int nrows() const {return _rows.size();}
	inline unsigned int ncols() const;

	Matrix<T> inverse() const;
	Matrix<T> transpose() const;


	boost::numeric::ublas::matrix<T> give_boost_matrix() const;
	void set_from_boost_matrix(const boost::numeric::ublas::matrix<T> &mat);

private:
	vector< Vector<T> > _rows;
};

template<typename T>
unsigned int Matrix<T>::ncols() const {
	if( _rows.size() == 0 ) {
		return 0;
	}
	else {
		return _rows[0].size();
	}
}

#endif /* MATRIX_H_ */
