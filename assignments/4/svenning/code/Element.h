/*
 * Element.h
 *
 *  Created on: Feb 6, 2017
 *      Author: svennine
 */

#ifndef ELEMENT_H_
#define ELEMENT_H_

#include <vector>
#include "Matrix.h"

using namespace std;

template<typename T>
class Node {

public:
	Node();
	virtual ~Node() {};

	void set_coords(const vector<T> coords) {_coords = coords;}

	void print_to_screen() const;

	const vector<T> &give_coords() const {return _coords;}

public:
	vector<T> _coords;

};


template<typename T>
class Element {

public:
	Element();
	virtual ~Element() {};

	void set_node_ind(const vector<unsigned int> node_ind) {_node_ind = node_ind;}
	void set_paraview_el_type(unsigned int paraview_el_type) {_paraview_el_type = paraview_el_type;}
	unsigned int give_paraview_el_type() const {return _paraview_el_type;}

	void print_to_screen() const;

	unsigned int give_num_nodes() const {return _node_ind.size();}
	const vector<unsigned int> &give_node_ind() const {return _node_ind;}

	Matrix<T> calc_dN_dxi(const Vector<T> xi) const;
	Matrix<T> calc_JT(const Vector<T> xi, const vector< Node<T> > &all_nodes) const;
	T calc_detJ(const Vector<T> xi, const vector< Node<T> > &all_nodes) const;

	Matrix<T> calc_dN_dx(const Vector<T> xi, const vector< Node<T> > &all_nodes) const;
	Matrix<T> calc_B_matrix(const Vector<T> xi, const vector< Node<T> > &all_nodes) const;

	Matrix<T> calc_D_matrix(const T G, const T K) const;

	Matrix<T> calc_element_stiffness(const T G, const T K, const vector< Node<T> > &all_nodes) const;


public:
	vector<unsigned int> _node_ind;
	unsigned int _paraview_el_type;
};




#endif /* ELEMENT_H_ */
