/*
 * SparseMatrix.cpp
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#include "SparseMatrix.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace ublas = boost::numeric::ublas;

template<typename T>
ublas::compressed_matrix<double, ublas::column_major, 0, ublas::unbounded_array<int>, ublas::unbounded_array<double> > SparseMatrix<T>::give_ublas_matrix() const {

	// With inspiration from
	// http://stackoverflow.com/questions/30506073/ax-b-solve-using-boost-1-58-for-sparse-matrix
	ublas::compressed_matrix<double, ublas::column_major, 0, ublas::unbounded_array<int>, ublas::unbounded_array<double> > A(num_rows(), num_cols(), num_entries());

	for(unsigned int i = 0; i < _data.size(); i++) {
		for( const auto& entry : _data[i] ) {
			A(i, entry.first) = entry.second;
		}
	}

	return A;
}

template<typename T>
void SparseMatrix<T>::read_from_matlab_file(const string file_name) {

	// Quick-and-dirty reading of a sparse matrix stored in coordinate form with Matlab/Octave.
	// More precisely, we assume that "rows", "cols" and "data" are stored.

	printf("Entering SparseMatrix<T>::read_from_matlab_file.\n");

	_num_rows = 0;
	_num_cols = 0;

	vector<string> names;
	vector< vector<T> > vals;
	unsigned int i = 0;

	ifstream file (file_name.c_str());
	if(file.is_open()) {

		string line;
		while( getline(file,line) ) {

			size_t pos = line.find("name: ");
			if (pos != string::npos) {
				string name = line.substr(pos);

				names.push_back(name);
				vals.push_back( vector<T>() );
				i++;
			}

			if( line[0] != '%' ) {
				 std::istringstream s(line);
				 T v;
				 s >> v;
				 vals[i-1].push_back(v);
			}

		}

	}
	else {
		cout << "Failed to open file.\n";
		return;
	}


	file.close();


	// Determine number of rows and columns
	unsigned int num_rows = 0;
	unsigned int num_cols = 0;

	for( size_t j = 0; j < names.size(); j++ ) {
		if( names[j].find("rows") != string::npos ) {
			for( T k : vals[j] ) {
				num_rows = std::max(num_rows, (unsigned int)(k) );
			}
		}
	}
	cout << "num_rows: " << num_rows << "\n";


	for( size_t j = 0; j < names.size(); j++ ) {
		if( names[j].find("cols") != string::npos ) {
			for( T k : vals[j] ) {
				num_cols = std::max(num_cols, (unsigned int)(k) );
			}
		}
	}
	cout << "num_cols: " << num_cols << "\n";

	_num_rows = num_rows;
	_num_cols = num_cols;
	_data.resize(_num_rows);


	// Insert data in the sparse matrix
	// (could be done more efficiently).

	vector<T> rows, cols, data;
	for( size_t j = 0; j < names.size(); j++ ) {
		if( names[j].find("rows") != string::npos ) {
			rows = vals[j];
		}

		if( names[j].find("cols") != string::npos ) {
			cols = vals[j];
		}

		if( names[j].find("data") != string::npos ) {
			data = vals[j];
		}
	}

	cout << "data.size(): " << data.size() << "\n";
	for(unsigned int j = 0; j < data.size(); j++) {
		add( rows[j]-1, cols[j]-1, data[j] );
	}
}

template<typename T>
void SparseMatrix<T>::print_to_matlab_file(const string file_name) const {

	cout << "Writing sparse matrix to " << file_name << "\n";

	vector<unsigned int> rows, cols;
	vector<T> data;

	give_coordinate_form(rows, cols, data);


	ofstream file (file_name.c_str());
	if(file.is_open()) {

		cout << "Successfully opened file.\n";

		// rows
		file << "rows = [";

		for(unsigned int i : rows) {
			file << i+1 << "\n";
		}

		file << "];\n";


		// cols
		file << "cols = [";

		for(unsigned int j : cols) {
			file << j+1 << "\n";
		}

		file << "];\n";



		// data
		file << "data = [";

		for(T x : data) {
			file << x << "\n";
		}

		file << "];\n";

	}
	else {
		cout << "Failed to open file.\n";
		return;
	}


	file.close();


}

template<typename T>
void SparseMatrix<T>::give_coordinate_form( vector<unsigned int> &rows, vector<unsigned int> &cols, vector<T> &data ) const {

	rows.clear();
	cols.clear();
	data.clear();

	for(unsigned int i = 0; i < _data.size(); i++) {
		for( const auto& entry : _data[i] ) {

			rows.push_back(i);
			cols.push_back(entry.first);
			data.push_back(entry.second);
		}
	}
}

template<typename T>
unsigned int SparseMatrix<T>::num_entries() const {

	unsigned int num_entries = 0;

	for(unsigned int i = 0; i < _data.size(); i++) {

		num_entries += _data[i].size();
	}

	printf("num_entries: %u\n", num_entries );

	return num_entries;
}

template<typename T>
const T &SparseMatrix<T>::operator()(unsigned int row, unsigned int col) const {

	auto it = _data[row].find(col);

	if( it == _data[row].end() ) {
		return zero;
	}
	else {
		return it->second;
	}

}

template<typename T>
SparseMatrix<T> SparseMatrix<T>::give_sub_matrix( vector<unsigned int> row_ind, vector<unsigned int> col_ind ) const {

	unsigned int num_rows_sub = row_ind.size();
	unsigned int num_cols_sub = col_ind.size();

	map<unsigned int, unsigned int> col_full_to_red;
	for(unsigned int i = 0; i < col_ind.size(); i++) {
		col_full_to_red[ col_ind[i] ] = i;
	}

	map<unsigned int, unsigned int> row_full_to_red;
	for(unsigned int i = 0; i < row_ind.size(); i++) {
		row_full_to_red[ row_ind[i] ] = i;
	}

	SparseMatrix<T> ans(num_rows_sub, num_cols_sub);

	for(unsigned int i = 0; i < _data.size(); i++) {
		for( const auto& entry : _data[i] ) {

			if( col_full_to_red.find(entry.first) != col_full_to_red.end() && row_full_to_red.find(i) != row_full_to_red.end() ) {
				ans.add( row_full_to_red[i], col_full_to_red[entry.first], entry.second );
			}

		}
	}


	return ans;
}



template class SparseMatrix<double>;
template class SparseMatrix<float>;

