/*
 * Assembler.h
 *
 *  Created on: Feb 7, 2017
 *      Author: svennine
 */

#ifndef ASSEMBLER_H_
#define ASSEMBLER_H_

#include "SparseMatrix.h"
#include "Mesh.h"


using namespace std;

template<typename T>
class Assembler {
public:
	Assembler();
	virtual ~Assembler();

	void assemble_elements(SparseMatrix<T> &K, Vector<T> &b, const Mesh<T> &mesh, const T shear_mod, const T bulk_mod) const;

	vector<unsigned int> give_node_dofs(unsigned int node_ind, const Mesh<T> &mesh) const;
	vector<unsigned int> give_el_dofs(unsigned int el_ind, const Mesh<T> &mesh) const;

	unsigned int give_num_dofs(const Mesh<T> &mesh) const;

	void add_dirichlet_bc(unsigned int dof_ind, T pres_val);

	vector<unsigned int> give_free_dofs(const Mesh<T> &mesh) const;

	Vector<T> give_full_solution_vector( Vector<T> a_free, const Mesh<T> &mesh ) const;

protected:

	// Prescribed values of Dirichlet BCs
	map<unsigned int, T> _dirichlet_bcs;
};

#endif /* ASSEMBLER_H_ */
