/*
 * Vector.cpp
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#include "Vector.h"

#include <iostream>
#include <fstream>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace ublas = boost::numeric::ublas;

using namespace std;

template<typename T>
void Vector<T>::print_to_matlab(string name) const {

	ofstream file;
	file.open( name.c_str() );

	file << "x = [\n";

	for( auto x : _data ) {
		file << double(x) << "\n";
	}

	file << "];\n";

	file.close();
}

template<typename T>
Vector<T> Vector<T>::give_sub_vector( vector<unsigned int> ind ) const {

	Vector<T> ans( ind.size() );

	for(unsigned int i = 0; i < ind.size(); i++) {
		ans[i] = (*this)[ ind[i] ];
	}

	return ans;
}

template<typename T>
ublas::vector<T> Vector<T>::give_ublas_vector() const {

	ublas::vector<T> vec( size() );

	for(unsigned int i = 0; i < size(); i++) {
		vec(i) = (*this)[i];
	}

	return vec;
}

template class Vector<double>;
template class Vector<float>;
