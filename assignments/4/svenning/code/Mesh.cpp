/*
 * Mesh.cpp
 *
 *  Created on: Feb 6, 2017
 *      Author: svennine
 */

#include "Mesh.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include "Element.h"

template<typename T>
Mesh<T>::Mesh() {
	// Do nothing.
}

template<typename T>
Mesh<T>::~Mesh() {
	// Do nothing.
}

template<typename T>
void Mesh<T>::read_from_vtk_file(const string file_name) {

	cout << "Reading mesh from " << file_name << "\n";

	ifstream file (file_name.c_str());
	if(file.is_open()) {

		cout << "Successfully opened file.\n";

		string line;
		while( getline(file,line) ) {

			size_t pos = line.find("POINTS");
			if (pos != string::npos) {
				size_t pos_end = line.find("double");
				string num_points_string = line.substr(pos+7,pos_end-8);

				 std::istringstream s(num_points_string);
				 unsigned int num_points;
				 s >> num_points;
				 printf("num_points: %u\n", num_points);


				 // Read all node positions
				 vector<T> coords;
				 T x;
				 while( getline(file,line) ) {

					 std::istringstream s(line);
					 while( s >> x) {
						 coords.push_back(x);

						 if( coords.size() == 3 ) {
							 Node<T> node;
							 node.set_coords(coords);

							 _nodes.push_back(node);

							 coords.clear();
						 }
					 }

					 if( _nodes.size() ==  num_points) {
						 break;
					 }

				 }

			}

			pos = line.find("CELLS");
			if (pos != string::npos) {
				 std::istringstream s(line.substr(pos+6) );

				 unsigned int num_cells;
				 s >> num_cells;

				 cout << "line: " << line << "\n";
				 printf("num_cells: %u\n", num_cells );


				 // Read all elements
				 while( getline(file,line) ) {

					 vector<unsigned int> el_nodes;
					 unsigned int n, num_el_nodes;
					 std::istringstream s(line);
					 s >> num_el_nodes;
					 while( s >> n) {
						 el_nodes.push_back(n);
					 }

					 Element<T> element;
					 element.set_node_ind(el_nodes);

					 _elements.push_back(element);

					 if( _elements.size() ==  num_cells) {
						 break;
					 }

				 }

			}




		}


		for(auto &el : _elements) {
			// For now, hard coded to 4-node tet.
			// Should ideally be read from file.
			el.set_paraview_el_type(10);
		}

	}
	else {
		cout << "Failed to open file.\n";
		return;
	}


	file.close();

}

template<typename T>
void Mesh<T>::print_nodes_to_screen() const {

	cout << "Number of nodes: " << _nodes.size() << "\n";
	cout << "Nodes:\n";

	for(unsigned int i = 0; i < _nodes.size(); i++) {
		cout << "\n\nNode " << i << ":\n";
		_nodes[i].print_to_screen();
	}

}

template<typename T>
void Mesh<T>::print_elements_to_screen() const {

	cout << "Number of elements: " << _elements.size() << "\n";
	cout << "Elements:\n";

	for(unsigned int i = 0; i < _elements.size(); i++) {
		cout << "\n\nElement " << i << ":\n";
		_elements[i].print_to_screen();
	}

}

template<typename T>
void Mesh<T>::write_to_vtk_file(const string file_name, const Vector<T> &node_solution) const {
	cout << "Writing mesh to " << file_name << "\n";

	ofstream file (file_name.c_str());
	if(file.is_open()) {

		cout << "Successfully opened file.\n";

		// Write header
		file << "# vtk DataFile Version 2.0\n";
		file << "FE mesh\n";
		file << "ASCII\n";
		file << "DATASET UNSTRUCTURED_GRID\n";

		// Write nodes
		unsigned int num_nodes = _nodes.size();
		file << "POINTS " << num_nodes << " float\n";

		for( const auto& node : _nodes ){
			for( const T &x : node.give_coords() ) {
				file << x << " ";
			}
			file << "\n";
		}


		// Write cells
		unsigned int num_cells = _elements.size();

		unsigned int num_entries = 0;
		for(const auto& el : _elements) {
			num_entries += el.give_num_nodes() + 1;
		}

		file << "CELLS " << num_cells << " " << num_entries << "\n";

		for(const auto& el : _elements) {
			file << el.give_num_nodes() << " ";
			for( unsigned int i : el.give_node_ind() ) {
				file << i << " ";
			}
			file << "\n";
		}


		// Write cell types
		file << "CELL_TYPES " << num_cells << "\n";
		for(const auto& el : _elements) {
			file << el.give_paraview_el_type() << "\n";
		}

		// Write point data
		file << "POINT_DATA " << num_nodes << "\n";
		file << "VECTORS Displacement float\n";
		for(unsigned int i = 0; i < node_solution.size()/3; i++) {
			file << node_solution[3*i] << " " << node_solution[3*i+1] << " " << node_solution[3*i+2] << "\n";
		}

	}
	else {
		cout << "Failed to open file.\n";
		return;
	}


	file.close();

}

template class Mesh<double>;
template class Mesh<float>;
