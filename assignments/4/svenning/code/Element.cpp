/*
 * Element.cpp
 *
 *  Created on: Feb 6, 2017
 *      Author: svennine
 */

#include "Element.h"

#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

template<typename T>
Node<T>::Node() {
	// Do nothing.

}

template<typename T>
void Node<T>::print_to_screen() const {
	cout << "Coords: ";
	for(T x : _coords) {
		cout << x << " ";
	}
	cout << "\n";
}

template class Node<double>;
template class Node<float>;


template<typename T>
Element<T>::Element() {
	// Do nothing.

}

template<typename T>
void Element<T>::print_to_screen() const {
	cout << "Node indices: ";
	for(unsigned int i : _node_ind) {
		cout << i << " ";
	}
	cout << "\n";
}

template<typename T>
Matrix<T> Element<T>::calc_dN_dxi(const Vector<T> xi) const {

	Matrix<T> dNdxi(3,4);

	dNdxi(0,0) = -1.;
	dNdxi(1,0) = -1.;
	dNdxi(2,0) = -1.;

	dNdxi(0,1) =  1.;

	dNdxi(1,2) =  1.;

	dNdxi(2,3) =  1.;




	return dNdxi;
}

template<typename T>
Matrix<T> Element<T>::calc_JT(const Vector<T> xi, const vector< Node<T> > &all_nodes) const {



	Matrix<T> dNdxi = calc_dN_dxi(xi);

	Matrix<T> node_coords(4,3);

	for(unsigned int i = 0; i < 4; i++) {
		unsigned int n = _node_ind[i];
		node_coords(i,0) = all_nodes[ n ].give_coords()[0];
		node_coords(i,1) = all_nodes[ n ].give_coords()[1];
		node_coords(i,2) = all_nodes[ n ].give_coords()[2];
	}


	Matrix<T> JT = dNdxi*node_coords;

	return JT;
}

template<typename T>
T Element<T>::calc_detJ(const Vector<T> xi, const vector< Node<T> > &all_nodes) const {

	Matrix<T> JT = calc_JT(xi, all_nodes);

	T detJ = JT(0,0)*(JT(1,1)*JT(2,2)-JT(1,2)*JT(2,1))-JT(0,1)*(JT(1,0)*JT(2,2)-JT(1,2)*JT(2,0))+JT(0,2)*(JT(1,0)*JT(2,1)-JT(1,1)*JT(2,0));

	return detJ;
}

template<typename T>
Matrix<T> Element<T>::calc_dN_dx(const Vector<T> xi, const vector< Node<T> > &all_nodes) const {

	Matrix<T> dNdxi = calc_dN_dxi(xi);
	Matrix<T> JT = calc_JT(xi, all_nodes);
	Matrix<T> JTinv = JT.inverse();

	Matrix<T> dNdx = JTinv*dNdxi;

	return dNdx;
}

template<typename T>
Matrix<T> Element<T>::calc_B_matrix(const Vector<T> xi, const vector< Node<T> > &all_nodes) const {

	Matrix<T> B(6,12);

	Matrix<T> dNdx = calc_dN_dx(xi, all_nodes);

	for(unsigned int i = 0; i < 4; i++) {

		B(0, 3*i	) = dNdx(0, i);
		B(1, 3*i + 1) = dNdx(1, i);
		B(2, 3*i + 2) = dNdx(2, i);

		B(3, 3*i	) = dNdx(1, i);
		B(3, 3*i + 1) = dNdx(0, i);

		B(4, 3*i + 1) = dNdx(2, i);
		B(4, 3*i + 2) = dNdx(1, i);

		B(5, 3*i    ) = dNdx(2, i);
		B(5, 3*i + 2) = dNdx(0, i);
	}

	return B;
}

template<typename T>
Matrix<T> Element<T>::calc_D_matrix(const T G, const T K) const {

	// G: Shear stiffness
	// K: Bulk modulus
	// The material constitutive matrix D is given by 2G*I_dev^sym + K I \otimes I

	Matrix<T> D(6,6);

	D(0,0) 	= 2.*G*( 2./3.) + K;
	D(0,1) 	= 2.*G*(-1./3.) + K;
	D(0,2) 	= 2.*G*(-1./3.) + K;

	D(1,0) 	= 2.*G*(-1./3.) + K;
	D(1,1) 	= 2.*G*( 2./3.) + K;
	D(1,2) 	= 2.*G*(-1./3.) + K;

	D(2,0) 	= 2.*G*(-1./3.) + K;
	D(2,1) 	= 2.*G*(-1./3.) + K;
	D(2,2) 	= 2.*G*( 2./3.) + K;

	D(3,3) 	= G;
	D(4,4) 	= G;
	D(5,5) 	= G;

	return D;
}

template<typename T>
Matrix<T> Element<T>::calc_element_stiffness(const T G, const T K, const vector< Node<T> > &all_nodes) const {

	Matrix<T> Ke(12,12), tmp(12,12);

	// One point quadrature is sufficient for a linear tetrahedron
	vector< Vector<T> > gauss_points;
	Vector<T> gp1(3);
	gp1[0] = gp1[1] = gp1[2] = T(1./4.);
	gauss_points.push_back( gp1 );

	vector<T> gauss_weights = { T(1./6.) };


	for(unsigned int i = 0; i < gauss_weights.size(); i++) {

		Vector<T> xi 	= gauss_points[i];
		Matrix<T> B 	= calc_B_matrix(xi, all_nodes);
		Matrix<T> BT 	= B.transpose();
		Matrix<T> D		= calc_D_matrix(G,K);

		T detJ 			= fabs( calc_detJ(xi, all_nodes) );

		tmp				= BT*(D*B);

		for(unsigned int m = 0; m < 12; m++) {
			for(unsigned int n = 0; n < 12; n++) {
				Ke(m,n) += tmp(m,n)*(gauss_weights[i]*detJ);
			}
		}

	}

	return Ke;
}


template class Element<double>;
template class Element<float>;



