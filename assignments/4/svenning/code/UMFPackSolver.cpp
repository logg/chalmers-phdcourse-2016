/*
 * UblasSolver.cpp
 *
 *  Created on: Feb 8, 2017
 *      Author: svennine
 */

#include "UMFPackSolver.h"

#ifdef USE_UMF_PACK

#include "Solver.h"
#include "Vector.h"
#include "SparseMatrix.h"

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>


// Umfpack bindings obtained separately from
// svn co http://svn.boost.org/svn/boost/sandbox/numeric_bindings
#include <boost/numeric/bindings/ublas.hpp>
#include <boost/numeric/bindings/umfpack/umfpack.hpp>


namespace ublas = boost::numeric::ublas;
namespace umf = boost::numeric::bindings::umfpack;

using namespace std;
using namespace boost::numeric::ublas;

template<typename T>
Vector<T> UMFPackSolver<T>::solve(const SparseMatrix<T> &A, const Vector<T> &b, const Vector<T> &x0) const {

	// With inspiration from
	// http://stackoverflow.com/questions/30506073/ax-b-solve-using-boost-1-58-for-sparse-matrix
	// http://stackoverflow.com/questions/3989094/umfpack-and-boosts-ublas-sparse-matrix

	printf("Creating ublas matrix...\n");
	ublas::compressed_matrix<double, ublas::column_major, 0, ublas::unbounded_array<int>, ublas::unbounded_array<double> > K = A.give_ublas_matrix();
	printf("done.\n");

	ublas::vector<double> f = b.give_ublas_vector();

	ublas::vector<double> a(K.size1());

    umf::symbolic_type<double> Symbolic;
    umf::numeric_type<double> Numeric;

    printf("Symbolic...\n");
    umf::symbolic (K, Symbolic);
	printf("done.\n");

    printf("Numeric...\n");
	umf::numeric (K, Symbolic, Numeric);
	printf("done.\n");


	printf("Solving...\n");
    umf::solve (K, a, f, Numeric);
	printf("done.\n");

	Vector<T> sol(K.size1());

	for(unsigned int i = 0; i < a.size(); i++ ) {
		sol[i] = a(i);
	}

	return sol;
}

template class UMFPackSolver<double>;
template class UMFPackSolver<float>;

#endif
