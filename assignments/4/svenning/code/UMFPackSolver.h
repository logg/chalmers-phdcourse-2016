/*
 * UblasSolver.h
 *
 *  Created on: Feb 8, 2017
 *      Author: svennine
 */

#ifndef UMFPACKSOLVER_H_
#define UMFPACKSOLVER_H_

#ifdef USE_UMF_PACK

#include "Solver.h"
#include "Vector.h"
#include "SparseMatrix.h"

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace ublas = boost::numeric::ublas;


template<typename T> class UMFPackSolver : public Solver<T> {
public:
	UMFPackSolver() { /*Do nothing.*/};
	virtual ~UMFPackSolver() { /*Do nothing.*/};

	virtual Vector<T> solve(const SparseMatrix<T> &A, const Vector<T> &b, const Vector<T> &x0) const;
};

#endif

#endif /* UMFPACKSOLVER_H_ */
