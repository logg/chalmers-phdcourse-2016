/*
 * CGSolver.h
 *
 *  Created on: Feb 2, 2017
 *      Author: svennine
 */

#ifndef CGSOLVER_H_
#define CGSOLVER_H_

#include "Solver.h"
#include "Vector.h"
#include "SparseMatrix.h"


template<typename T> class CGSolver : public Solver<T> {
public:
	CGSolver():Solver<T>() {/*Do nothing.*/};
	virtual ~CGSolver() {/*Do nothing.*/};

	virtual Vector<T> solve(const SparseMatrix<T> &A, const Vector<T> &b, const Vector<T> &x0) const;

};

#endif /* CGSOLVER_H_ */
