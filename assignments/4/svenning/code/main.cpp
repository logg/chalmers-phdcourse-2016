/*
 *   CA4 Advanced FEM.
 *
 *   Written by 
 *   Erik Svenning
 *   Chalmers University of Technology
 *   February 2017
 */

#include <iostream>
#include <memory>

#include "SparseMatrix.h"
#include "Vector.h"
#include "CGSolver.h"
#include "Mesh.h"
#include "Assembler.h"

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "Element.h"
#include "UMFPackSolver.h"

namespace ublas = boost::numeric::ublas;

int main()
{

	typedef double T;
//	typedef float T;

    unique_ptr< Solver<T> > solver( new CGSolver<T>() );
//    unique_ptr< Solver<double> > solver_umfpack( new UMFPackSolver<double>() );

#if 0
    // Small test of solver

    SparseMatrix<double> K(0,0);
    K.read_from_matlab_file("../../test_matrix/k.m");


    Vector<double> q( K.num_rows(), 1.0 );
    Vector<double> x0_1( K.num_rows(), 0.0 );

    Vector<double> x = solver->solve(K,q, x0_1);
//    Vector<double> x = solver_umfpack->solve(K,q, x0_1);

    cout << "\n\nx: \n";
    x.print_to_screen();
    x.print_to_matlab("x.m");

    exit(0);
#endif


    // Read a mesh from file.
    Mesh<T> mesh;
//    mesh.read_from_vtk_file("../../meshes1/mesh_test1.vtk");
    mesh.read_from_vtk_file("../../meshes1/mesh2.vtk");





    ///////////////////////////////////
    // Assemble stiffness matrix
    Assembler<T> assembler;
    T shear_mod = 3.8462e+00;
    T bulk_mod  = 8.3333e+00;


    // Add Dirichlet BCs: Prescribed gradient on the external boundary
    const T eps = 1.0e-9;
    const T Lx = 1.0, Ly = 1.0, Lz = 1.0;
    const T Hxx = 0.3, Hxy = 0.4, Hxz = 0.0;
    const T Hyx = 0.0, Hyy = 0.0, Hyz = 0.0;
    const T Hzx = 0.3, Hzy = 0.0, Hzz = 0.0;

    for(unsigned int i = 0; i < mesh.give_num_nodes(); i++) {
    	const auto& x = mesh.give_nodes()[i].give_coords();

    	bool on_bnd = false;
    	if( x[0] < eps || x[0] > Lx-eps ) {
    		on_bnd = true;
    	}

    	if( x[1] < eps || x[1] > Ly-eps ) {
    		on_bnd = true;
    	}

    	if( x[2] < eps || x[2] > Lz-eps ) {
    		on_bnd = true;
    	}

    	if(on_bnd) {

    		vector<unsigned int> node_dofs = assembler.give_node_dofs(i, mesh);

    		assembler.add_dirichlet_bc(node_dofs[0], Hxx*x[0] + Hxy*x[1] + Hxz*x[2] );
    		assembler.add_dirichlet_bc(node_dofs[1], Hyx*x[0] + Hyy*x[1] + Hyz*x[2] );
    		assembler.add_dirichlet_bc(node_dofs[2], Hzx*x[0] + Hzy*x[1] + Hzz*x[2] );
    	}
    }

    printf("Assembling elements.\n");

    unsigned int num_dofs = assembler.give_num_dofs(mesh);
    SparseMatrix<T> A(num_dofs,num_dofs);
    Vector<T> b(num_dofs);
    assembler.assemble_elements(A, b, mesh, shear_mod, bulk_mod);

    vector<unsigned int> free_dofs = assembler.give_free_dofs(mesh);

    cout << "free_dofs.size(): " << free_dofs.size() << "\n";


    /////////////////////////////////////////////////////////////
    // Educated initial guess: "Taylor assumption".
    Vector<T> x0(num_dofs);

    for(unsigned int i = 0; i < mesh.give_num_nodes(); i++) {

		vector<unsigned int> node_dofs = assembler.give_node_dofs(i, mesh);
    	const auto& x = mesh.give_nodes()[i].give_coords();

    	T ax = Hxx*x[0] + Hxy*x[1] + Hxz*x[2];
    	x0[ node_dofs[0] ] = ax;

    	T ay = Hyx*x[0] + Hyy*x[1] + Hyz*x[2];
    	x0[ node_dofs[1] ] = ay;

    	T az = Hzx*x[0] + Hzy*x[1] + Hzz*x[2];
    	x0[ node_dofs[2] ] = az;

    }




    Vector<T> b_free = b.give_sub_vector( free_dofs );
    Vector<T> x0_free = x0.give_sub_vector( free_dofs );
    SparseMatrix<T> A_free = A.give_sub_matrix( free_dofs, free_dofs );


    // Solve
    cout << "Solving...\n";
	Vector<T> a_free = solver->solve(A_free, b_free, x0_free);
//	Vector<T> a_free = solver_umfpack->solve(A_free, b_free, x0_free);
    cout << "done.\n";

    Vector<T> a = assembler.give_full_solution_vector(a_free, mesh);

    // Write to vtk
//    mesh.write_to_vtk_file("vtk_test1.vtk", a);
    mesh.write_to_vtk_file("vtk_test2.vtk", a);

    return 0;
}

