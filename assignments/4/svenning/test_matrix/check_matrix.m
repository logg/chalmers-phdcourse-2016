
close all
clear all
clc

load('k.m')
K = sparse(rows,cols,data);

N = length(K);

b(1:N) = 1.;
b = b';

xRef = K\b;

run('../code/build/x.m')

res = x - xRef;

res_norm = norm(res, 'fro')


