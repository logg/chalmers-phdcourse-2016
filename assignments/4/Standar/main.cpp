#include <iostream>
#include<fstream>
#include <vector>
#include <stdlib.h>
#include <map>
#include "Vector.cpp"
#include "Matrix.cpp"
#include "Mesh.cpp"
#include "Assembler.cpp"
#include "KrylovSolver.cpp"

using namespace std;

int main(){
	
	// Load nodes and triangles
	ifstream geo_stream("nodes.txt");
	
	vector<vector<double> > geo;
	vector<double> node(3);
	double xcoord, ycoord, on_boundary;
	
	for(int i = 0; i < 441; i++)
	{
		geo_stream >> xcoord;
		geo_stream >> ycoord;
		geo_stream >> on_boundary;
		node[0] = xcoord;
		node[1] = ycoord;
		node[2] = on_boundary;
		geo.push_back(node);	
	}
	
	ifstream elements_stream("triangles.txt");
	
	vector<vector<unsigned int> > elements;
	vector<unsigned int> triangle(3);
	unsigned int vert1, vert2, vert3;
	
	for(int i = 0; i < 800; i++)
	{
		elements_stream >> vert1;
		elements_stream >> vert2;
		elements_stream >> vert3;
		triangle[0] = vert1 - 1;
		triangle[1] = vert2 - 1;
		triangle[2] = vert3 - 1;
		elements.push_back(triangle);
	}
	
	// Create Mesh, Assembler and KrylovSolver
	Mesh mesh(geo, elements);
	Assembler assembler;
	KrylovSolver solver;
	
	// Assemble matrix and vector
	pair<Matrix, Vector> matvec = assembler.assemble(mesh);
	Matrix mat = matvec.first;
	Vector vec = matvec.second;
	
	// Solve the linear system mat*sol = vec
	Vector sol = solver.solve(mat, vec);
	cout << sol;
	
	return 0;
}
