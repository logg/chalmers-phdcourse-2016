#include <iostream>
#include <vector>
#include <stdlib.h> 

using namespace std;

class Vector{
	
	public:
	
	// Construct vector from std::vector
	Vector(vector<double> vec)
	{
		_vec = vec;
		_length = vec.size();
	}
	
	// Construct zero vector of given length
	Vector(int length)
	{
		_vec = vector<double>(length);
		_length = length;
	}
	
	// Return length of vector
	int length()
	{
		return _vec.size();
	}
	
	// Assign values to vector
	void assign(unsigned int i, double x)
	{
		if(i > _length - 1)
		{
			cout << "Error: Invalid Vector index" << endl;
			throw ("Invalid Vector index");
		}
		else
			_vec[i] = x;
	}
	
	// Add values to vector
	void add(unsigned int i, double x)
	{
		if(i > _length - 1)
		{
			cout << "Error: Invalid Vector index" << endl;
			throw ("Invalid Vector index");
		}
		else
			_vec[i] += x;
	}
	
	// Overload operator[]
	double operator[](int i)
	{
		if(i < 0 || i > _length - 1)
		{
			cout << "Invalid Vector index" << endl;
			throw ("Invalid Vector index");
		}
		else
			return _vec[i];
	}
	
	// Inner product of two vectors
	double inner(Vector vec)
	{
		if(_length != vec._length)
		{
			cout << "Dimension Mismatch" << endl;
			throw ("Dimension Mismatch");
		}
		
		double temp = 0.0;
		
		for(int i = 0; i < _length; i++)
		{
			temp += _vec[i]*vec[i];
		}
		
		return temp;
	}
	
	// Addition of vectors
	Vector operator+(Vector vec)
	{
		if(_length != vec._length)
		{
			cout << "Dimension Mismatch" << endl;
			throw ("Dimension Mismatch");
		}
		
		Vector newvec(_length);
		
		for(int i = 0; i < _length; i++)
		{
			newvec.assign(i, _vec[i] + vec[i]);
		}
		
		return newvec;
	}
	
	// Subtraction of vectors
	Vector operator-(Vector vec)
	{
		if(_length != vec._length)
		{
			cout << "Dimension Mismatch" << endl;
			throw ("Dimension Mismatch");
		}
		
		Vector newvec(_length);
		
		for(int i = 0; i < _length; i++)
		{
			newvec.assign(i, _vec[i] - vec[i]);
		}
		
		return newvec;
	}
	
	// Multiplication by a scalar
	Vector operator*(double x)
	{		
		Vector newvec(_length);
		
		for(int i = 0; i < _length; i++)
		{
			newvec.assign(i, _vec[i]*x);
		}
		
		return newvec;
	}
	
	private:
		vector<double> _vec;
		int _length;
	
};

// Printing vector
ostream& operator<<(ostream& os, Vector vec)
{
	os << "(";
	for(int i = 0; i < vec.length() - 1; i++)
	{
		os << vec[i] << ", ";
	}
	os << vec[vec.length() - 1] << ")" << endl;
}
