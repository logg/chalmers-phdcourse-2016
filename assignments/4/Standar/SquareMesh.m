function [nodes, triangles] = SquareMesh(a, b, N1, N2, h1, h2)
% Creates a mesh on a rectangle
% a, b -- Coordinates of the lower left corner in the rectangle
% N1, N2 -- Number of steps in each direction
% h1, h2 -- Step size in each direction

triangles =  zeros(2*N1*N2, 3);
n = (1:(N1 + 1)*(N2 + 1))';
N = (1:N1)';
nodes = [a + mod(n - 1, N1 + 1)*h1, b + ceil(n/(N1 + 1) - 1)*h2, ~((1 < n & n < N1 + 1) | (N2*(N1 + 1) < n & n < (N1 + 1)*(N2 + 1)) | (mod(n, N1 + 1) == 0) | (mod(n, N1 + 1) == 1))];

for i = 1:N2
    triangles((1 + 2*(i - 1)*N1):2*i*N1, :) = [N, N + 1, N + 2 + N1; N, N + 2 + N1, N + 1 + N1] + (i - 1)*(N1 + 1);
end

% Sort so that all interior points come first
[~, I] = sort(nodes(:, 3), 1, 'descend');
[~, J] = sort(I, 1, 'ascend');
nodes = nodes(I, :);
triangles = J(triangles);