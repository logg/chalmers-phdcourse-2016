#include <iostream>
#include <vector>
#include <math.h> 
#include <stdlib.h>

using namespace std;

// Approximating solution to linear system using conjugate gradient method
class KrylovSolver{
	
	public:
		
		// Constructor
		KrylovSolver()
		{
			_tol = pow(10, -10);
		}
		
		Vector solve(Matrix A, Vector b)
		{
			Vector x(b.length());
			Vector r = b;
			Vector p = r;
			Vector Ap(b.length());
			double rr, rrnext, alpha, beta;
			
			for(int i = 0; i < 10; i++)
			{
				rr = r.inner(r);
				Ap = A*p;
				alpha = rr/Ap.inner(p);
				x = x + p*alpha;
				r = r - Ap*alpha;
				rrnext = r.inner(r);
				
				if(sqrt(rrnext) < _tol)
				{
					break;
				}
				
				beta = rrnext/rr;
				p = r + p*beta;
			}
			
			return x;
		}
		
		// Change tolerance
		void change_tol(double tol)
		{
			_tol = tol;
		}
	
	private:
		double _tol;
};
