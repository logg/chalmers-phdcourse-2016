#include <iostream>
#include <vector>
#include <math.h> 
#include <stdlib.h>

using namespace std;

// Source term class for evaluation
class Source{
	
	public:
		
		//Constructor
		Source()
		{
			// Do nothing
		}
		
		double eval(double x, double y)
		{
			return 16*x*(1 - x) + 16*y*(1 - y);
		}
	
};

// Assembler for 2D Poisson's equation with homogeneuos boundary conditions (Dirichlet or Neumann)
class Assembler{
	
	public:
		
		//Constructor
		Assembler()
		{
			// Quadrature points
			_qp.push_back(0.0);
			_qp.push_back(-0.53847);
			_qp.push_back(0.53847);
			_qp.push_back(-0.90618);
			_qp.push_back(0.90618);
			
			// Quadrature weights
			_qw.push_back(0.56889);
			_qw.push_back(0.47863);
			_qw.push_back(0.47863);
			_qw.push_back(0.23693);
			_qw.push_back(0.23693);
		}
		
		pair<Matrix, Vector> assemble(Mesh mesh)
		{
			// Get number of dofs and elements
			unsigned int dof = mesh.get_dof();
			unsigned int nrofel = mesh.get_nrofel();
			
			// Initialize zero matrix and vector
			Matrix mat(dof);
			Vector vec(dof);
			
			// Loop over each triangle, calculating the contributions to the matrix and the vector (vector part uses quadrature)
			for(int i = 0; i < nrofel; i++)
			{
				// Get the vertices in the triangle. Also calculating area of triangle
				vector<unsigned int> element = mesh.get_element(i);
				vector<double> vert1 = mesh.get_geo(element[0]);
				vector<double> vert2 = mesh.get_geo(element[1]);
				vector<double> vert3 = mesh.get_geo(element[2]);
				double area = ((vert2[0] - vert1[0])*(vert3[1] - vert1[1]) - (vert3[0] - vert1[0])*(vert2[1] - vert1[1]))/2;
				
				// Check if some of the vertices lies on the boundary and add the suitable contribution to matrix and vector
				if(vert1[2] == 1 && vert2[2] == 1 && vert3[2] == 1) // all inner nodes
				{
					mat.add(element[0], element[0], (pow(vert2[1] - vert3[1], 2) + pow(vert3[0] - vert2[0], 2))/4/area);
					mat.add(element[0], element[1], ((vert2[1] - vert3[1])*(vert3[1] - vert1[1]) + (vert3[0] - vert2[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[0], element[2], ((vert2[1] - vert3[1])*(vert1[1] - vert2[1]) + (vert3[0] - vert2[0])*(vert2[0] - vert1[0]))/4/area);
					mat.add(element[1], element[0], ((vert2[1] - vert3[1])*(vert3[1] - vert1[1]) + (vert3[0] - vert2[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[1], element[1], (pow(vert3[1] - vert1[1], 2) + pow(vert1[0] - vert3[0], 2))/4/area);
					mat.add(element[1], element[2], ((vert1[1] - vert2[1])*(vert3[1] - vert1[1]) + (vert2[0] - vert1[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[2], element[0], ((vert2[1] - vert3[1])*(vert1[1] - vert2[1]) + (vert3[0] - vert2[0])*(vert2[0] - vert1[0]))/4/area);
					mat.add(element[2], element[1], ((vert1[1] - vert2[1])*(vert3[1] - vert1[1]) + (vert2[0] - vert1[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[2], element[2], (pow(vert1[1] - vert2[1], 2) + pow(vert2[0] - vert1[0], 2))/4/area);
					
					double temp0 = 0.0;
					double temp1 = 0.0;
					double temp2 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp0 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 - (1 + _qp[k])*(1 - _qp[l])/4 - (1 + _qp[l])/2)*(1 - _qp[l]);
							temp1 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[k])*(1 - _qp[l])/4*(1 - _qp[l]);
							temp2 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[l])/2*(1 - _qp[l]);
						}
					}
					
					vec.add(element[0], temp0*area/4);
					vec.add(element[1], temp1*area/4);
					vec.add(element[2], temp2*area/4);
				}
				else if(vert1[2] == 1 && vert2[2] == 1) // 1, 2 inner nodes
				{
					mat.add(element[0], element[0], (pow(vert2[1] - vert3[1], 2) + pow(vert3[0] - vert2[0], 2))/4/area);
					mat.add(element[0], element[1], ((vert2[1] - vert3[1])*(vert3[1] - vert1[1]) + (vert3[0] - vert2[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[1], element[0], ((vert2[1] - vert3[1])*(vert3[1] - vert1[1]) + (vert3[0] - vert2[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[1], element[1], (pow(vert3[1] - vert1[1], 2) + pow(vert1[0] - vert3[0], 2))/4/area);
					
					double temp0 = 0.0;
					double temp1 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp0 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 - (1 + _qp[k])*(1 - _qp[l])/4 - (1 + _qp[l])/2)*(1 - _qp[l]);
							temp1 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[k])*(1 - _qp[l])/4*(1 - _qp[l]);
						}
					}
					
					vec.add(element[0], temp0*area/4);
					vec.add(element[1], temp1*area/4);
				}
				else if(vert1[2] == 1 && vert3[2] == 1) // 1, 3 inner nodes
				{
					mat.add(element[0], element[0], (pow(vert2[1] - vert3[1], 2) + pow(vert3[0] - vert2[0], 2))/4/area);
					mat.add(element[0], element[2], ((vert2[1] - vert3[1])*(vert1[1] - vert2[1]) + (vert3[0] - vert2[0])*(vert2[0] - vert1[0]))/4/area);
					mat.add(element[2], element[0], ((vert2[1] - vert3[1])*(vert1[1] - vert2[1]) + (vert3[0] - vert2[0])*(vert2[0] - vert1[0]))/4/area);
					mat.add(element[2], element[2], (pow(vert1[1] - vert2[1], 2) + pow(vert2[0] - vert1[0], 2))/4/area);
					
					double temp0 = 0.0;
					double temp2 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp0 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 - (1 + _qp[k])*(1 - _qp[l])/4 - (1 + _qp[l])/2)*(1 - _qp[l]);
							temp2 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[l])/2*(1 - _qp[l]);
						}
					}
					
					vec.add(element[0], temp0*area/4);
					vec.add(element[2], temp2*area/4);
				}
				else if(vert2[2] == 1 && vert3[2] == 1) // 2, 3 inner nodes
				{
					mat.add(element[1], element[1], (pow(vert3[1] - vert1[1], 2) + pow(vert1[0] - vert3[0], 2))/4/area);
					mat.add(element[1], element[2], ((vert1[1] - vert2[1])*(vert3[1] - vert1[1]) + (vert2[0] - vert1[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[2], element[1], ((vert1[1] - vert2[1])*(vert3[1] - vert1[1]) + (vert2[0] - vert1[0])*(vert1[0] - vert3[0]))/4/area);
					mat.add(element[2], element[2], (pow(vert1[1] - vert2[1], 2) + pow(vert2[0] - vert1[0], 2))/4/area);
					
					double temp1 = 0.0;
					double temp2 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp1 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[k])*(1 - _qp[l])/4*(1 - _qp[l]);
							temp2 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[l])/2*(1 - _qp[l]);
						}
					}
					
					vec.add(element[1], temp1*area/4);
					vec.add(element[2], temp2*area/4);
				}
				else if(vert1[2] == 1) // 1 inner node
				{
					mat.add(element[0], element[0], (pow(vert2[1] - vert3[1], 2) + pow(vert3[0] - vert2[0], 2))/4/area);
					
					double temp0 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp0 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 - (1 + _qp[k])*(1 - _qp[l])/4 - (1 + _qp[l])/2)*(1 - _qp[l]);
						}
					}
					
					vec.add(element[0], temp0*area/4);
				}
				else if(vert2[2] == 1) // 2 inner node
				{
					mat.add(element[1], element[1], (pow(vert3[1] - vert1[1], 2) + pow(vert1[0] - vert3[0], 2))/4/area);
					
					double temp1 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp1 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[k])*(1 - _qp[l])/4*(1 - _qp[l]);
						}
					}
					
					vec.add(element[1], temp1*area/4);
				}
				else if(vert3[2] == 1) // 3 inner node
				{
					mat.add(element[2], element[2], (pow(vert1[1] - vert2[1], 2) + pow(vert2[0] - vert1[0], 2))/4/area);
					
					double temp2 = 0.0;
					
					for(int k = 0; k < 5; k++)
					{
						for(int l = 0; l < 5; l++)
						{
							double fx = vert1[0] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[0] - vert1[0]) + (1 + _qp[l])/2*(vert3[0] - vert1[0]);
							double fy = vert1[1] + (1 + _qp[k])*(1 - _qp[l])/4*(vert2[1] - vert1[1]) + (1 + _qp[l])/2*(vert3[1] - vert1[1]);
							
							temp2 += _qw[k]*_qw[l]*_f.eval(fx, fy)*(1 + _qp[l])/2*(1 - _qp[l]);
						}
					}
					
					vec.add(element[2], temp2*area/4);
				}
			}
			
			return make_pair(mat, vec);
		}
		
	private:
		Source _f;
		vector<double> _qp;
		vector<double> _qw;
		
};
