#include <iostream>
#include <vector>
#include <stdlib.h> 

using namespace std;

// Square matrices
class Matrix{
	
	public:
		
		// Construct zero matrix of dimension dim \cross dim
		Matrix(unsigned int dim)
		{
			vector<map<unsigned int, double> > data(dim);
			_data = data;
			_dim = dim;
		}
		
		// Return size of matrix
		int size()
		{
			return _dim;
		}
		
		// Return value in given row and column
		double get(unsigned int i, unsigned int j)
		{
			return _data[i][j];
		}
		
		// Assign multiple values to matrix from a Vector
		void assign(vector<unsigned int> rownr, vector<unsigned int> colnr, Vector vec)
		{
			if(rownr.size() != colnr.size() || rownr.size() != vec.length())
			{
				cout << "Error: Dimension mismatch" << endl;
				throw("Error: Dimension mismatch");
			}
			for(int i = 0; i < vec.length(); i++)
			{
				_data[rownr[i]][colnr[i]] = vec[i];
			}
		}
		
		// Assign single value to matrix
		void assign(unsigned int rownr, unsigned int colnr, double x)
		{
			_data[rownr][colnr] = x;
		}
		
		// Add multiple values to matrix from a Vector
		void add(vector<unsigned int> rownr, vector<unsigned int> colnr, Vector vec)
		{
			if(rownr.size() != colnr.size() || rownr.size() != vec.length())
			{
				cout << "Error: Dimension mismatch" << endl;
				throw("Error: Dimension mismatch");
			}
			for(int i = 0; i < vec.length(); i++)
			{
				_data[rownr[i]][colnr[i]] += vec[i];
			}
		}
		
		// Add single value to matrix
		void add(unsigned int rownr, unsigned int colnr, double x)
		{
			_data[rownr][colnr] += x;
		}
		
		//Matrix-Vector multiplication
		Vector operator*(Vector vec)
		{
			if(vec.length() != _dim)
			{
				cout << "Error: Dimension mismatch" << endl;
				throw("Error: Dimension mismatch");
			}
			
			Vector newvec(vec.length());
			for(int i = 0; i < _dim; i++)
			{
				double temp = 0;
				for(int j = 0; j< _dim; j++)
				{
					temp += _data[i][j]*vec[j];
				}
				newvec.assign(i, temp);
			}
			return newvec;
		}
	
	
	private:
		vector<map<unsigned int, double> > _data;
		unsigned int _dim;
	
};

// Printing matrix
ostream& operator<<(ostream& os, Matrix mat)
{
	for(int j = 0; j < mat.size(); j++)
	{
		os << "(";
		for(int i = 0; i < mat.size() - 1; i++)
		{
			os << mat.get(j, i) << ", ";
		}
		os << mat.get(j, mat.size() - 1) << ")" << endl;
	}
	return os;
}
