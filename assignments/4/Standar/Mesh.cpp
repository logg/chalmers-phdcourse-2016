#include <iostream>
#include <vector>
#include <stdlib.h> 

using namespace std;

// 2D mesh class
class Mesh{
	
	public:
		
		// Constructor
		// geo should contain coordinates for the vertices in the first two columns and a bool(double) in the third column  (0 if on boundary, 1 if in the interior)
		// geo should also be sorted such that all interior nodes are first
		// elements should in each row contain which vertices are present in a specific element (first vertex have number 0)
		Mesh(vector<vector<double> > geo, vector<vector<unsigned int> > elements)
		{
			_geo = geo;
			_elements = elements;
			
			double temp = 0;
			for(int i = 0; i < geo.size(); i++)
			{
				temp += geo[i][2];
			}
			_dof = (unsigned int) temp + 0.5;
		}
		
		// Return triangle
		vector<unsigned> get_element(int i)
		{
			return _elements[i];
		}
		
		// Return geometry
		vector<double> get_geo(int i)
		{
			return _geo[i];
		}
		
		// Return degrees of freedom
		unsigned int get_dof()
		{
			return _dof;
		}
		
		// Return number of elements
		unsigned int get_nrofel()
		{
			return _elements.size();
		}
		
	private:
		vector<vector<double> > _geo;
		vector<vector<unsigned int> > _elements;
		unsigned int _dof;
};
