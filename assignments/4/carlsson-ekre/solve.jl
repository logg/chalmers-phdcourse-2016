module HyperElasticity

using JuAFEM
using Tensors
using TimerOutputs
using ProgressMeter
using Calculus

const ∇ = Tensors.gradient

include("cgcpu.jl")
# include("cggpu.jl")
# include("compute_tangent.jl")

#############
# Materials #
#############

# Linear elastic material model
immutable LinearElastic{T, N}
    μ::T
    λ::T
    C::SymmetricTensor{4,3,T,N} # elasticity tensor
end
function LinearElastic(μ, λ)
    Is = one(SymmetricTensor{2, 3})
    IIs = one(SymmetricTensor{4, 3})
    C = λ * Is ⊗ Is + 2*μ*IIs
    LinearElastic(μ, λ, C)
end
function constitutive_driver(mp::LinearElastic, H)
    ɛ = symmetric(H)
    σ = mp.C ⊡ ɛ
    return σ, C = ∂σ∂ϵ
end

# St: Venant
immutable StVenant{T}
    μ::T
    λ::T
end

function constitutive_driver(mp::StVenant, E)
    f(E) = mp.λ * trace(E) * one(E) + 2*mp.μ*E
    ∂S∂E, S = ∇(f, E, :all)
    return S, ∂S∂E
end

# NeoHook
immutable NeoHook{T}
    μ::T
    λ::T
end

function ψ(mp::NeoHook, E)
    C = 2E + one(E)
    Ic = trace(C)
    J = sqrt(det(C))
    logJ = log(J)
    mp.μ / 2 * (Ic - 3) - mp.μ * logJ + mp.λ/2 * logJ*logJ
end

function compute_S(mp::NeoHook, E)
    I = one(E)
    C = 2E + one(E)
    invC = inv(C)
    J = sqrt(det(C))
    return mp.μ *(I - invC) + mp.λ * log(J) * invC
end

function constitutive_driver(mp::NeoHook, E)
    ∂S∂E, SPK = ∇(E -> compute_S(mp, E), E, :all)
    return SPK, ∂S∂E
end

############
# Assembly #
############

# convenience function for "edof"
function getcelldofs!(celldofs, cellnodes)
    k = 1
    for n in cellnodes, d in 2:-1:0
        celldofs[k] = 3*n-d
        k += 1
    end
end

# calculate global residual g and stiffness k
function get_g_K{dim}(grid::Grid{dim}, cv, mp, u)
    # cache some stuff
    ndofs = getnbasefunctions(cv)
    ke = zeros(ndofs, ndofs)
    ge = zeros(ndofs)
    _K = start_assemble(ndofs^2*length(grid.cells)) # stiffness matrix
    g = zeros(3 * getnnodes(grid)) # residual

    celldofs = zeros(Int, ndofs) # element dofs # LOL

    # loop over all cells in the grid
    @timeit "assemble" for cell in CellIterator(grid)
        # reset
        fill!(ke, 0)
        fill!(ge, 0)
        getcelldofs!(celldofs, getnodes(cell))
        ue = u[celldofs] # element dofs
        @timeit "inner assemble" assemble_element!(ke, ge, cell, cv, mp, ue)

        assemble!(_K, ke, celldofs)
        assemble!(g, ge, celldofs)
    end

    @timeit "create sparse matrix" begin
        K = end_assemble(_K)
    end
    return g, K
end

function assemble_Ψ(cell, cv, mp, ue)
    reinit!(cv, cell)
    Π = zero(eltype(ue))
    @inbounds for qp in 1:getnquadpoints(cv)
        ∇u = function_gradient(cv, qp, ue)
        dΩ = getdetJdV(cv, qp)
        F = ∇u + one(∇u)
        E = symmetric(1/2 * (F' ⋅ F - one(F)))
        Π += ψ(mp, E) * dΩ
    end
    return Π
end

function assemble_element!(ke, ge, cell, cv, mp, ue, assemble_tangent = true)

    ndofs = getnbasefunctions(cv)
    reinit!(cv, cell) # WOW
    assemble_tangent && fill!(ke, 0.0)
    fill!(ge, 0.0)
    δE = Vector{SymmetricTensor{2, 3, eltype(ue), 6}}(ndofs)


    @inbounds for qp in 1:getnquadpoints(cv)
        ∇u = function_gradient(cv, qp, ue)
        dΩ = getdetJdV(cv, qp)

        # strain and stress + tangent
        F = one(∇u) + ∇u
        E = symmetric(1/2 * (F' ⋅ F - one(F)))

        S, ∂S∂E = constitutive_driver(mp, E)


        # Hoist computations of δE
        for i in 1:ndofs
            δFi = shape_gradient(cv, qp, i)
            δE[i] = symmetric(1/2*(δFi'⋅F + F'⋅δFi))
        end

        for i in 1:ndofs
            δFi = shape_gradient(cv, qp, i)
            ge[i] += (δE[i] ⊡ S) * dΩ
            if assemble_tangent
                δE∂S∂E = δE[i] ⊡ ∂S∂E
                S∇δu = S ⋅ δFi'
                for j in 1:ndofs
                    δ∇uj = shape_gradient(cv, qp, j)
                    ke[i, j] += (δE∂S∂E ⊡ δE[j] + S∇δu ⊡ δ∇uj' ) * dΩ
                end
            end
        end
    end
end

# solve the problem
function solve{dim}(::Type{Dim{dim}} = Dim{3})
    @assert dim == 3
    # generate a grid
    N = 20
    L = 1.0
    left = zero(Vec{dim})
    right = L * ones(Vec{dim})
    grid = generate_grid(Tetrahedron, ntuple(x->N, dim), left, right)

    nnodes = getnnodes(grid)
    ndofs = dim * nnodes # in dimD

    # material parameters
    E = 10.0
    ν = 0.3
    μ = E / (2(1 + ν))
    λ = (E * ν) / ((1 + ν) * (1 - 2ν))
    mp = NeoHook(μ, λ)

    # finite element base
    ip = Lagrange{dim, RefTetrahedron, 1}()
    qr = QuadratureRule{dim, RefTetrahedron}(1)
    cv = CellVectorValues(qr, ip)

    function rotation(X, θ = deg2rad(60.0), scale = 0.5)
        x, y, z = X
        return scale * Vec{dim}(
            (0.0,
            L/2 - y + (y-L/2)*cos(θ) - (z-L/2)*sin(θ),
            L/2 - z + (y-L/2)*sin(θ) + (z-L/2)*cos(θ)
            ))
    end

    # boundary conditions
    addnodeset!(grid, "clamped", x -> norm(x[1]) ≈ 1)
    addnodeset!(grid, "rotation", x -> norm(x[1]) ≈ 0)

    alldofs = collect(1:ndofs)
    prescribeddofs = Int[]
    prescribedvals = Float64[]

    # this is so ugly... u're ugly...
    for node in getnodeset(grid, "clamped")
        offset = dim*(node-1) + 1
        append!(prescribeddofs, offset:offset + dim - 1)
        append!(prescribedvals, zeros(dim))
    end

    for node in getnodeset(grid, "rotation")
        offset = dim*(node-1) + 1
        append!(prescribeddofs, offset:offset + dim - 1)
        append!(prescribedvals, rotation(getnodes(grid)[node].x)) # This is ugly...
    end

    freedofs = setdiff(alldofs, prescribeddofs)
    nfreedofs = length(freedofs)

    # pre-allocate
    un = zeros(ndofs) # previous solution vector
    u  = zeros(ndofs) # solution vector
    Δu = zeros(nfreedofs)

    newton_itr = -1
    NEWTON_TOL = 1e-8
    println("Analysis with ", length(grid.cells), " elements")
    reset_timer!()
    prog = ProgressThresh(NEWTON_TOL, "Solving:")
    while true; newton_itr += 1
        copy!(u, un)
        u[prescribeddofs] = prescribedvals
        u[freedofs] += Δu

        g, K = get_g_K(grid, cv, mp, u)

        normg = norm(g[freedofs])
        ProgressMeter.update!(prog, normg; showvalues = [(:iter, newton_itr)])

        if normg < NEWTON_TOL
            break
        end
        if newton_itr > 30
            error("Did not converge.")
            break
        end

        @timeit "linear solve" ΔΔu = cholfact(Symmetric(K[freedofs, freedofs])) \ g[freedofs]
        # @timeit "linear solutionlve" k, ΔΔu = cpucg(K[freedofs, freedofs], g[freedofs])
        Δu -= ΔΔu
    end

    # save the solution
    @timeit "export" begin
        vtk = vtk_grid("solution", grid)
        vtk_point_data(vtk, u, "u")
        vtk_save(vtk)
    end

    print_timer()
end

end # module
