﻿using System;

class Assembler
{
    // Initializing fields
    public RectangleMesh Mesh { get; private set; }
    public string SourceType { get; private set; }

    // Constructor
    public Assembler(RectangleMesh mesh, string sourceType)
    {
	Mesh = mesh;
	SourceType = sourceType;
    }

    public void Assemble2DPoisson(Matrix A, Vector b)
    {
        int n = Mesh.NumberOfVertices();
        if (n != A.Size[0] || n != A.Size[1] || n != b.Size)
        {
            throw new ArgumentException("ERROR! The matrix/vector dimensions do not match the mesh size.");
        }
        else
        {
            double triangleArea, facAE, facbE;
            double d00, d01, d10, d11;
            double[,] AE = new double[3, 3];
            double[] bE = new double[3];
            int t = Mesh.NumberOfTriangles();
            // Loop over all the triangles in the mesh
            for (int k = 0; k < t; k++)
            {
                Triangle tri = Mesh.Triangle(k);
                triangleArea = tri.AreaOf();

                // Compute the element stiffness matrix
                facAE = 0.25 / triangleArea;
                d00 = tri.V2.Y - tri.V0.Y; d01 = tri.V0.Y - tri.V1.Y;
                d10 = tri.V0.X - tri.V2.X; d11 = tri.V1.X - tri.V0.X;

                AE[0, 0] = facAE * ((d00 + d01) * (d00 + d01) + (d10 + d11) * (d10 + d11));
                AE[0, 1] = facAE * -(d00 * (d00 + d01) + d10 * (d10 + d11));
                AE[0, 2] = facAE * -(d01 * (d00 + d01) + d11 * (d10 + d11));
                AE[1, 1] = facAE * (d00 * d00 + d10 * d10);
                AE[1, 2] = facAE * (d01 * d00 + d11 * d10);
                AE[2, 2] = facAE * (d01 * d01 + d11 * d11);
                AE[1, 0] = AE[0, 1];
                AE[2, 0] = AE[0, 2];
                AE[2, 1] = AE[1, 2];

                // Compute the element load vector
                facbE = triangleArea / 3.0;
                bE[0] = facbE * MainClass.SourceFunction(tri.V0, SourceType);
                bE[1] = facbE * MainClass.SourceFunction(tri.V1, SourceType);
                bE[2] = facbE * MainClass.SourceFunction(tri.V2, SourceType);

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        A.Add(tri.GlobalVertexID[i], tri.GlobalVertexID[j], AE[i, j]);
                    }

                    b.Add(tri.GlobalVertexID[i], bE[i]);
                }
            }
        }
    }
}
