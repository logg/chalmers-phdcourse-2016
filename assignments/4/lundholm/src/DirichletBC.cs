﻿using System;
using System.Windows;

class DirichletBC
{
    // Initializing fields
    public RectangleMesh Mesh { get; private set; }
    public string Domain { get; private set; }
    public double Value { get; private set; }

    // Constructor
    public DirichletBC(RectangleMesh mesh, double value, string domain)
	{
        Mesh = mesh;
        Domain = domain;
        Value = value;
	}

    public int[] BoundaryVertexIDs()
    {
        int[] vertexIDs;
        if (Domain == "Left")
        {
            vertexIDs = new int[Mesh.Ny + 1];
            for (int i = 0; i <= Mesh.Ny; i++)
            {
                vertexIDs[i] = i * (Mesh.Nx + 1);
            }
        }
        else if (Domain == "Right")
        {
            vertexIDs = new int[Mesh.Ny + 1];
            for (int i = 0; i <= Mesh.Ny; i++)
            {
                vertexIDs[i] = i * (Mesh.Nx + 1) + Mesh.Nx;
            }
        }
        else if (Domain == "Bottom")
        {
            vertexIDs = new int[Mesh.Nx + 1];
            for (int i = 0; i <= Mesh.Nx; i++)
            {
                vertexIDs[i] = i;
            }
        }
        else if (Domain == "Top")
        {
            vertexIDs = new int[Mesh.Nx + 1];
            for (int i = 0; i <= Mesh.Nx; i++)
            {
                vertexIDs[i] = Mesh.NumberOfVertices() - 1 - Mesh.Nx + i;
            }
        }
        else
        {
            throw new ArgumentException("ERROR! Domain " + Domain + " is not known.");
        }

        return vertexIDs;
    }

    public void Apply(Matrix A, Vector b)
    {
        int i;
        int[] boundaryVertexID = BoundaryVertexIDs();
        for (int k = 0; k < boundaryVertexID.Length; k++)
        {
            i = boundaryVertexID[k];        // Row to replace

            // Modify system matrix A
            for (int j = A.Offset[i]; j <= A.Offset[i + 1] - 1; j++)
            {
                A.Add(i, A.Columns[j], 0.0 - A.Data[j]);
            }

            A.Add(i, i, 1.0 - A.GetDataAt(i, i));

            // Modify load vector b
            b.Add(i, Value - b.GetDataAt(i));
        }
    }
}
