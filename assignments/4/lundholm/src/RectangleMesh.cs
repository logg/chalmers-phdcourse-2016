﻿using System;
using System.Windows;

class Triangle
{
    // Initializing fields
    public Point V0 { get; private set; }
    public Point V1 { get; private set; }
    public Point V2 { get; private set; }
    public int[] GlobalVertexID { get; private set; }

    // Constructor
    public Triangle(Point p0, Point p1, Point p2, int[] gvi)
    {
        V0 = p0; V1 = p1; V2 = p2;
        GlobalVertexID = new int[3] { gvi[0], gvi[1], gvi[2] };
    }

    // Method to get the area of a triangle
    public double AreaOf()
    {
        System.Windows.Vector w1, w2; 
        w1 = Point.Subtract(V1, V0); w2 = Point.Subtract(V2, V0);

        return 0.5 * Math.Abs(System.Windows.Vector.CrossProduct(w1, w2));
    }
}

class RectangleMesh
{
    // Initializing fields
    public double X0 { get; private set; }
    public double Y0 { get; private set; }
    public double XN { get; private set; }
    public double YN { get; private set; }
    public int Nx { get; private set; }
    public int Ny { get; private set; }

    // Constructors
    public RectangleMesh() :
        this(new Point(0.0, 0.0), new Point(1.0, 1.0), 8, 8) { }

    public RectangleMesh(Point p, Point q) :
        this(p, q, 8, 8) { }

    public RectangleMesh(int a, int b) : 
        this(new Point(0.0, 0.0), new Point(1.0, 1.0), a, b) { }
   
    public RectangleMesh(Point p, Point q, int a, int b)
    {
        X0 = p.X; Y0 = p.Y; XN = q.X; YN = q.Y;
        Nx = a; Ny = b;
    }

    // Method to get the number of vertices
    public int NumberOfVertices()
    {
        return (Nx + 1) * (Ny + 1);
    }

    // Method to get the number of triangles
    public int NumberOfTriangles()
    {
        return 2 * Nx * Ny;
    }

    // Method to get the i:th x-coordintate
    public double XCoordinate(int i)
    {     
        double diff = (XN - X0) / Nx;
        return X0 + i * diff;
    }

    // Method to get the i:th y-coordintate
    public double YCoordinate(int i)
    {
        double diff = (YN - Y0) / Ny;
        return Y0 + i * diff;
    }

    // Method to get the v:th vertex as a System.Windows.Point
    public Point Vertex(int v)
    {
        int i = v % (Nx + 1);
        int j = v / (Nx + 1);
        return new Point(XCoordinate(i), YCoordinate(j));
    }

    // Method to get the t:th triangle as a Triangle
    public Triangle Triangle(int t)
    {
        int i, j, k;
        if (t % 2 == 0)
        {
            i = t / 2 + (t / 2) / Nx; j = i + 1; k = i + Nx + 1;
        }
        else
        {
            i = t / 2 + (t / 2) / Nx + 1; j = i + Nx + 1; k = j - 1;
        }   
        return new Triangle(Vertex(i), Vertex(j), Vertex(k), new int[3] { i, j, k });
    }
}

class TestRectangleMesh
{
    public static void Test()
    {
        Console.WriteLine("///// Testing class RectangleMesh /////");
        // Generate a uniform mesh in the Unit square
        RectangleMesh Mesh = new RectangleMesh(4, 4);

        // Print stuff
        Console.WriteLine(Mesh.Vertex(23));
        Console.ReadLine();

        Console.WriteLine(Mesh.Triangle(18).V2);
        Console.ReadLine();
    }
}



