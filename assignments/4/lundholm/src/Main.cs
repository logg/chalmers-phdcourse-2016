﻿using System;
using System.Windows;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

public class MainClass
{
    // Define the source function
    public static double SourceFunction(Point p, string source)
    {
	if (source == "GaussianSpikeSource")
	{
	    // Source function as a Gaussian spike
            double xl = 0.75, yl = 0.25;    // Location of the spike
            double x = p.X, y = p.Y;        // x and y coordinates
            return 100.0*Math.Exp(-100.0*(Math.Pow(x - xl, 2.0) + Math.Pow(y - yl, 2.0)));
	}
	else if (source == "UnitTestSource")
	{
	    // Source function for unit testing
	    return 2.0;
	}
	else
	{
	    throw new ArgumentException("ERROR! Source type " + source + " is not defined.");
	}
    }

    // Method for solving a 2DPoisson problem
    public static void Run2DPoissonProblem(string problemType, int n)
    {
	Console.WriteLine("\n///// Running " + problemType + " /////");

	// Declare stopwatch object for benchmarking
        Stopwatch sw;

        // Define a mesh in the unit square
        RectangleMesh USMesh = new RectangleMesh(n, n);

        // Define BCs
        DirichletBC bcLeft = new DirichletBC(USMesh, 0.0, "Left");
        DirichletBC bcRight = new DirichletBC(USMesh, 0.0, "Right");
        List<DirichletBC> bcs = new List<DirichletBC> { bcLeft, bcRight };

        // Initialize system matrix and load vector
        int vs = USMesh.NumberOfVertices();
        Console.WriteLine("System size is {0}", vs);
        Matrix A = new Matrix(new int[2] { vs, vs });
        Vector b = new Vector(vs);


	// Choose correct source function for current problem
	string sourceType;
	if (problemType == "Main")
	{
	    sourceType = "GaussianSpikeSource";
	}
	else if (problemType == "UnitTest")
	{
	    sourceType = "UnitTestSource";
	}
	else
	{
	    sourceType = "None";
	    throw new ArgumentException("ERROR! Problem " + problemType + " is not defined.");
	}

	// Assemble system matrix and load vector
        Assembler LES = new Assembler(USMesh, sourceType);
	Console.WriteLine("----- Benchmarking -----");
        Console.WriteLine("Assembling system ...");
        sw = Stopwatch.StartNew();
        LES.Assemble2DPoisson(A, b);
        sw.Stop();
        Console.WriteLine("System assembled in {0}", sw.Elapsed);

        // Apply BCs
        Console.WriteLine("Applying BCs ...");
        sw = Stopwatch.StartNew();
        foreach (var bc in bcs)
        {
            bc.Apply(A, b);
        }
        sw.Stop();
        Console.WriteLine("BCs applied in {0}", sw.Elapsed);

        // Initialize solution vector
        Vector U = new Vector(vs);

        // Solve LES
        KrylovSolver Solver = new KrylovSolver("CG");
        Console.WriteLine("Solving system ...");
        sw = Stopwatch.StartNew();
        Solver.Solve(A, ref U, b);
        sw.Stop();
        Console.WriteLine("System solved in {0}", sw.Elapsed);

        // Compute the residual R = A*U - b
        Console.WriteLine("----- The residual R = A*U - b -----");
        Vector R = A * U - b;
        Console.WriteLine("Residual max value: {0}", R.Data.Max());
        Console.WriteLine("Residual min value: {0}", R.Data.Min());

	if (problemType == "UnitTest")
	{
	    // Compute and compare with exact solution vector for unit test, i.e, u = x * (1 - x)
	    double x; Vector U_exact = new Vector(vs);
	    for (int i = 0; i < vs; i++)
	    {
 	        x = USMesh.Vertex(i).X;
	        U_exact.Add(i, x*(1.0 - x));
	    }
	    Console.WriteLine("----- Comparison between exact and FEM solution -----");
	    Vector D = U - U_exact;
            Console.WriteLine("Difference max value: {0}", D.Data.Max());
            Console.WriteLine("Difference min value: {0}", D.Data.Min());
	}

	Console.WriteLine();
    }

    static void Main()
    {
	// Run the main Poisson problem
	Run2DPoissonProblem("Main", 32);

	// Runt the unit test Poisson problem
	Run2DPoissonProblem("UnitTest", 32);

	// ----- Test some classes -----
        // TestRectangleMesh.Test();

        // TestVector.Test();

        // TestMatrix.Test();

        // TestKrylovSolver.Test1();
        // TestKrylovSolver.Test2();
    }
}
