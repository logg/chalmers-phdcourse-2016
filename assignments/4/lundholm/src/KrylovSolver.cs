﻿using System;

class KrylovSolver
{
    // Initializing fields
    public string Type { get; private set; }

    // Constructors
    public KrylovSolver() :
        this("CG") { }

    public KrylovSolver(string type)
    {
        Type = type;
    }

    // Solve method
    public void Solve(Matrix A, ref Vector x, Vector b)
    {
        if (Type == "CG")
        {
            ConjugateGradient(A, ref x, b);
        }
        else
        {
            throw new ArgumentException("Error using " + Type + ". Solve algorithm does not exist.");
        }
    }

    // Method for the Conjugate gradient algorithm
    public void ConjugateGradient(Matrix A, ref Vector x, Vector b)
    {
        Vector Ap; double alpha; double rsnew;

        Vector r = b - A * x;
        Vector p = new Vector(r.Size, r.Index, r.Data);
        double rsold = r * r;
        for (int i = 0; i < b.Size; i++)
        {
            Ap = A * p;
            alpha = rsold / (p * Ap);
            x += alpha * p;
            r -= alpha * Ap;
            rsnew = r * r;
            if (Math.Sqrt(rsnew) < 1e-10)
            {
                break;
            }
            p = r + (rsnew / rsold) * p;
            rsold = rsnew;
        }
    }
}

class TestKrylovSolver
{
    static void RunTest(Matrix A, Vector x, Vector b)
    {
        Console.WriteLine("A = ");
        A.PrintFull();

        Console.WriteLine("b = ");
        b.PrintFull();

        Console.WriteLine("x0 = ");
        x.PrintFull();

        KrylovSolver Solver = new KrylovSolver("CG");
        Solver.Solve(A, ref x, b);
        Console.WriteLine("Done solving!");
        Console.ReadLine();
        Console.WriteLine("The solution:");
        x.PrintFull();

        Vector r = A * x - b;
        Console.WriteLine("The residual:");
        r.PrintFull();
    }

    public static void Test1()
    {
        Console.WriteLine("///// Testing class Krylov Solver /////");
        // Example LES 1 (From the Wikipedia article about CG)
        int[] ASize = new int[2] { 2, 2 };
        int[] ACols = new int[4] { 0, 1, 0, 1 };
        int[] AOff = new int[3] { 0, 2, 4 };
        double[] AData = new double[4] { 4.0, 1.0, 1.0, 3.0 };
        Matrix A = new Matrix(ASize, ACols, AOff, AData);
        Vector b = new Vector(2, new int[2] { 0, 1 }, new double[2] { 1.0, 2.0 });
        Vector x = new Vector(b.Size, new int[2] { 0, 1 }, new double[2] { 2.0, 1.0 });

        RunTest(A, x, b);
    }

    public static void Test2()
    {
        Console.WriteLine("///// Testing class Krylov Solver /////");
        // Example LES 2
        int[] ASize = new int[2] { 4, 4 };
        int[] ACols = new int[10] { 0, 1, 0, 1, 2, 1, 2, 3, 2, 3 };
        int[] AOff = new int[5] { 0, 2, 5, 8, 10 };
        double[] AData = new double[10] { 3.0, 2.0, 2.0, 5.0, 7.0, 7.0, 9.0, 2.0, 2.0, 1.0 };
        Matrix A = new Matrix(ASize, ACols, AOff, AData);
        Vector b = new Vector(4, new int[4] { 0, 1, 2, 3 }, new double[4] { 1.0, -2.0, 7.0, 3.0 });
        Vector x = new Vector(b.Size, new int[] { 0 }, new double[] { 0.0 });

        RunTest(A, x, b);
    }
}
