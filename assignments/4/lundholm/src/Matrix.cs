﻿using System;
using System.Collections.Generic;

class Matrix
{
    // Initializing fields
    public int[] Size { get; private set; }
    public List<int> Columns { get; private set; }
    public List<int> Offset { get; private set; }
    public List<double> Data { get; private set; }

    // Creates a zero integer array of size k
    public static int[] ZeroArray(int k)
    {
        int[] zeroArray = new int[k];
        for (int i = 0; i < k; i++)
        {
            zeroArray[i] = 0;
        }

        return zeroArray;
    }

    // Constructors
    public Matrix(int[] size, int[] cols, int[] off, double[] data) :
        this(size, new List<int>(cols), new List<int>(off), new List<double>(data)) { }

    public Matrix(int[] size) :
        this(size, new List<int>(), new List<int>(ZeroArray(size[0] + 1)), new List<double>()) { }

    public Matrix(int[] size, List<int> cols, List<int> off) :
        this(size, cols, off, new List<double>()) { }

    public Matrix(int[] size, List<int> cols, List<int> off, List<double> data)
    {
        Size = size;
        Columns = new List<int>(cols);
        Offset = new List<int>(off);
        Data = new List<double>(data);
    }

    // Get data at (i, j)
    public double GetDataAt(int i, int j)
    {
        double data = 0.0;
        int k = GetCDIndexAt(i, j);
        if (k > -1)
        {
            data = Data[k];
        }

        return data;
    }

    // Get CDIndex at (i, j)
    public int GetCDIndexAt(int i, int j)
    {
        return Columns.FindIndex(Offset[i], Offset[i + 1] - Offset[i], x => x == j);
    }

    // Get row number given CDIndex
    public int GetRow(int k)
    {
        for (int i = 0; i < Size[0]; i++)
        {
            if (Offset[i] <= k && k < Offset[i + 1])
            {
                return i;
            }
        }
        return -1;
    }

    // Print the full matrix
    public void PrintFull()
    {
        for (int i = 0; i < Size[0]; i++)
        {
            for (int j = 0; j < Size[1]; j++)
            {
                Console.Write(GetDataAt(i, j));
                Console.Write(" ");
            }
            Console.WriteLine();
        }
        Console.ReadLine();
    }

    // Insert new element with Columns & Data index k at (i, j) with data d 
    private void InsertNewAt(int k, int i, int j, double d)
    {
        Columns.Insert(k, j);
        for (int l = i + 1; l <= Size[0]; l++)
            Offset[l] += 1;
        Data.Insert(k, d);
    }

    // Add element to matrix. If element already non-zero, then it's addition
    public void Add(int i, int j, double d)
    {
        int minCI = Offset[i], maxCI = Offset[i + 1] - 1;
        if (maxCI - minCI < 0)          // Check if row i is a zero row
        {
            InsertNewAt(minCI, i, j, d);
        }
        else
        {
            int k = Columns.FindIndex(minCI, maxCI - minCI + 1, x => x == j);
            if (k > -1)     // Check if there's already a non-zero element at the given location
            {
                Data[k] += d;
            }
            else            // Then the new element is inserted at a zero location
            {
                if (0 <= j && j < Columns[minCI])
                {
                    InsertNewAt(minCI, i, j, d);
                }
                else if (Columns[maxCI] < j && j < Size[1])
                {
                    InsertNewAt(maxCI + 1, i, j, d);
                }
                else
                {
                    for (int l = minCI; l < maxCI; l++)
                    {
                        if (Columns[l] < j && j < Columns[l + 1])
                        {
                            InsertNewAt(l + 1, i, j, d);
                            break;
                        }
                    }
                }
            }
        }
    }

    // Overload * for scalar multiplication
    public static Matrix operator* (double c, Matrix a)
    {
        Matrix scaledMatrix = new Matrix(a.Size, a.Columns, a.Offset);
        for (int k = 0; k < a.Data.Count; k++)
        {
            scaledMatrix.Data.Add(c * a.Data[k]);
        }

        return scaledMatrix;
    }

    public static Matrix operator* (Matrix a, double c)
    {
       return c * a;
    }
    
    // Overload * for matrix-vector multiplication
    public static Vector operator* (Matrix a, Vector b)
    {
        if (a.Size[1] == b.Size)
        {
            Vector product = new Vector(a.Size[0]);
            List<int> rowIndex; List<double> rowData;
            for (int i = 0; i < a.Size[0]; i++)
            {
                int minCI = a.Offset[i], maxCI = a.Offset[i + 1] - 1;
                if (maxCI - minCI > -1)     // Only proceed if we're at a non-zero row
                {
                    rowIndex = new List<int>(a.Columns.GetRange(minCI, maxCI + 1 - minCI));
                    rowData = new List<double>(a.Data.GetRange(minCI, maxCI + 1 - minCI));
                    Vector matrixRow = new Vector(a.Size[1], rowIndex, rowData);
                    product.Index.Add(i);
                    product.Data.Add(matrixRow * b);
                }
            }

            return product;
        }
        else
        {
            throw new ArgumentException("ERROR using *, the matrix and vector dimensions must agree!");
        }
    }
    
    // Overload * for matrix-matrix multiplication
    public static Matrix operator* (Matrix a, Matrix b)
    {
        if (a.Size[1] == b.Size[0])
        {
            Matrix matProduct = new Matrix(new int[2] { a.Size[0], b.Size[1] });
            Vector vecProduct;
            List<int> bColumnIndex; List<double> bColumnData;
            for (int j = 0; j < b.Size[1]; j++)
            {
                if (b.Columns.Exists(x => x == j))   // Only proceed if we're at a non-zero column
                {
                    // Create a Vector of current b column
                    bColumnIndex = new List<int>(); bColumnData = new List<double>();
                    for (int k = b.Columns.IndexOf(j); k <= b.Columns.LastIndexOf(j); k++)
                    {
                        if (b.Columns[k] == j)
                        {
                            bColumnIndex.Add(b.GetRow(k));
                            bColumnData.Add(b.Data[k]);
                        }
                    }
                    Vector bVectorColumn = new Vector(b.Size[0], bColumnIndex, bColumnData);

                    // Insert the vector a * b(:,j) in the product matrix
                    vecProduct = a * bVectorColumn;
                    for (int k = 0; k < vecProduct.Index.Count; k++)
                    {
                        matProduct.Add(vecProduct.Index[k], j, vecProduct.Data[k]);
                    }
                }
            }
            return matProduct;
        }
        else
        {
            throw new ArgumentException("ERROR using *, the inner matrix dimensions must agree!");
        }
    }
}

class TestMatrix
{
    public static void Test()
    {
        Console.WriteLine("///// Testing class Matrix /////");
        // TESTING Matrix class
        int[] ASize = new int[2] { 2, 3 };
        int[] ACols = new int[4] { 0, 2, 1, 2 };
        int[] AOff = new int[5] { 0, 0, 2, 3, 4 };
        double[] AData = new double[4] { 5.0, 8.0, 3.0, 6.0 };
        Matrix A = new Matrix(ASize, ACols, AOff, AData);
        Console.WriteLine("A = ");
        A.PrintFull();

        int[] BSize = new int[2] { 3, 4 };
        int[] BCols = new int[4] { 0, 1, 2, 3 };
        int[] BOff = new int[5] { 0, 1, 1, 3, 4 };
        double[] BData = new double[4] { 1.0, 1.0, 1.0, 1.0 };
        Matrix B = new Matrix(BSize, BCols, BOff, BData);
        Console.WriteLine("B = ");
        B.PrintFull();

        Matrix C = A * B;
        Console.WriteLine("A * B = ");
        C.PrintFull();
    }
}
