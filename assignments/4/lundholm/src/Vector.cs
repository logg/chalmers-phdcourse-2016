﻿using System;
using System.Collections.Generic;
using System.Linq;

class Vector
{
    // Initializing fields
    public int Size { get; private set; }
    public List<int> Index { get; private set; }
    public List<double> Data { get; private set; }

    // Constructors
    public Vector() :
       this(1) { }

    public Vector(int size) :
       this(size, new int[1] { 0 }, new double[1] { 0.0 }) { }

    public Vector(int[] index, double[] data) :
        this(index.Max() + 1, index, data) { }

    public Vector(int size, int[] index, double[] data) :
        this(size, new List<int>(index), new List<double>(data)) { }

    public Vector(int size, List<int> index) :
        this(size, index, new List<double>()) { }

    public Vector(int size, List<int> index, List<double> data)
    {
        Size = size;
        Index = new List<int>(index);
        Data = new List<double>(data);
    }

    // Get data given index
    public double GetDataAt(int i)
    {
        double data = 0.0;
        int k = Index.FindIndex(x => x == i);
        if (k > -1)
        {
            data = Data[k];
        }

        return data;
    }

    // Get first index given data
    public double GetIndexFor(double d)
    {
        int index = -1;
        int k = Data.FindIndex(x => x == d);
        if (k > -1)
        {
            index = Index[k];
        }

        return index;
    }

    // Print the full vector
    public void PrintFull()
    {
        for (int i = 0; i < Size; i++)
        {
            Console.WriteLine(GetDataAt(i));
        }
        Console.ReadLine();
    }

    // Add element to vector. If element already exists, then it's just addition
    public void Add(int i, double d)
    {
        int k = Index.FindIndex(x => x == i);
        if (k > -1) // Check if there's already a non-zero element at the given location
        {
            Data[k] += d;
        }
        else        // The new element is inserted at a zero location
        {

            if (0 <= i && i < Index[0])
            {
                Index.Insert(0, i);
                Data.Insert(0, d);
            }
            else if (Index.Max() < i && i < Size)
            {
                Index.Add(i);
                Data.Add(d);
            }
            else
            {
                for (int l = Index[0]; l < Index.Count - 1; l++)
                {
                    if (Index[l] < i && i < Index[l + 1])
                    {
                        Index.Insert(l + 1, i);
                        Data.Insert(l + 1, d);
                        break;
                    }
                }
            }
        }
    }

    // Remove element from vector if it exists given index
    public void Remove(int i)
    {
        int k = Index.FindIndex(x => x == i);
        if (k > -1)
        {
            Index.RemoveAt(k);
            Data.RemoveAt(k);
        }
    }

    // Remove first element with data d if it exists
    public void Remove(double d)
    {
        int k = Data.FindIndex(x => x == d);
        if (k > -1)
        {
            Index.RemoveAt(k);
            Data.RemoveAt(k);
        }

        return;
    }

    // Overload + for vector addition
    public static Vector operator+ (Vector a, Vector b)
    {
        if (a.Size == b.Size)
        {
            Vector sum = new Vector(a.Size, a.Index, a.Data);
            sum.Size = Math.Max(a.Size, b.Size);
            for (int i = 0; i < b.Index.Count; i++)
            {
                sum.Add(b.Index[i], b.Data[i]);
            }

            return sum;
        }
        else
        {
            throw new ArgumentException("ERROR using +, vector dimensions must agree!");
        }
    }

    // Overload - for vector subtraction
    public static Vector operator- (Vector a, Vector b)
    {
        if (a.Size == b.Size)
        {
            return a + -1 * b;
        }
        else
        {
            throw new ArgumentException("ERROR using -, vector dimensions must agree!");
        }
    }

    // Overload * for scalar multiplication
    public static Vector operator* (double c, Vector a)
    {
        Vector scaledVector = new Vector(a.Size, a.Index);
        for (int i = 0; i < a.Data.Count; i++)
        {
            scaledVector.Data.Add(c * a.Data[i]);
        }

        return scaledVector;
    }

    // Overload * for the dot product
    public static double operator* (Vector a, Vector b)
    {
        if (a.Size == b.Size)
        {
            double sum = 0.0;
            for (int i = 0; i < a.Index.Count; i++)
            {
                for (int j = 0; j < b.Index.Count; j++)
                {
                    if (a.Index[i] == b.Index[j])
                    {
                        sum += a.Data[i] * b.Data[j];
                    }
                }
            }

            return sum;
        }
        else
        {
            throw new ArgumentException("ERROR using *, vector dimensions must agree!");
        }
    }
}

class TestVector
{
    public static void Test()
    {
        Console.WriteLine("///// Testing class Vector /////");
        //int[] vicky_index = new int[4] { 0, 5, 7, 3 };
        //double[] vicky_data = new double[4] { -0.4, 10.2, 4.87, -3.0 };
        //Vector Vicky = new Vector(vicky_index, vicky_data);

        Vector Vicky = new Vector(new int[4] { 1, 5, 7, 3 }, new double[4] { -0.4, 10.2, 4.87, -3.0 });
        Vector Victor = new Vector(new int[5] { 1, 3, 7, 5, 6 }, new double[5] { 1.0, 2.3, 3.14, -20.0, 13.0 });
        Console.WriteLine("Vicky = ");
        Vicky.PrintFull();
        Console.WriteLine("Victor = ");
        Victor.PrintFull();

        Vector Vincent = Vicky - Victor;
        Console.WriteLine("Vicky - Victor = ");
        Vincent.PrintFull();
    }
}
