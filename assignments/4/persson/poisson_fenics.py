from dolfin import *

time_mesh = Timer("Create Mesh")
mesh = UnitSquareMesh(32, 32)
time_mesh.stop();

V = FunctionSpace(mesh, "Lagrange", 1)

# Dirichlet boundary
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

uD = Constant(0.0)
bc = DirichletBC(V, uD, boundary)

u = TrialFunction(V)
v = TestFunction(V)
f = Expression("1.0", degree=0)
a = inner(grad(u), grad(v))*dx
L = f*v*dx

u = Function(V)
time_solve = Timer("Assembly + Solve")
solve(a == L, u, bc)
time_solve.stop()

list_timings(TimingClear_keep, [TimingType_wall])
