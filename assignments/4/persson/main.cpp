#include <iostream>
# include <ctime>
#include <vector>

#include "vector.h"
#include "matrix.h"
#include "krylovsolver.h"
#include "mesh.h"
#include "assemble.h"

int main() {
  //Solve Poissons eq with f=1.

  Matrix M;
  Vector b;

  std::clock_t begin = std::clock();
  Mesh mesh(32,32);
  std::clock_t end = std::clock();
  double time_elapsed = double(end-begin)/ CLOCKS_PER_SEC;
  std::cout << "Create Mesh = " << time_elapsed << std::endl;

  begin = std::clock();
  Assemble assemble(M,b,mesh);

  Vector x;
  KrylovSolver CG(M, x, b);
  CG.solve();
  end = std::clock();
  time_elapsed = double(end-begin)/ CLOCKS_PER_SEC;
  std::cout << "Assembly + Solve = " << time_elapsed << std::endl;

  return 0;
}
