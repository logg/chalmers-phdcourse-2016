#include <iostream>
#include <numeric>

#include "mesh.h"

Mesh::Mesh() {}

Mesh::Mesh(int x_num, int y_num) {
  //Fixes a uniform mesh on the unit square

  double h_x = 1.0/x_num;
  double h_y = 1.0/y_num;
  int num_nodes = (x_num + 1)*(y_num + 1);

  //Coordinates
  for (int i = 0; i <= x_num; i++) {
    for (int j = 0; j <= y_num; j++) {
      std::vector<double> v = std::vector<double>(_space_dim);
      v[0] = i*h_x;
      v[1] = j*h_y;
      _coords.push_back(v);
    }
  }

  //Nodes (global numbering)
  _nodes = std::vector<int>(num_nodes);
  std::iota(_nodes.begin(), _nodes.end(), 0);

  //Edges
  //Not implemented yet (not needed in this application)

  //Triangles
  for (int i = 0; i < x_num; i++) {
    for (int j = 0; j < y_num; j++) {
      std::vector<int> v(3);
      v[0] = _nodes[i + j*(y_num + 1)];
      v[1] = _nodes[i + j*(y_num + 1) + 1];
      v[2] = _nodes[(x_num + 1) + (i + j*(y_num + 1)) + 1];
      _triangles.push_back(v);
      v[0] = _nodes[i + j*(y_num + 1)];
      v[1] = _nodes[(x_num + 1) + (i + j*(y_num + 1))];
      v[2] = _nodes[(x_num + 1) + (i + j*(y_num + 1)) + 1];
      _triangles.push_back(v);
    }
  }
}

void Mesh::print_coords() const {
  for (auto i = _coords.begin(); i != _coords.end(); ++i)
    std::cout << (*i)[0] << ' ' << (*i)[1] << std::endl;
}

void Mesh::print_triangles() const {
  for (auto i = _triangles.begin(); i != _triangles.end(); ++i)
    std::cout << (*i)[0] << ' ' << (*i)[1] << ' ' << (*i)[2] << ' ' << std::endl;
}
