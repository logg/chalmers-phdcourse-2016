#include <iostream>
#include <cmath>

#include "mesh.h"
#include "matrix.h"
#include "vector.h"
#include "assemble.h"

Assemble::Assemble(Matrix& A, Vector& b, Mesh& mesh) : _A(A), _b(b), _mesh(mesh) {
  assemble_A_b();
}

void Assemble::assemble_A_b() {
  auto f = [](const std::vector<double>& x) {return 1.0;}; //right hand side
  double DC = 0.0; //Dirichlet BC value

  std::vector<std::vector<double>> coords = _mesh.get_coords();
  std::vector<std::vector<int>> triangles = _mesh.get_triangles();
  _A = Matrix(coords.size(), coords.size());
  _b = Vector(coords.size());

  for (int n = 0; n < triangles.size(); n++) {
    std::vector<int> loc2glb = triangles[n]; //local to global index mapping
    double T_area = area_T(coords[loc2glb[0]], coords[loc2glb[1]], coords[loc2glb[2]]);
    Matrix grads = gradients(coords[loc2glb[0]], coords[loc2glb[1]], coords[loc2glb[2]], T_area);
    Matrix A_T = grads*grads.transpose()*T_area; //stiffness on element T

    Vector b_T(3);
    for (int i = 0; i < 3; i++)
      b_T(i) = f(coords[loc2glb[i]]);
    b_T = b_T*(T_area/3);

    for (int i = 0; i < 3; i++) {
      if (apply_dirichletBC(coords[loc2glb[i]])) {
        b_T(i) = DC;
        for (int j = 0; j < 3; j++){ A_T(i,j) = 0;}
        A_T(i,i) = 1.0;
        fix_symmetry(A_T, b_T, i);
      }
    }


    _A.add_local(A_T, loc2glb);
    _b.add_local(b_T, loc2glb);
  }
}

double Assemble::area_T(const std::vector<double>& x_1, const std::vector<double>& x_2, const std::vector<double>& x_3) const{
  return std::abs(x_1[0]*(x_2[1]-x_3[1]) + x_2[0]*(x_3[1]-x_1[1]) + x_3[0]*(x_1[1]-x_2[1]))/2.0;
}

Matrix Assemble::gradients(const std::vector<double>& x_1, const std::vector<double>& x_2, const std::vector<double>& x_3, double area) const{
  Matrix grad(3,2);
  grad(0,0) = (x_2[1]-x_3[1])/(2*area); grad(0,1) = (x_3[0]-x_2[0])/(2*area);
  grad(1,0) = (x_3[1]-x_1[1])/(2*area); grad(1,1) = (x_1[0]-x_3[0])/(2*area);
  grad(2,0) = (x_1[1]-x_2[1])/(2*area); grad(2,1) = (x_2[0]-x_1[0])/(2*area);
  return grad;
}

bool Assemble::apply_dirichletBC(const std::vector<double>& x) const{
  double eps = 2.22e-16;
  if (std::abs(x[0])<eps || std::abs(x[0]-1.0)<eps || std::abs(x[1])<eps || std::abs(x[1]-1.0)<eps) {
    return true;
  }
  return false;
}

void Assemble::fix_symmetry(Matrix& A, Vector& b, int ind) const{
  for (int i = 0; i < 3; i++) {
    if (i != ind) {
      A(i, ind) = 0;
      b(i) += -A(i, ind);
    }
  }
}
