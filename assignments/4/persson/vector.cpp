#include <cassert>
#include <iostream>

#include "vector.h"

Vector::Vector() {
  _length = 0;
}

Vector::Vector(int length) : _length(length) {
  _data = std::vector<double>(length);
}

int Vector::size() const{
  return _length;
}

double Vector::operator()(int index) const {
  return _data[index];
}

double& Vector::operator()(int index) {
  return _data[index];
}

Vector Vector::operator+(const Vector& v) const {
  assert(_length == v.size());
  Vector w(_length);
  for(int i = 0; i < _length; i++) {w(i) = _data[i] + v(i);}
  return w;
}

Vector Vector::operator-(const Vector& v) const {
  assert(_length == v.size());
  Vector w(_length);
  for(int i = 0; i < _length; i++) {w(i) = _data[i] - v(i);}
  return w;
}

Vector Vector::operator*(const double& a) const {
  Vector w(_length);
  for(int i = 0; i < _length; i++) {w(i) = _data[i]*a;}
  return w;
}

double Vector::dot(const Vector& v) const {
  assert(_length == v.size());
  double dot_value = 0;
  for(int i = 0; i < _length; i++) {dot_value += _data[i]*v(i);}
  return dot_value;
}

void Vector::add_local(const Vector& v, const std::vector<int>& loc2glb) {
  for(int i = 0; i < 3; i++) {
    _data[loc2glb[i]] += v(i);
  }
}

void Vector::print() const {
  for (int i = 0; i < _length; i++) {
    std::cout << _data[i] << ' ' << std::endl;
  }
}
