#pragma once
#include <vector>

class Mesh{
  private:
    int _space_dim = 2;
    std::vector<std::vector<double>> _coords;
    std::vector<int> _nodes;
    //std::vector<std::vector<int>> _edges;
    std::vector<std::vector<int>> _triangles;


  public:
    Mesh();
    Mesh(int x_num, int y_num);
    std::vector<std::vector<double>> get_coords() const {return _coords;};
    std::vector<std::vector<int>> get_triangles() const {return _triangles;};
    void print_coords() const;
    void print_triangles() const;
};
