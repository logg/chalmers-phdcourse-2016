#include <cassert>
#include <math.h>
#include <iostream>

#include "vector.h"
#include "matrix.h"
#include "krylovsolver.h"

KrylovSolver::KrylovSolver(const Matrix& A, Vector& x, const Vector& b) : _A(A), _x(x), _b(b) {
  assert(A.get_rows() == A.get_cols());
  assert(b.size() == A.get_rows());
}


void KrylovSolver::solve() {
  //updates x with solution

  _x = Vector(_A.get_cols()); // initial guess x=0.
  Vector r = _b - _A*_x;
  Vector p = r;
  double tol = 1e-10;
  int max_iter = 1000, iter = 0;
  double diff = sqrt(r.dot(r));
  double alpha, beta;

  while (diff > tol && iter < max_iter){
    alpha = r.dot(r)/p.dot(_A*p);
    _x = _x + p*alpha;
    r = r - (_A*p)*alpha;
    beta = r.dot(r)/pow(diff,2);
    p = r + p*beta;
    diff = r.dot(r);
    iter++;
  }

}
