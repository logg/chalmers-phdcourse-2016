#pragma once
#include <iostream>

#include "mesh.h"
#include "matrix.h"
#include "vector.h"

class Assemble{
  private:
    Mesh _mesh;
    Matrix& _A;
    Vector& _b;
    void assemble_A_b();
    double area_T(const std::vector<double>& x_1, const std::vector<double>& x_2, const std::vector<double>& x_3) const;
    Matrix gradients(const std::vector<double>& x_1, const std::vector<double>& x_2, const std::vector<double>& x_3, double T_area) const;
    bool apply_dirichletBC(const std::vector<double>& x) const;
    void fix_symmetry(Matrix& A, Vector& b, int ind) const;

  public:
    Assemble(Matrix& A, Vector& b, Mesh& mesh);
};
