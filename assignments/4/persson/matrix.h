#pragma once
#include <vector>
#include "vector.h"

class Matrix {
  private:
    int _rows, _cols;
    std::vector<double> _data;

  public:
    Matrix();
    Matrix(int rows, int cols);
    int get_rows() const;
    int get_cols() const;
    double&  operator() (int row, int col);
    double  operator() (int row, int col) const;
    Matrix transpose() const;
    void add_local(const Matrix& M, const std::vector<int>& loc2glb);
    Matrix operator+(const Matrix& M) const;
    Matrix operator-(const Matrix& M) const;
    Vector operator*(const Vector& v) const;
    Matrix operator*(const Matrix& M) const;
    Matrix operator*(double a) const;
    void print() const;
};
