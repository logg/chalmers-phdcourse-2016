#pragma once
#include <vector>

class Vector{
  private:
    int _length;
    std::vector<double> _data;

  public:
    Vector();
    Vector(int length);
    int size() const;
    double  operator() (int index) const;
    double&  operator() (int index);
    Vector operator+(const Vector& v) const;
    Vector operator-(const Vector& v) const;
    Vector operator*(const double& a) const;
    double dot(const Vector& v) const;
    void add_local(const Vector& v, const std::vector<int>& loc2glb);
    void print() const;
};
