#include <cassert>
#include <iostream>

# include "matrix.h"

Matrix::Matrix() {
  _rows = 0;
  _cols = 0;
}

Matrix::Matrix(int rows, int cols) : _rows(rows), _cols(cols) {
  _data = std::vector<double>(rows*cols);
}

int Matrix::get_rows() const {return _rows;}
int Matrix::get_cols() const {return _cols;}

double Matrix::operator()(int row, int col) const {
  return _data[row*_cols + col];
}

double& Matrix::operator()(int row, int col) {
  return _data[row*_cols + col];
}

Matrix Matrix::transpose() const {
  Matrix M(_cols,_rows);
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < _cols; j++) {
      M(j,i) = _data[i*_cols + j];
    }
  }
  return M;
}

void Matrix::add_local(const Matrix& M, const std::vector<int>& loc2glb) {
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      _data[loc2glb[i]*_cols + loc2glb[j]] += M(i,j);
    }
  }
}

Matrix Matrix::operator+(const Matrix& M) const {
  assert(_rows == M.get_rows());
  assert(_cols == M.get_cols());
  Matrix A(_rows,_cols);
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < _cols; j++) {
      A(i,j) = _data[i*_cols + j] + M(i,j);
    }
  }
  return A;
}

Matrix Matrix::operator-(const Matrix& M) const {
  assert(_rows == M.get_rows());
  assert(_cols == M.get_cols());
  Matrix A(_rows,_cols);
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < _cols; j++) {
      A(i,j) = _data[i*_cols + j] - M(i,j);
    }
  }
  return A;
}

Vector Matrix::operator*(const Vector& v) const {
  assert(_cols == v.size());
  Vector w(_rows);
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < _cols; j++) {
      w(i) = w(i) +_data[i*_cols + j]*v(j);
    }
  }
  return w;
}

Matrix Matrix::operator*(const Matrix& M) const {
  assert(_cols == M.get_rows());
  Matrix A(_rows, M.get_cols());
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < M.get_cols(); j++) {
      for (int k = 0; k < _cols; k++)
      A(i,j) += _data[i*_cols + k]*M(k,j);
    }
  }
  return A;
}

Matrix Matrix::operator*(double a) const {
  Matrix A(_rows,_cols);
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < _cols; j++) {
      A(i,j) = _data[i*_cols + j]*a;
    }
  }
  return A;
}

void Matrix::print() const {
  for (int i = 0; i < _rows; i++) {
    for (int j = 0; j < _cols; j++) {
      std::cout << _data[i*_cols + j] << ' ';
    }
    std::cout << std::endl;
  }
}
