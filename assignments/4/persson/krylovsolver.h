#pragma once
#include "vector.h"
#include "matrix.h"

class KrylovSolver{
  private:
    Vector& _x;
    Vector _b;
    Matrix _A;

  public:
    KrylovSolver(const Matrix& A, Vector& x, const Vector& b);
    void solve();
};
