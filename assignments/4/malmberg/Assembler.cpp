#include"headers/Assembler.h"
#include<vector>
#include<math.h>

using namespace std;

void Assembler::SetQuadratureRule(const unsigned int n)
{

  qp.resize(n);
  qw.resize(n);

  if (n == 6)
    {
      qp[0] = -0.9324695142031521;
      qp[1] = -0.6612093864662645;
      qp[2] = -0.2386191860831969;
      qp[3] =  0.2386191860831969;
      qp[4] =  0.6612093864662645;
      qp[5] =  0.9324695142031521;
      
      qw[0] = 0.1713244923791704;
      qw[1] = 0.3607615730481386;
      qw[2] = 0.4679139345726910;
      qw[3] = 0.4679139345726910;
      qw[4] = 0.3607615730481386;
      qw[5] = 0.1713244923791704;
    }
  
  if (n == 11)
    {
      qp[ 0] = -0.9782286581460570;
      qp[ 1] = -0.8870625997680953;
      qp[ 2] = -0.7301520055740494;
      qp[ 3] = -0.5190961292068118;
      qp[ 4] = -0.2695431559523450;
      qp[ 5] =  0.0000000000000000;
      qp[ 6] =  0.2695431559523450;
      qp[ 7] =  0.5190961292068118;
      qp[ 8] =  0.7301520055740494;
      qp[ 9] =  0.8870625997680953;
      qp[10] =  0.9782286581460570;
 
      qw[ 0] = 0.0556685671161737;
      qw[ 1] = 0.1255803694649046;
      qw[ 2] = 0.1862902109277343;
      qw[ 3] = 0.2331937645919905;
      qw[ 4] = 0.2628045445102467;
      qw[ 5] = 0.2729250867779006;
      qw[ 6] = 0.2628045445102467;
      qw[ 7] = 0.2331937645919905;
      qw[ 8] = 0.1862902109277343;
      qw[ 9] = 0.1255803694649046;
      qw[10] = 0.0556685671161737;
    }

}

void Assembler::AssembleHelmholtz(double (*k2)(const double*), double (*f)(const double*), double (*g)(const double*), bool (*on_boundary)(const double*))
{
  SetQuadratureRule(6);
  
  double x0, y0, x1, y1, x2, y2, J, x[2], v0, v1, v2, w, m01[2], m02[2], m12[2], l;
  int i[3]; // local to global
  vector<double> a(6), Lf(3), Lg(3); // local matrix (symmetric, so only 6 elements needed), and vector from source and boundary condition
  
  // Loop over all elements
  for(int e = 0; e < _mesh.NumberOfElements(); e++)
    {
      a  = vector<double>(6, 0.0);
      Lf = vector<double>(3, 0.0);
      Lg = vector<double>(3, 0.0);
      
      i[0] = _mesh._elements[e][0];
      i[1] = _mesh._elements[e][1];
      i[2] = _mesh._elements[e][2];
	
      x0 = _mesh._nodes[i[0]][0];
      y0 = _mesh._nodes[i[0]][1];
      x1 = _mesh._nodes[i[1]][0];
      y1 = _mesh._nodes[i[1]][1];
      x2 = _mesh._nodes[i[2]][0];
      y2 = _mesh._nodes[i[2]][1];

      m01[0] = (x0 + x1)/2;
      m01[1] = (y0 + y1)/2;
      m02[0] = (x0 + x2)/2;
      m02[1] = (y0 + y2)/2;
      m12[0] = (x1 + x2)/2;
      m12[1] = (y1 + y2)/2;

      J = ((x1 - x0)*(y2 - y0) - (x2 - x0)*(y1 - y0)); // Jacobi determinant
      
      // Contributions to matrix from k2*phi_i*phi_j, and to vector from f*phi_i
      for (int k = 0; k < qp.size(); k++)
	{
	  for (int l = 0; l < qp.size(); l++)
	    {
	      x[0] = x0 + (1 + qp[k])*(x1 - x0)/2 + (1 - qp[k])*(1 + qp[l])*(x2 - x0)/4;
	      x[1] = y0 + (1 + qp[k])*(y1 - y0)/2 + (1 - qp[k])*(1 + qp[l])*(y2 - y0)/4;
	      
	      w = qw[k]*qw[l];
	      
	      v1 = (1 + qp[k])/2;
	      v2 = (1 - qp[k])*(1 + qp[l])/4;
	      v0 = 1 - v1 - v2;
	      
	      a[0] += w*k2(x)*v0*v0*(1 - qp[k]);
	      a[1] += w*k2(x)*v0*v1*(1 - qp[k]);
	      a[2] += w*k2(x)*v0*v2*(1 - qp[k]);
	      a[3] += w*k2(x)*v1*v1*(1 - qp[k]);
	      a[4] += w*k2(x)*v1*v2*(1 - qp[k]);
	      a[5] += w*k2(x)*v2*v2*(1 - qp[k]);
	      
	      Lf[0] += w*f(x)*v0*(1 - qp[k]);
	      Lf[1] += w*f(x)*v1*(1 - qp[k]);
	      Lf[2] += w*f(x)*v2*(1 - qp[k]);
	    }
	}
      
      a[0] *= J/8;
      a[1] *= J/8;
      a[2] *= J/8;
      a[3] *= J/8;
      a[4] *= J/8;
      a[5] *= J/8;
      
      Lf[0] *= J/8;
      Lf[1] *= J/8;
      Lf[2] *= J/8;
      
      
      // Contributions from boundary term to vector
      if (on_boundary(m01))
	{
	  for (int k = 0; k < qp.size(); k++)
	    {
	      x[0] = x0 + (1 + qp[k])*(x1 - x0)/2;
	      x[1] = y0 + (1 + qp[k])*(y1 - y0)/2;
	      
	      Lg[0] -= qw[k]*g(x)*(1 - qp[k]);
	      Lg[1] -= qw[k]*g(x)*(1 + qp[k]);
	    }
	  
	  l = sqrt( (x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0) );
	  
	  Lg[0] *= l;
	  Lg[1] *= l;
	}
      
      if (on_boundary(m02))
	{
	  for (int k = 0; k < qp.size(); k++)
	    {
	      x[0] = x0 + (1 + qp[k])*(x2 - x0)/2;
	      x[1] = y0 + (1 + qp[k])*(y2 - y0)/2;
	      
	      Lg[0] -= qw[k]*g(x)*(1 - qp[k]);
	      Lg[2] -= qw[k]*g(x)*(1 + qp[k]);
	    }
	  
	  l = sqrt( (x2 - x0)*(x2 - x0) + (y2 - y0)*(y2 - y0) );

	  Lg[0] *= l;
	  Lg[2] *= l;
	}
      
      if (on_boundary(m12))
	{
	  for (int k = 0; k < qp.size(); k++)
	    {
	      x[0] = x1 + (1 + qp[k])*(x2 - x1)/2;
	      x[1] = y1 + (1 + qp[k])*(y2 - y1)/2;
	      
	      Lg[1] -= qw[k]*g(x)*(1 - qp[k]);
	      Lg[2] -= qw[k]*g(x)*(1 + qp[k]);
	    }
	  
	  l = sqrt( (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) );
	  
	  Lg[1] *= l;
	  Lg[2] *= l;
	}
      
      Lg[0] *= 0.25;
      Lg[1] *= 0.25;
      Lg[2] *= 0.25;
      
      
      // Contributions to matrix from -grad(phi_i)*grad(phi_j)
      a[0] -= ( (y1 - y2)*(y1 - y2) + (x2 - x1)*(x2 - x1) ) / (2*J);
      a[1] -= ( (y1 - y2)*(y2 - y0) + (x2 - x1)*(x0 - x2) ) / (2*J);
      a[2] -= ( (y1 - y2)*(y0 - y1) + (x2 - x1)*(x1 - x0) ) / (2*J);
      a[3] -= ( (y2 - y0)*(y2 - y0) + (x0 - x2)*(x0 - x2) ) / (2*J);
      a[4] -= ( (y2 - y0)*(y0 - y1) + (x0 - x2)*(x1 - x0) ) / (2*J);
      a[5] -= ( (y0 - y1)*(y0 - y1) + (x1 - x0)*(x1 - x0) ) / (2*J);
      
      
      // Insert into global matrix and vector
      LHSmatrix.AddToEntry(a[0], i[0], i[0]);
      LHSmatrix.AddToEntry(a[1], i[0], i[1]);
      LHSmatrix.AddToEntry(a[2], i[0], i[2]);
      LHSmatrix.AddToEntry(a[1], i[1], i[0]);
      LHSmatrix.AddToEntry(a[3], i[1], i[1]);
      LHSmatrix.AddToEntry(a[4], i[1], i[2]);
      LHSmatrix.AddToEntry(a[2], i[2], i[0]);
      LHSmatrix.AddToEntry(a[4], i[2], i[1]);
      LHSmatrix.AddToEntry(a[5], i[2], i[2]);
      
      RHSvector.AddToEntry(Lf[0] + Lg[0], i[0]);
      RHSvector.AddToEntry(Lf[1] + Lg[1], i[1]);
      RHSvector.AddToEntry(Lf[2] + Lg[2], i[2]);
    }
  
}
