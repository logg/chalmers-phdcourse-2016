#include"headers/Matrix.h"
#include"headers/Vector.h"

using namespace std;

// Add a given value to the entry in the given row and col(umn)
void Matrix::AddToEntry(const double value, const unsigned int row, const unsigned int col)
{
  (_rows[row])[col] += value;
}

// Same as above, but several entries at once, in (std::vector)s
void Matrix::AddToEntries(const vector<double> values, const vector<unsigned int> rows, const vector<unsigned int> cols)
{
  for(int i = 0; i < values.size(); i++)
    AddToEntry(values[i], rows[i], cols[i]);
  }

// Matrix-vector multiplication
Vector Matrix::operator*(Vector& v)
{
  vector<double> vals(v.dim(), 0.0);
  for (int i = 0; i < _rows.size(); i++)
    {
      for (auto const &idx_val : (_rows[i])) // idx_val.first is column index, idx_val.second is value at that colum
	{
	  vals[i] += v(idx_val.first)*idx_val.second;
	}
    }
  Vector w((vals));
  return w;
}

// Print the matrix
void Matrix::PrintMe()
{
  for(int i = 0; i < _rows.size(); i++)
    {
      for(int j = 0; j < _rows.size(); j++)
	{
	  std::cout << "\t" << (_rows[i])[j];
	}
      cout << std::endl;
    }
}
