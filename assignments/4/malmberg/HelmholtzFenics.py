from fenics import *
from dolfin import *

# Mesh
time_meshing = Timer("Meshing")
nx = 50
ny = 50
mesh = UnitSquareMesh(nx, ny)
time_meshing.stop()

# Squared wave number
k2 = Expression("2.0 + sin(2*pi*x[0])*sin(2*pi*x[1])", degree = 2)

# Source
f = Expression("(2.0 + sin(2*pi*x[0])*sin(2*pi*x[1]) - 32*pi*pi)*sin(4*pi*x[0])*sin(4*pi*x[1])", degree = 2)

# Boundary condition
g = Expression("4*pi*( (2*x[0] - 1.0)*sin(4*pi*x[1]) + (2*x[1] - 1.0)*sin(4*pi*x[0]) )", degree = 2)

# Function space, test and trial functions
V = FunctionSpace(mesh, "Lagrange", 1)
u = TrialFunction(V)
v = TestFunction(V)

# Bilinear and linear forms
a = -dot(grad(u), grad(v))*dx + k2*u*v*dx
L = f*v*dx - g*v*ds

# Assemble
time_assembly = Timer("Assembling")
A = assemble(a)
b = assemble(L)
time_assembly.stop()


# Solve
U = Vector(mpi_comm_world(), (nx + 1)*(ny + 1))
time_solving = Timer("Solving")
solve(A, U, b, "cg", "amg")
time_solving.stop()

# Write to file
file = File("HelmholtzSolution.pvd")
file << Function(V, U)


list_timings(TimingClear_keep, [TimingType_wall])

