#ifndef __VECTOR_H__
#define __VECTOR_H__

#include<iostream>
#include<vector>

class Vector
{
  
 public:
 Vector(const unsigned int n) : _entries(n, 0.0) {}
 Vector(const std::vector<double> v) : _entries(v) {}
 Vector(const Vector& v) : _entries(v._entries) {}
  
  void AddToEntry(const double value, const unsigned int position);
  unsigned int dim() const;
  double operator()(const unsigned int i) const;
  Vector operator+(const Vector& v) const;
  Vector operator-(const Vector& v) const;
  double operator*(const Vector& v);
  friend Vector operator*(const double c, const Vector& v);
  void PrintMe();
  
 private:
  std::vector<double> _entries;
  
};

#endif
