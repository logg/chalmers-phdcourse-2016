#ifndef __ASSEMBLER_H__
#define __ASSEMBLER_H__

#include"Mesh.h"
#include"Matrix.h"
#include"Vector.h"
#include<vector>

class Assembler
{
 public:
  
 Assembler(const Mesh& mesh) : LHSmatrix(mesh.NumberOfNodes()), RHSvector(mesh.NumberOfNodes()), _mesh(mesh) {}

  void SetQuadratureRule(const unsigned int n);
  void AssembleHelmholtz(double (*k2)(const double*), double (*f)(const double*), double (*g)(const double*), bool (*on_boundary)(const double*));
  Matrix LHSmatrix;
  Vector RHSvector;
  std::vector<double> qp, qw; // quadrature points and weights
  
 private:
  Mesh _mesh;

};

#endif
