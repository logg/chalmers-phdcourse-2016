#ifndef __MATRIX_H__
#define __MATRIX_H__

#include<iostream>
#include<vector>
#include<map>

class Vector;

class Matrix
{
  
 public:
 Matrix(const unsigned int n_rows) : _rows(n_rows, std::map<unsigned int, double>()) {}
 Matrix(const Matrix &M) : _rows(M._rows) {}
  
  void AddToEntry(const double value, const unsigned int row, const unsigned int col);
  void AddToEntries(const std::vector<double> values, const std::vector<unsigned int> rows, const std::vector<unsigned int> cols);  
  Vector operator*(Vector& v);
  void PrintMe();
  
 private:
  
  std::vector<std::map<unsigned int, double> > _rows;
  
};

#endif
