#ifndef __CGSOLVER_H__
#define __CGSOLVER_H__

#include"Vector.h"
#include"Matrix.h"

class CGSolver
{
  
 public:
 CGSolver(const Matrix& A, const Vector& b) : _A(A), _b(b) {}
  
  Vector Solve(const Vector& x0, const double tol);  

 private:
  Matrix _A;
  Vector _b;
};


#endif
