#ifndef __MESH_H__
#define __MESH_H__

#include<stdio.h>
#include<vector>
#include"Vector.h"

class Mesh
{
 public:
 Mesh() : _nodes(1, std::vector<double>(2, 0.0)), _elements(1, std::vector<unsigned int>(3, 0)) {}
 Mesh(const Mesh& mesh) : _nodes(mesh._nodes), _elements(mesh._elements) {}  
 Mesh(const std::vector<std::vector<double> >& nodes, const std::vector<std::vector<unsigned int> >& elements) : _nodes(nodes), _elements(elements) {}
  
  unsigned int NumberOfNodes() const;
  unsigned int NumberOfElements() const;
  void RectangleMesh(const double p0[], const double p1[], const unsigned int nx, const unsigned int ny);
  void WriteToFile(const char name[]);
  void WriteToFile(const char name[], const Vector& u);
  std::vector< std::vector<double> > _nodes;
  std::vector< std::vector<unsigned int> > _elements;
};


#endif
