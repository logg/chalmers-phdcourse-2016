#include<iostream>
#include<stdlib.h>
#include<math.h>
#include<ctime>
#include"headers/Mesh.h"
#include"headers/Assembler.h"
#include"headers/CGSolver.h"

using namespace std;


double SquareWaveNumber(const double *x)
{
  return 2.0 + sin(2*M_PI*x[0])*sin(2*M_PI*x[1]);
}

double Source(const double *x)
{
  return (2.0 + sin(2*M_PI*x[0])*sin(2*M_PI*x[1]) - 32*M_PI*M_PI)*sin(4*M_PI*x[0])*sin(4*M_PI*x[1]);
}

double BoundaryCondition(const double *x)
{
  return 4*M_PI*((2*x[0] - 1.0)*sin(4*M_PI*x[1]) + (2*x[1] - 1.0)*sin(4*M_PI*x[0]));
}

bool approx(double x, double y)
{
  double yes = false;
  double diff = x - y;
  double tol = 1e-6;
  if (-tol < diff && diff < tol)
    yes = true;

  return yes;
}

bool OnBoundary(const double *x)
{
  double on = false;
  if ( approx(x[0], 0.0) || approx(x[0], 1.0) || approx(x[1], 0.0) || approx(x[1], 1.0))
    on = true;
  
  return on;
}

int main()
{
  Mesh mesh;
  double p0[] = {0.0, 0.0}, p1[] = {1.0, 1.0};
  unsigned int nx = 51, ny = 51;
  clock_t start_meshing = clock();
  mesh.RectangleMesh(p0, p1, nx, ny);
  clock_t stop_meshing = clock();
  double time_meshing = double(stop_meshing - start_meshing) / CLOCKS_PER_SEC;
  cout << "Meshing \t" << time_meshing << endl;

  clock_t start_assembling = clock();
  Assembler problem(mesh);
  problem.AssembleHelmholtz(&SquareWaveNumber, &Source, &BoundaryCondition, &OnBoundary);
  clock_t stop_assembling = clock();
  double time_assembling = double(stop_assembling - start_assembling) / CLOCKS_PER_SEC;
  cout << "Assembling \t" << time_assembling << endl; 
  
  CGSolver cgs(problem.LHSmatrix, problem.RHSvector);
  Vector u(nx*ny);
  clock_t start_solving = clock();
  u = cgs.Solve(u, 1e-6);
  clock_t stop_solving = clock();
  double time_solving = double(stop_solving - start_solving) / CLOCKS_PER_SEC;
  cout << "Solving \t" << time_solving << endl;
  mesh.WriteToFile("HelmholtzSolution.inp", u);

  return 0;
}
