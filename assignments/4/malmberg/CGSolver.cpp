#include <math.h>
#include"headers/CGSolver.h"
#include"headers/Vector.h"
#include"headers/Matrix.h"

using namespace std;

// Solve symmetric linear system with the conjugate gradient method, starting at x0, with a residual size tolerance tol 
Vector CGSolver::Solve(const Vector& x0, const double tol)
{
  Vector r0(x0.dim()), r1(x0.dim()), p(x0.dim()), Ap(x0.dim()), x(x0.dim());
  double alpha, beta;
  x  = x0;
  r0 = _b - _A*x;
  p  = r0;
  for (int i = 0; i < x0.dim(); i++)
    {
      Ap    = _A*p;
      alpha = (r0*r0) / (p*Ap);
      x     = x + alpha*p;
      r1    = r0 - alpha*Ap;
      if (sqrt(r1*r1) < tol)
	break;
      beta = (r1*r1) / (r0*r0);
      p    = r1 + beta*p;
      r0   = r1;
    }
  
  return x;
}
