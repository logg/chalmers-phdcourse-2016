#include"headers/Vector.h"

using namespace std;

// Add given value to the entry at given position
void Vector::AddToEntry(const double value, const unsigned int position)
{
  _entries[position] += value;
}

// Return the dimension, the number of entries
unsigned int Vector::dim() const
{
  return _entries.size();
}

// Get entry at given position
double Vector::operator()(const unsigned int i) const
{
  double value = _entries[i];
  return value;
}

// Componentwise addition
Vector Vector::operator+(const Vector& v) const
{
  vector<double> vals(_entries);
  for (int i = 0; i < v.dim(); i++)
    {
      vals[i] += v(i);
    }
  Vector w((vals));
  return w;
}

// Componentwise subtraction
Vector Vector::operator-(const Vector& v) const
{
  vector<double> vals(_entries);
  for (int i = 0; i < v.dim(); i++)
      {
	vals[i] -= v(i);
      }
  Vector w((vals));
  return w;
}

// Scalar product
double Vector::operator*(const Vector& v)
{
  double product = 0.0;
  for (int i = 0; i < v.dim(); i++)
    {
      product += _entries[i]*v(i);
    }
  return product;
}

// Scale vector by constant
Vector operator*(const double c, const Vector& v)
{
  vector<double> vals(v._entries);
  for (int i = 0; i < v.dim(); i++)
    {
	vals[i] *= c;
    }
  Vector w((vals));
  return w;
}

// Print out the vector
void Vector::PrintMe()
{
  for(int i = 0; i < dim(); i++)
    cout << "\t" << _entries[i] << endl;
}


