#include"headers/Mesh.h"

using namespace std;

// Get the number of nodes
unsigned int Mesh::NumberOfNodes() const
{
  unsigned int nno = _nodes.size();
  return nno;
}

// Get the number of elements
unsigned int Mesh::NumberOfElements() const
{
  unsigned int nel = _elements.size();
  return nel;
}

// Create a uniform mesh in a rectangle with corners p0 (lower left) and p1 (upper right), using nx times ny nodes
void Mesh::RectangleMesh(const double p0[], const double p1[], const unsigned int nx, const unsigned int ny)
{
  _nodes.resize(nx*ny, vector<double>(2, 0.0));
  _elements.resize(2*(nx - 1)*(ny - 1), vector<unsigned int>(3, 0));
  
  double dx = (p1[0] - p0[0])/(nx - 1);
  double dy = (p1[1] - p0[1])/(ny - 1);
  
  unsigned int basenode;
  
  for (int n = 0; n < NumberOfNodes(); n++)
    {
      _nodes[n][0] = p0[0] + (n % nx)*dx;
      _nodes[n][1] = p0[1] + (n / nx)*dy;
    }
  
  for (int n = 0; n < NumberOfElements()/2; n++)
    {
      basenode = (n % (nx - 1)) + (n / (ny - 1))*nx;
      
      _elements[2*n][0] = basenode;
      _elements[2*n][1] = basenode + 1;
      _elements[2*n][2] = basenode + nx;
      
      _elements[2*n + 1][0] = basenode + 1;
      _elements[2*n + 1][1] = basenode + nx + 1;
      _elements[2*n + 1][2] = basenode + nx;
    }
  
}

// Print the mesh in *.inp format
void Mesh::WriteToFile(const char name[])
{
  FILE* meshfile = fopen(name, "w");
  
  fprintf(meshfile, "%i %i %i %i %i \n", NumberOfNodes(), NumberOfElements(), NumberOfNodes(), 0, 0);
  for(int n = 0; n < _nodes.size(); n++)
    fprintf(meshfile, "%i %f %f %f \n", n, _nodes[n][0], _nodes[n][1], 0.0);
  
  for(int n = 0; n < _elements.size(); n++)
    fprintf(meshfile, "%i %i %s %i %i %i \n", n, 0, "tri", _elements[n][0], _elements[n][1], _elements[n][2]);
  
  fprintf(meshfile, "%i %i \n %s, %s \n", 1, 1, "u", "--");
  for(int n = 0; n < _nodes.size(); n++)
    fprintf(meshfile, "%i %f \n", n, 0.0);
  
  fclose(meshfile);
}

// Print function with nodal values u in *.inp format
void Mesh::WriteToFile(const char name[], const Vector& u)
{
  FILE* meshfile = fopen(name, "w");
  
  fprintf(meshfile, "%i %i %i %i %i \n", NumberOfNodes(), NumberOfElements(), NumberOfNodes(), 0, 0);
  for(int n = 0; n < _nodes.size(); n++)
    fprintf(meshfile, "%i %f %f %f \n", n, _nodes[n][0], _nodes[n][1], 0.0);
  
  for(int n = 0; n < _elements.size(); n++)
    fprintf(meshfile, "%i %i %s %i %i %i \n", n, 0, "tri", _elements[n][0], _elements[n][1], _elements[n][2]);
  
  fprintf(meshfile, "%i %i \n %s, %s \n", 1, 1, "u", "--");
  for(int n = 0; n < _nodes.size(); n++)
    fprintf(meshfile, "%i %f \n", n, u(n));
  
  fclose(meshfile);
}
