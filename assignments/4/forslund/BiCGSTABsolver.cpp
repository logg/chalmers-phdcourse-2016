#include "BiCGSTABsolver.h"

// Solve linear system
Vector<double> BiCGSTABsolver::solve(Matrix<double>& A, Vector<double>& b,
                                     const Vector<unsigned int>& boundary, const Vector<double>& dirValues,
                                     double tol)
{
    Vector<double> x(b.size());

    for(int i = 0; i < boundary.size(); ++i)
    {
        A.replaceRow(boundary[i]);
        b[boundary[i]] = dirValues[i];
    }

    Vector<double> r(b.size());
    Vector<double> rHat_0(b.size());
    Vector<double> x_0(b.size());
    Vector<double> v(b.size());
    Vector<double> p(b.size());
    Vector<double> h(b.size());
    Vector<double> s(b.size());
    Vector<double> t(b.size());
    double rho_0, rho_1, alpha, omega, beta;

    r = b - A*x_0;
    rHat_0 = r;

    rho_0 = 1;
    alpha = 1;
    omega = 1;

    for(unsigned int i = 0; i < 100; ++i)
    {
        rho_1 = rHat_0*r;
        beta = rho_1/rho_0*(alpha/omega);
        p = r + (p*beta - v*omega*beta);
        v = A*p;
        alpha = rho_1/(rHat_0*v);
        h = x_0 + p*alpha;

        if (std::sqrt((A*h - b)*(A*h - b)) < tol)
        {
            x = h;
            break;
        }

        s = r - v*alpha;
        t = A*s;
        omega = (t*s)/(t*t);
        x = h + s*omega;

        if (std::sqrt((A*x - b)*(A*x - b)) < tol)
        {
            break;
        }

        r = s - t*omega;

        rho_0 = rho_1;
        x_0 = x;
    }

    return x;
}
