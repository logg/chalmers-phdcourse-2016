#include "Mesh.h"

#include <cmath>

// Constructors
Mesh::Mesh()
{
    // Do nothing
}

Mesh::Mesh(double x0, double x1, double y0, double y1, int nx, int ny)
{
    double lx = (x1 - x0)/double(nx);
    double ly = (y1 - y0)/double(ny);

    _meshSize = std::sqrt(lx*lx + ly*ly);

    _vertices.resize( (nx + 1)*(ny + 1), Vector<double>(2) );
    _edges.resize( (nx + ny)*2, Vector<unsigned int>(2) );
    _triangles.resize( 2*nx*ny, Vector<unsigned int>(3) );
    _connections.resize( (nx + 1)*(ny + 1), Vector<unsigned int>(0) );

    // Fill vertices, connections, and boundary
    for (int i = 0; i < ny + 1; ++i)
    {
        for (int j = 0; j < nx + 1; ++j)
        {
            _vertices[(nx + 1)*i + j][0] = j*lx;
            _vertices[(nx + 1)*i + j][1] = i*ly;

            if (i != 0 && j != 0)
            {
                _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j  - (nx + 1) - 1);
            }
            if (i != 0)
            {
                _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j  - (nx + 1));
            }
            if (j != 0)
            {
                _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j  - 1);
            }

            _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j );

            if (j != nx)
            {
                _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j  + 1);
            }
            if (i != ny)
            {
                _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j  + (nx + 1));
            }
            if (i != ny && j != nx)
            {
                _connections[(nx + 1)*i + j].push_back((nx + 1)*i + j  + (nx + 1) + 1);
            }


            if (i == 0 || i == ny)
            {
                _boundary.push_back((nx + 1)*i + j);
            }
            else if(j == 0 || j == nx)
            {
                _boundary.push_back((nx + 1)*i + j);
            }
        }
    }

    // Fill edges
    for (int j = 0; j < nx; ++j)
    {
        _edges[j][0] = j;
        _edges[j][1] = j + 1;

        _edges[j + nx + ny][0] = _vertices.size() - 1 - j;
        _edges[j + nx + ny][1] = _vertices.size() - 1 - (j + 1);
    }
    for (int i = 0; i < ny; ++i)
    {
        _edges[i + nx][0] = (nx + 1)*(i + 1) - 1;
        _edges[i + nx][1] = (nx + 1)*(i + 2) - 1;

        _edges[i + 2*nx + ny][0] = _vertices.size() - nx - (nx + 1)*i - 1;
        _edges[i + 2*nx + ny][1] = _vertices.size() - nx - (nx + 1)*(i + 1) - 1;
    }

    // Fill triangles
    for (int i = 0; i < ny; ++i)
    {
        for (int j = 0; j < nx; ++j)
        {
            _triangles[2*(nx*i + j)][0] = ((nx + 1)*i) + j;
            _triangles[2*(nx*i + j)][1] = ((nx + 1)*i) + j + 1;
            _triangles[2*(nx*i + j)][2] = ((nx + 1)*i) + (nx + 1) + j + 1;

            _triangles[2*(nx*i + j) + 1][0] = ((nx + 1)*i) + j;
            _triangles[2*(nx*i + j) + 1][1] = ((nx + 1)*i) + (nx + 1) + j + 1;
            _triangles[2*(nx*i + j) + 1][2] = ((nx + 1)*i) + (nx + 1) + j;
        }
    }
}

Mesh::Mesh(const Mesh& mesh)
{
    _vertices = mesh._vertices;
    _edges = mesh._edges;
    _triangles = mesh._triangles;
    _connections = mesh._connections;
    _boundary = mesh._boundary;
    _meshSize = mesh._meshSize;
}

// Destructor
Mesh::~Mesh()
{
    // Do nothing
}

// Get some mesh data
unsigned int Mesh::nDofs() const
{
    return _vertices.size();
}

unsigned int Mesh::nTriangles() const
{
    return _triangles.size();
}

unsigned int Mesh::nConnections() const
{
    unsigned int nConnections = 0;
    for(unsigned int i = 0; i < _connections.size(); ++i)
    {
        nConnections += _connections[i].size();
    }

    return nConnections;
}


// Compute areas of triangles using Gauss's area formula
Vector<double> Mesh::triangleAreas() const
{
    double x1, y1, x2, y2, x3, y3;
    Vector<double> triangleAreas(_triangles.size());
    for(int i = 0; i < _triangles.size(); ++i)
    {
        x1 = _vertices[_triangles[i][0]][0];
        y1 = _vertices[_triangles[i][0]][1];
        x2 = _vertices[_triangles[i][1]][0];
        y2 = _vertices[_triangles[i][1]][1];
        x3 = _vertices[_triangles[i][2]][0];
        y3 = _vertices[_triangles[i][2]][1];

        triangleAreas[i] = 0.5*std::abs(x1*y2 + x2*y3 + x3*y1 - x2*y1 - x3*y2 - x1*y3);
    }
    return triangleAreas;
}

// Obtain the dofs that are connected to a given dof n
Vector<unsigned int> Mesh::connectedTo(unsigned int n) const
{
    return _connections[n];
}

// Print fields
void Mesh::printVertices() const
{
    for (unsigned int i = 0; i < _vertices.size(); ++i)
    {
        for (unsigned int j = 0; j < 2; ++j)
        {
            std::cout.width(4);
            std::cout << _vertices[i][j] << "    ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Mesh::printEdges() const
{
    for (unsigned int i = 0; i < _edges.size(); ++i)
    {
        for (unsigned int j = 0; j < 2; ++j)
        {
            std::cout.width(4);
            std::cout << _edges[i][j] << "    ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Mesh::printTriangles() const
{
    for (unsigned int i = 0; i < _triangles.size(); ++i)
    {
        for (unsigned int j = 0; j < 3; ++j)
        {
            std::cout.width(4);
            std::cout << _triangles[i][j] << "    ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Mesh::printConnections() const
{
    for (unsigned int i = 0; i < _connections.size(); ++i)
    {
        for (unsigned int j = 0; j < _connections[i].size(); ++j)
        {
            std::cout.width(4);
            std::cout << _connections[i][j] << "    ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Mesh::printBoundary() const
{
    for (unsigned int i = 0; i < _boundary.size(); ++i)
    {
        std::cout.width(4);
        std::cout << _boundary[i] << "    ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// Get fields
std::vector< Vector<double> > Mesh::vertices() const
{
    return _vertices;
}

std::vector< Vector<unsigned int> > Mesh::edges() const
{
    return _edges;
}

std::vector< Vector<unsigned int> > Mesh::triangles() const
{
    return _triangles;
}

std::vector< Vector<unsigned int> > Mesh::connections() const
{
    return _connections;
}

Vector<unsigned int> Mesh::boundary() const
{
    return _boundary;
}

double Mesh::meshSize() const
{
    return _meshSize;
}

// Operator Overloads
Mesh& Mesh::operator=(const Mesh& meshrhs)
{
    _vertices = meshrhs._vertices;
    _edges = meshrhs._edges;
    _triangles = meshrhs._triangles;
    _connections = meshrhs._connections;

    return *this;
}
