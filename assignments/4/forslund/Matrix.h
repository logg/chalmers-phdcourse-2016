#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED

#include "Mesh.h"
#include "Vector.h"

// This class uses compressed row storage (CRS) format.

template<class T>
class Matrix
{
private:
    Vector<T> _val; // values of non-zero elements
    Vector<unsigned int> _colInd; // column indices of elements in val
    Vector<unsigned int> _rowPtr; // positions in val that start a row;
    // rowPtr[i+1] - rowPtr[i] = no. non-zero elements in row i

public:
    // Constructors
    Matrix();
    Matrix(std::vector<T> val,
           std::vector<unsigned int> colInd,
           std::vector<unsigned int> rowPtr);
    Matrix(Vector<T> val,
           Vector<unsigned int> colInd,
           Vector<unsigned int> rowPtr);
    Matrix(const Matrix& mat);

    // Destructor
    ~Matrix();

    // Print fields
    void printVal() const;
    void printColInd() const;
    void printRowPtr() const;

    // Replace row row with a vector [0, ..., 0, 1, 0, ..., 0]
    // (i.e. enforce Dirichlet BC on dof row).
    void replaceRow(int row);

    // Operator Overloads
    Matrix<T>& operator=(const Matrix<T>& matrhs);

    Matrix<T> operator+(const Matrix<T>& mat) const;// element wise addition

    Vector<T> operator*(const Vector<T>& vec) const; // matrix-vector multiplication
};

#endif // MATRIX_H_INCLUDED
