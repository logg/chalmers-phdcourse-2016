#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include<vector>
#include<iostream>

template<class T>
class Vector
{
private:
    std::vector<T> _elems; // elements in vector

public:
    // Constructors
    Vector<T>();
    Vector<T>(unsigned int n);
    Vector<T>(const std::vector<T>& vec);
    Vector<T>(const Vector<T>& vec);

    // Destructor
    ~Vector<T>();

    // Useful functions from std::vector
    unsigned int size() const;
    void resize(unsigned int n);
    void push_back(const T& t);

    typedef typename std::vector<T>::iterator iterator;
    iterator erase(iterator position)
    {
        return _elems.erase(position);
    }
    iterator begin()
    {
        return _elems.begin();
    }

    // Operator overloads
    T& operator[](unsigned int i); // to get access to elements in vector
    const T& operator[](unsigned int i) const;

    Vector<T>& operator=(const Vector<T>& vecrhs);
    Vector<T>& operator=(const std::vector<T>& vecrhs);

    Vector<T>& operator+=(const Vector<T>& vec);
    Vector<T>& operator-=(const Vector<T>& vec);

    Vector<T> operator+(const Vector<T>& vec) const; // element wise addition
    Vector<T> operator-(const Vector<T>& vec) const; // element wise subtraction

    T operator*(const Vector<T>& vec) const; // inner product
    Vector<T> operator*(const double& prop) const; // multiplication with scalar

    // Overload << globally (since we can't modify the class ostream).
    // Need to define the friend function in header. For some reason this is the only
    // suggestion of the three given in http://stackoverflow.com/a/4661372 that works.
    friend std::ostream& operator<<(std::ostream& os, const Vector<T>& vec)
    {
        for(unsigned int i = 0; i < vec.size(); ++i)
        {
            os << vec._elems[i] << " ";
        }
        return os;
    }
};

#endif // VECTOR_H_INCLUDED
