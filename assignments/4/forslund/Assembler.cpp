#include "Assembler.h"

// Get the assembled matrix in CRS format
Matrix<double> Assembler::tableToMatrix(const std::map< std::vector<unsigned int>, double >& table) const
{
    std::vector<double> val;
    std::vector<unsigned int> colInd;
    std::vector<unsigned int> rowPtr(1,0);
    unsigned int iPrev = 0;
    std::vector <unsigned int> inVect;

    for(std::map< std::vector<unsigned int>, double >::const_iterator ii = table.begin(); ii!= table.end(); ++ii)
    {
        inVect = (*ii).first;

        while (iPrev < inVect[0])
        {
            rowPtr.push_back(val.size());
            iPrev +=1;
        }
        if ((*ii).second > 1e-14 || (*ii).second < -1e-14) // the table may contain zeros, don't include them
        {
            val.push_back((*ii).second);
            colInd.push_back(inVect[1]);
        }
    }

    rowPtr.push_back(val.size());
    Matrix<double> mat(val, colInd, rowPtr);
    return mat;
}

// Assembly methods
void Assembler::assembleStiffness(Matrix<double>& A, const Mesh& mesh)
{
    std::map< std::vector<unsigned int>, double > stiffnessTable;

    std::vector<double> b(3,0);
    std::vector<double> c(3,0);
    std::vector< Vector<double> > vertices = mesh.vertices();
    std::vector< Vector<unsigned int> > triangles = mesh.triangles();
    Vector<double> triAreas = mesh.triangleAreas();

    for(int k = 0; k < mesh.nTriangles(); k++)
    {
        b[0] = (vertices[triangles[k][1]][1] -
                vertices[triangles[k][2]][1])/2.0/triAreas[k];
        b[1] = (vertices[triangles[k][2]][1] -
                vertices[triangles[k][0]][1])/2.0/triAreas[k];
        b[2] = (vertices[triangles[k][0]][1] -
                vertices[triangles[k][1]][1])/2.0/triAreas[k];

        c[0] = (vertices[triangles[k][1]][0] -
                vertices[triangles[k][2]][0])/2.0/triAreas[k];
        c[1] = (vertices[triangles[k][2]][0] -
                vertices[triangles[k][0]][0])/2.0/triAreas[k];
        c[2] = (vertices[triangles[k][0]][0] -
                vertices[triangles[k][1]][0])/2.0/triAreas[k];

        for(unsigned int i = 0; i < 3; ++i)
        {
            for(unsigned int j = 0; j < 3; ++j)
            {
                stiffnessTable[ {triangles[k][i], triangles[k][j]} ] += (b[i]*b[j] + c[i]*c[j])*triAreas[k];
            }
        }
    }

    Matrix<double> stiffnessMatrix;
    A = tableToMatrix(stiffnessTable);
}

void Assembler::assembleSource(Vector<double>& b, const Mesh& mesh, const Vector<double>& source)
{
    std::map< std::vector<unsigned int>, double > massTable;

    std::vector< Vector<unsigned int> > triangles = mesh.triangles();
    Vector<double> triAreas = mesh.triangleAreas();

    for(int k = 0; k < mesh.nTriangles(); k++)
    {
        for(unsigned int i = 0; i < 3; ++i)
        {
            for(unsigned int j = 0; j < 3; ++j)
            {
                massTable[ {triangles[k][i], triangles[k][j]} ] += (1.0 + (i==j))*triAreas[k]/12.0;

            }
        }
    }

    b = tableToMatrix(massTable)*source;
}
