#ifndef MESH_H_INCLUDED
#define MESH_H_INCLUDED

#include "Vector.h"

class Mesh
{
private:
    std::vector< Vector<double> > _vertices;
    std::vector< Vector<unsigned int> > _edges;
    std::vector< Vector<unsigned int> > _triangles;
    std::vector< Vector<unsigned int> > _connections;
    Vector<unsigned int> _boundary;

    double _meshSize;

public:
    // Constructors
    Mesh();
    Mesh(double x0, double x1, double y0, double y1, int nx, int ny);
    Mesh(const Mesh& mesh);

    // Destructor
    ~Mesh();

    // Get some mesh data
    unsigned int nDofs() const;
    unsigned int nTriangles() const;
    unsigned int nConnections() const; // sum of all connections for all dofs

    Vector<double> triangleAreas() const;

    // Obtain the dofs that are connected to a given dof n
    Vector<unsigned int> connectedTo(unsigned int n) const;

    // Print fields
    void printVertices() const;
    void printEdges() const;
    void printBoundary() const;
    void printTriangles() const;
    void printConnections() const;

    // Get fields
    std::vector< Vector<double> > vertices() const;
    std::vector< Vector<unsigned int> > edges() const;
    std::vector< Vector<unsigned int> > triangles() const;
    std::vector< Vector<unsigned int> > connections() const;
    Vector<unsigned int> boundary() const;
    double meshSize() const;

    // Operator Overloads
    Mesh& operator=(const Mesh& meshrhs);
};

#endif // MESH_H_INCLUDED
