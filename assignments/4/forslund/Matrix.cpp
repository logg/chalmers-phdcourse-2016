#include "Matrix.h"

// Constructors
template<class T>
Matrix<T>::Matrix()
{
    // Do nothing
}

template<class T>
Matrix<T>::Matrix(std::vector<T> val,
                  std::vector<unsigned int> colInd,
                  std::vector<unsigned int> rowPtr)
{
    _val = val;
    _colInd = colInd;
    _rowPtr = rowPtr;
}

template<class T>
Matrix<T>::Matrix(Vector<T> val,
                  Vector<unsigned int> colInd,
                  Vector<unsigned int> rowPtr)
{
    _val = val;
    _colInd = colInd;
    _rowPtr = rowPtr;
}

template<class T>
Matrix<T>::Matrix(const Matrix& mat)
{
    _val = mat._val;
    _colInd = mat._colInd;
    _rowPtr = mat._rowPtr;
}

// Destructor
template<class T>
Matrix<T>::~Matrix()
{
    // Do nothing
}

// Print fields
template<class T>
void Matrix<T>::printVal() const
{
    for (unsigned int i = 0; i < _val.size(); ++i)
    {
        std::cout.width(10);
        std::cout << _val[i];
    }
    std::cout << std::endl;
}

template<class T>
void Matrix<T>::printColInd() const
{
    for (unsigned int i = 0; i < _colInd.size(); ++i)
    {
        std::cout.width(10);
        std::cout << _colInd[i];
    }
    std::cout << std::endl;
}

template<class T>
void Matrix<T>::printRowPtr() const
{
    for (unsigned int i = 0; i < _rowPtr.size(); ++i)
    {
        std::cout.width(10);
        std::cout << _rowPtr[i];
    }
    std::cout << std::endl;
}

// Replace row row with a vector [0, ..., 0, 1, 0, ..., 0]
template<class T>
void Matrix<T>::replaceRow(int row)
{
    unsigned int start = _rowPtr[row];
    for (unsigned int i = start; i < _rowPtr[row+1]; ++i)
    {
        if (_colInd[i] == row)
        {
            _val[i] = 1.0;
        }
        else
        {
            _val[i] = 0.0;
        }
    }
}

// Overload operators
template<class T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& matrhs)
{
    _val = matrhs._val;
    _colInd = matrhs._colInd;
    _rowPtr = matrhs._rowPtr;

    return *this;
}

template<class T>
Matrix<T> Matrix<T>::operator+(const Matrix& mat) const
{
    Matrix<T> matlhs(*this);
    for(int i = 0; i < _val.size(); ++i)
    {
        matlhs._val[i] += mat._val[i];
    }
    return matlhs;
}

template<class T>
Vector<T> Matrix<T>::operator*(const Vector<T>& vec) const
{
    Vector<T> veclhs(vec.size());
    for(unsigned int i = 0; i < vec.size(); ++i)
    {
        for(unsigned int j = _rowPtr[i]; j < _rowPtr[i+1]; ++j)
        {
            veclhs[i] += _val[j]*vec[_colInd[j]];
        }
    }

    return veclhs;
}

// See http://stackoverflow.com/a/8752879
template class Matrix<int>;
template class Matrix<unsigned int>;
template class Matrix<double>;
