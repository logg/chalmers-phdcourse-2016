#ifndef ASSEMBLER_H_INCLUDED
#define ASSEMBLER_H_INCLUDED

#include "Matrix.h"
#include "Vector.h"
#include "Mesh.h"

#include<map>

// This class assembles matrices in row-col-value format. When the
// assembly is complete, the corresponding CRS representation of the matrix
// is obtained.

class Assembler
{
private:
    // Get the CRS representation
    Matrix<double> tableToMatrix(const std::map< std::vector<unsigned int>, double >&  table) const;

public:
    // Assemble functions
    void assembleStiffness(Matrix<double>& A, const Mesh& mesh);
    void assembleSource(Vector<double>& b, const Mesh& mesh, const Vector<double>& source);
};

#endif // ASSEMBLER_H_INCLUDED
