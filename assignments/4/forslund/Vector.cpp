#include "Vector.h"

// Constructors
template<class T>
Vector<T>::Vector()
{
    // Do nothing
}

template<class T>
Vector<T>::Vector(unsigned int n)
{
    _elems.resize(n);
}

template<class T>
Vector<T>::Vector(const std::vector<T>& vec)
{
    _elems = vec;
}

template<class T>
Vector<T>::Vector(const Vector<T>& vec)
{
    _elems = vec._elems;
}

// Destructor
template<class T>
Vector<T>::~Vector()
{
    // Do nothing
}

// Useful functions from std::vector
template<class T>
unsigned int Vector<T>::size() const
{
    return _elems.size();
}

template<class T>
void Vector<T>::resize(unsigned int n)
{
    _elems.resize(n);
}

template<class T>
void Vector<T>::push_back(const T& t)
{
    _elems.push_back(t);

}

// Operator overloads
template<class T>
T& Vector<T>::operator[](unsigned int i)
{
    return _elems[i];
}
template<class T>
const T& Vector<T>::operator[](unsigned int i) const
{
    return _elems[i];
}

template<class T>
Vector<T>& Vector<T>::operator=(const Vector<T>& vecrhs)
{
    _elems = vecrhs._elems;

    return *this;
}
template<class T>
Vector<T>& Vector<T>::operator=(const std::vector<T>& vecrhs)
{
    _elems = vecrhs;

    return *this;
}

template<class T>
Vector<T>& Vector<T>::operator+=(const Vector<T>& vecrhs)
{
    for(unsigned int i=0; i<vecrhs.size(); ++i)
    {
        _elems[i] += vecrhs._elems[i];
    }

    return *this;
}

template<class T>
Vector<T>& Vector<T>:: operator-=(const Vector<T>& vecrhs)
{
    for(unsigned int i=0; i<vecrhs.size(); ++i)
    {
        _elems[i] -= vecrhs._elems[i];
    }

    return *this;
}

template<class T>
Vector<T> Vector<T>::operator+(const Vector<T>& vec) const
{
    Vector<T> veclhs(*this);
    for(unsigned int i=0; i<vec.size(); ++i)
    {
        veclhs[i] += vec[i];
    }

    return veclhs;
}

template<class T>
Vector<T> Vector<T>::operator-(const Vector<T>& vec) const
{
    Vector<T> veclhs(*this);
    for(unsigned int i=0; i<vec.size(); ++i)
    {
        veclhs[i] -= vec[i];
    }

    return veclhs;
}

template<class T>
T Vector<T>::operator*(const Vector<T>& vec) const
{
    T scalarProduct = 0;
    for(unsigned int i = 0; i < vec.size(); ++i)
    {
        scalarProduct += (*this)[i] * vec[i];
    }
    return scalarProduct;
}

template<class T>
Vector<T> Vector<T>::operator*(const double& prop) const
{
    Vector<T> veclhs(*this);
    for(unsigned int i = 0; i < veclhs.size(); ++i)
    {
        veclhs[i] = veclhs[i]*prop;
    }
    return veclhs;
}

// See http://stackoverflow.com/a/8752879
template class Vector<int>;
template class Vector<unsigned int>;
template class Vector<double>;
