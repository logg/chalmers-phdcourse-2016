#ifndef BICGSTABSOLVER_H_INCLUDED
#define BICGSTABSOLVER_H_INCLUDED

#include "Matrix.h"
#include "Vector.h"
#include "Mesh.h"

#include <cmath>

// The biconjugate gradient stabilized method does not require that A is symmetric.

class BiCGSTABsolver
{
public:
    // Solve linear system as described in
    // https://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method
    Vector<double> solve(Matrix<double>& A, Vector<double>& b,
                         const Vector<unsigned int>& boundary, const Vector<double>& dirValues,
                         double tol);
};

#endif // BICGSTABSOLVER_H_INCLUDED
