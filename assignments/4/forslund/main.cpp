#include "Mesh.h"
#include "Vector.h"
#include "Matrix.h"
#include "Assembler.h"
#include "BiCGSTABsolver.h"

#include<cstdio>
#include<fstream>
#include<chrono>

#define PI 3.1415926535897932384

// Solve Poisson's equation with inhomogeneous Dirichlet BC,
// - div grad u(x, y) = f(x, y) in Omega
//            u(x, y) = g(x, y) on Gamma

// Set f here. The function returns the linear interpolant of f onto the mesh
// (or rather, the nodal values of f).
void sourceFcn(Vector<double>& sourceNodalValues, const std::vector< Vector<double> >& vertices)
{
    sourceNodalValues.resize(vertices.size());
    double x, y;
    for(unsigned int i = 0; i < vertices.size(); ++i)
    {
        x = vertices[i][0];
        y = vertices[i][1];

        sourceNodalValues[i] = std::sin(2.0*PI*x)*std::sin(2.0*PI*y);
    }
}

// Set g here. The function returns the linear interpolant of g onto the mesh.
void dirichletBdryFcn(Vector<double>& dirNodalValues, const std::vector< Vector<double> >& vertices,
                      const Vector<unsigned int>& boundary)
{
    dirNodalValues.resize(boundary.size());
    double x, y;
    for(unsigned int i = 0; i < boundary.size(); ++i)
    {
        double x = vertices[boundary[i]][0];
        double y = vertices[boundary[i]][1];

        dirNodalValues[i] = 0.05*x*x*y*y;
    }
}

int main()
{
    Mesh mesh(0.0, 1.0, 0.0, 1.0, 50, 50);

    Vector<double> sourceValues, dirValues;
    sourceFcn(sourceValues, mesh.vertices());
    dirichletBdryFcn(dirValues, mesh.vertices(), mesh.boundary());

    Matrix<double> A; // stiffness matrix
    Vector<double> b; // load vector
    Assembler a;
    a.assembleStiffness(A, mesh);
    a.assembleSource(b, mesh, sourceValues);

    Vector<double> u;
    BiCGSTABsolver solver;
    u = solver.solve(A, b, mesh.boundary(), dirValues, 1e-10);

//    FILE* uFile;
//    uFile = fopen( "u.txt", "w" );
//    for(unsigned int i = 0; i < u.size(); ++i)
//    {
//        fprintf(uFile, "%d %f %f %f \n", i, mesh.vertices()[i][0], mesh.vertices()[i][1], u[i]);
//    }
//    fclose(uFile);
}
