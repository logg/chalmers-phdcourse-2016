// Compiles with g++ -std=c++0x Poisson_replication.cpp -o poisson
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <vector>
#include <ufc.h>
#include <chrono>
using namespace std;
class MeshGeometry
// Vector of node coordinates
{
public:
	MeshGeometry(vector<vector<double>> cc)
	: coordinates{cc}
	{
	}
	
	vector<double> get_coordinate(int i) const{
		return coordinates[i];
	};
private:
	vector<vector<double>> coordinates;
};

class MeshConnectivity
// Connections for one node
{
public:
	MeshConnectivity(vector<int> cc)
	: _connections{cc}
	{
	}
	vector<int> _connections;
};

class MeshTopology
// Vector of connections
{
public:
	MeshTopology(vector<MeshConnectivity> cc)
	: connectivity{cc}
	{
	}
	
	bool is_connected(int i, int j) const{
		bool result = 0;
		for(int k = 0; k < connectivity[i]._connections.size(); k++){
			if(connectivity[i]._connections[k] == j){result = 1;};
		}
		return result;
	}
private:
	vector<MeshConnectivity> connectivity;
};

class Mesh 
// Triangular 2D mesh
{
public:
	Mesh(MeshTopology tt, MeshGeometry gg, int n)
	: _topology{tt}, _geometry{gg}, N{n}
	{
	}
	
	vector<vector<int>> get_cells() const {
		vector<vector<int>> result; 
		int tot=0;
		for(int i=0; i < N-2; i++){
			for(int j=i+1; j < N-1; j++){
				for(int k=j+1; k < N; k++){
					vector<int> v;
					v.resize(3);
					v[0]=i; v[1] = j; v[2] = k;
					if(is_cell(v)){
						result.push_back(v);
				  }     
				}
			}
		}
		return result;
	};
	
	bool is_cell(vector<int> v) const {
		return(_topology.is_connected(v[0], v[1]) && _topology.is_connected(v[1], v[2]) && _topology.is_connected(v[2], v[0]));
	};

	vector<double> get_coordinate(int node_label) const {
		return _geometry.get_coordinate(node_label);
	};
	
	int number_of_nodes() const{
		return N;
	};
private:
	MeshTopology _topology;
	MeshGeometry _geometry;
	int N; // number of nodes
};

Mesh UnitSquareMesh(int M, int N){
// Constructs a Mesh on the unit square, equal number of noces in each direction
	vector<vector<double>> coordinates;
	vector<MeshConnectivity> connectivity;
	int node_label = 0;
	// Loop through nodes:
	for(int i=0; i<M; i++){ // Rows, starting from origin moving up
		for(int j=0; j<N; j++){ // Cols, moving right
			// Add node coordinate to geometry: 
			vector<double> coordinate {double(j)/(N-1),double(i)/(M-1)};
			coordinates.push_back(coordinate);
			// Add connections to node
			vector<int> cc;
			if(node_label-(N+1) >= 0 && node_label-(N+1) < M*N){
				cc.push_back(node_label-(N+1));
			};			
			if(node_label-(N) >= 0 && node_label-(N) < M*N){
				cc.push_back(node_label-(N));
			};		
			if(node_label-1 >= 0 && node_label-1 < M*N && node_label%N != 0){
				cc.push_back(node_label-1);
			};
			if(node_label+(N+1) >= 0 && node_label+(N+1) < M*N){
				cc.push_back(node_label+(N+1));
			};			
			if(node_label+(N) >= 0 && node_label+(N) < M*N){
				cc.push_back(node_label+(N));
			};		
			if(node_label+1 >= 0 && node_label+1 < M*N && node_label%N != N-1){
				cc.push_back(node_label+1);
			};
			MeshConnectivity connections {cc};
			connectivity.push_back(connections);
			node_label+=1;
		};
	};
	MeshTopology top {connectivity};
	MeshGeometry geom {coordinates};
	Mesh result {top,geom, M*N};
	return result;
};

class Vector
// Sparse vector
{
public:
	Vector(int nr)
	: num_rows{nr}
	{
	}

	void print(){
		if(data.empty()){
			int j = 0;
			while(j<num_rows) {
				cout << 0 << ' ';
				j+=1;	
			}
		} else{
			int k=0; 
			for(int i = 0; i < num_rows; i++){
				if(k < rows.size() && rows[k]==i){ 
					cout << data[k] << ' ';
					k+=1;
				} else{
					cout << 0 << ' ';
				}
			}
		}
	};

	void insert(double d, int i){
		// add nze at back if empty or last row
		if(data.empty() || i > rows.back() ){ 
			data.push_back(d);
			rows.push_back(i);
		} else{
			auto it_data = data.begin();
			auto it_rows = rows.begin();
			int jj = 0; // iterator for nze
			while(true){
				if(i < rows[jj]){ // insert before an nze	
					data.insert(it_data+jj,d);
					rows.insert(it_rows+jj,i);
					break;
				} else if(i == rows[jj]){ // insert at existing nze
					data[jj] = d;
					break;
				}
				jj += 1; // move to next nze
			}
		}
	};

	double get_element(int i) const {
		int j=0;
		if(rows.empty()){
			return 0;
		}
		while(rows[j] <= i){
			if(rows[j]==i){
				return data[j];
			}
			j += 1;
		}
		return 0;
	};
	
	vector<double> get_data() const { return data; }
	vector<int> get_rows() const { return rows; }
	int get_num_rows() const { return num_rows; }
	double sum() const {
		double s=0;
		for(int j=0; j<data.size(); j++){
			s+=data[j];
		}
	};
private:
	vector<double> data;
	vector<int> rows;
	int num_rows;
};

// Helper functions for Vector:
double inner_product(const Vector &v, const Vector &w){
	vector<int> vrows = v.get_rows(); 
	vector<int> wrows = w.get_rows();
	if(vrows.empty() || wrows.empty()){ // If either vector is empty return 0
		return 0;
	}
	int vnr = vrows.size(); 
	int wnr = wrows.size();
	vector<double> vdat = v.get_data(); 
	vector<double> wdat = w.get_data();
	int j = 0; 
	int k = 0; 
	double result = 0;
	while(j < vnr && k < wnr ){ // Loop through common rows
			if(vrows[j]<wrows[k]){
				j+=1;
			}else if(vrows[j]==wrows[k]){
				result += vdat[j]*wdat[k];
				j+=1;
			} else{
				k+=1;
			}
	}
	return result;	
}

Vector operator+(const Vector &v, const Vector &w){
	vector<int> vrows = v.get_rows(); 
	vector<int> wrows = w.get_rows();
	if(vrows.empty()){
		return w;
	} else if(wrows.empty()){
		return v;
	}
	vector<double> wdat = w.get_data();
	Vector result = v;
	for(int j = 0; j < wrows.size(); j++){
		result.insert(wdat[j]+v.get_element(wrows[j]),wrows[j]);
	}
	return result;	
}

Vector operator*(const Vector &v, double d){
	vector<double> vdat = v.get_data();
	vector<int> vrows = v.get_rows();
	Vector result {v.get_num_rows()};
	for(int j = 0; j < vdat.size(); j++){
		result.insert(vdat[j]*d,vrows[j]);
	}
	return result;	
}

Vector operator*(double d,const Vector &v){
	return v*d;	
}
// End, helper functions


class Matrix
// Matrix class as collection of Vectors
// VCRS (vector compressed row storage)
{
public:
	Matrix(int nr, int nc){
		num_rows = nr;
		num_cols = nc;
		for(int i = 0; i<nr; i++){
			Vector v {nc};
			row_vectors.push_back(v);
		}
	}

	void print(){
		for(int i = 0; i < num_rows; i++){
			row_vectors[i].print();
			cout << '\n';
		}
	}

	void insert(double d, int i, int j)
	{
		row_vectors[i].insert(d,j);
	};

	double get_element(int i, int j) const{
		return row_vectors[i].get_element(j);
	}

	int get_num_cols() const{
		return num_cols;
	}

	Vector get_row(int i) const{
		return row_vectors[i];
	}
	
	void set_row(Vector v, int i){
		row_vectors[i] = v;
	}
	
	Vector vector_multiply(const Vector &v) const{
		Vector w {num_rows};
		double d = 0;		
		for(int i = 0; i < num_rows; i++){
			d = inner_product(row_vectors[i], v);
			if(d!=0){
				w.insert(d,i);
			}
		}
		return w;
	}

	Vector transpose_vector_multiply(const Vector &v) const{
		Vector w {num_cols};
		double d = 0;		
		for(int i = 0; i < num_rows; i++){
			w = w + row_vectors[i]*v.get_element(i);
		}
		return w;
	}	
private:
	vector<Vector> row_vectors;
	int num_rows;
	int num_cols;
};

double source_term(double x, double y){
// Source term for right hand side in Poisson equation
	double dx = x - 0.5;
	double dy = y - 0.5;
	return 10*exp(-(dx*dx + dy*dy) / 0.02);
}

class KrylovSolver
// Solver for symmetric and non-symmetric matrices
{
public:
	static Vector solve(const Matrix &A, const Vector &b,char SolveType){
		// Use 'C' for the conjugate gradient method and 'G' for the generalized minimal residual method
		if(SolveType == 'C'){
			return solve_conjugate_gradient(A,b);
		} else if(SolveType == 'G'){
			return solve_gmres(A,b);
		}
	}

private:
	static Vector solve_conjugate_gradient(const Matrix &A, const Vector &b){
		// Solve system with the conjugate gradient method
		Vector x {b.get_num_rows()}; 
		Vector p = b; 
		Vector r0 = b; 
		Vector z {b.get_num_rows()}; 
		Vector r1 {b.get_num_rows()};
		double tol=pow(10,-25); 
		double nu=0; 
		double mu = 0;
		int k = 0; 
		double err = 1; 
		while(err>tol){
			k = k+1;
			z = A.vector_multiply(p);
			nu = inner_product(r0,r0)/inner_product(p,z);
			x = x + p*nu;
			r1 = r0 + z*(-nu); 
			mu = inner_product(r1,r1)/inner_product(r0,r0);
			p = r1 + p*mu;
			r0 = r1;
			err = inner_product(r0,r0);
		}
		return(x);
	}	

	static Vector least_square(const Matrix &A, const Vector &b){
		// Compute the LSQ-solution x that minimizes ||A*x - b||
		// by using the conjugate gradient method
		Vector x {A.get_num_cols()}; 
		Vector p = A.transpose_vector_multiply(b); 
		Vector r0 = p; 
		Vector z {A.get_num_cols()}; 
		Vector r1 {A.get_num_cols()};
		double tol=pow(10,-25); double nu=0; double mu = 0; int k = 0; double err = 1; 
		while(err>tol){
			k = k+1;
			z = A.transpose_vector_multiply(A.vector_multiply(p));
			nu = inner_product(r0,r0)/inner_product(p,z);
			x = x + p*nu;
			r1 = r0 + z*(-nu); 
			mu = inner_product(r1,r1)/inner_product(r0,r0);
			p = r1 + p*mu;
			r0 = r1;
			err = inner_product(r0,r0);
		}
		return(x);
	}

	static Vector solve_gmres(const Matrix &A, const Vector &b){
		// Solve system with the generalized minimal residual method, GMRES(m)
		// Find approximate solution in m-th Krylov subspace
		// https://math.berkeley.edu/~mgu/MA228A/saad-schultz.pdf
		// Repeat until error is small
		int m = 2;
		double tol=pow(10,-6); 
		double err = 1; 
		Vector x {b.get_num_rows()}; 
		Vector r0 = b; 
		double beta = sqrt(inner_product(b,b)); 
		while(err>tol && m<100){ 
			Vector temp_q {b.get_num_rows()}; 
			Vector y {m};
			Vector e {m+1}; 
			e.insert(1,0); 
			int k = 0; 
			Vector e1 {m+1};
			double h = 0; 
			double temp_q_norm = 0; 
			Matrix QT = {m,b.get_num_rows()}; // Q contains m orthogonal vectors
			QT.set_row(r0*(1/sqrt(inner_product(r0,r0))),0);
			// Find Q with Arnoldi method
			Matrix H {m+1,m};
			for(int j = 0; j < m; j++){
				temp_q = Vector {b.get_num_rows()};
				for(int i = 0; i < j+1; i++){
					h = inner_product(A.vector_multiply(QT.get_row(j)),QT.get_row(i));
					H.insert(h, i, j);
					temp_q = temp_q + h*QT.get_row(i);
				}
				temp_q = A.vector_multiply(QT.get_row(j)) + (-1)*temp_q;
				temp_q_norm = sqrt(inner_product(temp_q,temp_q));
				H.insert(temp_q_norm, j+1, j);
				if(j < m-1){	
					QT.set_row(temp_q*(1/temp_q_norm),j+1);
				}
			}
			// Find LSQ solution y
			e1 = beta*e;
			y = least_square(H, e1);
			// Compute x_m = x_0 + Q*y
			x = x + QT.transpose_vector_multiply(y);
			r0 = b + (-1)*A.vector_multiply(x);
			err = sqrt(inner_product(r0,r0));
			beta = err;
			m+=1;
		}
		return(x);
	}
};

class Assembler
// Assembles the linear system
// Contains pointers to mesh, matrix and vector
{
public:
	Assembler(Mesh* mm, Matrix* ll, Vector* vv)
	: M{mm}, LeftHandSide{ll}, RightHandSide{vv}
	{
	};

	void assemble(){
	// Assemble the linear system
		vector<vector<int>> cells = (*M).get_cells();
		// Loop over cells
		for (int i=0; i < cells.size(); i++) { 	
			// Iniatilize coordinate and source vectors/arrays
			double coordinates[6]; 
			vector<double> source_values; 
			// Loop over nodes in cell
			for (int j=0; j < 3; j++){ 
				// Fill arrays
				vector<double> node_coordinate = (*M).get_coordinate(cells[i][j]); 
				coordinates[j*2] = node_coordinate[0];
				coordinates[j*2+1] = node_coordinate[1];
				source_values.push_back(source_term(node_coordinate[0],node_coordinate[1]));
			}
			// Compute local matrix and vector
			vector<double> local_tensors = tabulate_tensors(&coordinates[0],source_values); 
			vector<double> local_tensor_A(&local_tensors[0],&local_tensors[9]);
			vector<double> local_tensor_F(&local_tensors[9],&local_tensors[12]);
			// Map local to global
			for (int j=0; j < 3; j++){ 
				(*RightHandSide).insert((*RightHandSide).get_element(cells[i][j])+local_tensor_F[j],cells[i][j]);
				for (int k=0; k < 3; k++){ 
					(*LeftHandSide).insert((*LeftHandSide).get_element(cells[i][j],cells[i][k])+local_tensor_A[3*j+k],cells[i][j],cells[i][k]);
				}
			}
		}
	};

	void apply_homogenous_dirichlet_bc(double dd){ // Domain dependent
		Vector empty_row {(*LeftHandSide).get_num_cols()}; 
		for(int i=0; i< (*M).number_of_nodes(); i++){
			vector<double> node_cc = (*M).get_coordinate(i);
			double eps = pow(2,-50);
			if(node_cc[0] < eps || node_cc[0]>1-eps || node_cc[1] < eps || node_cc[1]>1-eps){ // definition of boundary
				(*RightHandSide).insert(dd,i); // homogenous boundary conditions
				(*LeftHandSide).set_row(empty_row,i);
				(*LeftHandSide).insert(1,i,i);
			}
		}
	};
	
private:
	vector<double> tabulate_tensors(const double * coordinate_dofs, vector<double> w) { 
	// Code copied from Poisson.h   
		// Compute Jacobian
		double J[4];
		compute_jacobian_triangle_2d(J, coordinate_dofs);
    	
		// Compute Jacobian inverse and determinant
		double K[4];
		double detJ;
		compute_jacobian_inverse_triangle_2d(K, detJ, J);
    	
		// Set scale factor
		const double det = std::abs(detJ);
    	
		// Compute geometry tensor
		const double G0_0_0 = det*(K[0]*K[0] + K[1]*K[1]);
		const double G0_0_1 = det*(K[0]*K[2] + K[1]*K[3]);
		const double G0_1_0 = det*(K[2]*K[0] + K[3]*K[1]);
		const double G0_1_1 = det*(K[2]*K[2] + K[3]*K[3]);
    	
		// Compute element tensor	
		vector<double> A;
		A.resize(12);
	
		A[0] = 0.5*G0_0_0 + 0.5*G0_0_1 + 0.5*G0_1_0 + 0.5*G0_1_1;
		A[1] = -0.5*G0_0_0 - 0.5*G0_1_0;
		A[2] = -0.5*G0_0_1 - 0.5*G0_1_1;
		A[3] = -0.5*G0_0_0 - 0.5*G0_0_1;
		A[4] = 0.5*G0_0_0;
		A[5] = 0.5*G0_0_1;
		A[6] = -0.5*G0_1_0 - 0.5*G0_1_1;
		A[7] = 0.5*G0_1_0;
		A[8] = 0.5*G0_1_1;
	
		// Compute geometry tensor
		const double G0_0 = det*w[0]*(1.0);
		const double G0_1 = det*w[1]*(1.0);
		const double G0_2 = det*w[2]*(1.0);
    	
		// Compute element tensor
		A[9] = 0.0833333333333334*G0_0 + 0.0416666666666667*G0_1 + 0.0416666666666667*G0_2;
		A[10] = 0.0416666666666667*G0_0 + 0.0833333333333333*G0_1 + 0.0416666666666666*G0_2;
		A[11] = 0.0416666666666667*G0_0 + 0.0416666666666666*G0_1 + 0.0833333333333333*G0_2;
		return A;
	}
	
	Mesh* M;
	Matrix* LeftHandSide;
	Vector* RightHandSide;
};

int main() {
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  	
	int m = 20;
	int n = 20;
	Mesh M = UnitSquareMesh(m,n);
	Matrix A {m*n,m*n};
	Vector f {m*n};
	Assembler Poisson {&M,&A,&f};
	Poisson.assemble();
	Poisson.apply_homogenous_dirichlet_bc(0.0);
	Vector solution = KrylovSolver::solve(A,f,'G');
	
	std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
  std::chrono::duration<double> elapsed_seconds = end-begin;
  std::cout << "Time difference = " << elapsed_seconds.count() << " seconds." << '\n';
  	
  // Save the solution and coordinates to plot in matlab
  	
  ofstream sol;
	sol.open ("solution.txt");  
	ofstream MeshY;
	MeshY.open ("MeshY.txt");  
	ofstream MeshX;
	MeshX.open ("MeshX.txt");
	for(int i = 0; i<m; i++){
		for(int j = 0; j<n; j++){
			vector<double> cc = M.get_coordinate(i*n+j);
			sol << solution.get_element(i*n+j) << ' ';
			MeshX << cc[0] << ' ';
			MeshY << cc[1] << ' ';
		}
		sol << '\n';
		MeshX <<  '\n';
		MeshY <<  '\n';
	}  	
}
