#include <dolfin.h>
#include <math.h>
#include "Heat_VF.h"

using namespace dolfin;

// Defining source term
class Source : public Expression
{
public:
	double time;
		
	Source()
	{
		time = 0.0;
	}
	
	void eval(Array<double>& values, const Array<double>& x) const
	{
		values[0] = 2*pow(time, 3.0)*x[0]*(x[0] - 1)*pow(2*x[0] - 1, 2.0) + 2*time*(pow(time, 2.0)*pow(x[0], 2.0)*pow(x[0] - 1, 2.0) + 1) - x[0]*(x[0] - 1);
	}
	
};

// Defining Dirichlet conditions
class DirichletValue : public Expression
{
	void eval(Array<double>& values, const Array<double>& x) const
	{
		values[0] = 0;
	}
};

class DirichletBoundary : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return (x[0] < DOLFIN_EPS || x[0] > 1 - DOLFIN_EPS) && on_boundary;
	}
};

// Defining initial condition
class InitialCondition : public Expression
{
	void eval(Array<double>& values, const Array<double>& x) const
	{
		values[0] = 0;
	}
};

int main()
{
	// Partition
	double Nt = 50.0;
	double T = 1.0;
	double dt = T/Nt;
	double t = 0.0;
	
	// Mesh and functionspace
	auto mesh = std::make_shared<UnitSquareMesh>(16, 16);
	auto V = std::make_shared<Heat_VF::FunctionSpace>(mesh);
	
	// Problem data
	auto u = std::make_shared<Function>(V);
	auto uprev = std::make_shared<Function>(V);
	auto u_0 = std::make_shared<InitialCondition>();
	auto k = std::make_shared<Constant>(dt);
	auto f = std::make_shared<Source>();
	
	// Create residualform and its derivative
	Heat_VF::ResidualForm F(V);
	Heat_VF::JacobianForm J(V, V);
	
	// Assigning coefficients to the forms
	F.u = u;
	F.f = f;
	F.uprev = u_0;
	F.dt = k;
	J.u = u;
	J.dt = k;
	
	// Boundary conditions
	auto u_D = std::make_shared<DirichletValue>();
	auto Gamma_D = std::make_shared<DirichletBoundary>();
	DirichletBC bc(V, u_D, Gamma_D);
	
	// Save and update time
	File file("heat/solution.pvd");
	file << *u;
	t += dt;
	
	while(t < T + DOLFIN_EPS)
	{	
		// Change time on source term
		(*f).time = t;
		
		// Solve nonlinear equation
		solve(F == 0, *u, bc, J);
		
		// Save data
		file << *u;
		
		// Update
		t += dt;
		*uprev = *u;
		F.uprev = uprev;
	}
	return 0;
}
