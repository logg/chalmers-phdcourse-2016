// Solution of a coupled problem with displacements and temperature
// as unknowns in a finite strain setting. The material is a
// Sant Venant-Kirchoff model, modified to include the strain from
// the thermal expansion. The geometry is a simple cube, which is
// extended and twisted.
//
// compile by running: cmake . && make && ./assignment3
//
// Fredrik Ekre

#include <dolfin.h>
#include <stdio.h>
#include "ThermoElasticity.h"

using namespace dolfin;

// Sub domain for bottom side
class Bot : public SubDomain {
    bool inside(const Array<double>& x, bool on_boundary) const {
        const double Lz = 1.0;
        return (std::abs(x[2] + Lz) < DOLFIN_EPS) && on_boundary;
    }
};

// Sub domain for top side
class Top : public SubDomain {
    bool inside(const Array<double>& x, bool on_boundary) const {
        const double Lz = 1.0;
        return (std::abs(x[2] - Lz) < DOLFIN_EPS) && on_boundary;
    }
};

// Dirichlet boundary condition for clamp at bottom end
class Fixed : public Expression {
public:
    Fixed() : Expression(3) {}
    void eval(Array<double>& values, const Array<double>& x) const {
        values[0] = 0.0;
        values[1] = 0.0;
        values[2] = 0.0;
    }
};

// Dirichlet boundary condition for rotation at right end
class Rotation : public Expression {
public:
    Rotation() : Expression(3) {
        scale = 0.0;
    }

    void eval(Array<double>& values, const Array<double>& x) const {
        const double Lz = 1.0;
        // Angle of rotation (pi/4)
        const double angle = 0.7853981633974483;
        // Rotate and extend the top part
        values[0] = scale * ((x[0]*cos(angle) - x[1]*sin(angle)) - x[0]);
        values[1] = scale * ((x[0]*sin(angle) + x[1]*cos(angle)) - x[1]);
        values[2] = scale * 0.5 * Lz;
    }
    double scale;
};

class BotTemperature : public Expression {
public:
    BotTemperature(double ti, double Ti, double t0) : Expression(), Time(Ti), t_0(t0) {
        time = ti;
    }

    void eval(Array<double>& values, const Array<double>& x) const {
        const double pi = atan(1)*4;
        values[0] = t_0 + std::max(t_0*sin(pi*time/Time), 0.0);
    }
    double time;
    const double Time;
    const double t_0;
};

class InitialCondition : public Expression {
public:
    InitialCondition(double u, double v, double w, double t) : Expression(4), _u(u), _v(v), _w(v), _t(t) {}
    void eval(Array<double>& values, const Array<double>& x) const {
        values[0] = _u;
        values[1] = _v;
        values[2] = _w;
        values[3] = _t;
    }
private:
    const double _u;
    const double _v;
    const double _w;
    const double _t;
};

int main(){
    // create a simple mesh
    double Lx = 1.0, Ly = 1.0, Lz = 1.0;
    int nelx = 10, nely = 10, nelz = 10;
    auto mesh = std::make_shared<BoxMesh>(Point(-Lx, -Ly, -Lz), Point(Lx, Ly, Lz), nelx, nely, nelz);

    // elastic parameters
    double E = 10.0;
    double nu = 0.3;
    auto lambd = std::make_shared<Constant>(E*nu/((1 + nu)*(1 - 2*nu))); // Lame's first parameter
    auto mu = std::make_shared<Constant>(E/(2*(1 + nu)));                // Lame's second parameter

    // thermal parameters
    auto alpha = std::make_shared<Constant>(0.05); // thermal expansion coefficient
    auto k = std::make_shared<Constant>(1.0);      // heat conductivity
    auto c = std::make_shared<Constant>(1.0);      // heat capacity
    auto t0 = std::make_shared<Constant>(5.0);     // initial temperature in domain

    // simulation parameters
    const bool updatedisp = true;
    const bool updatetemp = true;

    const int nsteps = 30;
    const double Ti = 5.0;
    auto deltat = std::make_shared<Constant>(Ti/nsteps);
    double ti = 0.0;
    const double t_0 = 5.0;

    // function space
    auto V = std::make_shared<ThermoElasticity::FunctionSpace>(mesh);

    // define Dirichlet boundaries
    auto bot = std::make_shared<Bot>();
    auto top = std::make_shared<Top>();

    // define Dirichlet boundary functions
    auto fu_fix = std::make_shared<Fixed>();
    auto fu_rot = std::make_shared<Rotation>();
    auto ft_bot = std::make_shared<BotTemperature>(ti, Ti, (t0->values())[0]);

    // create Dirichlet boundary conditions
    DirichletBC u_fix(V->sub(0), fu_fix, bot);
    DirichletBC u_rot(V->sub(0), fu_rot, top);
    DirichletBC t_bot(V->sub(1), ft_bot, bot);
    std::vector<const DirichletBC*> bcs = {{&u_fix, &u_rot, &t_bot}};

    // define solution function
    auto w = std::make_shared<Function>(V);

    // define initial condition
    auto wn = std::make_shared<Function>(V);
    InitialCondition ic(0.0,0.0,0.0,(t0->values())[0]);
    wn->interpolate(ic);

    // create residual and jacobian
    ThermoElasticity::ResidualForm F(V);
    F.mu = mu; F.lambd = lambd;
    F.alpha = alpha; F.t0 = t0;
    F.c = c; F.k = k;
    F.w = w; F.wn = wn;
    F.deltat = deltat;

    ThermoElasticity::JacobianForm J(V, V);
    J.mu = mu; J.lambd = lambd;
    J.alpha = alpha; J.t0 = t0;
    J.c = c; J.k = k;
    J.w = w;
    J.deltat = deltat;

    // output files
    File u_file("results/displacement.pvd");
    File t_file("results/temperature.pvd");

    // save initial config
    Function u_init = (*wn)[0];
    Function t_init = (*wn)[1];
    u_file << u_init;
    t_file << t_init;

    for (unsigned int i = 1; i < nsteps; i++){
        // update boundary conditions
        ti += (deltat -> values())[0];

        if (updatedisp){
            fu_rot -> scale = ti/Ti;
        }
        if (updatetemp){
            ft_bot -> time = ti;
        }

        // solve nonlinear variational problem F(u; v) = 0
        solve(F == 0, *w, bcs, J);

        // save solution
        Function u = (*w)[0];
        Function t = (*w)[1];
        u_file << u;
        t_file << t;

        // set old solution as new ??
        wn -> interpolate(*w);
    }
    return 0;
}
