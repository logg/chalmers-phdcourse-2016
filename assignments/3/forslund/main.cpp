#include <chrono>
#include <dolfin.h>
#include <mshr.h>
#include "RadiativeHeatTransfer.h"

using namespace dolfin;

// Source terms (right-hand side)
class Source : public Expression
{
public:
    Source() : Expression(2), t(t) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
        values[0] = 0.0;
        values[1] = 0.0;
    }

    double t;
};

// Boundary source terms
class BoundarySource : public Expression
{
public:
    BoundarySource(double t, double t_f) : Expression(2), t(t), t_f(t_f) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
        if (t >= 0.0 && t <= 0.2*t_f)
        {
            values[0] = 800.0 + 100.0*t/(0.2*t_f);
        }
        else if (t > 0.2*t_f && t <= 0.6*t_f)
        {
            values[0] = 900.0;
        }
        else if (t > 0.6*t_f && t <= 0.65*t_f)
        {
            values[0] = 900.0 + 100.0/(0.05*t_f)*(t - 0.6*t_f);
        }
        else if (t > 0.65*t_f && t <= 0.75*t_f)
        {
            values[0] = 1000.0;
        }
        else if (t > 0.75*t_f && t <= 0.8*t_f)
        {
            values[0] = 1000.0 - 150.0/(0.05*t_f)*(t - 0.75*t_f);
        }
        else if (t > 0.8*t_f)
        {
            values[0] = 850.0;
        }

        values[1] = 4*5.670367e-8*values[0]*values[0]*values[0]*values[0];
    }

    double t;
    double t_f;
};

// Initial condition
class InitialCondition : public Expression
{
public:
    InitialCondition() : Expression(2) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
        values[0] = 800;
        values[1] = 0.0;
    }
};

//// Robin boundary
//class RobinBoundary : public SubDomain
//{
//    bool inside(const Array<double>& x, bool on_boundary) const
//    {
//        return on_boundary;
//    }
//};

int main()
{
    auto start = std::chrono::high_resolution_clock::now();

    // Create mesh and function space
    std::size_t nSegments = 32; // Default size
    auto circle = std::make_shared<mshr::Circle>(Point(0.0,0.0), 0.5, nSegments);
    auto mesh = std::make_shared<Mesh>();
    mesh = generate_mesh(*circle, 10);

    auto MP = std::make_shared<RadiativeHeatTransfer::FunctionSpace>(mesh);

    // Set time stepping data
    double t = 0.0;
    double t_f = 0.002;
    int nSteps = 200;
    double dt = t_f/nSteps;

    // Declare constituents of variational form
    auto u = std::make_shared<Function>(MP);
    auto u0 = std::make_shared<Function>(MP);
    InitialCondition ic;
    (*u0).interpolate(ic);

    auto source = std::make_shared<Source>();
    auto amb = std::make_shared<BoundarySource>(t, t_f/2.0);

    auto k = std::make_shared<Constant>(1.0); // Heat coefficient
    auto a = std::make_shared<Constant>(5.670367e-8/3.14159265359); // S-B const. / pi
    auto kappa = std::make_shared<Constant>(1.0); // Absorption coefficient
    auto epsilon = std::make_shared<Constant>(0.001); // Scaled optical thickness
    auto h = std::make_shared<Constant>(1.0); // Heat transfer coefficient
    auto tau = std::make_shared<Constant>(dt); // Time step duration

    // Create variational forms
    // F is a linear form (w.r.t. the test function)
    RadiativeHeatTransfer::LinearForm F(MP);
    F.u = u;
    F.u0 = u0;

    F.source = source;
    F.amb = amb;

    F.k = k;
    F.a = a;
    F.kappa = kappa;
    F.epsilon = epsilon;
    F.h = h;
    F.tau = tau;

    // Create Jacobian form J = F' (for use in nonlinear solver)
    RadiativeHeatTransfer::JacobianForm J(MP, MP);
    J.u = u;
    J.k = k;
    J.a = a;
    J.kappa = kappa;
    J.epsilon = epsilon;
    J.h = h;
    J.tau = tau;

    // Set solver parameter
    Parameters solver_params("nonlinear_variational_solver");
    Parameters newton_params("newton_solver");
    newton_params.add("relative_tolerance", 1e-9);
    newton_params.add("maximum_iterations", 50);
    newton_params.add("absolute_tolerance", 1e-10);
    newton_params.add("relaxation_parameter", 1.0);
    solver_params.add(newton_params);

    // Save initial data
    File Tfile("results/T.pvd");
    File rhofile("results/rho.pvd");
    Function& T0 = (*u0)[0];
    Tfile << T0;

    for (int n = 0; n < nSteps; n += 1)
    {
        // Update current time
        t += dt;

        // Update functions
        source->t = t;
        amb->t = t;

        // Solve nonlinear variational problem
        // solve creates a NonLinearVariationalProblem and
        // a NonLinearVariationalSolver
        solve(F == 0, *u, J, solver_params);

        // Save solution
        Function& T = (*u)[0];
        Function& rho = (*u)[1];
        Tfile << T;
        rhofile << rho;

        // Update u^(n-1)
        *u0 = *u;
    }

    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Execution time: " << elapsed.count() << " s." << std::endl;

    return 0;
}
