#include <dolfin.h>
#include <iostream>
#include <math.h>
#include <chrono>
#include "StochasticParabolic.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_matrix.h>

using namespace dolfin;

gsl_rng * r;  /* global random numbers generator */

// Dirichlet boundary condition
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};

// Initial value
class Initial : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 10 *(x[0]*(1-x[0]))*(x[1]*(1-x[1]));
  }
};

// a(x) term 
class Aterm : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 1*x[0]*x[0]+0.1;
  }
};

// c(x) term 
class Cterm : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = x[0]*x[1];
  }
};

// Q-Wiener process increment 
class WienerIncrement : public Expression
{
public:

  WienerIncrement(int N, double dt) : Expression(), Gaussians(gsl_matrix_alloc(N,N)), N(N), dt(dt) { } // Constructor

  void eval(Array<double>& values, const Array<double>& x) const
  {
	double v = 0;
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
			v += 2*sqrt(dt)*sin((i+1)*M_PI*x[0])*sin((j+1)*M_PI*x[1])*pow(1.0/(i+1)*1.0/(j+1),3/2)*gsl_matrix_get(Gaussians, i, j);
		}
	}
	values[0] = v;
  }

  void update() { // Call this each time step to update random numbers
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
			gsl_matrix_set(Gaussians, i, j, gsl_ran_gaussian(r, 1));
		}
	}
  }
  
private:
  gsl_matrix * Gaussians; // matrix of random numbers
  int N; // N^2 terms in K-L expansion
  double dt;  // time step size
};

int main()
{
  // Measure time
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  // Set up random number generator
  gsl_rng_env_setup();
  r = gsl_rng_alloc (gsl_rng_default);
  // Create mesh
  int nx = 8;
  int ny = 8;
  auto mesh = std::make_shared<UnitSquareMesh>(nx,ny);
  // Create function space
  auto V = std::make_shared<StochasticParabolic::FunctionSpace>(mesh);  
  // Parameters for time-stepping
  const double T = 1.0;
  const double k = 1.0/100;
  double t = k;

  // Initialize terms
  //   Coefficients
  auto ac = std::make_shared<Aterm>();
  auto cc = std::make_shared<Cterm>();
  //   Wiener process
  auto dW = std::make_shared<WienerIncrement>(10,k);
  (*dW).update();
  //   Solution
  auto u = std::make_shared<Function>(V);
  Initial s; 
  (*u).interpolate(s);
  // Set up forms
  StochasticParabolic::BilinearForm a(V, V);
  StochasticParabolic::LinearForm L(V);
  L.u0 = u; L.dWc = dW; a.ac = ac; a.cc = cc;
  // Set boundary condition
  auto g = std::make_shared<Constant>(0.0);
  auto boundary = std::make_shared<DirichletBoundary>();
  DirichletBC bc(V, g, boundary);

  // Linear system
  std::shared_ptr<Matrix> A(new Matrix);
  Vector b;
  assemble(*A, a);
  bc.apply(*A);
  // LU solver
  LUSolver lu(A);
  lu.parameters["reuse_factorization"] = true;

  // Output files for solution and wiener process
  File filesol("results/solution.pvd");
  File fileWiener("results/wiener.pvd");

  // Start time-stepping
  auto Wplot = Function(V);
  auto cumW = Function(V);
  cumW.interpolate(Constant(0));
  while (t < T)
  {
    // Assemble vector and apply boundary conditions
    assemble(b, L);
    bc.apply(b);
    // Add up Wiener process
    Wplot.interpolate(*dW);
    cumW = cumW+Wplot;
    // Solve the linear system
    lu.solve(*u->vector(), b);
    // Save solution and Wiener process
    filesol << std::pair<const Function*, double>(u.get(), t);
    Function* cumWtosave = &cumW;
    fileWiener << std::pair<const Function*, double>(cumWtosave, t);
    // Move to next interval
    std::cout << t << '\n';
    t += k;
    (*dW).update();
  }
  // Print end time
  std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
  std::chrono::duration<double> elapsed_seconds = end-begin;
  std::cout << "Time difference = " << elapsed_seconds.count() << " seconds." << '\n';
}
