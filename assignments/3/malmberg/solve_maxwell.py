from dolfin import *
from mshr import *

T = 1.0
Nt = 40
Nx = 20
Ny = 10
Nz = 10
dt = T/Nt
x0 = Point(0.0, 0.0, 0.0)
x1 = Point(2.0, 1.0, 1.0)
mesh  = BoxMesh(x0, x1, Nx, Ny, Nz)
X     = FunctionSpace(mesh, "N1curl", 1)
eps   = Expression("2.0 + sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])", degree = 2)
muinv = Expression("1.0", degree = 2)
Fk    = Expression(("-4*pi*pi*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])*sin(2*pi*t)*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*cos(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*sin(2*pi*x[1])*cos(2*pi*x[2])"),
                       degree = 2, t = 0.0)
Fkm1  = Expression(("-4*pi*pi*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])*sin(2*pi*t)*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*cos(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*sin(2*pi*x[1])*cos(2*pi*x[2])"),
                       degree = 2, t = 0.0)
Fkm2  = Expression(("-4*pi*pi*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])*sin(2*pi*t)*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*cos(2*pi*x[1])*sin(2*pi*x[2])",
                        "4*pi*pi*sin(2*pi*t)*cos(2*pi*x[0])*sin(2*pi*x[1])*cos(2*pi*x[2])"),
                       degree = 2, t = 0.0)
E0    = Expression(("0.0", "0.0", "0.0"), degree = 2)
dtdE0 = Expression(("2*dt*pi*sin(2*pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])", "0.0", "0.0"), degree = 2, dt = 0.0)
Eb    = Expression(("0.0", "0.0", "0.0"), degree = 2)

Ekm2  = project(E0, X)
dtdE0.dt = dt
Ekm1  = project(dtdE0, X)

xdmffile = XDMFFile("py_maxwell_solution.xdmf")

E = Function(X)

print('Time step ' + str(0) + '\n')
E.assign(Ekm2)
t = 0.0
xdmffile.write(E, t)

print('Time step ' + str(1) + '\n')
E.assign(Ekm1)
t += dt
xdmffile.write(E, t)

u = TrialFunction(X)
v = TestFunction(X)
a = 6*inner(eps*u, v)*dx + dt*dt*inner(muinv*curl(u), curl(v))*dx 
L = dt*dt*inner(Fk, v)*dx + 4*dt*dt*inner(Fkm1, v)*dx + dt*dt*inner(Fkm2, v)*dx + 12*inner(eps*Ekm1, v)*dx - 6*inner(eps*Ekm2, v)*dx - 4*dt*dt*inner(muinv*curl(Ekm1), curl(v))*dx - dt*dt*inner(muinv*curl(Ekm2), curl(v))*dx

def DirichletBoundary(x, on_boundary):
    return on_boundary
bc = DirichletBC(X, Eb, DirichletBoundary)

for k in range(Nt - 1):
    print('Time step ' + str(k + 2) + '\n')
    t += dt
    Fk.t   = t 
    Fkm1.t = t - dt
    Fkm2.t = t - 2*dt
    
    solve(a == L, E, bc)

    xdmffile.write(E, t)
    Ekm2.assign(Ekm1)
    Ekm1.assign(E)

