#include<dolfin.h>
#include<math.h>
#include"Maxwell.h"
#include"ProjectP2toN1curl.h"

using namespace dolfin;
using namespace std;

class Permittivity : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 2.0 + sin(2*M_PI*x[0])*sin(2*M_PI*x[1])*sin(2*M_PI*x[2]);
  }
};

class InversePermeability : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 1.0;
  }
};

class Source : public Expression
{
  
public:
  
  Source() : Expression(3)
  {
    //Do nothing
  }
  
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = -4*M_PI*M_PI*sin(2*M_PI*x[0])*sin(2*M_PI*x[1])*sin(2*M_PI*x[2])*sin(2*M_PI*t)*sin(2*M_PI*x[0])*sin(2*M_PI*x[1])*sin(2*M_PI*x[2]);
    values[1] = 4*M_PI*M_PI*sin(2*M_PI*t)*cos(2*M_PI*x[0])*cos(2*M_PI*x[1])*sin(2*M_PI*x[2]);
    values[2] = 4*M_PI*M_PI*sin(2*M_PI*t)*cos(2*M_PI*x[0])*sin(2*M_PI*x[1])*cos(2*M_PI*x[2]);
  }

  void SetTime(const double new_t)
  {
    t = new_t;
  }
  
private:

  double t;
};

class InitialCondition0 : public Expression
{
public:
  
  InitialCondition0() : Expression(3)
  {
    // Do nothing
  }
  
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
  }
};

class InitialCondition1 : public Expression
{

public:

  InitialCondition1() : Expression(3)
  {
    //Do nothing
  }
  
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = dt*2*M_PI*sin(2*M_PI*x[0])*sin(2*M_PI*x[1])*sin(2*M_PI*x[2]);
    values[1] = 0.0;
    values[2] = 0.0;
  }

  void SetTimeStep(const double new_dt)
  {
    dt = new_dt;
  }
  
private:

  double dt;
};

class BoundaryCondition : public Expression
{
public:
  
  BoundaryCondition() : Expression(3)
  {
    // Do nothing
  }
  
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
  }
};


class DirichletBoundary : public SubDomain
{
public:
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};


int main()
{
  double T = 1.0, t;
  int Nt = 40, Nx = 20, Ny = 10, Nz = 10;

  auto dt = make_shared<Constant>(T/Nt);

  Array<double> x0(3);
  x0[0] = x0[1] = x0[2] = 0.0;
  Array<double> x1(3);
  x1[0] = 2.0;
  x1[1] = x1[2] = 1.0;
  auto mesh  = make_shared<BoxMesh>(x0, x1, Nx, Ny, Nz);
  auto X     = make_shared<Maxwell::FunctionSpace>(mesh);
  auto eps   = make_shared<Permittivity>();
  auto muinv = make_shared<InversePermeability>();
  auto Fk    = make_shared<Source>();
  auto Fkm1  = make_shared<Source>();
  auto Fkm2  = make_shared<Source>();
  auto E0    = make_shared<InitialCondition0>();
  auto dtdE0 = make_shared<InitialCondition1>();
  auto Eb    = make_shared<BoundaryCondition>();
  auto Ekm1  = make_shared<Function>(X);
  auto Ekm2  = make_shared<Function>(X);
  
  ProjectP2toN1curl::BilinearForm a_proj(X, X);
  ProjectP2toN1curl::LinearForm L_proj(X);

  L_proj.f = E0;
  solve(a_proj == L_proj, *Ekm2);
  (*dtdE0).SetTimeStep(*dt);
  L_proj.f = dtdE0;
  solve(a_proj == L_proj, *Ekm1);
  
   
  XDMFFile file(MPI_COMM_WORLD, "cpp_maxwell_solution.xdmf");

  auto E = make_shared<Function>(X);

  std::cout << "Time step " << 0 << std::endl;
  assign(E, Ekm2);
  t = 0.0;
  file.write(*E, t);

  std::cout << "Time step " << 1 << std::endl;
  assign(E, Ekm1);
  t += (*dt);
  file.write(*E, t);
  
  Maxwell::BilinearForm a(X, X);
  Maxwell::LinearForm L(X);
  a.eps   = eps;
  a.muinv = muinv;
  a.dt    = dt;
  L.eps   = eps;
  L.muinv = muinv;
  L.dt    = dt;

  auto boundary = make_shared<DirichletBoundary>();
  DirichletBC bc(X, Eb, boundary);
  
  for (int k = 2; k <= Nt; k++)
    {
      std::cout << "Time step " << k << std::endl;
      t += (*dt);
      (*Fk).SetTime(t);
      (*Fkm1).SetTime(t - (*dt));
      (*Fkm2).SetTime(t - 2*(*dt));
      L.Ekm2 = Ekm2;
      L.Ekm1 = Ekm1;
      L.Fkm2 = Fkm2;
      L.Fkm1 = Fkm1;
      L.Fk   = Fk;

      solve(a == L, *E, bc);

      file.write(*E, t);
      assign(Ekm2, Ekm1);
      assign(Ekm1, E);
    }
  
  return 0;
}
