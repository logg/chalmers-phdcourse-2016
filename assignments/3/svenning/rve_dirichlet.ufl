###
#   Demo for solution of a finite strain elasticity problem with
#   weak Dirichlet boundary conditions. This is a mixed problem
#   where the unknowns are the displacement u in the whole domain 
#   and the traction t on the faces of the external
#   boundary.
#
#   The material is modeled using St.Venant-Kirchhoff elasticity, 
#   and for the geometry we use the most classical Representative
#   Volume Element (RVE): A cube with a spherical hole.
#
#   Compile this form with FFC: ffc -l dolfin -feliminate_zeros -fprecompute_basis_const -fprecompute_ip_const ../rve_dirichlet.ufl
#
#   Written by 
#   Erik Svenning
#   Chalmers University of Technology
#   January 2017
###

################################################
# Material parameters
mp_mu = Constant(tetrahedron)
mp_lambda = Constant(tetrahedron)

Hxx = Constant(tetrahedron)
Hxy = Constant(tetrahedron)
Hxz = Constant(tetrahedron)
Hyx = Constant(tetrahedron)
Hyy = Constant(tetrahedron)
Hyz = Constant(tetrahedron)
Hzx = Constant(tetrahedron)
Hzy = Constant(tetrahedron)
Hzz = Constant(tetrahedron)


# Define function spaces
P2 = VectorElement("CG", tetrahedron, 2)
T1 = VectorElement("CG", tetrahedron, 1)
PT = P2 * T1

Hx = Coefficient(P2)

# Define functions
vw  = TestFunction(PT)          # Test function
(vu,vt) = split(vw)

dw  = TrialFunction(PT)          # Test function

w = Coefficient(PT) # Displacement and traction from previous iteration
(u,t) = split(w)

# Kinematics
dim = len(u)
I = Identity(dim)             # Identity tensor

# Strain measure
Fu = grad(u) + I
Cu = Fu.T*Fu
Eu = 0.5*(Cu-I)

# Gradient of test function
Fv = grad(vu)

# Stress
S2 = mp_lambda*tr(Eu)*I + 2*mp_mu*Eu # Second Piola-Kirchhoff stress
P = dot(Fu,S2)  # First Piola-Kirchhoff stress

F = inner(P, Fv )*dx - inner(t,vu)*ds - inner(u,vt)*ds + inner(Hx,vt)*ds

# Compute Jacobian of F
J = derivative(F, w, dw)

