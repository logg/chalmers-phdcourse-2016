/*
 *   Demo for solution of a finite strain elasticity problem with
 *   weak Dirichlet boundary conditions. This is a mixed problem
 *   where the unknowns are the displacement u in the whole domain 
 *   and the traction t on the faces of the external
 *   boundary.
 *
 *   The material is modeled using St.Venant-Kirchhoff elasticity, 
 *   and for the geometry we use the most classical Representative
 *   Volume Element (RVE): A cube with a spherical hole.
 *
 *   Written by 
 *   Erik Svenning
 *   Chalmers University of Technology
 *   January 2017
 */

#include <iostream>

#include <dolfin.h>
#include <mshr.h>

#include "build/rve_dirichlet.h"

using namespace dolfin;


// Expression for prescribed macroscopic gradient H
// multiplied by x.
class Hx : public Expression
{
public:

    Hx() : Expression(3) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
        const double Hxx = 0.3;
        const double Hxy = 0.4;
        const double Hxz = 0.0;

        const double Hyx = 0.0;
        const double Hyy = 0.0;
        const double Hyz = 0.0;

        const double Hzx = 0.3;
        const double Hzy = 0.0;
        const double Hzz = 0.0;

        values[0] = Hxx*x[0] + Hxy*x[1] + Hxz*x[2];
        values[1] = Hyx*x[0] + Hyy*x[1] + Hyz*x[2];
        values[2] = Hzx*x[0] + Hzy*x[1] + Hzz*x[2];
    }
};

class Interior : public SubDomain
{
    bool inside(const Array<double>& x, bool on_boundary) const
    {
        if( !(x[0] > DOLFIN_EPS && x[0] < 1.0 - DOLFIN_EPS) ) {
            return false;
        }

        if( !(x[1] > DOLFIN_EPS && x[1] < 1.0 - DOLFIN_EPS) ) {
            return false;
        }

        if( !(x[2] > DOLFIN_EPS && x[2] < 1.0 - DOLFIN_EPS) ) {
            return false;
        }

        return true;

    }
};


int main()
{
    // Material parameters
    const double E      = 10.0;
    const double nu     =  0.3;
    auto mp_mu          = std::make_shared<Constant>(E/(2*(1 + nu)));
    auto mp_lambda      = std::make_shared<Constant>(E*nu/((1+nu)*(1-2*nu)));
 

    ////////////////////////////////////////////////
    // Create mesh
    auto center = Point(0.5,0.5,0.5);
    mshr::Sphere sphere(center,0.25);
    mshr::Box cube( Point(0.0,0.0,0.0), Point(1.0,1.0,1.0) );
    std::shared_ptr<mshr::CSGGeometry> domain(cube - sphere);
    auto mesh   = generate_mesh(*domain,10);

    // Write mesh to file
    File mesh_file("mesh.pvd");
    mesh_file << *mesh;

    // Define function spaces
    auto W = std::make_shared<rve_dirichlet::FunctionSpace>(mesh);
    auto w = std::make_shared<Function>(W);

    // Define boundary condition:
    // Dirichlet BCs for traction in the interior 
    // of the domain.
    auto t0 = std::make_shared<Constant>(0.0, 0.0, 0.0);
    auto boundary = std::make_shared<Interior>();
    DirichletBC bc(W->sub(1), t0, boundary);


    auto hx = std::make_shared<Hx>();

    rve_dirichlet::ResidualForm F(W);
    F.mp_mu = mp_mu; F.mp_lambda = mp_lambda; 
    F.w = w;
    F.Hx = hx;

    rve_dirichlet::JacobianForm J(W, W);
    J.mp_mu = mp_mu; J.mp_lambda = mp_lambda; 
    J.w = w;

    solve( F == 0, *w, bc, J);


    Function u = (*w)[0];
    Function t = (*w)[1];

    // Write solution to file
    File ufile_pvd("u.pvd");
    ufile_pvd << u;
    File tfile_pvd("t.pvd");
    tfile_pvd << t;



    return 0;
}

