#include <iostream>
#include <sys/stat.h>
#include <fstream>
#include <dolfin.h>
#include <mshr.h>
#include "Wave.h"

using namespace dolfin;
using namespace mshr;

class u1Drum : public Expression
{
  public:

    u1Drum() : _dt(0.03) {}
    u1Drum(double dt) : _dt(dt) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      values[0] = -1.0*exp(-100*((x[0] - 2.0)*(x[0] - 2.0) + x[1]*x[1]))*_dt;
    }

  private:
    double _dt;
};

int main()
{
  // Define the domain and generate a mesh
  Point c_center(2.0, 0.0);
  double c_radius = 1.0;
  Circle domain(c_center, c_radius);
  auto mesh = std::make_shared<Mesh>();
  CSGCGALMeshGenerator2D mg;
  mg.parameters["mesh_resolution"] = -1.0;
  mg.parameters["cell_size"] = 0.06666;
  mg.generate(domain, *mesh);
  File("solutions/mesh.pvd") << *mesh;

  // Define final time and time-step
  double T = 10.0;
  double dt = 0.03;         // Fulfills the CFL-conditon when dt < hmin = 0.035
  auto kn = std::make_shared<Constant>(dt);

  // Define FE-space
  auto V = std::make_shared<Wave::FunctionSpace>(mesh);

  // Define problem data
  auto f = std::make_shared<Constant>(0.0);
  auto u_boundary = std::make_shared<Constant>(0.0);
  Constant u0_values(0.0);
  u1Drum u1_values(dt);

  auto u0 = std::make_shared<Function>(V);
  u0->interpolate(u0_values);
  auto u1 = std::make_shared<Function>(V);
  u1->interpolate(u1_values);

  // Define boundary conditions
  auto boundary = std::make_shared<DomainBoundary>();
  DirichletBC bc(V, u_boundary, boundary);

  // Choose weights for numerical scheme
  auto w0 = std::make_shared<Constant>(1.0);
  auto w1 = std::make_shared<Constant>(2.0);
  auto w2 = std::make_shared<Constant>(1.0);

  // Create bilinear and linear forms
  Wave::BilinearForm a(V, V);
  Wave::LinearForm L(V);

  // Assign coefficients and constants to the forms
  a.w0 = w0; a.w1 = w1; a.w2 = w2; a.kn = kn;
  L.f = f; L.u0 = u0; L.u1 = u1; L.w0 = w0; L.w1 = w1; L.w2 = w2; L.kn = kn;

  // Assemble system matrix and apply bcs
  Matrix A;
  assemble(A, a);
  bc.apply(A);

  // Create load vector
  Vector b;

  // Initialize the FE-solution u2
  auto u2 = std::make_shared<Function>(V);

  // Create energy functional
  Wave::Functional E(mesh);

  // Assign coefficients and constants to the energy functional
  E.u0 = u0; E.u1 = u1; E.u2 = u2; E.w0 = w0; E.w1 = w1; E.w2 = w2; E.kn = kn;

  // Create a solution file for storage
  //File u_file("solutions/u_cppFE.pvd");
  //File u_file("solutions/u_cppBE.pvd");
  //File u_file("solutions/u_cppCN.pvd");
  File u_file("solutions/u_cpp.pvd");

  // Create an energy file for storage
  mkdir("energies", 0775);

  //std::ofstream energy_file("energies/energy_cppFE.csv");
  //std::ofstream energy_file("energies/energy_cppBE.csv");
  //std::ofstream energy_file("energies/energy_cppCN.csv");
  std::ofstream energy_file("energies/energy_cpp.csv");

  energy_file << "time t, energy E" << std::endl;

  // Start time-stepping
  double t(dt), t0, t1, t2;
  while (t < T)
  {
    // Update times
    t0 = t - dt; t1 = t; t2 = t + dt;

    // Assemble load vector and apply bcs
    assemble(b, L);
    bc.apply(b);

    // Solve the system
    solve(A, *(*u2).vector(), b);

    // Save solution to file
    u_file << std::make_pair(u0.get(), t0);

    // Compute and save the total energy of the system to file
    energy_file << t1 << ", " << assemble(E) << std::endl;

    // Update previous solutions and time
    *u0 = *u1;
    *u1 = *u2;
    t += dt;
  }

  return 0;
}
