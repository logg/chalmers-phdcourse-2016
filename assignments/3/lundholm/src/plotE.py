import matplotlib.pyplot as plt
import numpy as np

#data = np.genfromtxt('build/energies/energy_cppFE.csv', delimiter=',',
#                     skip_header=1, names=['x', 'y'])
#data = np.genfromtxt('build/energies/energy_cppBE.csv', delimiter=',',
#                     skip_header=1, names=['x', 'y'])
#data = np.genfromtxt('build/energies/energy_cppCN.csv', delimiter=',',
#                     skip_header=1, names=['x', 'y'])
data = np.genfromtxt('build/energies/energy_cpp.csv', delimiter=',',
                     skip_header=1, names=['x', 'y'])

timevec = data['x']; E = data['y']; T = timevec[-1]

plt.plot(timevec, E)
plt.xlabel('Time')
plt.ylabel('Total energy of the system')
#plt.title('Forward Euler')
#plt.title('Backward Euler')
plt.title('Crank-Nicolson')
#plt.axis([0, 0.4, 0, 1000])
#plt.axis([0, 10, 0, 0.009])
plt.axis([0, T, 0, 0.009])
plt.show()
