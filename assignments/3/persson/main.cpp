# include <ctime>
# include <dolfin.h>
# include "jouleheating.h"

using namespace dolfin;

class Boundary_u : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};

class Boundary_phi : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    double tol = 1E-14;
    return on_boundary && near(x[0], 0.0, tol);
  }
};

class Phi_D : public Expression
{
public:
  void eval(Array<double>& values, const Array<double>& x) const
  { values[0] = x[1]; }
};

class Initial_Data : public Expression
{
public:
  Initial_Data() : Expression(2) {}

  void eval(Array<double>& values, const Array<double>& x) const
  { values[0] = x[0]*(1-x[0])*x[1]*(1-x[1]);
    values[1] = 0.0;
  }
};

int main()
{
  std::clock_t begin = std::clock();
  // Define mesh and spaces to ufl-file
  auto mesh = std::make_shared<UnitSquareMesh>(32, 32);
  auto V = std::make_shared<jouleheating::FunctionSpace>(mesh);

  // Set BCs
  auto boundary_u = std::make_shared<Boundary_u>();
  auto boundary_phi = std::make_shared<Boundary_phi>();
  auto phi_D = std::make_shared<Phi_D>();
  DirichletBC bc1(V->sub(0), std::make_shared<Constant>(0.0), boundary_u);
  DirichletBC bc2(V->sub(1), phi_D, boundary_phi);
  std::vector<const DirichletBC*> bcs;
  bcs.push_back(&bc1);
  bcs.push_back(&bc2);

  // Set spaces and functions in ufl-file
  jouleheating::ResidualForm F(V);
  jouleheating::JacobianForm J(V,V);
  auto v = std::make_shared<Function>(V);
  F.v = v;
  J.v = v;

  // Intial data
  auto u_0 = std::make_shared<Initial_Data>();
  auto v_n = std::make_shared<Function>(V);
  v_n->interpolate(*u_0);
  F.v_n = v_n;

  //Time paramters
  double dt = 0.02;
  auto k = std::make_shared<Constant>(dt);
  double T = 1;
  double num_steps = T/dt;
  F.dt = k;
  J.dt = k;

  double t = 0;
  while(t < T)
  {
    // Update t
    t += dt;

    // Solve
    solve(F==0, *v, bcs, J);

    // Update v_n with v
    F.v_n = v;
  }

  Function& u = (*v)[0];
  Function& phi = (*v)[1];

  std::clock_t end = std::clock();
  // Print time
  double time_elapsed = double(end-begin)/ CLOCKS_PER_SEC;
  std::cout << "Time elapsed = " << time_elapsed << std::endl;

  plot(u);
  plot(phi);
  interactive();

  return 0;
}
