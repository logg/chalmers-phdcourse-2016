#include <dolfin.h>
#include "TH_NeoHook.h"
#include "NeoHook.h"

using namespace dolfin;

// Sub domain for clamp at left end
class Left : public SubDomain
{
    bool inside(const Array<double>& x, bool on_boundary) const
    {
        return near(x[0], 0.0) && on_boundary;
    }
};

// Dirichlet boundary condition for clamp at left end
class Clamp : public Expression
{
public:

    Clamp() : Expression(3) {}

    void eval(Array<double>& values, const Array<double>&) const
    {
        values[0] = 0.0;
        values[1] = 0.0;
        values[2] = 0.0;
    }

};


int main()
{
    // Create mesh and define function space
    auto mesh = std::make_shared<BoxMesh>(Point(0.0, 0.0, 0.0),
                                          Point(10.0, 10.0, 10.0),
                                          8, 8, 8);
    auto TH_V = std::make_shared<TH_NeoHook::FunctionSpace>(mesh);
    auto V = std::make_shared<NeoHook::FunctionSpace>(mesh);

    // Define Dirichlet boundaries
    auto left = std::make_shared<Left>();

    // Define Dirichlet boundary functions
    auto c = std::make_shared<Clamp>();

    // Create Dirichlet boundary conditions
    DirichletBC TH_bcl(TH_V->sub(0), c, left);
    DirichletBC bcl(V, c, left);

    std::vector<const DirichletBC*> TH_bcs = {{&TH_bcl}};
    std::vector<const DirichletBC*> bcs = {{&bcl}};

    // Define source and boundary traction functions
    auto B = std::make_shared<Constant>(0.0, 0.0, 0.0);
    auto T = std::make_shared<Constant>(0.0, 1.0 / 64.0, 0.0);

    // Define solution function
    auto up = std::make_shared<Function>(TH_V);
    auto u = std::make_shared<Function>(V);

    // Set material parameters
    const double E  = 10.0;
    const double nu = 0.3;
    auto mu = std::make_shared<Constant>(E/(2*(1 + nu)));
    auto lambda = std::make_shared<Constant>(E*nu/((1 + nu)*(1 - 2*nu)));

    // Create (linear) form defining (nonlinear) variational problem
    TH_NeoHook::ResidualForm TH_F(TH_V);
    TH_F.mu = mu; TH_F.up = up;
    TH_F.B = B; TH_F.T = T;

    NeoHook::ResidualForm F(V);
    F.mu = mu; F.lmbda = lambda; F.u = u;
    F.B = B; F.T = T;

    // Create jacobian dF = F' (for use in nonlinear solver).
    TH_NeoHook::JacobianForm TH_J(TH_V, TH_V);
    TH_J.mu = mu; TH_J.up = up;

    NeoHook::JacobianForm J(V, V);
    J.mu = mu; J.lmbda = lambda; J.u = u;

    // Solve nonlinear variational problem F(u; v) = 0
    solve(F == 0, *u, bcs, J);
    File file2("displacement_lin.pvd");
    file2 << *u;

    solve(TH_F == 0, *up, TH_bcs, TH_J);
    Function& TH_u = (*up)[0];
    Function& TH_p = (*up)[1];

    File file2("displacement_TH.pvd");
    file2 << *TH_u;
    File file3("pressure_TH.pvd");
    file3 << *TH_p;

    return 0;
}
