// unit testing with Google Test, https://github.com/google/googletest.git
// to run: git clone the repo in to this directory, and run make
#include "rational.h"
#include "polynomial.h"
#include "googletest/googletest/include/gtest/gtest.h"

TEST(Rational, Unary_operators){

    EXPECT_TRUE(-Rational<int>(1, 2) == Rational<int>(-1, 2));
    EXPECT_TRUE(+Rational<int>(1, 2) == Rational<int>(1, 2));

}

TEST(Rational, rational_binary_operators){

    Rational<int> a (1, 2);
    Rational<int> b (1, 4);

    // +
    EXPECT_TRUE(a + a == Rational<int>(2) * a);
    EXPECT_TRUE(a + a == Rational<int>(1, 1));
    EXPECT_TRUE(a + b == b + a);
    EXPECT_TRUE(a + b == Rational<int>(3, 4));

    // -
    EXPECT_TRUE(a - a == Rational<int>(0, 1));
    EXPECT_TRUE(a - a == Rational<int>(0) * a);
    EXPECT_TRUE(a - b == Rational<int>(1, 4));
    EXPECT_TRUE(b - a == Rational<int>(-1, 4));

    // *
    EXPECT_TRUE(a * a == Rational<int>(1, 4));
    EXPECT_TRUE(a * b == b * a);
    EXPECT_TRUE(a * b == Rational<int>(1, 8));

    // /
    EXPECT_TRUE(a / a == Rational<int>(1, 1));
    EXPECT_TRUE(a / b == Rational<int>(2, 1));
    EXPECT_TRUE(b / a == Rational<int>(1, 2));

}

TEST(Rational, comparison_operators){

    Rational<int> a (1, 2);
    Rational<int> b (1, 3);

    // ==
    EXPECT_TRUE(a == a);
    EXPECT_TRUE(!(a == b));

    // !=
    EXPECT_TRUE(!(a != a));
    EXPECT_TRUE(a != b);

    // <
    EXPECT_TRUE(!(a < a));
    EXPECT_TRUE(!(a < b));
    EXPECT_TRUE(b < a);

    // >
    EXPECT_TRUE(!(a > a));
    EXPECT_TRUE(a > b);
    EXPECT_TRUE(!(b > a));

    // <=
    EXPECT_TRUE(a <= a);
    EXPECT_TRUE(!(a <= b));
    EXPECT_TRUE(b <= a);

    // >=
    EXPECT_TRUE(a >= a);
    EXPECT_TRUE(a >= b);
    EXPECT_TRUE(!(b >= a));

}

template<typename T>
bool operator==(const Polynomial<T>& a, const Polynomial<T>& b){
    auto ac = a.coefficients();
    auto bc = b.coefficients();
    auto na = ac.size();
    auto nb = bc.size();
    if (na != nb){
        return false;
    }
    for (unsigned long int i = 0; i < na; i++){
        if (ac[i] != bc[i]){
            return false;
        }
    }
    return true;
}

TEST(Polynomial, unary){
    Rational<int> Ax = Rational<int>(1, 2);
    Rational<int> Ac = Rational<int>(1, 3);

    Polynomial< Rational<int> > A({Ax, Ac});

    EXPECT_TRUE(+A == A);
    EXPECT_TRUE(-A == Polynomial< Rational<int> > ({-Ax, -Ac}));

}

TEST(Polynomial, binary){
    Rational<int> Ax = Rational<int>(1, 2);
    Rational<int> Ac = Rational<int>(1, 3);
    Polynomial< Rational<int> > A({Ax, Ac});

    Rational<int> Bx2 = Rational<int>(1, 4);
    Rational<int> Bx = Rational<int>(1, 5);
    Rational<int> Bc = Rational<int>(1, 6);
    Polynomial< Rational<int> > B({Bx2, Bx, Bc});

    // +
    EXPECT_TRUE(A + B == B + A);
    EXPECT_TRUE(A + B == Polynomial< Rational<int> > ({Bx2, Ax + Bx, Ac + Bc}));

    // -
    EXPECT_TRUE(!(A - B == B - A));
    EXPECT_TRUE(A - B == Polynomial< Rational<int> > ({-Bx2, Ax - Bx, Ac - Bc}));
    EXPECT_TRUE(B - A == Polynomial< Rational<int> > ({Bx2, Bx - Ax, Bc - Ac}));

    // *
    EXPECT_TRUE(A * B == B * A);
    EXPECT_TRUE(A * B == Polynomial< Rational<int> > ({Ax * Bx2, // x^3
                                                       Ax * Bx + Ac * Bx2, // x^2
                                                       Ax * Bc + Ac * Bx, // x
                                                       Ac * Bc // c
                                                       }));

}

TEST(Polynomial, differentation_integration){
    Rational<int> Ax = Rational<int>(1, 2);
    Rational<int> Ac = Rational<int>(1, 3);
    Polynomial< Rational<int> > A({Ax, Ac});

    // diff
    Polynomial< Rational<int> > Adiff({Ax, Ac});
    Adiff.differentiate();

    EXPECT_TRUE(Adiff == Polynomial< Rational<int> > (Ax));

    // int
    Polynomial< Rational<int> > Aint({Ax, Ac});
    Aint.integrate();
    Polynomial< Rational<int> > Aintc({Ax, Ac});
    Aintc.integrate(1);

    EXPECT_TRUE(Aint == Polynomial< Rational<int> > ({Ax/2,Ac, Rational<int>(0)}));
    EXPECT_TRUE(Aintc == Polynomial< Rational<int> > ({Ax/2,Ac, Rational<int>(1)}));

}

TEST(Polynomial, evaluation){
    Rational<int> Ax2 = Rational<int>(1, 2);
    Rational<int> Ax = Rational<int>(1, 3);
    Rational<int> Ac = Rational<int>(1, 4);
    Polynomial< Rational<int> > A({Ax2, Ax, Ac});

    Rational<int> x = Rational<int>(1, 5);
    Rational<int> y = Rational<int>(101, 300);

    // evaluation
    EXPECT_TRUE(A(x) == y);
}
