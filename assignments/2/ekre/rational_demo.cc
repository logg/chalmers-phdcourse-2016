// Demonstrates the use of the `Rational` class, defined in "rational.h"
// Fredrik Ekre
//
// Can be compiled and run with:
// g++ -g -Wall -Wextra -Wpedantic -std=c++11 -o rational_demo rational_demo.cc && ./rational_demo

#include <iostream>
#include "rational.h"

std::string boolstring(const bool& x){ return x ? "true" : "false"; }

int main ()
{
    std::cout << "Testing the Rational class:" << std::endl << std::endl;
    // Define some numbers
    long int a = 4, b = 6, c = -5, d = -12;

    // Create `Rational`s, the class is parametrized, but will only work with integer types
    // such as `int`, `long int` etc. We use `long int` here for fun
    Rational<long int> A(a, b);
    Rational<long int> B(c, d);

    // Unary operators
    std::cout << "Unary operators:" << std::endl;
    std::cout << "A = " << A << ", +A = " << +A << std::endl;
    std::cout << "A = " << A << ", -A = " << -A << std::endl << std::endl;

    // Binary operators between rationals
    std::cout << "Binary operators:" << std::endl;
    std::cout << A << " + " << B << " = " << A + B << std::endl;
    std::cout << A << " - " << B << " = " << A - B << std::endl;
    std::cout << A << " * " << B << " = " << A * B << std::endl;
    std::cout << A << " / " << B << " = " << A / B << std::endl << std::endl;

    // Boolean operators between rationals
    std::cout << "Boolean operators:" << std::endl;
    std::cout << A << " == " << B << " = " << boolstring(A == B) << std::endl;
    std::cout << A << " != " << B << " = " << boolstring(A != B) << std::endl;
    std::cout << A << " < " << B << " = " << boolstring(A < B) << std::endl;
    std::cout << A << " > " << B << " = " << boolstring(A >= B) << std::endl;
    std::cout << A << " <= " << B << " = " << boolstring(A <= B) << std::endl;
    std::cout << A << " >= " << B << " = " << boolstring(A >= B) << std::endl << std::endl;

    // Operators also work between T and Rational<T> by promoting
    std::cout << "Operators between T and Rational<T>:" << std::endl;
    std::cout << a << " + " << B << " = " << a + B << std::endl;
    std::cout << a << " - " << B << " = " << a - B << std::endl;
    std::cout << a << " * " << B << " = " << a * B << std::endl;
    std::cout << a << " / " << B << " = " << a / B << std::endl;

    return 0;
}
