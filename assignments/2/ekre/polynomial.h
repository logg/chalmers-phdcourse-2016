// Defines a small class `Polynomial` to represent and work with polynomials
// The class only works with positive exponents
// Fredrik Ekre, 2016

#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include <iostream>
#include <vector>

template<typename T>
class Polynomial{
public:
    typedef long int Int64;
    typedef unsigned long int UInt64;
    // constructor
    Polynomial<T>(std::vector<T> coefficients) : _coefficients(coefficients){
        // do nothing
    }
    Polynomial<T>() : _coefficients({T(0)}){
        // do nothing
    }
    Polynomial<T>(T c) : _coefficients({c}){
        // do nothing
    }

    // getter
    std::vector<T> coefficients() const { return _coefficients; }

    // unary operators: +, -
    Polynomial<T> operator+() const { return *this; }
    Polynomial<T> operator-() const {
        auto dthis = degree();
        Polynomial<T> p(std::vector<T> (dthis + 1));
        for (UInt64 d = 0; d <= dthis; d++){ // loop over all degrees
            p.setindex(d, -getindex(d));
        }
        return p;
    }

    // binary operators: +, -, *
    Polynomial<T> operator+(const Polynomial<T>& other) const {
        auto dthis = degree();
        auto dother = other.degree();
        if (dother > dthis){ return other + *this; } // hehe

        Polynomial<T> p(std::vector<T> (dthis + 1));
        for (UInt64 d = 0; d <= dthis; d++){ // loop over all degrees
            if (d > dother){
                p.setindex(d, getindex(d));
            }
            else {
                p.setindex(d, getindex(d) + other.getindex(d));
            }
        }
        return p;
    }
    Polynomial<T> operator-(const Polynomial<T>& other) const {
        return *this + (-other);
    }
    Polynomial<T> operator*(const Polynomial<T>& other) const {
        Int64 dthis = degree();
        Int64 dother = other.degree();

        Polynomial<T> p(std::vector<T> (dthis + dother + 1));
        for (Int64 di = 0; di <= dthis; di++){ // loop over all degrees
            for (Int64 dj = 0; dj <= dother; dj++){
                p.setindex(di + dj, getindex(di) * other.getindex(dj));
            }
        }
        return p;
    }

    // differentiate
    void differentiate(){
        for (UInt64 d = 1; d <= degree(); d++){
            _coefficients[degree_to_idx(d)] *= d;
        }
        _coefficients.resize(_coefficients.size() - 1); // cut off the constant term
    }

    // integrate
    void integrate(T c = T()){
        _coefficients.resize(_coefficients.size() + 1); // add the constant term
        setindex(0, c);
        for (UInt64 d = 1; d <= degree(); d++){
            _coefficients[degree_to_idx(d)] /= d;
        }
    }

    // evaluation
    T operator()(const T& x) const {
        // Horner's method
        T y = _coefficients[0];
        for (UInt64 i = 1; i < _coefficients.size(); i++){
            y *= x;
            y += _coefficients[i];
        }
        return y;
    }

    // some helper functions
    UInt64 degree() const {
        return _coefficients.size()-1;
    }
    void setindex(const Int64 deg, const T value){
        _coefficients[degree_to_idx(deg)] += value;
    }
    T getindex(const Int64 deg) const {
        return _coefficients[degree_to_idx(deg)];
    }
    UInt64 degree_to_idx(const Int64 deg) const {
        return degree() - deg;
    }

private:
    std::vector<T> _coefficients; // first index for highest degree
};

// Promotion to polynomial, for `T op Polynomial<T>`
template<typename T>
Polynomial<T> operator+(const T& a, const Polynomial<T>& b){ return Polynomial<T>(a) + b; }
template<typename T>
Polynomial<T> operator-(const T& a, const Polynomial<T>& b){ return Polynomial<T>(a) - b; }
template<typename T>
Polynomial<T> operator*(const T& a, const Polynomial<T>& b){ return Polynomial<T>(a) * b; }

template<typename T>
std::ostream& operator<<(std::ostream& os, const Polynomial<T>& P)
{
    auto coefficients = P.coefficients();
    auto deg = P.degree();
    os << "(";

    // send first coefficient
    if (coefficients[0] < T(0)){ os << "- " << -coefficients[0]; }
    else if (coefficients[0] == T(0)){ os << T(0) << ")"; return os; }
    else { os << coefficients[0]; }
    if (deg > 1){ os << "x^" << deg; }
    else if (deg != 0){os << "x"; }

    // send rest of the coefficients to the stream
    for (long int d = deg-1; d >= 0; d--){
        auto idx = P.degree_to_idx(d);
        auto val = coefficients[idx];
        if (val == T(0)){ continue; } // Should not compare like this but meh...

        if (val < T(0)){
            os << " - " << -val;
        }
        else {
            os << " + " << val;
        }
        if (d > 1){ os << "x^" << d; }
        else if (d != 0){os << "x"; }
    }

    os << ")";
    return os;
}

#endif
