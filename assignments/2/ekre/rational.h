// Defines a small class `Rational` to represent and work with rational numbers
// Fredrik Ekre, 2016

#ifndef RATIONAL_H
#define RATIONAL_H

#include <iostream>

template<typename T>
class Rational{
public:
    // Constructors
    // Guarantees a simplified result, with positive denominator
    // This property is important for other functions (eg. comparison operators etc.)
    Rational<T>() : _num(T(0)), _den(T(1)) {
        // No need to simplify
    }
    Rational<T>(T num) : _num(num), _den(T(1)) {
        // No need to simplify
    }
    Rational<T>(T num, T den) : _num(num), _den(den) {
        // checks that denominator is not 0 and simplifies
        _simplify();
    }

    // Getters
    T numerator() const { return _num; }
    T denominator() const { return _den; }

    // Unary operators: +, -
    Rational<T> operator+() const { return *this; }
    Rational<T> operator-() const { return Rational<T>(-_num, _den); }

    // Binary operators: +, -, *, /
    Rational<T> operator+(const Rational<T>& b) const {
        return Rational<T>(_num * b._den + _den * b._num, _den * b._den);
    }
    Rational<T> operator-(const Rational<T>& b) const {
        return Rational<T>(_num * b._den - _den * b._num, _den * b._den);
    }
    Rational<T> operator*(const Rational<T>& b) const {
        return Rational<T>(_num * b._num, _den * b._den);
    }
    Rational<T> operator/(const Rational<T>& b) const {
        return Rational<T>(_num * b._den, _den * b._num);
    }

    // Modifiers
    void operator+=(const Rational<T>& b){
        _num = _num * b._den + _den * b._num;
        _den = _den * b._den;
        _simplify();
        return;
    }
    void operator-=(const Rational<T>& b){
        _num = _num * b._den - _den * b._num;
        _den = _den * b._den;
        _simplify();
        return;
    }
    void operator*=(const Rational<T>& b){
        _num = _num * b._num;
        _den = _den * b._den;
        _simplify();
        return;
    }
    void operator/=(const Rational<T>& b){
        _num = _num * b._den;
        _den = _den * b._num;
        _simplify();
        return;
    }

    // Boolean operators: ==, !=, <, >, <=, >=
    // Only works since we guarantee the numbers are simplified
    bool operator==(const Rational<T>& b) const { return _num == b._num && _den == b._den; }
    bool operator!=(const Rational<T>& b) const { return !(*this == b); }
    bool operator< (const Rational<T>& b) const { return _num * b._den < _den * b._num; }
    bool operator> (const Rational<T>& b) const { return _num * b._den > _den * b._num; }
    bool operator<=(const Rational<T>& b) const { return !(*this > b); }
    bool operator>=(const Rational<T>& b) const { return !(*this < b); }

private:
    T _num; // Numerator
    T _den; // Denominator

    // Greatest common divisor
    // http://codereview.stackexchange.com/a/66735
    T _gcd(T num, T den) const {
        return den == 0 ? num : _gcd(den, num % den);
    }
    void _simplify(){
        if (_den == 0){
            throw std::invalid_argument("denominator is zero");
        }
        T gcd = _gcd(_num, _den);
        _num /= gcd;
        _den /= gcd;
        if (_den < T(0)){ // flip sign so denominator is always positive
            _num = -_num;
            _den = -_den;
        }
    }
}; // end class

// Promotion to rational, for `T op Rational<T>`
template<typename T>
Rational<T> operator+(const T& a, const Rational<T>& b){ return Rational<T>(a) + b; }
template<typename T>
Rational<T> operator-(const T& a, const Rational<T>& b){ return Rational<T>(a) - b; }
template<typename T>
Rational<T> operator*(const T& a, const Rational<T>& b){ return Rational<T>(a) * b; }
template<typename T>
Rational<T> operator/(const T& a, const Rational<T>& b){ return Rational<T>(a) / b; }
template<typename T>
bool operator==(const T& a, const Rational<T>& b){ return Rational<T>(a) == b; }
template<typename T>
bool operator!=(const T& a, const Rational<T>& b){ return Rational<T>(a) != b; }
template<typename T>
bool operator<(const T& a, const Rational<T>& b){ return Rational<T>(a) < b; }
template<typename T>
bool operator>(const T& a, const Rational<T>& b){ return Rational<T>(a) > b; }
template<typename T>
bool operator<=(const T& a, const Rational<T>& b){ return Rational<T>(a) <= b; }
template<typename T>
bool operator>=(const T& a, const Rational<T>& b){ return Rational<T>(a) >= b; }

template<typename T>
std::ostream& operator<<(std::ostream& os, const Rational<T>& Q){
    // Show a double // to separate rational from division, like in Julia :)
    os << Q.numerator() << "//" << Q.denominator();
    return os;
}

#endif
