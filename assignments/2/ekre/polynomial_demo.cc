// Demonstrates the use of the `Polynomial` class, defined in "rational.h"
// written by Fredrik Ekre
//
// Can be compiled and run with:
// g++ -g -Wall -Wextra -Wpedantic -std=c++11 -o polynomial_demo polynomial_demo.cc && ./polynomial_demo

#include <iostream>
#include <vector>
#include "rational.h"
#include "polynomial.h"

int main ()
{
    // Create vectors with coefficients
    std::vector<double> doubles1 = {1.0, 2.0, 3.0};
    std::vector<double> doubles2 = {4.0, 5.0};

    std::vector< Rational<long int> > rationals1 = {Rational<long int>(1, 2), Rational<long int>(1, 3), Rational<long int>(1, 4)};
    std::vector< Rational<long int> > rationals2 = {Rational<long int>(1, 5), Rational<long int>(1, 6), Rational<long int>(1, 7)};

    // Create `Polynomials`s, the class is parametrized, so we create a
    // Polynomial<double> and one Polynomial< Rational<long int> >
    // such as `int`, `long int` etc. We use `long int` here for fun
    Polynomial<double> Ad (doubles1), Bd (doubles2);
    Polynomial< Rational<long int> > Ar (rationals1), Br (rationals2);

    // Unary operators
    std::cout << "Unary operators: +, -" << std::endl;
    std::cout << "A = " << Ad << ", +A = " << +Ad << std::endl;
    std::cout << "A = " << Ad << ", -A = " << -Ad << std::endl;
    std::cout << "A = " << Ar << ", +A = " << +Ar << std::endl;
    std::cout << "A = " << Ar << ", -A = " << -Ar << std::endl << std::endl;

    // Binary operators between rationals
    std::cout << "Binary operators: +, -, *" << std::endl;
    std::cout << Ad << " + " << Bd << " = " << Ad + Bd << std::endl;
    std::cout << Ad << " - " << Bd << " = " << Ad - Bd << std::endl;
    std::cout << Ad << " * " << Bd << " = " << Ad * Bd << std::endl;
    std::cout << Ar << " + " << Br << " = " << Ar + Br << std::endl;
    std::cout << Ar << " - " << Br << " = " << Ar - Br << std::endl;
    std::cout << Ar << " * " << Br << " = " << Ar * Br << std::endl << std::endl;


    // Differentiation
    std::cout << "Differentiation:" << std::endl;
    std::cout << "A = " << Ad << std::endl;
    Ad.differentiate();
    std::cout << "dA/dx = " << Ad << std::endl;
    std::cout << "B = " << Br << std::endl;
    Br.differentiate();
    std::cout << "dB/dx = " << Br << std::endl << std::endl;

    // Integration, with optional integration constant
    double c = 3;
    std::cout << "Integration:" << std::endl;
    std::cout << "A = " << Ad << std::endl;
    Ad.integrate(c);
    std::cout << "int(A) = " << Ad << " // with integration constant c = " << c << std::endl;
    std::cout << "B = " << Br << std::endl;
    Br.integrate();
    std::cout << "int(B) = " << Br << std::endl << std::endl;

    // Evaluation
    std::cout << "Evaluation:" << std::endl;
    double xd = 2.5;
    Rational<long int> xr(1, 3);
    std::cout << "A(x) = " << Ad << std::endl;
    std::cout << "A(x = " << xd <<") = " << Ad(xd) << std::endl;
    std::cout << "B(x) = " << Br << std::endl;
    std::cout << "B(x = " << xr <<") = " << Br(xr) << std::endl;

    return 0;
}
