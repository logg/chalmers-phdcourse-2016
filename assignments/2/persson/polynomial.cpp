#include "polynomial.h"
#include <algorithm>
#include <cmath>

using namespace std;

template <class T>
Polynomial<T>::Polynomial(vector<T> v) : _v(v) { normalize(); }

template <class T>
vector<T> Polynomial<T>::get_coefficients() const { return _v; }

template <class T>
Polynomial<T> Polynomial<T>::operator+(const Polynomial& b) const
{
  vector<T> c_v(_v);
  int _n = _v.size();
  vector<T> b_v = b.get_coefficients();
  int b_n = b_v.size();

  int m = min(_n, b_n);
  for (int i = 0; i < m; i++)
    c_v[i] = c_v[i] + b_v[i];

  for (int i = m; i < b_n; i++)
      c_v.push_back(b_v[i]);

  return Polynomial(c_v);
}

template <class T>
Polynomial<T> Polynomial<T>::operator-(const Polynomial& b) const
{
  vector<T> b_v = b.get_coefficients();
  for (int i = 0; i < b_v.size(); i++)
    b_v[i] = b_v[i]*(-1);

  return *this + Polynomial(b_v);
}

template <class T>
Polynomial<T> Polynomial<T>::operator*(const Polynomial& b) const
{
  vector<T> b_v = b.get_coefficients();
  int b_n = b_v.size();
  int _n = _v.size();
  vector<T> c_v(_n*b_n);

  for (int i = 0; i < _n; i++)
    for (int j = 0; j < b_n; j++)
      c_v[i+j] = _v[i]*b_v[j] + c_v[i+j];

  return Polynomial(c_v);
}

template <class T>
double Polynomial<T>::operator()(const double& x) const
{
  double y = 0.0;
  int _n = _v.size();

  for (int i = 0; i < _n; i++)
  {
    double r = (double) _v[i];
    y += r*pow(x, i);
  }

  return y;
}

template <class T>
ostream& Polynomial<T>::print(ostream& output) const
{
  int _n = _v.size();

  output << _v[0];
  for (int i = 1; i < _n; i++)
    output << " + " << _v[i] << "x^" << i;

  return output;
}

template <class T>
void Polynomial<T>::differentiate()
{
  int _n = _v.size();

  for(int i = 0; i < _n - 1; i++)
    _v[i] = _v[i + 1]*(i+1);

  _v.pop_back();
}

template <class T>
void Polynomial<T>::integrate()
{
  int _n = _v.size();

  for(int i = 0; i < _n; i++)
    _v[i] = _v[i]/(i + 1);

  _v.emplace(_v.begin(), T(0));
}

template <class T>
void Polynomial<T>::normalize()
{
  auto it = find_if(_v.rbegin(),_v.rend(), [](const T& q) -> bool { return q != T(0);});
  _v.erase(it.base(), _v.end());
}

template class Polynomial<Rational>;
template class Polynomial<double>;
