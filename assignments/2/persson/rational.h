#pragma once
# include <iostream>

class Rational
{
  private:
    int _p, _q;
    int GCD(int u, int v) const;
    void normalize();

  public:
    Rational (int p, int q);
    Rational (int p);
    Rational ();
    int get_numerator() const;
    int get_denominator() const;
    Rational operator+(const Rational& x) const;
    Rational operator+(const int& x) const;
    Rational operator-(const Rational& x) const;
    Rational operator-(const int& x) const;
    Rational operator*(const Rational& x) const;
    Rational operator*(const int& x) const;
    Rational operator/(const Rational& x) const;
    Rational operator/(const int& x) const;
    bool operator==(const Rational& x) const;
    bool operator!=(const Rational& x) const;
    bool operator<(const Rational& x) const;
    bool operator<=(const Rational& x) const;
    bool operator>(const Rational& x) const;
    bool operator>=(const Rational& x) const;
    operator double() const;
    std::ostream& print(std::ostream& output) const;
    friend std::ostream& operator<<(std::ostream& output, const Rational& x)
    {
      return x.print(output);
    }
    friend Rational operator*(const int& i, const Rational& x) {return x*i;}
    friend Rational operator/(const int& i, const Rational& x) {return x/i;}
};
