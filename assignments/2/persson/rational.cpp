#include "rational.h"
#include <cmath>
using namespace std;

Rational::Rational (int p, int q) : _p(p), _q(q)
{
  if(_q == 0)
    cout << "Error: Denominator is zero" << endl;
  normalize();
}
Rational::Rational (int p) : _p(p), _q(1) {}
Rational::Rational () : _p(0), _q(1) {}
int Rational::get_numerator() const { return _p; }
int Rational::get_denominator() const { return _q;}

Rational Rational::operator+(const Rational& x) const
{
  int p,q;
  int x_p = x.get_numerator();
  int x_q = x.get_denominator();

  p = _p * x_q + x_p * _q; q = _q * x_q;
  return Rational(p, q);;
}

Rational Rational::operator+(const int& i) const
{
  Rational b(i, 1);

  return *this + b;
}

Rational Rational::operator-(const Rational& x) const
{
  int p,q;
  int x_p = x.get_numerator();
  int x_q = x.get_denominator();

  Rational tmp_x(-x_p, x_q);

  return *this + tmp_x;
}

Rational Rational::operator-(const int& i) const
{
  Rational b(i, 1);

  return *this - b;
}

Rational Rational::operator*(const Rational& x) const
{
  int p,q;
  int x_p = x.get_numerator();
  int x_q = x.get_denominator();

  p = _p * x_p; q = _q * x_q;

  return Rational(p, q);;
}

Rational Rational::operator*(const int& x) const
{
  Rational r(x, 1);
  return *this * r;
}

Rational Rational::operator/(const Rational& x) const
{
  int p,q;
  int x_p = x.get_numerator();
  int x_q = x.get_denominator();

  Rational tmp_x(x_q,x_p);
  return *this*tmp_x;
}

Rational Rational::operator/(const int& x) const
{
  Rational tmp_x(x, 1);
  return *this/tmp_x;
}

bool Rational::operator==(const Rational& x) const
{
  int x_p = x.get_numerator();
  int x_q = x.get_denominator();
  int u = GCD(_p, _q); int u_x = GCD(x_p, x_q);

  return (_p/u == x_p/u_x && _q/u == x_q/u_x);
}

bool Rational::operator!=(const Rational& x) const { return !(*this == x); }

bool Rational::operator<(const Rational& x) const
{
  int x_p = x.get_numerator();
  int x_q = x.get_denominator();

  return (_p*x_q-x_p*_q<0);
}

bool Rational::operator<=(const Rational& x) const
{
  return (*this < x || *this == x);
}

bool Rational::operator>(const Rational& x) const { return !(*this < x); }

bool Rational::operator>=(const Rational& x) const
{
  return (*this > x || *this == x);
}

Rational::operator double() const
{
  double p = (double) _p;
  double q = (double) _q;
  return p/q;
}

ostream& Rational::print(ostream& output) const
{
  return output << _p << "/" << _q;
}

int Rational::GCD(int u, int v) const
{
  while(v != 0) {
    int r = u % v;
    u = v;
    v = r;
  }
  return u;
}

void Rational::normalize()
{
  int u = GCD(abs(_p),abs(_q));
  _p = _p/u;
  _q = _q/u;
}
