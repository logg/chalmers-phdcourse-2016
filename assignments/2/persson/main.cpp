#include "rational.h"
#include "polynomial.h"

int main()
{
  //Create some rational numbers
  Rational a(1, 5);
  Rational b(-2, 8);
  Rational c(0, 1);

  //Basic algebra
  Rational d = a + b;
  Rational e = a * b;
  Rational f = a / b;

  //Print
  std::cout << b << " " << d <<" "<< e << " "<< f << std::endl;

  //Basic comparisons
  bool g = (a == b);
  bool h = (a >= b);
  std::cout << g <<" "<< h << std::endl;

  //Create vectors with Rationals
  std::vector<Rational> v_p = {a, b};
  std::vector<Rational> v_q = {a, b, c, a, c, c, c, c};
  Polynomial<Rational> p(v_p);
  Polynomial<Rational> q(v_q); //Many leading zeroes

  //Print
  std::cout << p << std::endl;
  std::cout << q << std::endl;

  //Basis algebra
  Polynomial<Rational> z = p + q;
  Polynomial<Rational> r = p * q;
  std::cout << z << std::endl;
  std::cout << r << std::endl;

  //Other features
  double y = p(0.0);
  p.differentiate();
  std::cout << y << std::endl;
  std::cout << p << std::endl;

  //Polynomial with doubles
  std::vector<double> v_s = {2.0, 3.63, 0.0, -4.0};
  Polynomial<double> s(v_s);
  std::cout << s << std::endl;

  return 0;
}
