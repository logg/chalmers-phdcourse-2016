#pragma once
#include "rational.h"
#include <iostream>
#include <vector>

template <class T>
class Polynomial
{
  private:
    std::vector<T> _v;
    void normalize();

  public:
    Polynomial(std::vector<T> v);
    std::vector<T> get_coefficients() const;
    Polynomial operator+(const Polynomial& b) const;
    Polynomial operator-(const Polynomial& b) const;
    Polynomial operator*(const Polynomial& b) const;
    double operator()(const double& x) const;
    std::ostream& print(std::ostream& output) const;
    void differentiate();
    void integrate();

    friend std::ostream& operator<<(std::ostream& output, const Polynomial& p)
    {
      return p.print(output);
    }
};
