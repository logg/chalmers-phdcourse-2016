// Test of Rational class
#include <iostream>
#include "Rational.cpp"
#include "Polynomial.cpp"

using namespace std;

int main()
{
// Test for Rational
	
	cout << "Test for Rational" << endl;
	
	Rational a(-3, 9);
	Rational b(1, 2);
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "a + b = " << a + b << endl;
	cout << "a - b = " << a - b << endl;
	cout << "a*b = " << a*b << endl;
	cout << "a/b = " << a/b << endl;
	cout << "a = b is " << boolalpha << (a == b) << endl;
	cout << "a != b is " << boolalpha << (a != b) << endl;
	cout << "a < b is " << boolalpha << (a < b) << endl;
	cout << "a > b is " << boolalpha << (a > b) << endl;
	cout << "a <= b is " << boolalpha << (a <= b) << endl;
	cout << "a >= b is " << boolalpha << (a >= b) << endl;
	cout << "b^4 = " << b.pow(4) << endl;
	
// Test for Polynomial
	
	cout << "Test for Polynomial" << endl;
		
	Rational a0(1, 3);
	Rational a1(-3, 2);
	Rational a2(2, 1);
	Rational b0(2, 3);
	Rational b1(3, 2);
	Rational b2(3, 1);
	vector<Rational> pkoeff;
	pkoeff.push_back(a0);
	pkoeff.push_back(a1);
	pkoeff.push_back(a2);
	Polynomial p(pkoeff);
	vector<Rational> qkoeff;
	qkoeff.push_back(b0);
	qkoeff.push_back(b1);
	qkoeff.push_back(b2);
	Polynomial q(qkoeff);
	cout << "p(x) = " << p << endl;
	cout << "q(x) = " << q << endl;
	cout << "p(x) + q(x) = " << p + q << endl;
	cout << "p(x) - q(x) = " << p - q << endl;
	cout << "p(x)*q(x) = " << p*q << endl;
	cout << "p(1/3) = " << p(a0) << endl;
	cout << "p'(x) = " << p.differentiate() << endl;
	cout << "P(x) = " << p.integrate(a2) << endl;
	return 0;
}
