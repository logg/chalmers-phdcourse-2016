#include <iostream>
#include <vector>
#include <stdlib.h> 

using namespace std;

class Rational{
	
	public:
		
		int p;
		int q;
		
		friend ostream& operator<<(ostream& os, Rational b);
		
		// Constructor
		Rational(int p, int q)
		{
			if ( q > 0){
				_p = p;
				_q = q;
			}
			else if (q < 0){
				_p = -p;
				_q = -q;
			}
			// Throw error if q = 0
			else{
				cout << "Warning! Division by zero!" << endl;
				throw ("Warning! Division by zero!"); 
				
			}
			simplify();
		}
		
		void show()
		{
			if ((_q != 1) && (_p != 0))
				cout<< _p<<"/"<<_q;
			else
				cout<<_p;
		}
		
		// Simplify the quotient
		void simplify()
		{
			for(int k = min(abs(_p), abs(_q)); k > 1; k--)
				
				if ((_p %k == 0) && (_q %k == 0))
				{
					_p = _p/k;
					_q = _q/k;
				}
			
		}
		
		// Addition
		Rational operator+(Rational b) const
		{
			Rational a(_p*b._q + _q*b._p, _q*b._q);
			return a;
		}
		
		// Subtraction
		Rational operator-(Rational b) const
		{
			Rational a(_p*b._q - _q*b._p, _q*b._q);
			return a;
		}
		
		// Multiplication
		Rational operator*(Rational b) const
		{
			Rational a(_p*b._p, _q*b._q);
			return a;
		}
		
		// Division
		Rational operator/(Rational b) const
		{
			Rational a(_p*b._q, _q*b._p);
			return a;
		}
		
		// Boolean operators
		bool operator==(Rational b) const
		{
			if (_p*b._q - _q*b._p == 0)
				return true;
			else
				return false;
		}
		
		bool operator!=(Rational b) const
		{
			if (_p*b._q - _q*b._p == 0)
				return false;
			else
				return true;
		}
		
		bool operator<(Rational b) const
		{
			if (_p*b._q - _q*b._p < 0)
				return true;
			else
				return false;
		}
		
		bool operator>(Rational b) const
		{
			if (_p*b._q - _q*b._p > 0)
				return true;
			else
				return false;
		}
		
		bool operator<=(Rational b) const
		{
			if (_p*b._q - _q*b._p <= 0)
				return true;
			else
				return false;
		}
		
		bool operator>=(Rational b) const
		{
			if (_p*b._q - _q*b._p >= 0)
				return true;
			else
				return false;
		}
		
		// Power
		Rational pow(int n) const
		{
			int ptemp = 1;
			int qtemp = 1;
			for(int i = 1; i <= n; i++)
			{
				ptemp *= _p;
				qtemp *= _q;
			}
			Rational a(ptemp, qtemp);
			return a;
		}
		
	private:
		
		int _p;
		int _q;
			
};

// Printing rational number
ostream& operator<<(ostream& os, Rational b)
{
	if ((b._q != 1) && (b._p != 0))
		return os<< b._p<<"/"<< b._q;
	else
		return os<< b._p;
}
