#include <iostream>
#include <vector>
//#include "Rational.cpp"

using namespace std;

class Polynomial{
	
		public:
		
		friend ostream& operator<<(ostream& os, Polynomial poly);
		
		// Constructor
		Polynomial(vector<Rational> p)
		{
			_p = p;
			_deg = p.size() - 1;
		}
		
		int degree()
		{
			return _p.size() - 1;
		}
		
		// Addition
		Polynomial operator+(Polynomial poly)
		{
			int deg = max(_deg, poly.degree());
			vector<Rational> p;
			
			for(int i = 0; i<= deg; i++)
			{
				Rational koeff(_p[i] + poly._p[i]);
				p.push_back(koeff);
			}
			Polynomial q(p);
			return q;
		}
		
		// Subtraction
		Polynomial operator-(Polynomial poly)
		{
			int deg = max(_deg, poly.degree());
			vector<Rational> p;
			
			for(int i = 0; i<= deg; i++)
			{
				Rational koeff(_p[i] - poly._p[i]);
				p.push_back(koeff);
			}
			Polynomial q(p);
			return q;
		}
		
		// Multiplication
		Polynomial operator*(Polynomial poly) const
		{
			int deg = _deg + poly.degree();
			vector<Rational> koeff;
			Rational zero(0, 1);
			
			for(int i = 0; i <= deg; i++)
			{
				koeff.push_back(zero);
			}
			
			for(int i = 0; i <= _deg; i++)
			{
				for(int j = 0; j <= poly.degree(); j++)
				{
					koeff[i + j] = koeff[i + j] + _p[i]*poly._p[j];
				}
			}
			Polynomial q(koeff);
			return q;
		}
		
		// Evaluation
		Rational operator()(Rational b) const
		{
			Rational r(0, 1);
			
			for(int i = 0; i <= _deg; i++)
			{
				r = r + _p[i]*b.pow(i);
			}
			
			return r;
		}
		
		// Differentiation
		Polynomial differentiate() const
		{
			vector<Rational> koeff;
			
			for(int i = 1; i <= _deg; i++)
			{
				Rational pot(i, 1);
				koeff.push_back(_p[i]*pot);
			}
			Polynomial q(koeff);
			return q;
		}
		
		// Primitive function with constant equal to C
		Polynomial integrate(Rational C) const
		{
			vector<Rational> koeff;
			koeff.push_back(C);
			
			for(int i = 0; i <= _deg; i++)
			{
				Rational pot(i + 1, 1);
				koeff.push_back(_p[i]/pot);
			}
			Polynomial q(koeff);
			return q;
		}
		
		// Primitive function with constant equal to zero
		Polynomial integrate() const
		{
			vector<Rational> koeff;
			Rational zero(0, 1);
			koeff.push_back(zero);
			
			for(int i = 0; i <= _deg; i++)
			{
				Rational pot(i + 1, 1);
				koeff.push_back(_p[i]/pot);
			}
			Polynomial q(koeff);
			return q;
		}
		
		private:
		
		vector<Rational> _p;
		int _deg;
	
};

// Printing
ostream& operator<<(ostream& os, Polynomial poly)
{
	Rational zero(0, 1);
	Rational minus(-1, 1);
	
	if(poly._p[0] != zero)
	{
		os << poly._p[0];
	}
	for(int i = 1; i < poly.degree(); i++)
	{
		if(poly._p[i] > zero)
		{
			os << " + " << poly._p[i] << "*x^" << i;			
		}
		else if(poly._p[i] < zero)
		{
			os << " - " << poly._p[i]*minus << "*x^" << i;
		}
	}
	if(poly._p[poly.degree()] > zero)
	{
		os << " + " << poly._p[poly.degree()] << "*x^" << poly.degree();
	}
	else if(poly._p[poly.degree()] < zero)
	{
		os << " - " << poly._p[poly.degree()]*minus << "*x^" << poly.degree();
	}
	return os;
}

