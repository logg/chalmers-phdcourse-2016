//
// Created by kristoffer on 2016-12-08.
//

#ifndef MATHTOOLS_POLYNOMIAL_H_H
#define MATHTOOLS_POLYNOMIAL_H_H

#include <array>

namespace MathTools {

template <typename T> class Polynomial {
public:
  Polynomial<T>(std::vector<T> coefficients) : p(coefficients) {
    if (p.size() == 0)
      throw std::runtime_error(
          "coefficient vector must be at least of length 1");
  }

  int degree() const { return (this->p).size() - 1; }

  const std::vector<T> coefficients() const { return this->p; }

  // Returns the coefficient for the given degree
  T coefficient(int degree) const {
    if (degree < 0)
      throw std::runtime_error("degree must be >= 0");

    auto idx = this->degree() - degree;
    return idx < 0 ? T() : this->p[idx];
  }

  inline Polynomial<T> operator+(const Polynomial<T> p2) const {
    auto p1 = *this;
    auto max_degree = std::max(p1.degree(), p2.degree());
    std::vector<T> result(max_degree + 1);

    for (int degree = max_degree; degree >= 0; degree--) {
      result[max_degree - degree] =
          p1.coefficient(degree) + p2.coefficient(degree);
    }
    return Polynomial<T>(result);
  }

  inline Polynomial<T> operator-(const Polynomial<T> p2) const {
    auto p1 = *this;
    auto max_degree = std::max(p1.degree(), p2.degree());
    std::vector<T> result(max_degree + 1);

    for (int degree = max_degree; degree >= 0; degree--) {
      result[max_degree - degree] =
          p1.coefficient(degree) - p2.coefficient(degree);
    }
    return Polynomial<T>(result);
  }

  inline Polynomial<T> operator*(const Polynomial<T> p2) const {
    auto p1 = *this;
    auto new_degree = p1.degree() + p2.degree();
    std::vector<T> result(new_degree + 1);

    for (int i = p1.degree(); i >= 0; i--) {
      for (int j = p2.degree(); j >= 0; j--) {
        result[new_degree - (i + j)] = result[new_degree - (i + j)] +
                                       p1.coefficient(i) * p2.coefficient(j);
      }
    }

    return Polynomial<T>(result);
  }

  inline T operator()(const T x) const {
    auto s = this->coefficient(this->degree());
    // Use Horner's method
    for (int degree = this->degree() - 1; degree >= 0; degree--) {
      s = s * x + this->coefficient(degree);
    }
    return s;
  }

  inline void differentiate() {
    if (p.size() == 1) {
      p[0] = T();
      return;
    }
    for (int degree = this->degree(); degree >= 0; degree--) {
      p[idx(degree)] = coefficient(degree) * degree;
    }
    p.resize(p.size() - 1);
    return;
  }

  // Optional integration constant
  inline void integrate(T c = T()) {
    for (int degree = this->degree(); degree >= 0; degree--) {
      p[idx(degree)] = coefficient(degree) / (degree + 1);
    }
    p.resize(p.size() + 1);
    p[p.size() - 1] = c;
    return;
  }

private:
  std::vector<T> p;
  int idx(const int degree) const { return this->degree() - degree; }
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const Polynomial<T> &r) {
  auto coffs = r.coefficients();
  auto n = r.degree();
  if (n == 0) {
    return os << r.coefficient(0);
  }

  for (const auto &v : coffs) {
    if (v > 0 && n != r.degree())
      os << "+";
    if (v != 0) {
      os << v;
      if (n > 0)
        os << "x";
      if (n > 1)
        os << "^" << n;
    }
    n -= 1;
  }
  return os;
}
}

#endif // MATHTOOLS_POLYNOMIAL_H_H
