//
// Created by kristoffer on 2016-12-08.
//

#ifndef MATHTOOLS_RATIONAL_H
#define MATHTOOLS_RATIONAL_H

#include <tuple>

namespace MathTools {

template <typename T> class Rational {

public:
  Rational<T>(T p, T q) : vals(simplify(p, q)) {
    // Do nothing
  }

  Rational<T>() : vals(0, 1) {
    // Do nothing
  }

  Rational<T>(const int v) : vals(v, 1) {
    // Do nothing
  }

  Rational<T>(const Rational<T> &r2) : vals(r2.numerator(), r2.denominator()) {
    // Do nothing
  }

  std::pair<T, T> const simplify(T p, T q) {
    if (q == 0)
      throw std::runtime_error("denominator not allowed to be zero");

    if (p == 0)
      return std::pair<T, T>(0, 1);

    // Move minus to numerator or remove double minuses
    if ((q < 0 && p > 0) || (q < 0 && p < 0)) {
      q = -q;
      p = -p;
    }

    for (auto i = std::min(abs(p), abs(q)); i >= 1; i--) {
      if (p % i == 0 && q % i == 0) {
        p /= i;
        q /= i;
      }
    }
    return std::pair<T, T>(p, q);
  }

  inline T numerator() const { return vals.first; }

  inline T denominator() const { return vals.second; }

  // This is annoyingly verbose...
  inline Rational<T> operator+(const Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return Rational<T>(p1 * q2 + p2 * q1, q1 * q2);
  }

  inline Rational<T> operator-(const Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return Rational<T>(p1 * q2 - p2 * q1, q1 * q2);
  }

  inline Rational<T> operator-() const {
    T p1, q1;
    std::tie(p1, q1) = this->vals;
    return Rational<T>(-p1, q1);
  }

  inline Rational<T> operator+() const { return *this; }

  inline Rational<T> operator*(Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return Rational<T>(p1 * p2, q1 * q2);
  }

  inline Rational<T> operator/(Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return Rational<T>(p1 * q2, q1 * p2);
  }

  inline bool operator==(Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return p1 == p2 && q1 == q2;
  }

  inline bool operator!=(Rational<T> r2) const { return !(*this == r2); }

  inline bool operator<(Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return p1 * q2 < p2 * q1;
  }

  inline bool operator<=(Rational<T> r2) const {
    T p1, q1, p2, q2;
    std::tie(p1, q1) = this->vals;
    std::tie(p2, q2) = r2.vals;
    return p1 * q2 <= p2 * q1;
  }

  inline bool operator>(Rational<T> r2) const { return !(*this <= r2); }

  inline bool operator>=(Rational<T> r2) const { return !(*this < r2); }

private:
  std::pair<T, T> vals;
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const Rational<T> &r) {
  os << r.numerator();
  if (r.denominator() != 1)
    os << "/" << r.denominator();
  return os;
}
}

#endif // MATHTOOLS_RATIONAL_H
