#include <iostream>
#include <vector>

#include "Rational.h"
#include "Polynomial.h"

int main() {
    using R = MathTools::Rational<long int>;
    using P = MathTools::Polynomial<R>;

    std::cout << "Testing Rational class" << std::endl;
    auto a = R(15, 5);
    auto b = R(12, 5);
    std::cout << "a = " << a << "\n";
    std::cout << "b = " << b << "\n";
    std::cout << "a + b = " << a + b << "\n";
    std::cout << "a + 4 = " << a + 4 << "\n";
    std::cout << "4 + a = " << a + 4 << "\n";
    std::cout << "-a = " << -a << "\n";
    std::cout << "+a = " << +a << "\n";
    std::cout << "a - b = " << a - b << "\n";
    std::cout << "a * b = " << a * b << "\n";
    std::cout << "a / b = " << a / b << "\n";
    std::cout << "a < b = " << ((a < b) ? "true" : "false") << "\n";
    std::cout << "a > b = " << ((a > b) ? "true" : "false") << "\n";
    std::cout << "a <= b = " << ((a <= b) ? "true" : "false") << "\n";
    std::cout << "a >= b = " << ((a >= b) ? "true" : "false") << "\n";
    std::cout << "a == b = " << ((a == b) ? "true" : "false") << "\n";
    std::cout << "a != b = " << ((a != b) ? "true" : "false") << "\n";
    std::cout << std::flush;

    std::cout << "Testing Polynomial class" << "\n";

    std::cout << "Double parameter:" << "\n";
    std::vector<double> ps {2, -6, 2, -1};
    std::vector<double> ps2 {1, 3, 1};

    auto p1 = MathTools::Polynomial<double>(ps);
    auto p2 = MathTools::Polynomial<double>(ps2);

    std::cout << "p(x) = " << p1 << "\n";
    std::cout << "p(3) = " << p1(3) << "\n\n";
    std::cout << "p2(x) = " << p2 << "\n\n";
    std::cout << "p(x) + p2(x) = " << p1 + p2 << "\n";
    std::cout << "p(x) - p2(x) = " << p1 - p2 << "\n";
    std::cout << "p(x) * p2(x) = " << p1 * p2 << "\n";

    std::cout << "\nDifferentiation:" << "\n";
    std::cout << "p(x) = " << p1 << "\n";
    p1.differentiate();
    std::cout << "p'(x) = " << p1 << "\n";
    p1.differentiate();
    std::cout << "p''(x) = " << p1 << "\n";
    p1.differentiate();
    std::cout << "p'''(x) = " << p1 << "\n";
    p1.differentiate();
    std::cout << "p''''(x) = " << p1 << "\n";

    std::cout << "Integration:" << "\n";
    p1 = MathTools::Polynomial<double>(ps);
    std::cout << "p(x) = " << p1 << "\n";
    p1.integrate();
    std::cout << "P(x) = " << p1 << "\n";


    std::cout << "\nNow using rational parameters for coefficients:" << "\n";
    std::vector<R> psR {R(2,3), R(-2,5), R(1,3), R(2,1)};
    std::vector<R> psR2 {R(-4,6), R(-1,7), R(2,4), R(2,1)};

    auto p1R = P(psR);
    auto p2R = P(psR2);

    std::cout << "p(x) = " << p1R << "\n";
    std::cout << "p(3) = " << p1R(R(3,1)) << "\n\n";
    std::cout << "p2(x) = " << p2R << "\n\n";
    std::cout << "p(x) + p2(x) = " << p1R + p2R << "\n";
    std::cout << "p(x) - p2(x) = " << p1R - p2R << "\n";
    std::cout << "p(x) * p2(x) = " << p1R * p2R << "\n";

    std::cout << "\nDifferentiation:" << "\n";
    std::cout << "p(x) = " << p1R << "\n";
    p1R.differentiate();
    std::cout << "p'(x) = " << p1R << "\n";
    p1R.differentiate();
    std::cout << "p''(x) = " << p1R << "\n";
    p1R.differentiate();
    std::cout << "p'''(x) = " << p1R << "\n";
    std::cout << std::endl;

    std::cout << "Integration:" << "\n";
    p1R = P(psR);
    std::cout << "p(x) = " << p1R << "\n";
    p1R.integrate();
    std::cout << "P(x) = " << p1R;
    std::cout << std::flush;

    return 0;
}