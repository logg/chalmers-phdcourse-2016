#include <iostream>
using namespace std;

#include "Rational.h"
#include <cstring>

#include <vector>

class Polynomial {



    public:
        int degree;
            Rational *v;
    Polynomial(Rational c[], int a);
    void setvalues(Rational c[]);
    //int degree();
    friend ostream& operator<<(ostream& os, const Polynomial& pol);
    Polynomial operator+(Polynomial v);
    Polynomial operator-(Polynomial v);
    Polynomial operator*(Polynomial v);
    Polynomial operator/(Polynomial v);
    Rational operator()(Rational v);
    Polynomial derive();
    Polynomial integrate();




};

Polynomial Polynomial::integrate() {

    Rational null;
    null.set_values(0,1);

Rational der[degree+1];
der[0]=null;
for(int i=1; i<=degree+1;i++) {

    der[i]=v[i-1]/i;
}
Polynomial result(der, degree+1);

return result;
}

Polynomial Polynomial::derive() {
Rational der[degree];

for(int i=0; i<degree;i++){

    der[i]=v[i+1]*(i+1);
}
Polynomial result(der,degree-1);

return result;

}

Polynomial Polynomial:: operator/(Polynomial c) {

    Rational null;
    null.set_values(0,1);
    Rational arr[0];
    arr[0]=null;
    Rational unit;
    unit.set_values(1,1);


           int place=degree;
            Rational coeff[degree];
           for(int b=0;b<=degree;b++){


                coeff[b]=v[b];
            }
           Polynomial p(coeff,place);

           Rational div[degree-c.degree];
           for(int n=0;n<=degree-c.degree;n++){

            div[n]=null;

           }

while(p.degree>=c.degree){

        int u=p.degree-c.degree;
        Rational un[u];
        for(int y=0; y<u; y++){

            un[y]=null;
        }
        Rational q=p.v[p.degree]/c.v[c.degree];
        un[u]=q;

        div[p.degree-c.degree]=q;
        Polynomial x(un,u);
        Polynomial pro=x*c;
        p=p-pro;



}
Polynomial fin(div,degree-c.degree);



return fin;
}

Rational Polynomial::operator()(Rational s) {

Rational u;
u.set_values(0,1);
for(int i=degree;i>=0;i--){

    u=u*s+v[i];
}
return u;
}


Polynomial Polynomial::operator*(Polynomial s){
        Rational null;

    null.set_values(0,1);

    int newdeg=degree+s.degree;
    Rational f[degree+s.degree];
    for(int r=0;r<=degree+s.degree;r++){

        f[r]=null;
    }
    for(int k=0;k<=degree; k++){
            for(int i=0; i<=s.degree; i++){


            f[k+i]=f[k+i]+v[k]*s.v[i];
            }

    }


Polynomial sum(f,newdeg);

return sum;

}

Polynomial Polynomial::operator+(Polynomial s){
            Rational null;

    null.set_values(0,1);
    int newdeg=max(degree+1,s.degree+1);
    int d=min(degree+1,s.degree+1);
    Rational f[newdeg];
    for(int k=0;k<=d; k++){
            f[k]=v[k]+s.v[k];


    }
    for(int i=d; i<=newdeg; i++){
            if(degree<s.degree){

        f[i]=s.v[i];
            }

    else{
        f[i]=v[i];
    }
    }
        int stop=newdeg;
    while(f[stop]==null){
        stop=stop-1;



    }
    Rational g[stop];
    for(int m=0;m<=stop; m++){

    g[m]=f[m];
    }


Polynomial sum(g,newdeg-1);


return sum;

}
Polynomial Polynomial::operator-(Polynomial s){
        Rational null;

    null.set_values(0,1);
    int newdeg=max(degree+1,s.degree+1);
    int d=min(degree+1,s.degree+1);
    Rational f[newdeg];
    for(int k=0;k<d; k++){
            f[k]=v[k]-s.v[k];


    }
    for(int i=d; i<newdeg; i++){
        if(degree<s.degree){
        f[i]=null-s.v[i];
        }
        else{
            f[i]=v[i];

        }
    }
    int stop=newdeg;
    while(f[stop]==null){
        stop=stop-1;



    }
    Rational g[stop];
    for(int m=0;m<=stop; m++){

    g[m]=f[m];
    }


Polynomial sum(g,newdeg-1);

return sum;

}

Polynomial::Polynomial(Rational c[], int a) {

        Rational null;

    null.set_values(0,1);
while(c[a]==null){
    a=a-1;

}
degree=a;
v=new Rational[a+1];
for(int k=0; k<a+1; k++){
v[k]=c[k];


}

}

void Polynomial::setvalues(Rational c[]){

memcpy(c, v, sizeof(c));

}

//int Polynomial::degree(){

//return v.size();
//return 1;

//}

ostream& operator<<(ostream& os, const Polynomial& pol)  {
    Rational null;
    int i=0;
    null.set_values(0,1);

    for(int k=0; k<pol.degree+1; k++) {

    if(pol.v[k]!=null){

      i=1;
    }


      if(pol.v[k]!=null){


                   cout << pol.v[k];
                   if(k>0){


        cout  << "x^" << k;
                   }

              if(k<pol.degree){
              cout << "+" << endl;

      }

      }
    }


    if(i==0){
        cout << 0;
    }

cout << endl << endl;
}
