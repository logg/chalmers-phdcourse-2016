#include <iostream>
using namespace std;

class Rational {
    int _p, _q;
  public:
    void set_values (int,int);
//    int area () {return width*height;}

    void print();
    void simplify();
    void approximate(int);
    int p();
    int q();
    Rational operator +(Rational);
    Rational operator -(Rational);
    Rational operator *(Rational);
    Rational operator *(int);
    Rational operator /(Rational);
    Rational operator /(int);
    bool operator ==(Rational);
    bool operator !=(Rational);
    bool operator <(Rational);
    bool operator >(Rational);
    bool operator <=(Rational);
    bool operator >=(Rational);
    Rational operator^(int);
    friend ostream& operator<<(ostream& os, const Rational& rat);
};

int Rational::p() {

return _p;

}

int Rational::q() {

return _q;
}

void Rational::approximate(int c) {

if(10^c<_p&&10^c<_p){

    _p=_p/10^c;
    _q=_q/10^c;
    simplify();

}

}

Rational Rational::operator^(int s) {

Rational u;
u.set_values(1,1);
Rational v;
v.set_values(_p,_q);
for(int i=1; i<=s; i++) {
    u=u*v;


}
return u;



}


bool Rational::operator<(Rational c) {

return(_p*c._q-_q*c._p<0);

}

bool Rational::operator>(Rational c) {

return(_p*c._q-_q*c._p>0);

}

bool Rational::operator<=(Rational c) {

return(_p*c._q-_q*c._p<=0);

}

bool Rational::operator>=(Rational c) {

return(_p*c._q-_q*c._p>=0);

}

bool Rational::operator !=(Rational c) {

if(_p!=c._p||_q!=c._q){

    return true;
}
else{

    return false;
}
}

bool Rational::operator==(Rational c) {

if(_p==0&&c._p==0){

    return true;
}

else if(_p==c._p&&_q==c._q) {

    return true;
}
 else {

    return false;
 }
}

Rational Rational::operator*(Rational c) {

Rational result;

result.set_values(_p*c._p,_q*c._q);

return result;

}
Rational Rational::operator*(int c) {

Rational result;

result.set_values(_p*c,_q);

return result;

}

Rational Rational:: operator/(Rational c) {

Rational result;

result.set_values(_p*c._q,_q*c._p);

return result;

}

Rational Rational:: operator/(int c) {

Rational result;

result.set_values(_p,_q*c);

return result;

}

Rational Rational::operator-(Rational c) {
Rational result;
int a=_p*c._q-_q*c._p;
int b=_q*c._q;
result.set_values(a,b);


return result;

}
Rational Rational::operator+(Rational c) {
    Rational result;
    int a=_p*c._q+_q*c._p;
    int b=_q*c._q;
    result.set_values(a,b);

    return result;


}

void Rational::set_values (int x, int y) {

    if(y==0){

         throw;
    }
  _p = x;
  _q = y;
  simplify();
}

ostream& operator<<(ostream& os, const Rational& rat) {

    if(rat._q==1){
        os << rat._p;
    }
    else{
os << rat._p << "/" << rat._q;
    }
return os;
}

void Rational::simplify() {

    if(_q<0){

        _q=-_q;
        _p=-_p;

    }
    if(_p==0){

        _q=1;
    }
    if(_p!=0){

 //   int p2=_p;
 //   int q2=_q;
       for(int k=std::max(_p,_q); k>=2; k--)
        {

          if(_p%k==0 && _q%k==0)
           {
                _p=_p/k;
                _q=_q/k;
            }
        }
    }
}
