// Defines the polynomial class template
#include<iostream>
#include<vector>
using namespace std;

template<class T> class Polynomial;

template<class T>
ostream& operator<<(ostream& os, const Polynomial<T>& p);

template <class T>
class Polynomial
{
public:

  // Constructor
  Polynomial(vector<T>& coeff) : _coeff (coeff)
  {
    // Do nothing
  }

  unsigned int degree() const
  {
    unsigned int d = _coeff.size() - 1;
    return d; 
  }

  // Addition
  Polynomial<T> operator+(const Polynomial<T> q)
  {
    vector<T> sum_coeff;
    if (degree() >= q.degree())
      {
	sum_coeff = _coeff;
	for (int i = 0; i <= q.degree(); i++)
	  sum_coeff[i] = sum_coeff[i] + q._coeff[i];
      }
    else
      {
	sum_coeff = q._coeff;
	for (int i = 0; i <= degree(); i++)
	  sum_coeff[i] = sum_coeff[i] + _coeff[i];
      }
    Polynomial<T> r((sum_coeff));
    
    return r;
  }

  // Subtraction
  Polynomial<T> operator-(const Polynomial<T> q)
  {
    vector<T> diff_coeff;
    if (degree() >= q.degree())
      {
	diff_coeff = _coeff;
	for (int i = 0; i <= q.degree(); i++)
	  diff_coeff[i] = diff_coeff[i] - q._coeff[i];
      }
    else
      {
	diff_coeff.resize(q.degree() + 1);
	for (int i = 0; i <= degree(); i++)
	  diff_coeff[i] = _coeff[i] - q._coeff[i];
	for (int i = degree() + 1; i <= q.degree(); i++)
	  diff_coeff[i] = T() - q._coeff[i];
      }
    Polynomial<T> r((diff_coeff));
    
    return r;
  }

  // Multiplication
  Polynomial<T> operator*(const Polynomial<T> q)
  {
    vector<T> prod_coeff(degree() + q.degree() + 1, T());
    for (int i = 0; i <= degree(); i++)
      {
	for (int j = 0; j <= q.degree(); j++)
	  prod_coeff[i + j] = prod_coeff[i + j] + _coeff[i]*q._coeff[j];
      }
    Polynomial<T> r((prod_coeff));
    
    return r;
  }

  // Multiplication by scalar
  template<class U>
  friend Polynomial<U> operator*( U c, const Polynomial<U> P);
  
  // Evaluation
  T operator()(const T x)
  {
    T y = _coeff[0];
    T x_to_i = x;
    for (int i = 1; i <= degree(); i++)
      {
	y = y + _coeff[i]*x_to_i;
	x_to_i = x_to_i * x;
      }
    
    return y;
  }

  // Derivative
  Polynomial<T> derivative()
  {
    vector<T> deriv_coeff;
    deriv_coeff.resize(degree());
    for (int i = 0; i < degree(); i++)
      deriv_coeff[i] = (i + 1)*_coeff[i + 1];
    Polynomial<T> dpdx((deriv_coeff));
    
    return dpdx;
  }

  // Integral
  Polynomial<T> integral()
  {
    vector<T> integ_coeff;
    integ_coeff.resize(degree() + 2);
    integ_coeff[0] = T();
    for (int i = 0; i <= degree(); i++)
      integ_coeff[i + 1] = _coeff[i]/(i + 1);
    Polynomial<T> P((integ_coeff));

    return P;
  }
 
  // Print
  friend ostream& operator<< <> (ostream&, const Polynomial<T>&);
  
private:

  vector<T> _coeff;

};

// Multiplication by scalar
template <class T>
Polynomial<T> operator*(T c, const Polynomial<T> p)
{
  vector<T> scaled_coeff = p._coeff;
  for (int i = 0; i <= p.degree(); i++)
    scaled_coeff[i] = c * scaled_coeff[i];

  Polynomial<T> scaled_p((scaled_coeff));

  return scaled_p;
}

// Printing polynomials
template <class T>
ostream& operator<<(ostream& os, const Polynomial<T>& p)
{
  os << p._coeff[0];
  for (int i = 1; i <= p.degree(); i++)
    os << " + " <<  p._coeff[i] << " x^" << i;

  return os;
}
