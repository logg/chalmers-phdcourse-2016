#include<iostream>
#include<vector>
#include"rational.cpp"
#include"polynomial.cpp"

using namespace std;

string truename(bool x)
{
  string s;
  if (x)
    s = "true";
  else
    s = "false";

  return s;
}

int main()
{
  cout << "Example of rationals:" << endl;
  Rational r1(6, -18), r2(6, 11);
  int i = 3;
  cout << "r1 = " << r1 << ", r2 = " << r2 << ", and i = " << i << endl;
  cout << "r1 + r2 = " << r1 + r2 << endl;
  cout << " r1 + i = " << r1 + i << endl;
  cout << " i + r2 = " << i + r2 << endl;
  cout << "r2 - r1 = " << r2 - r1 << endl;
  cout << " r1 - i = " << r1 - i << endl;
  cout << " i - r2 = " << i - r2 << endl;
  cout << "r1 * r2 = " << r1 * r2 << endl;
  cout << " r1 * i = " << r1 * i << endl;
  cout << " i * r2 = " << i * r2 << endl;
  cout << "r2 / r1 = " << r2 / r1 << endl;
  cout << " r2 / i = " << r2 / i << endl;
  cout << " i / r1 = " << i / r1 << endl;
  cout << "   r1 == r2 is " << truename(r1 == r2) << endl;
  cout << "-1/r1 == i  is " << truename((-1)/r1 == i) << endl;
  cout << "    i == r2 is " << truename(i == r2) << endl;
  cout << "   r1 != r2 is " << truename(r1 != r2) << endl;
  cout << "   r1 != i  is " << truename(r1 != i) << endl;
  cout << "    i != r2 is " << truename(i != r2) << endl;
  cout << "   r1 < r2  is " << truename(r1 < r2) << endl;
  cout << "    i < r1  is " << truename(i < r1) << endl;
  cout << "   r2 < i   is " << truename(r2 < i) << endl;
  cout << "   r2 <= r1 is " << truename(r2 <= r2) << endl;
  cout << "    i <= r2 is " << truename(i <= r2) << endl;
  cout << "   r1 <= i  is " << truename(r1 <= i) << endl;
  cout << "   r2 > r1  is " << truename(r2 >= r1) << endl;
  cout << "   r1 > i   is " << truename(r1 >= i) << endl;
  cout << "    i > r2  is " << truename(i >= r2) << endl;
  cout << "   r2 >= r1 is " << truename(r2 >= r1) << endl;
  cout << "   r1 >= i  is " << truename(r1 >= i) << endl;
  cout << "    i >= r2 is " << truename(i >= r2) << endl;
  cout << "(i*r1 + r2/i - r1)/i == (i*r2 - r1/r2) is " << truename( (i*r1 + r2/i - r1)/i == (i*r2 - r1/r2)) << endl;
  
  cout << " " << endl;
  cout << "Example of polynomials over Q:" << endl;
  Rational onehalf(1, 2);
  Rational cp0(1, 2), cp1(3, 4);
  Rational cq0(4, 3), cq1(-2, 1), cq2(1, 2);
  Rational cpa[] = {cp0, cp1};
  Rational cqa[] = {cq0, cq1, cq2};
  vector<Rational> cp(cpa, cpa + sizeof(cpa)/sizeof(cpa[0]));
  vector<Rational> cq(cqa, cqa + sizeof(cqa)/sizeof(cqa[0]));
  Polynomial<Rational> p((cp)), q((cq));
  Rational x(0, 1);
  cout << "p(x) = " << p << endl;
  cout << "q(x) = " << q << endl;
  cout << "(1/2)*p(x) + q(x) = " << onehalf*p + q << endl;
  cout << "q(x) - p(x) = " << p - q << endl;
  cout << "p(x) * q(x) = " << p * q << endl;
  cout << "p(q(" << x << ")) = " << p(q(x)) << endl;
  cout << "p'(x) = " << p.derivative() << endl;
  cout << "P(x) = " << p.integral() << endl;

  cout << " " << endl;
  cout << "Example of polynomials over R:" << endl;
  double cr0 = 1.23, cr1 = -3.98, cr2 = 0.09;
  double cs0 = 0.00, cs1 = 2.73;
  double cra[] = {cr0, cr1, cr2};
  double csa[] = {cs0, cs1};
  vector<double> cr(cra, cra + sizeof(cra)/sizeof(cra[0]));
  vector<double> cs(csa, csa + sizeof(csa)/sizeof(csa[0]));
  Polynomial<double> r((cr)), s((cs));
  cout << "r(x) = " << r << endl;
  cout << "s(x) = " << s << endl;
  Polynomial<double> t = s.integral() - 2.19*r.derivative() * s + s.derivative();
  cout << "t(x) := S(x) - 2.19*r'(x)s(x) + s'(x) = " << t << endl;
  return 0;
}
