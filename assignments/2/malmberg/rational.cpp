#include<iostream>

using namespace std;

class Rational
{
  
public:
  
  // Constructors
  Rational() : _numerator(0), _denominator(1)
  {
    // Do nothing
  }
  
  Rational(int numerator, int denominator) : _numerator(numerator), _denominator(denominator)  
  {
    // Denominator cannot be zero
    if (_denominator == 0)
      throw runtime_error("Expected nonzero denominator in initialization.");
    
    // Associate sign to the numerator and cancel common factors 
    _simplify();
  }

  // Addition -- rational + rational
  Rational operator+(const Rational b)
  {
    Rational sum(_numerator*b._denominator + _denominator*b._numerator, _denominator*b._denominator);
    return sum;
  }

  // Addition -- rational + integer
  Rational operator+(const int b)
  {
    Rational sum(_numerator + _denominator*b, _denominator);
    return sum;
  }

  // Addition -- integer + rationl
  friend Rational operator+(int a, const Rational &b);

  // Subtraction -- rational - rational
  Rational operator-(const Rational b)
  {
    Rational difference(_numerator*b._denominator - _denominator*b._numerator, _denominator*b._denominator);
    return difference;
  }

  // Subtraction -- rational - integer
   Rational operator-(const int b)
  {
    Rational difference(_numerator - _denominator*b, _denominator);
    return difference;
  }

  // Subtraction -- integer - rational
  friend Rational operator-(int a, const Rational &b);
  
  // Multiplication -- rational * rational
  Rational operator*(const Rational b)
  {
    Rational product(_numerator*b._numerator, _denominator*b._denominator);
    return product;
  }

  // Multiplication -- rational * integer
  Rational operator*(const int b)
  {
    Rational product(_numerator*b, _denominator);
    return product;
  }

  // Multiplication -- integer * rational
  friend Rational operator*(int a, const Rational &b);
  
  // Division -- rational / rational
  Rational operator/(const Rational b)
  {
    // Rule out division by zero
    if (b._numerator == 0)
      throw runtime_error("Cannot divide by zero.");
          
    Rational quotient(_numerator*b._denominator, _denominator*b._numerator);

    return quotient;
  }

  // Division -- rational / integer
  Rational operator/(const int b)
  {
    // Rule out division by zero
    if (b == 0)
      throw runtime_error("Cannot divide by zero.");
          
    Rational quotient(_numerator, _denominator*b);

    return quotient;
  }

  // Division -- integer / rational
  friend Rational operator/(int a, const Rational &b);

  // Equality -- rational == rational
  bool operator==(const Rational b)
  {
    bool eq = false;
    if (_numerator*b._denominator == _denominator*b._numerator)
      eq = true;
    
    return eq;
  }

  // Equality -- rational == integer
  bool operator==(const int b)
  {
    bool eq = false;
    if (_numerator == _denominator*b)
      eq = true;
    
    return eq;
  }

  // Equality -- integer == rational
  friend bool operator==(const int a, Rational b);

  // Not equality -- rational != rational
  bool operator!=(const Rational b)
  {
    bool neq = false;
    if (_numerator*b._denominator != _denominator*b._numerator)
      neq = true;
    
    return neq;
  }

  // Not equality -- rational != integer
  bool operator!=(const int b)
  {
    bool neq = false;
    if (_numerator != _denominator*b)
      neq = true;
    
    return neq;
  }

  // Not equality -- integer != rational
  friend bool operator!=(int a, const Rational b);
  
  // Less than -- rational < rational 
  bool operator<(const Rational b)
  {
    bool l = false;
    if (_numerator*b._denominator < _denominator*b._numerator)
      l = true;
    
    return l;
  }

  // Less than -- rational < integer 
  bool operator<(const int b)
  {
    bool l = false;
    if (_numerator*b < _denominator*b)
      l = true;
    
    return l;
  }

  // Less than -- integer < rational
  friend bool operator<(int a, const Rational b);

  // Less than or equal -- rational <= rational
  bool operator<=(const Rational b)
  {
    bool leq = false;
    if (_numerator*b._denominator <= _denominator*b._numerator)
      leq = true;
    
    return leq;
  }

  // Less than or equal -- rational <= int
  bool operator<=(const int b)
  {
    bool leq = false;
    if (_numerator <= _denominator*b)
      leq = true;
    
    return leq;
  }

  // Less than or equal -- int <= rational
  friend bool operator<=(int a, const Rational b);
  
  // Greater than -- rational > rational 
  bool operator>(const Rational b)
  {
    bool g = false;
    if (_numerator*b._denominator > _denominator*b._numerator)
      g = true;
    
    return g;
  }

  // Greater than -- rational > int
  bool operator>(const int b)
  {
    bool g = false;
    if (_numerator > _denominator*b)
      g = true;
    
    return g;
  }

  // Greater than -- integer > rational
  friend bool operator>(int a, const Rational b);
  
  // Greater than or equal -- rational >= rational
  bool operator>=(const Rational b)
  {
    bool geq = false;
    if (_numerator*b._denominator >= _denominator*b._numerator)
      geq = true;
    
    return geq;
  }

  // Greater than or equal -- rational >= integer
  bool operator>=(const int b)
  {
    bool geq = false;
    if (_numerator >= _denominator*b)
      geq = true;
    
    return geq;
  }

  // Greater than or equal -- integer >= rational
  friend bool operator>=(int a, const Rational b);
  
  // For printing, see below
  friend ostream& operator<<(ostream& os, const Rational& quotient); 
  
private:

  int _numerator;
  int _denominator;

  // Simplify
  void _simplify()
  {
    // Use the convention that the sign is associated to the numerator
    if (_denominator < 0)
      {
	_numerator   *= -1;
	_denominator *= -1;
      }

    // Use convention 0 = 0/1 (i.e. the denomimator of 0 is 1)
    if (_numerator == 0)
      _denominator = 1;

    // Remove commom factors
    for (int f = min(abs(_numerator), abs(_denominator)); f > 1; f--)
      if (_numerator % f == 0 && _denominator % f == 0)
	{
	  _numerator   /= f;
	  _denominator /= f;
	}
  }
  
};

// Addition -- integer + rational
Rational operator+(int a, const Rational &b)
{
  Rational sum(a*b._denominator + b._numerator, b._denominator);
  return sum;
}

// Subtraction -- integer - rational
Rational operator-(int a, const Rational &b)
{
  Rational sum(a*b._denominator - b._numerator, b._denominator);
  return sum;
}

// Multiplication -- integer * rational
Rational operator*(int a, const Rational &b)
{
  Rational product(a*b._numerator, b._denominator);
  return product;
}

// Division -- integer / rational
Rational operator/(int a, const Rational &b)
{
  // Rule out division by zero
  if (b._numerator == 0)
    throw runtime_error("Cannot divide by zero.");
  
  Rational quotient(a*b._denominator, b._numerator);
  
  return quotient;
}

// Equality -- integer == rational
bool operator==(const int a, Rational b)
{
  bool eq = false;
  if (a*b._denominator == b._numerator)
    eq = true;
  
  return eq;
}

// Not equality -- integer != rational
bool operator!=(int a, const Rational b)
{
  bool neq = false;
  if (a*b._denominator != b._numerator)
      neq = true;
    
    return neq;
}

// Less than -- integer < rational
bool operator<(int a, const Rational b)
{
  bool l = false;
  if (a*b._denominator < b._numerator)
    l = true;
  
  return l;
}

// Less than or equal -- integer <= rational
bool operator<=(int a, const Rational b)
{
  bool leq = false;
  if (a*b._denominator <= b._numerator)
    leq = true;
  
  return leq;
}

// Greater than -- integer > rational
bool operator>(int a, const Rational b)
{
  bool g = false;
  if (a*b._denominator > b._numerator)
    g = true;
  
  return g;
}

// Greater than or equal -- integer >= rational
bool operator>=(int a, const Rational b)
{
  bool geq = false;
  if (a*b._denominator >= b._numerator)
    geq = true;
  
  return geq;
}

// Printing rationals
ostream& operator<<(ostream& os, const Rational& quotient)
{
  os << quotient._numerator << "/" << quotient._denominator;
  return os;
}


