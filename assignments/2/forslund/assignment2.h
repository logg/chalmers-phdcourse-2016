#ifndef ASSIGNMENT2_H
#define ASSIGNMENT2_H

#include <iostream>
#include <vector>

class Rational{
private:
    int _p;
    int _q;
    void _simplify();
public:
    Rational(int p);
    Rational(int p, int q);
    Rational operator+(const Rational& r);
    Rational operator-(const Rational& r);
    Rational operator*(const Rational& r);
    Rational operator/(const Rational& r);
    bool operator==(const Rational& r);
    bool operator!=(const Rational& r);
    bool operator<(const Rational& r);
    bool operator<=(const Rational& r);
    bool operator>(const Rational& r);
    bool operator>=(const Rational& r);
    int getNominator() const;
    int getDenominator() const;
    friend std::ostream& operator<<(std::ostream& os, const Rational& r);
};

class Polynomial{
private:
    std::vector<Rational> _coeffs;
public:
    Polynomial(std::vector<Rational> coeffs);
    Polynomial operator+(const Polynomial& p);
    Polynomial operator-(const Polynomial& p);
    Polynomial operator*(const Polynomial& p);
    double operator()(double x_value);
    Polynomial derivative();
    void differentiate();
    Polynomial integral(Rational& c);
    void integrate(Rational& c);
    friend std::ostream& operator<<(std::ostream& os, const Polynomial& p);
};

#endif // ASSIGNMENT2_H
