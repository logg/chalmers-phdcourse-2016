#include "assignment2.h"

using namespace std;

int main(){

    cout << "Test class Rational:" << endl;
    Rational a(-100, -6.5); // double converted to int automatically during construction
    Rational b(20); // 20 = 20/1
    Rational bb(10,-6); // 10/(-6) = -5/3
    Rational c = b + a;
    Rational d = b - a;
    Rational e = b*a;
    Rational f = b/a;
    cout << "a     = " << a << endl;
    cout << "b     = " << b << endl;
    cout << "bb    = " << bb << endl;
    cout << "b + a = " << c << endl;
    cout << "b - a = " << d << endl;
    cout << "b*a   = " << e << endl;
    cout << "b/a   = " << f << endl;
    if (b == a){
        cout << b << " = " << a << endl;
    }
    if (b != a){
        cout << b << " != " << a << endl;
    }
    if (b < a){
        cout << b << " < " << a << endl;
    }
    if (b <= a){
        cout << b << " <= " << a << endl;
    }
    if (b > a){
        cout << b << " > " << a << endl;
    }
    if (b >= a){
        cout << b << " >= " << a << endl;
    }

    cout << endl << "Test class Polynomial:" << endl;
    Rational A(-10, 5.5);
    Rational B(20, 4);
    Rational C(0);
    Rational D(4, 5);

    Rational v[2] = {A, B};
    Rational w[6] = {B, C, D, A, C, B};
    Polynomial p(vector<Rational>(&v[0], &v[0]+2));
    Polynomial q(vector<Rational>(&w[0], &w[0]+6));
    Polynomial r = q + p;
    Polynomial s = q - p;
    Polynomial t = q*p;

    cout << "p     = " << p << endl;
    cout << "q     = " << q << endl;
    cout << "q + p = " << r << endl;
    cout << "q - p = " << s << endl;
    cout << "q*p   = " << t << endl;

    double pointOfEval = 2.0;
    double evaluation = q(pointOfEval);
    cout << endl << "Evaluation: q(" << pointOfEval << ") = " << evaluation << endl;

    Polynomial q_prim = q.derivative(); // return new Polynomial
    cout << endl << "q  = " << q << endl;
    cout << "q' = " << q_prim << " <- derivative of q" << endl;
    q.differentiate(); // update q
    cout << "q  = " << q << " <- polynomial q has been differentiated" << endl;

    Rational const_of_int(5, 1);
    Polynomial Q = q.integral(const_of_int); // return new Polynomial
    cout << "Q  = " << Q << " <- integral of q" << endl;
    q.integrate(const_of_int); // update q
    cout << "q  = " << q << " <- polynomial q has been integrated" << endl;

    return 0;
}
