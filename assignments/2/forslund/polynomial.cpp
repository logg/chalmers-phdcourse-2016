#include "assignment2.h"
#include <cmath>
using namespace std;

// CLASS POLYNOMIAL
Polynomial::Polynomial(vector<Rational> coeffs){
    _coeffs = coeffs;
}

// Overload addition operator
Polynomial Polynomial::operator+(const Polynomial& p){
    vector<Rational> coeffs_sum;
    if (_coeffs.size() <= p._coeffs.size()){
        coeffs_sum = p._coeffs;
        for (int k = 0; k < _coeffs.size(); k++){
            coeffs_sum[k] = coeffs_sum[k] + _coeffs[k];
        }
    }
    else{
        coeffs_sum = _coeffs;
        for (int k = 0; k < p._coeffs.size(); k++){
                coeffs_sum[k] = coeffs_sum[k] + p._coeffs[k];
        }
    }
    return Polynomial(coeffs_sum);
}

// Overload subtraction operator
Polynomial Polynomial::operator-(const Polynomial& p){
    vector<Rational> coeffs_diff;
    if (_coeffs.size() <= p._coeffs.size()){
        coeffs_diff = p._coeffs;
        for (int k = 0; k < _coeffs.size(); k++){
                coeffs_diff[k] = _coeffs[k] - coeffs_diff[k];
        }
    }
    else{
        coeffs_diff = _coeffs;
        for (int k = 0; k < p._coeffs.size(); k++){
                coeffs_diff[k] = coeffs_diff[k] - p._coeffs[k];
        }
    }
    return Polynomial(coeffs_diff);
}

// Overload multiplication operator
// The coefficient vector of the product is obtained
// by convolving the two original coefficient vectors
Polynomial Polynomial::operator*(const Polynomial& p){
    vector<Rational> coeffs_prod;
    int k;
    for (int i = 0; i < _coeffs.size() + p._coeffs.size() - 1; i++){
        k = i;
        Rational temp(0, 1);
        for (int j = 0; j < p._coeffs.size(); j++){
            if (k >= 0 && k < _coeffs.size()){
                temp = temp + _coeffs[k]*p._coeffs[j];
            }
            k -= 1;
        }
        coeffs_prod.push_back(temp);
    }
    return Polynomial(coeffs_prod);
}

// Overload parenthesis operator
double Polynomial::operator()(double x_value){
    double value;
    for (int i = 0; i < _coeffs.size(); i++){
        value += double(_coeffs[i].getNominator())/double(_coeffs[i].getDenominator())*pow(x_value, i);
    }
    return value;
}

// Return derivative
Polynomial Polynomial::derivative(){
    vector<Rational> coeffs_deriv;
    for (int i = 1; i < _coeffs.size(); i++){
        Rational expo(i, 1);
        coeffs_deriv.push_back(expo*_coeffs[i]);
    }
    return Polynomial(coeffs_deriv);
}

// Differentiate and update coefficients
void Polynomial::differentiate(){
    vector<Rational> coeffs_deriv;
    for (int i = 1; i < _coeffs.size(); i++){
        Rational expo(i, 1);
        coeffs_deriv.push_back(expo*_coeffs[i]);
    }
    _coeffs = coeffs_deriv;
}

// Return integral
Polynomial Polynomial::integral(Rational& c){
    vector<Rational> coeffs_int;
    coeffs_int.push_back(c);
    for (int i = 0; i < _coeffs.size(); i++){
        Rational expo(1, i+1);
        coeffs_int.push_back(expo*_coeffs[i]);
    }
    return Polynomial(coeffs_int);
}

// Integrate and update coefficients
void Polynomial::integrate(Rational& c){
    vector<Rational> coeffs_int;
    coeffs_int.push_back(c);
    for (int i = 0; i < _coeffs.size(); i++){
        Rational expo(1, i+1);
        coeffs_int.push_back(expo*_coeffs[i]);
    }
    _coeffs = coeffs_int;
}

// Overload << globally (since we can't modify the class ostream)
ostream& operator<<(ostream& os, const Polynomial& p){
    int ind = 0; // controls the writing of plus sign
    if (p._coeffs[0].getNominator() != 0){ // special case, write x^0 as (1)
        os << p._coeffs[0];
        ind = 1;
    }
    if (p._coeffs[1].getNominator() != 0){ // special case, write x^1 as x
        if (ind) cout << " + ";
        os << p._coeffs[1] << "*x";
        ind = 1;
    }
    for (int i = 2; i < p._coeffs.size(); ++i) // remaining terms
    {
        if (p._coeffs[i].getNominator() != 0){
            if (ind) cout << " + ";
            os << p._coeffs[i] << "*x^" << i;
            ind = 1;
        }
    }
    return os;
}

