#include "assignment2.h"
#include <cstdlib>
#include <stdexcept>

using namespace std;

// CLASS RATIONAL
Rational::Rational(int p): _p(p), _q(1){
}
Rational::Rational(int p, int q): _p(p), _q(q){
    try // check for division by zero
    {
        _simplify();
    }
    catch(runtime_error& error){
        cerr << "Caught error: " << error.what() << endl;
        exit(EXIT_FAILURE);
    }
}

// Simplify rational number
void Rational::_simplify(){
    if( _q == 0 ) {
        throw runtime_error("can't divide by 0.");
    }

    for (int k = min(abs(_p), abs(_q)); k > 1; k--){ // divide both p and q by their gcd
        if (_p % k == 0 && _q % k == 0)
        {
            _p /= k;
            _q /= k;
        }
    }
    if (_q < 0){ // put minus sign in the nominator
        _p *= -1;
        _q *= -1;
    }
}

// (p1, q1) + (p2, q2) = (p1q2 + q1p2, q1q2)
Rational Rational::operator+(const Rational& r){
    Rational c(_p*r._q + _q*r._p, _q*r._q);
    return c;
}

// (p1, q1) - (p2, q2) = (p1q2 - q1p2, q1q2)
Rational Rational::operator-(const Rational& r){
    Rational c(_p*r._q - _q*r._p, _q*r._q);
    return c;
}

// (p1, q1) * (p2, q2) = (p1p2, q1q2)
Rational Rational::operator*(const Rational& r){
    Rational c(_p*r._p, _q*r._q);
    return c;
}

// (p1, q1) / (p2, q2) = (p1q2, q1p2)
Rational Rational::operator/(const Rational& r){
    Rational c(_p*r._q, _q*r._p);
    return c;
}

// (p1, q1) = (p2, q2) iff p1q2 = q1p2
bool Rational::operator==(const Rational& r){
    return (_p*r._q == _q*r._p);
}

// (p1, q1) != (p2, q2) iff p1q2 != q1p2
bool Rational::operator!=(const Rational& r){
    return (_p*r._q != _q*r._p);
}

// (p1, q1) < (p2, q2) iff p1q2 < q1p2 (since p2q2 is positive due to simplify())
bool Rational::operator<(const Rational& r){
    return (_p*r._q < _q*r._p);
}

// (p1, q1) <= (p2, q2) iff p1q2 <= q1p2 (since p2q2 is positive due to simplify())
bool Rational::operator<=(const Rational& r){
    return (_p*r._q <= _q*r._p);
}

// (p1, q1) > (p2, q2) iff p1q2 > q1p2 (since p2q2 is positive due to simplify())
bool Rational::operator>(const Rational& r){
    return (_p*r._q > _q*r._p);
}

// (p1, q1) >= (p2, q2) iff p1q2 >= q1p2 (since p2q2 is positive due to simplify())
bool Rational::operator>=(const Rational& r){
    return (_p*r._q >= _q*r._p);
}

// Set to const because it is called via reference to const in
// friend std::ostream& operator<<(std::ostream& os, const Polynomial& a);
int Rational::getNominator() const{
    return _p;
}

int Rational::getDenominator() const{
    return _q;
}

// Overload << globally (since we can't modify the class ostream)
ostream& operator<<(ostream& os, const Rational& r){
    if (r._p == 0){
        os << 0;
    }
    else if (r._q == 1){
        os << r._p;
    }
    else{
        os << r._p << "/" << r._q;
    }
    return os;
}
