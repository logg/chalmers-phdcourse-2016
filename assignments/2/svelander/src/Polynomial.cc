/*
 * Polynomial.cc
 *
 *  Created on: Dec 13, 2016
 *      Author: Frida Svelander
 */

#include "../include/Polynomial.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Operator overloading
Polynomial Polynomial::operator+(const Polynomial &p) const {
	std::vector<Rational> coeffsum;
	coeffsum.resize(std::max(p.coeffs_.size(), coeffs_.size()));

	for(unsigned int i = 0; i < coeffs_.size(); i++) {
		coeffsum[i] = coeffsum[i] + coeffs_[i];
	}

	for(unsigned int i = 0; i < p.coeffs_.size(); i++) {
		coeffsum[i] = coeffsum[i] + p.coeffs_[i];
	}

	return Polynomial(coeffsum);
}

Polynomial Polynomial::operator-(const Polynomial &p) const {
	std::vector<Rational> coeffsum;
	coeffsum.resize(std::max(p.coeffs_.size(), coeffs_.size()));

	for(unsigned int i = 0; i < coeffs_.size(); i++) {
		coeffsum[i] = coeffsum[i] + coeffs_[i];
	}

	for(unsigned int i = 0; i < p.coeffs_.size(); i++) {
		coeffsum[i] = coeffsum[i] - p.coeffs_[i];
	}

	return Polynomial(coeffsum);
}

Polynomial Polynomial::operator*(const Polynomial &p) const {
	std::vector<Rational> coeffprod;
	coeffprod.resize(degree() + p.degree() + 1);

	for(unsigned int i = 0; i <= degree(); i++) {
		for(unsigned int j = 0; j <= p.degree(); j++) {
			coeffprod[i + j] = coeffprod[i + j] + (coeffs_[i] * p.coeffs()[j]);
		}
	}

	return Polynomial(coeffprod);
}
double Polynomial::operator()(const double &x) const {
	double px = 0.0;
	for(unsigned int i = 0; i <= degree(); i++) {
		px += (double)coeffs_[i].p() / (double)coeffs_[i].q() * pow(x, i);
	}

	return px;
}

// Integrate and differentiate functions
Polynomial Polynomial::differentiate() const {
	unsigned int degreediff = 0;
	if(degree() > 0) {
		degreediff = degree() - 1;
	}

	std::vector<Rational> coeffsdiff(degreediff + 1, Rational(0, 1));
	for(unsigned int i = 0; i <= degreediff; i++) {
		coeffsdiff[i] = Rational(i + 1, 1) * coeffs_[i + 1];
	}

	return Polynomial(coeffsdiff);
}
Polynomial Polynomial::integrate() const {
	unsigned int degreeint = degree() + 1;
	std::vector<Rational> coeffsint(degreeint + 1, Rational(0, 1));

	for(unsigned int i = 1; i <= degreeint; i++) {
		coeffsint[i] = coeffs_[i - 1] / Rational(i, 1);
	}

	return Polynomial(coeffsint);
}

std::ostream& operator<<(std::ostream& out, const Polynomial &p) {
	if(p.degree() == 0) {
		out << "0";
	}
	else {
		for(unsigned int i = p.degree(); i >= 2; i--) {
			if(abs(p.coeffs()[i].p()) > 0) {
				out << p.coeffs()[i] << "*x^" << i << " + ";
			}
		}

		if(p.degree() >= 1) {
			if(abs(p.coeffs()[1].p()) > 0) {
				out << p.coeffs()[1] << "*x";
			}
		}

		if(abs(p.coeffs()[0].p()) > 0) {
			out << " + " << p.coeffs()[0];
		}
	}

	return out;
}


