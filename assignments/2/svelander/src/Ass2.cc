/*
 * Ass2.cc
 *
 *  Created on: Dec 13, 2016
 *      Author: Frida Svelander
 */

#include "../include/Polynomial.h"
#include <stdio.h>

int main() {
	//////////////////////////////////  Test Rational class  //////////////////////////////////
	std::cout << "------------------------------ Test Rational -----------------------------\n";
	Rational r0(4, 3);
	Rational r1(5, 7);
	Rational r2(1, 2);

	// Test + operator
	std::cout << "Test + operator \n";
	std::cout << r1 << " + " << r1 << " = " << r1 + r1 << "\n";
	std::cout << r0 << " + " << r2 << " = " << r0 + r2 << "\n";

	// Test - operator
	std::cout << "\nTest - operator \n";
	std::cout << r1 << " - " << r0 << " = " << r1 - r0 << "\n";
	std::cout << r0 << " - " << r2 << " = " << r0 - r2 << "\n";

	// Test * operator
	std::cout << "\nTest * operator \n";
	std::cout << r1 << " * " << r0 << " = " << r1 * r0 << "\n";
	std::cout << r0 << " * " << r2 << " = " << r0 * r2 << "\n";

	// Test / operator
	std::cout << "\nTest / operator \n";
	std::cout << r1 << " / " << r0 << " = " << r1 / r0 << "\n";
	std::cout << r0 << " / " << r2 << " = " << r0 / r2 << "\n";

	// Test == operator
	std::cout << "\nTest == operator \n";
	std::cout << r0 << " == " << r0 << " ? " << (r0 == r0) << "\n";
	std::cout << r0 << " == " << r1 << " ? " << (r0 == r1) << "\n";
	std::cout << r1 << " == " << r0 << " ? " << (r1 == r0) << "\n";

	// Test != operator
	std::cout << "\nTest != operator \n";
	std::cout << r0 << " != " << r0 << " ? " << (r0 != r0) << "\n";
	std::cout << r0 << " != " << r1 << " ? " << (r0 != r1) << "\n";
	std::cout << r1 << " != " << r0 << " ? " << (r1 != r0) << "\n";

	// Test > operator
	std::cout << "\nTest > operator\n";
	std::cout << r0 << " > " << r0 << " ? " << (r0 > r0) << "\n";
	std::cout << r0 << " > " << r1 << " ? " << (r0 > r1) << "\n";
	std::cout << r1 << " > " << r0 << " ? " << (r1 > r0) << "\n";

	// Test < operator
	std::cout << "\nTest < operator \n";
	std::cout << r0 << " < " << r0 << " ? " << (r0 < r0) << "\n";
	std::cout << r0 << " < " << r1 << " ? " << (r0 < r1) << "\n";
	std::cout << r1 << " < " << r0 << " ? " << (r1 < r0) << "\n";

	// Test >= operator
	std::cout << "\nTest >= operator \n";
	std::cout << r0 << " >= " << r0 << " ? " << (r0 >= r0) << "\n";
	std::cout << r0 << " >= " << r1 << " ? " << (r0 >= r1) << "\n";
	std::cout << r1 << " >= " << r0 << " ? " << (r1 >= r0) << "\n";

	// Test <= operator
	std::cout << "\nTest <= operator \n";
	std::cout << r0 << " <= " << r0 << " ? " << (r0 <= r0) << "\n";
	std::cout << r0 << " <= " << r1 << " ? " << (r0 <= r1) << "\n";
	std::cout << r1 << " <= " << r0 << " ? " << (r1 <= r0) << "\n";
	std::cout << "---------------------------------------------------------------------------\n";
	////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////// Test Polynomial class //////////////////////////////////
	std::cout << "------------------------------ Test Polynomial -----------------------------\n";
	// Polynomial 1
	std::vector<Rational> coeffs1(3, Rational(0, 1));
	coeffs1[0] = Rational(1, 2);
	coeffs1[1] = Rational(3, 5);
	coeffs1[2] = Rational(3, 2);
	Polynomial p1(coeffs1);

	// Polynomial 2
	std::vector<Rational> coeffs2(4, Rational(0, 1));
	coeffs2[0] = Rational(4, 5);
	coeffs2[1] = Rational(13, 17);
	coeffs2[2] = Rational(3, 2);
	coeffs2[3] = Rational(1, 3);
	Polynomial p2(coeffs2);

	// Test polynomial addition
	std::cout << "Test polynomial addition\n";
	std::cout << p1 << " + " << p2 << " = " << p1 + p2 << "\n";

	// Test polynomial subtraction
	std::cout << "\nTest polynomial subtraction\n";
	std::cout << p1 << " - " << p2 << " = " << p1 - p2 << "\n";

	// Test polynomial multiplication
	Polynomial p4 = p1 * p2;
	std::cout << "\nTest polynomial multiplication\n(" << p1 << ") * (" << p2 << ") = " << p4 << "\n";

	// Test polynomial evaluation
	std::cout << "\nTest polynomial evaluation\n";
	double x = 2.0;
	std::cout << "p(x) = " << p1 << "\n";
	printf("p(%f) = %f\n", x, p1(x));
	double p1x = p1(2.0);

	// Test polynomial differentiation
	std::cout << "\nTest polynomial differentiation\n" << "d/dx (" << p2 << ") = " << p2.differentiate() << "\n";

	// Test polynomial integration
	std::cout << "\nTest polynomial integration\n" << "int (" << p2 << ") = " << p2.integrate() << "\n";
	std::cout << "---------------------------------------------------------------------------\n";
	////////////////////////////////////////////////////////////////////////////////////////////
	return 0;
}
