/*
 * Rational.cc
 *
 *  Created on: Dec 12, 2016
 *      Author: Frida Svelander
 */

#include "../include/Rational.h"

// Operator overloading
Rational Rational::operator+(const Rational &r) const {
	Rational tmp(p_*r.q_ + r.p_*q_, q_*r.q_);
	int d = gcd(tmp.p_, tmp.q_);
	tmp.p_ /= d;
	tmp.q_ /= d;
	return tmp;
}
Rational Rational::operator-(const Rational &r) const {
	return *this + Rational(-r.p_, r.q_);
}

Rational Rational::operator*(const Rational &r) const {
	Rational tmp(p_*r.p_, q_*r.q_);
	int d = gcd(tmp.p_, tmp.q_);
	tmp.p_ /= d;
	tmp.q_ /= d;
	return tmp;
}
Rational Rational::operator/(const Rational &r) const {
	return *this * Rational(r.q_, r.p_);
}
bool Rational::operator==(const Rational &r) const {
	int d1 = gcd(p_, q_);
	int d2 = gcd(r.p_, r.q_);
	Rational tmp1(p_/d1, q_/d1);
	Rational tmp2(r.p_/d2, r.q_/d2);
	return tmp1.p_ == tmp2.p_ && tmp1.q_ == tmp2.q_; //todo call a/b==1 or both are zero, make sure denomninator is non-zero
}
bool Rational::operator!=(const Rational &r) const {
	return !(*this == r);
}
bool Rational::operator<(const Rational &r) const {
	return p_*r.q_ < r.p_*q_;
}
bool Rational::operator>(const Rational &r) const {
	return p_*r.q_ > r.p_*q_;
}
bool Rational::operator<=(const Rational &r) const {
	return p_*r.q_ <= r.p_*q_;
}
bool Rational::operator>=(const Rational &r) const {
	return p_*r.q_ >= r.p_*q_;
}

std::ostream& operator<<(std::ostream& out, const Rational &r) {
	return out << r.p() << '/' << r.q();
}


