/*
 * Polynomial.h
 *
 *  Created on: Dec 13, 2016
 *      Author: Frida Svelander
 */

#ifndef INCLUDE_POLYNOMIAL_H_
#define INCLUDE_POLYNOMIAL_H_

#include <vector>
#include "../include/Rational.h"
#include <stdio.h>

class Polynomial {
public:
	Polynomial(const std::vector<Rational> &coeffs):
		coeffs_(coeffs) {};

	// Degree of the polynomial
	unsigned int degree() const {return coeffs_.size() == 0 ? 0 : coeffs_.size() - 1;}
	const std::vector<Rational> coeffs() const {return coeffs_; }

	// Operator overloading
	Polynomial operator+(const Polynomial &p) const;
	Polynomial operator-(const Polynomial &p) const;
	Polynomial operator*(const Polynomial &p) const;
	double operator()(const double &x) const;

	// Integrate and differentiate functions
	Polynomial differentiate() const;
	Polynomial integrate() const;

private:
	std::vector<Rational> coeffs_; // p(x) = coeffs[n]*x^n + ... + coeffs[1]*x + coeffs[0]
};


std::ostream& operator<<(std::ostream& out, const Polynomial &p);



#endif /* INCLUDE_POLYNOMIAL_H_ */
