/*
 * Rational.h
 *
 *  Created on: Dec 12, 2016
 *      Author: Frida Svelander
 */

#ifndef INCLUDE_RATIONAL_H_
#define INCLUDE_RATIONAL_H_

#include <iostream>
#include <stdlib.h>

class Rational {

public:
	Rational():p_(0), q_(1) {};
	Rational(int p, int q): p_(p), q_(q) {};

	int p() const {return p_;}
	int q() const {return q_;}

	// Operator overloading
	Rational operator+(const Rational &r) const;
	Rational operator-(const Rational &r) const;
	Rational operator*(const Rational &r) const;
	Rational operator/(const Rational &r) const;
	bool operator==(const Rational &r) const;
	bool operator!=(const Rational &r) const;
	bool operator<(const Rational &r) const;
	bool operator>(const Rational &r) const;
	bool operator<=(const Rational &r) const;
	bool operator>=(const Rational &r) const;

private:
	// Methods
	int gcd(int a, int b) const {
		return b == 0 ? abs(a) : gcd(abs(b), abs(a) % abs(b)); }

	// Variables
	int p_; // numerator
	int q_; // denominator
};


std::ostream& operator<<(std::ostream& out, const Rational &r);


#endif /* INCLUDE_RATIONAL_H_ */
