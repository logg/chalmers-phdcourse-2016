/*
 *      Rational: A simple class for representation of rational numbers.
 *      
 *      Member variables:
 *          _e: enumerator
 *          _d: denominator
 *
 *      Written by
 *      Erik Svenning
 *      Chalmers University of Technology
 *      December 2016
*/
#include <ostream>
#include <stdexcept>
#include <cmath>
#include <limits>
#include "boost/math/common_factor.hpp"


template <typename T>
class Rational 
{
    public:

        Rational<T>():_e(T(0)),_d(T(1)) { };
        Rational<T>(T i):_e(i),_d(1) { };
        Rational<T>(T e, T d):_e(e),_d(d) 
        { 
            if(d == 0) {
                throw std::invalid_argument("Division by zero.");
            }

            simplify(); 
        };

        template<typename T2>
        friend std::ostream& operator<<(std::ostream& os, const Rational<T2>& r);

        Rational<T> operator+(const Rational<T> &r)
        {
            return Rational<T>( _e*r._d + _d*r._e, _d*r._d );
        }

        Rational<T> operator-(const Rational<T> &r)
        {
            return Rational<T>( _e*r._d - _d*r._e, _d*r._d );
        }

        Rational<T> operator*(const Rational &r)
        {
            return Rational<T>( _e*r._e , _d*r._d );
        }

        Rational<T> operator/(const Rational &r)
        {
            // Check for division y zero
            // We assume that the original denominators are valid (nonzero),
            // hence, only r._e needs to be checked
            if(r._e == 0) {
                throw std::invalid_argument("Division by zero.");
            }

            return Rational<T>( _e*r._d , _d*r._e );
        }

        Rational<T> operator+=(const Rational<T> &r)
        {
            *this = Rational<T>( _e*r._d + _d*r._e, _d*r._d );
            return *this;
        }


        template<typename T2>
        friend Rational<T2> pow(Rational<T2> x, int p);

        // Comparison operators.
        // We implement ==  and < for our class, and express the rest
        // of the operators in terms of these two.
        inline bool operator==(const Rational<T> &r) const
        {
            return ( (_e == r._e) && (_d == r._d) );
        }

        inline bool operator<(const Rational<T> &r) const
        {
            return ( (_e*r._d < _d*r._e) );
        }

        // Standard expressions for >, <=, >= and !=, taken from 
        // http://en.cppreference.com/w/cpp/language/operators
        inline bool operator> (const Rational<T>& r) const { return r < *this; }
        inline bool operator<=(const Rational<T>& r) const { return !(*this > r); }
        inline bool operator>=(const Rational<T>& r) const { return !(*this < r); }
        inline bool operator!=(const Rational<T>& r) const { return !(*this == r); }


        void simplify();

    protected:
    
        // Enumerator and denominator
        T _e, _d;
};

template<typename T2>
std::ostream& operator<<(std::ostream& os, const Rational<T2>& r)
{
    return os << r._e << "/" << r._d;
}

template <typename T>
void Rational<T>::simplify()
{
#if 0
    // Brute force simplification.
    for(int i = std::max(_e,_d); i > 1; i--) 
    {
        if( (_e%i == 0) && (_d%i == 0) )
        {
            _e /= i;
            _d /= i;
        }
    }
#else
    // Greatest common divisor from Boost.
    int i = boost::math::gcd(_e,_d);
    _e /= i;
    _d /= i;
#endif
}

template<typename T2>
Rational<T2> pow(Rational<T2> x, int p)
{
    Rational<T2> val( pow(x._e,p), pow(x._d,p) );
    return val;
}

// Specialize std::numeric_limits for Rational<T2>. 
// Implemented with inspiration from http://stackoverflow.com/questions/16122912/is-it-ok-to-specialize-stdnumeric-limitst-for-user-defined-number-like-class.
// See also http://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
namespace std {
    template<typename T2> class numeric_limits< Rational<T2> > {
    public:
       static Rational<T2> epsilon() {return Rational<T2> ( numeric_limits<T2>::epsilon() ) ;};
    };
}


