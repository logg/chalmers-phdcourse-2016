/*
 *      Polynomial: A simple class for representation of polynomials.
 *      
 *
 *      Written by
 *      Erik Svenning
 *      Chalmers University of Technology
 *      December 2016
*/
#include <ostream>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>

using namespace std;

// Help class for sorting coefficients
template <typename T>
class Sort_func
{
public:
    Sort_func() 
    {
        // Do nothing
    }

    ~Sort_func() 
    {
        // Do nothing
    }

    bool operator()(const pair<T,int> &term1, const pair<T,int> &term2) const
    {
        return term1.second < term2.second;
    }

};


template <typename T>
class Polynomial
{
    public:

        Polynomial() {_terms.clear();};

        void add_term(pair<T,int> term);
        void subtract_term(pair<T,int> term) { add_term( pair<T,int>( term.first*T(-1), term.second ) ); };

        Polynomial<T> operator+(const Polynomial<T> &p);
        Polynomial<T> operator-(const Polynomial<T> &p);
        Polynomial<T> operator*(const Polynomial<T> &p);

        T operator() (T x);

        void differentiate();
        void integrate();

        template<typename T2>
        friend std::ostream& operator<<(std::ostream& os, const Polynomial<T2>& p);


        // Sort the terms so that the exponents come in ascending order
        void sort_terms(); 

        // Remove zero terms
        void remove_zero_terms();

    protected:
        vector< pair<T,int> > _terms;
};


template <typename T>
void Polynomial<T>::add_term(pair<T,int> term)
{
    // Check if a term with the given exponent already exists
    // in the polynomial. If so, just add the coefficients.
    bool found_term = false;
    for( size_t i = 0; i < _terms.size(); i++ ) 
    {
        if( _terms[i].second == term.second )
        {
            _terms[i].first += term.first;
            found_term = true;
        }
    }

    // Otherwise, add the new term.
    if( !found_term )
    {
        _terms.push_back(term);
    }

    sort_terms();
    remove_zero_terms();
}

template <typename T>
Polynomial<T> Polynomial<T>::operator+(const Polynomial<T> &p)
{
    Polynomial p2 = *this;
    for( size_t i = 0; i < p._terms.size(); i++ )    
    {
        p2.add_term(p._terms[i]);
    }

    return p2;
}

template <typename T>
Polynomial<T> Polynomial<T>::operator-(const Polynomial<T> &p)
{
    Polynomial p2 = *this;
    for( size_t i = 0; i < p._terms.size(); i++ )    
    {
        p2.subtract_term(p._terms[i]);
    }

    return p2;
}

template <typename T>
Polynomial<T> Polynomial<T>::operator*(const Polynomial<T> &p)
{
    Polynomial p2;
    for( size_t i = 0; i < _terms.size(); i++ )    
    {
        for( size_t j = 0; j < p._terms.size(); j++ )    
        {
            p2.add_term( pair<T,int>( T(_terms[i].first*p._terms[j].first), (_terms[i].second + p._terms[j].second) ) );
        }
    }
    return p2;
}

template<typename T>
T Polynomial<T>::operator() (T x)
{
    T val = T(0);

    for( size_t i = 0; i < _terms.size(); i++ )    
    {
        val += _terms[i].first*( pow(x, _terms[i].second) );
    }
    
    return val;
}

template<typename T>
void Polynomial<T>::differentiate()
{
    // Loop over terms and differentiate term-wise.
    for( size_t i = 0; i < _terms.size(); i++ )    
    {
        // This covers the constant term as well,
        // because the coefficient gets multiplied by zero.
        _terms[i].first = _terms[i].first*T(_terms[i].second);
        _terms[i].second--;
    }

    remove_zero_terms();
}

template<typename T>
void Polynomial<T>::integrate()
{
    // Loop over terms and integrate term-wise.
    for( size_t i = 0; i < _terms.size(); i++ )    
    {
        // We need to check for the special case x^(-1),
        // because integration of this term does not
        // give a polynomial.
        if( _terms[i].second != -1 )    
        {
            _terms[i].second++;
            _terms[i].first = _terms[i].first/T(_terms[i].second);
        }
        else
        {
            throw std::invalid_argument("Integrating x^-1 does not give polynomial result.");
        }
    }
}

template<typename T2>
std::ostream& operator<<(std::ostream& os, const Polynomial<T2>& p)
{
    for( size_t i = 0; i < p._terms.size()-1; i++ )
    {
        os << p._terms[i].first << "*x^" << p._terms[i].second << " + ";
    }

    os << p._terms.back().first << "*x^" << p._terms.back().second;

    return os;
}

template <typename T>
void Polynomial<T>::sort_terms()
{
    // Sort in terms of exponent
    sort( _terms.begin(), _terms.end(), Sort_func<T>( ) );
} 

template <typename T>
void Polynomial<T>::remove_zero_terms()
{
    T small_val = T( numeric_limits<T>::epsilon() );
    //cout << "small_val: " << small_val << endl;

    // Remove terms with zero coefficients
    vector< pair<T,int> > new_terms;

    for( size_t i = 0; i < _terms.size(); i++ )
    {
        // It is better to do an epsilon test than
        // just comparing with zero.
        if( _terms[i].first > small_val || _terms[i].first < T(0)-small_val )
        {
            new_terms.push_back( _terms[i] );    
        }
    }

    _terms = new_terms;
}
