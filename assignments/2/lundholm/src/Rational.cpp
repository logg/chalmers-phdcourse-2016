#include <iostream>
using namespace std;
#include "Rational.h"

int Rational::n() const
{
  return _n;
}

int Rational::d() const
{
  return _d;
}

// Constructors
Rational::Rational()
{

}

Rational::Rational(int nu, int de)
{
  if (de == 0)
  {
    throw invalid_argument("ERROR! Division by zero is not allowed");
  }
  else
  {
    _n = nu; _d = de;
    _simplify();
  }
}

// Overload elementary arithmetic operations
Rational Rational::operator+(const Rational& b) const
{
  Rational r(_n*b._d + b._n*_d,  _d*b._d);
  return r;
}

void Rational::operator+=(const Rational& b)
{
  _n = _n*b._d + b._n*_d;
  _d = _d*b._d;
  _simplify();
}

Rational Rational::operator-(const Rational& b) const
{
  Rational r(_n*b._d - b._n*_d, _d*b._d);
  return r;
}

void Rational::operator-=(const Rational& b)
{
  _n = _n*b._d - b._n*_d;
  _d = _d*b._d;
  _simplify();
}

Rational Rational::operator*(const Rational& b) const
{
  Rational r(_n*b._n, _d*b._d);
  return r;
}

Rational Rational::operator/(const Rational& b) const
{
  Rational r(_n*b._d, _d*b._n);
  return r;
}

// Overload some binary realtions
bool Rational::operator==(const Rational& b) const
{
  return _n*b._d == _d*b._n;
}

bool Rational::operator!=(const Rational& b) const
{
  return _n*b._d != _d*b._n;
}

bool Rational::operator<(const Rational& b) const
{
  return _n*b._d < _d*b._n;
}

bool Rational::operator>(const Rational& b) const
{
  return _n*b._d > _d*b._n;
}

bool Rational::operator<=(const Rational& b) const
{
  return _n*b._d <= _d*b._n;
}

bool Rational::operator>=(const Rational& b) const
{
  return _n*b._d >= _d*b._n;
}

// Simplify a rational number
void Rational::_simplify()
{
  for (int i = 2;  i <= min(abs(_n), abs(_d)); i++)
  {
    while (_n % i == 0 && _d % i == 0)
    {
      _n /= i; _d /= i;
    }
  }
  _n *= _d/abs(_d); _d = abs(_d);
}

// Display a rational number
ostream& operator<<(ostream& os, const Rational& r)
{
  os << r.n() << '/' << r.d();
  return os;
}
