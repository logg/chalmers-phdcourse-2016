#include <iostream>
using namespace std;
#include "Rational.h"

int main()
{
  int n1 = 20, d1 = 4, n2 = 9, d2 = 3;
  string answer0, answer1;

  while(answer0 != "n" )
  {
    cout << "Would you like to give two rational numbers? (y/n)" << endl;
    cout << "(If not, previously given values will be used)" << endl;
    cin >> answer1;

    if (answer1 != "n")
    {
      cout << "First rational number as 'nominator' 'denominator': ";
      cin >> n1 >> d1;
      cout << "Second rational number as 'nominator' 'denominator': ";
      cin >> n2 >> d2;
    }

    Rational r1(n1, d1), r2(n2, d2);

    // Display the given rational numbers
    cout << "The two rational numbers are " << r1 << " and " << r2 << "\n" << endl;

    // Display the results from some binary operations
    cout << "Their sum is " << r1 + r2 << endl;
    cout << "Their difference is " << r1 - r2 << endl;
    cout << "Their product is " << r1 * r2 << endl;
    cout << "Their ratio is " << r1 / r2 << "\n" << endl;

    // Display relations
    cout << "Are they equal? " << (r1 == r2) << endl;
    cout << "Are they not equal? " << (r1 != r2) << endl;
    cout << "Is the first smaller than the second? " << (r1 < r2) << endl;
    cout << "Is the first larger than the second? " << (r1 > r2) << endl;
    cout << "Is the first smaller than or equal to the second? " << (r1 <= r2) << endl;
    cout << "Is the first larger than or equal to the second? " << (r1 >= r2) << endl;

    // Run program again?
    cout << "Would you like to run again? (y/n)" << endl;
    cin >> answer0;
  }

  return 0;
}
