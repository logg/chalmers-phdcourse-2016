#include <iostream>
using namespace std;
#include "Rational.h"
#include "Polynomial.h"

int Polynomial::deg() const
{
  return _deg;
}

Rational Polynomial::rcs(int k) const
{
  return _rcs[k];
}

// Constructors
Polynomial::Polynomial(int deg)
{
  _deg = deg;
  _rcs = new Rational [_deg + 1];
  Rational rzero(0, 1);
  for (int k = 0; k <= _deg; k++)
    _rcs[k] = rzero;
}

Polynomial::Polynomial(int deg, int rcs[][2])
{
  _deg = deg;
  _rcs = new Rational [_deg + 1];
  for (int k = 0; k <= _deg; k++)
  {
    Rational rc(rcs[k][0], rcs[k][1]);
    _rcs[k] = rc;
  }
}

Polynomial::Polynomial(int deg, Rational rcs[])
{
  _deg = deg;
  _rcs = new Rational [_deg + 1];
  for (int k = 0; k <= _deg; k++)
    _rcs[k] = rcs[k];
}

// Destructor
Polynomial::~Polynomial()
{
  //delete[] _rcs;
}

// Overload elementary arithmetic operations
Polynomial Polynomial::operator+(const Polynomial& b) const
{
  int maxdeg = max(_deg, b._deg), mindeg = min(_deg, b._deg);
  Polynomial p(maxdeg);

  for (int k = 0; k <= mindeg; k++)
    p._rcs[k] = _rcs[k] + b._rcs[k];

  if (maxdeg == _deg)
  {
    for (int k = mindeg + 1; k <= maxdeg; k++)
      p._rcs[k] = _rcs[k];
  }
  else if(maxdeg == b._deg)
  {
    for (int k = mindeg + 1; k <= maxdeg; k++)
      p._rcs[k] = b._rcs[k];
  }

  return p;
}

Polynomial Polynomial::operator-(const Polynomial& b) const
{
  int maxdeg = max(_deg, b._deg), mindeg = min(_deg, b._deg);
  Polynomial p(maxdeg);

  for (int k = 0; k <= mindeg; k++)
    p._rcs[k] = _rcs[k] - b._rcs[k];

  if (maxdeg == _deg)
  {
    for (int k = mindeg + 1; k <= maxdeg; k++)
      p._rcs[k] = _rcs[k];
  }
  else if(maxdeg == b._deg)
  {
    Rational rm1(-1, 1);
    for (int k = mindeg + 1; k <= maxdeg; k++)
      p._rcs[k] = rm1*b._rcs[k];
  }

  return p;
}

Polynomial Polynomial::operator*(const Polynomial& b) const
{
  Polynomial p(_deg + b._deg);
  for (int k = 0; k <= _deg; k++)
    for (int l = 0; l <= b._deg; l++)
      p._rcs[k + l] += _rcs[k]*b._rcs[l];

  return p;
}

// Differentiation and integration
void Polynomial::differentiate()
{
  if (_deg > 0)
  {
    _deg = _deg - 1;
    for (int k = 0; k <= _deg; k++)
    {
      Rational rfac(k + 1, 1);
      _rcs[k] = rfac * _rcs[k + 1];
    }
  }
  else if (_deg == 0)
  {
    Rational rzero(0, 1);
    _rcs[0] = rzero;
  }
}

void Polynomial::integrate()
{
  _deg = _deg + 1;
  for (int k = _deg; k >= 1 ; k--)
  {
    Rational rfac(k, 1);
    _rcs[k] = _rcs[k - 1] / rfac;
  }
  Rational r1(_deg, 1);
  _rcs[0] = r1;
}

// Display a polynomial in an unnecessarily nice way
ostream& operator<<(ostream& os, const Polynomial& p)
{
  int pdeg = p.deg();
  Rational rzero(0, 1), rc = p.rcs(pdeg);

  if (pdeg > 1)
  {
    for (int k = pdeg; k > 1; k--)
    {
      rc = p.rcs(k);
      if (rc != rzero)
      {
	if (k != pdeg)
	{
          os << "  +  (" << p.rcs(k) << ")*x^" << k;
	}
	else
	{
	  os << "(" << p.rcs(k) << ")*x^" << k;
	}
      }
      else
      {
	pdeg -= 1;
      }
    }
  }

  if (pdeg > 0)
  {
    rc = p.rcs(1);
    if (rc != rzero)
    {
      if (1 != pdeg)
      {
	os << "  +  (" << p.rcs(1) << ")*x";
      }
      else
      {
	os << "(" << p.rcs(1) << ")*x";
      }
    }
    else
    {
      pdeg -= 1;
    }
  }

  rc = p.rcs(0);
  if (rc != rzero)
  {
    if (0 != pdeg)
    {
      os << "  +  (" << p.rcs(0) << ")";
    }
    else
    {
      os << "(" << p.rcs(0) << ")";
    }
  }
  else if (pdeg == 0)
  {
    os << "The zero polynomial";
  }

  return os;
}
