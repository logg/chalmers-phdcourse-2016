#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

class Polynomial
{
  public:
    int deg() const;
    Rational rcs(int k) const;

    // Constructors
    Polynomial(int deg);
    Polynomial(int deg, int rcs[][2]);
    Polynomial(int deg, Rational rcs[]);

    // Destructor
    ~Polynomial();

    // Overload elementary arithmetic operations
    Polynomial operator+(const Polynomial& b) const;
    Polynomial operator-(const Polynomial& b) const;
    Polynomial operator*(const Polynomial& b) const;

    // Differentiation and integration
    void differentiate();
    void integrate();

  private:
    int _deg;
    Rational * _rcs;
};

// Display a polynomial in an unnecessary nice way
ostream& operator<<(ostream& os, const Polynomial& p);

#endif
