#ifndef RATIONAL_H
#define RATIONAL_H

class Rational
{
  public:
   int n() const;
   int d() const;

    // Constructors
    Rational();
    Rational(int n, int d);

    // Overload elementary arithmetic operations
    Rational operator+(const Rational& b) const;
    void operator+=(const Rational& b);
    Rational operator-(const Rational& b) const;
    void operator-=(const Rational& b);
    Rational operator*(const Rational& b) const;
    Rational operator/(const Rational& b) const;

    // Overload some binary realtions
    bool operator==(const Rational& b) const;
    bool operator!=(const Rational& b) const;
    bool operator<(const Rational& b) const;
    bool operator>(const Rational& b) const;
    bool operator<=(const Rational& b) const;
    bool operator>=(const Rational& b) const;

  private:
    int _n, _d;

    // Simplify a rational number
    void _simplify();
};

ostream& operator<<(ostream& os, const Rational& r);

#endif
