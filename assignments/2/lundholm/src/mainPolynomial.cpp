#include <iostream>
using namespace std;
#include "Rational.h"
#include "Polynomial.h"

int main()
{
  int p1rcs[][2] = {{5, -4}, {1, 5}, {17, 5}, {32, 16}, {44, 12}};
  int p1deg = sizeof(p1rcs)/sizeof(p1rcs[0]) - 1;

  Polynomial p1(p1deg, p1rcs);
  cout << "\nThe polynomial p1 is: \n" << p1 << endl;

  int p2rcs[][2] = {{15, 5}, {2, 5}, {3, 5}};
  int p2deg = sizeof(p2rcs)/sizeof(p2rcs[0]) - 1;

  Polynomial p2(p2deg, p2rcs);
  cout << "The polynomial p2 is: \n" << p2 << endl;

  cout << "\n --- ADDITION ---" << endl;
  cout << "The sum p1 + p2 is: " << endl;
  cout << p1 + p2 << endl;

  cout << "\n --- SUBTRACTION ---" << endl;
  cout << "The difference p1 - p2 is: " << endl;
  cout << p1 - p2 << endl;

  cout << "\n --- MULTIPLICATION ---" << endl;
  cout << "The product p1 * p2 is: " << endl;
  cout << p1 * p2 << endl;

  cout << "\n --- DISTRIBUTIVITY ---" << endl;
  cout << "The polynomial p2 * (p1 + p2) is: " << endl;
  cout << p2 * (p1 + p2) << endl;
  cout << "The polynomial p2 * p1 + p2 * p2 is: " << endl;
  cout << p2 * p1 + p2 * p2 << endl;

  cout << "\n --- DIFFERENTIATION ---" << endl;
  cout << "The polynomial p1 is: \n" << p1 << endl;
  for (int k = 0; k <= p1deg + 1; k++)
  {
    cout << "Differentiating p1 " << k + 1 << " times gives us: " << endl;
    p1.differentiate();
    cout << p1 << endl;
  }

  cout << "\n --- INTEGRATION ---" << endl;
  cout << "The polynomial p2 is: \n" << p2 << endl;
  for (int k = 0; k <= 2; k++)
  {
    cout << "Integrating p2 " << k + 1 << " times gives us: " << endl;
    p2.integrate();
    cout << p2 << endl;
  }

  return 0;
}
