using namespace std;
namespace Rationals {

class Rational {
public:
	class Invalid {}; 	// throw exception for division by zero
	Rational(int p, int q); // check valid and initialize
	Rational(int p);	// int to rational conversion
	Rational();		// default constructor, return 0
	// nonmodifying operations
	int num() const { return p; }
	int den() const { return q; }
	int sign() const {return (p*q>0)-(p*q<0);}
	int absnum() const {return abs(p);}
	int absden() const {return abs(q);}
	// explicit conversion to double
	explicit operator double() const {return double(p)/double(q);}
private:
	int p; // numerator
	int q; // denominator
}; // Rational
int gcd(int pp, int qq);
Rational operator+(const Rational&a, const Rational& b);
Rational operator-(const Rational&a, const Rational& b);
Rational operator*(const Rational&a, const Rational& b);
Rational operator/(const Rational&a, const Rational& b);
bool operator==(const Rational&a, const Rational& b);
bool operator!=(const Rational&a, const Rational& b);
bool operator<(const Rational&a, const Rational& b);
bool operator>(const Rational&a, const Rational& b);
bool operator<=(const Rational&a, const Rational& b);
bool operator>=(const Rational&a, const Rational& b);
ostream& operator<<(ostream& os, const Rational& r);
} // Rationals
