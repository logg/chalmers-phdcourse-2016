#include<iostream>
#include<iomanip>
#include<cstdlib>
#include "Rationals.h"
using namespace std;

namespace Rationals {

// member functions

Rational::Rational(int pp, int qq)
{
	if(qq == 0){
	throw Invalid{};
	} else {
	int gg = gcd(abs(pp),abs(qq));
	p = pp/gg; q = qq/gg;
	}
}

Rational::Rational(int pp)
	 : p{pp}, q{1}
{
}

const Rational& default_rational()
{
	static Rational r {0,1};
	return r;
}

Rational::Rational()
	 :p{default_rational().num()},
	 q{default_rational().den()}
{
}

// helper functions:

int gcd(int pp, int qq)
{
	return (pp == qq || pp == 0) ? (qq) : gcd(max(pp,qq)-min(pp,qq),min(pp,qq));
}

Rational operator+(const Rational&a, const Rational& b)
{
	Rational r {a.num()*b.den()+b.num()*a.den(), a.den()*b.den()};
	return r;
}

Rational operator-(const Rational&a, const Rational& b)
{
	Rational r {a.num()*b.den()-b.num()*a.den(), a.den()*b.den()};
	return r;
}

Rational operator*(const Rational&a, const Rational& b)
{
	Rational r {a.num()*b.num(), a.den()*b.den()};
	return r;
}

Rational operator/(const Rational&a, const Rational& b)
{
	Rational r {a.num()*b.den(), a.den()*b.num()};
	return r;
}

bool operator==(const Rational&a, const Rational& b)
{
	return a.num()*b.den() == b.num()*a.den();
}

bool operator!=(const Rational&a, const Rational& b)
{
	return a.num()*b.den() != b.num()*a.den();
}

bool operator<(const Rational&a, const Rational& b)
{
	return double(a)<double(b);
}

bool operator>(const Rational&a, const Rational& b)
{
	return double(a)>double(b);
}

bool operator<=(const Rational&a, const Rational& b)
{
	return double(a)<=double(b);
}

bool operator>=(const Rational&a, const Rational& b)
{
	return double(a)>=double(b);
}

ostream& operator<<(ostream& os, const Rational& r)
{
	if(r.absden()==1){ // If den is 1, print only num
		return r.sign()<0 ? ( os << '-' << r.absnum() ) : ( os << r.absnum() );
	} else {
		return r.sign()<0 ? ( os << '-' << r.absnum() << '/' << r.absden() ) : ( os << r.absnum() << '/' << r.absden() );
	}
}

} // Rationals
