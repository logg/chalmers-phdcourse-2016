namespace Polynomials { // declarations
template<typename T>
class Polynomial {
public:
	Polynomial(vector<T> cc); // initialize with vector
	Polynomial(T cc); // initialize with scalar
	// nonmodifying operations
	int degree() const {return deg;}
	vector<T> getcoeffs() const {return coefficients;}
	double operator()(double x);
	// modifying operations
	void differentiate();
	void integrate();
	// arithmetic operators
	friend Polynomial operator+(const Polynomial& a, const Polynomial& b)
	{	
	vector<T> rescoeff;
	int mindeg = min(a.degree(),b.degree());	
	if(a.degree()>b.degree()){ // Copy the highest degree polynomial
	rescoeff = a.getcoeffs();
	} else { rescoeff = b.getcoeffs(); }
	for(int i=0; i < mindeg + 1; i++){ // Add up the coefficients to the first part
		rescoeff[i] = a.getcoeffs()[i] + b.getcoeffs()[i];
	}
	Polynomial<T> res {rescoeff};
	return res;	
	}

	friend Polynomial operator*(const Polynomial<T>& a, const Polynomial<T>& b)
	{	
	int s = a.degree()+b.degree()+1;
	vector<T> rescoeff(s);
	for(int i = 0; i < a.degree()+1; i++){
		for(int j = 0; j < b.degree()+1; j++){
		rescoeff[i + j] = rescoeff[i + j] + a.getcoeffs()[i]*b.getcoeffs()[j];
	}}
	Polynomial<T> res {rescoeff};
	return res;	
	}

	friend Polynomial operator-(const Polynomial<T>& a, const Polynomial<T>& b)
	{
	T mm = T(-1);
	return a+mm*b;
	}
private:
	vector<T> coefficients;
	int deg;

}; // Polynomial
// helper functions
template<typename T>
ostream& operator<<(ostream& os, const Polynomial<T>& p);
} // Polynomials

namespace Polynomials { // definitions
// member functions
template<typename T>
Polynomial<T>::Polynomial(vector<T> cc)
	   : coefficients{cc}, deg{int(cc.size())-1}
{
	while(coefficients.back()==0 && deg != 0){ // Ignore extra zeros	
		coefficients.resize(coefficients.size()-1);
		--deg;
	}
}

template<typename T>
Polynomial<T>::Polynomial(T cc)
	   : coefficients{cc}, deg{0}
{
}

template<typename T>
double Polynomial<T>::operator()(double x)
{
	double r = 0;
	for(int i = 0; i<deg+1; i++){
	double dd = double(coefficients[i]);
	r=r+dd*pow(x,i);
	}
	return r;
} 

template<typename T>
void Polynomial<T>::differentiate()
{
	if(deg<1){
	vector<T> cc(1);
	coefficients = cc; 
	} else {
	coefficients.erase(coefficients.begin());
	for(int i = 0; i<deg; i++){coefficients[i]=(i+1)*coefficients[i];}
	deg = deg - 1;
	}
}

template<typename T>
void Polynomial<T>::integrate()
{
	if(deg == 0 && coefficients[0]==0){
	deg = 0; 
	vector<T> cc(1);
	coefficients = cc;
	} else {
	coefficients.insert(coefficients.begin(),0);
	for(int i = 2; i<deg+2; i++){coefficients[i]=coefficients[i]/i;}
	deg = deg + 1;
	}
}

// helper functions
template<typename T>
ostream& operator<<(ostream& os, const Polynomial<T>& p)
{
	T cc = 0;
	int add_indicator=0;  // To keep track of print +/- or not
	// Degree 0
	if(p.degree()==0 || p.getcoeffs()[0]!=0){
	os << p.getcoeffs()[0];
	add_indicator = 1;
	}
	// Degree >0
	for(int i = 1; i<p.degree()+1; i++){ // Loop through all terms
	cc = p.getcoeffs()[i];
	if(cc!=0){ // Print at all?
		if(cc > 0 && add_indicator != 0){os << '+';} else if(cc<0){os << '-';}
		if(abs(double(p.getcoeffs()[i])-1)>pow(10,-12)){ (p.getcoeffs()[i] < 0) ? (os << (-1)*p.getcoeffs()[i]) : (os << p.getcoeffs()[i]);} // Don't print 1-coefficient
		if(i==1){os << 'x';} else { os << "x^" << i;} // Don't print 1 in 1-degree
		add_indicator = 1;
	}
	}
	return os;
}
}
