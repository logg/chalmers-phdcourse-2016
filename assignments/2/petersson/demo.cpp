#include<iostream>
#include<iomanip>
#include<vector>
#include<cstdlib>
#include<cmath>
#include "Rationals.h"	
#include "Polynomials.h"

// Testing
using namespace Polynomials;
using namespace Rationals;
using namespace std;

int main()
{

// Test rationals

cout << "--- Test rationals ---" << '\n';
Rational r1 = {3,4};
Rational r2 = {5,7};
Rational r3 = {0,10};
int i1 = 5;
cout << "Rational r1 = " << r1 << '\n';
cout << "Rational r2 = " << r2 << '\n';
cout << "Rational r3 = " << r3 << '\n';
cout << "int i1 = " << i1 << '\n';
cout << "- Arithmetic -" << '\n';
cout << "r1+r2 = " << r1+r2 << '\n';
cout << "r1-r2 = " << r1-r2 << '\n';
cout << "r1*r2 = " << r1*r2 << '\n';
cout << "- Arithmetic with zero-" << '\n';
cout << "r1+r3 = " << r1+r3 << '\n';
cout << "r2*r3 = " << r2*r3 << '\n';
cout << "r3+r3 = " << r3+r3 << '\n';
cout << "- Arithmetic with integer-" << '\n';
cout << "r1+i1 = " << r1+i1 << '\n';	
cout << "r2*i1 = " << r2*i1 << '\n';
cout << "r3+i1 = " << r3+i1 << '\n';
cout << "- Order operations -" << '\n';
cout << "r1<r2 = " << int(r1<r2) << '\n';
cout << "r2<r1 = " << int(r2<r1) << '\n';
cout << "r1<=r3 = " << int(r1<=r3) << '\n';
cout << "r1<=i1 = " << int(r1<=i1) << '\n';
cout << "- Logical operations -" << '\n';
cout << "r1==r2 = " << int(r1==r2) << '\n';
cout << "r2==r2 = " << int(r2==r2) << '\n';
cout << "r1!=r2 = " << int(r1!=r2) << '\n';
cout << "r1!=i1 = " << int(r1!=r2) << '\n';

// Test double polynomial

cout << '\n' << "--- Test double polynomials ---" << '\n';
vector<double> p1c  {0.5,2.3,3.14};
Polynomial<double> p1 = p1c;
vector<double> p2c  {0,1,0,5};
Polynomial<double> p2 = p2c;
double d1 = 0;
double d2 = 5;
cout << "p1 = " << p1 << '\n';
cout << "p2 = " << p2 << '\n';
cout << "d1 = " << d1 << '\n';
cout << "d2 = " << d2 << '\n';
cout << "- Arithmetic -" << '\n';
cout << "p1+p2 = " << p1+p2 << '\n';
cout << "p1*p2 = " << p1*p2 << '\n';
cout << "p1+d1 = " << p1+d1 << '\n';
cout << "p1-d1 = " << p1-d1 << '\n';
cout << "p1*d1 = " << p1*d1 << '\n';
cout << "p1+d2 = " << p1+d2 << '\n';
cout << "p1-d2 = " << p1-d2 << '\n';
cout << "p1*d2 = " << p1*d2 << '\n';
cout << "- Evaluation -" << '\n';
cout << "p1(2) = " << p1(2) << '\n';
cout << "- Differentiaion -" << '\n';
p1.differentiate();
cout << "p1.differentiate(), p1 = " << p1 << '\n';
cout << "- Integration -" << '\n';
p1.integrate();
cout << "p1.integrate(), p1 = " << p1 << '\n';

// Test rational polynomial

cout << '\n' << "--- Test rational polynomials ---" << '\n';
vector<Rational> pr1c  {{1,2},{13,10},{314,100}};
Polynomial<Rational> pr1 = pr1c;
vector<Rational> pr2c  {0,1,0,5};
Polynomial<Rational> pr2 = pr2c;
Rational dr1 = 0;
Rational dr2 = {5,3};
cout << "p1 = " << pr1 << '\n';
cout << "p2 = " << pr2 << '\n';
cout << "dr1 = " << dr1 << '\n';
cout << "dr2 = " << dr2 << '\n';
cout << "- Arithmetic -" << '\n';
cout << "p1+p2 = " << pr1+pr2 << '\n';
cout << "p1*p2 = " << pr1*pr2 << '\n';
cout << "p1+dr1 = " << pr1+dr1 << '\n';
cout << "p1-dr1 = " << pr1-dr1 << '\n';
cout << "p1*dr1 = " << pr1*dr1 << '\n';
cout << "p1+dr2 = " << pr1+dr2 << '\n';
cout << "p1-dr2 = " << pr1-dr2 << '\n';
cout << "p1*dr2 = " << pr1*dr2 << '\n'; 
cout << "- Evaluation -" << '\n';
cout << "pr1(2) = " << pr1(2) << '\n';
cout << "- Differentiaion -" << '\n';
pr1.differentiate();
cout << "pr1.differentiate(), pr1 = " << pr1 << '\n';
cout << "- Integration -" << '\n';
pr1.integrate();
cout << "pr1.integrate(), pr1 = " << pr1 << '\n';


}
