Add your assignment (code + report + results) inside the directory
by creating a subdirectory named <num>/<name>, for example:

  1/lundholm/
  1/forslund/
  2/lundholm/
  2/forslund/

etc.
